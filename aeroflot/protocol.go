package aeroflot

import "encoding/xml"

type ActorObjAttrGroup struct {
    Metadata string `xml:",attr,omitempty"`
    Refs     string `xml:",attr,omitempty"`
}

type ActorObjectType struct {
    Metadata string `xml:",attr,omitempty"`
    Refs     string `xml:",attr,omitempty"`
}

type InstanceClassSimpleType string

type CoreBaseType struct {
    AugPointAssocs *AugPointAssocType `xml:"http://www.iata.org/IATA/EDIST AugPointAssocs,omitempty"`
    Key            string             `xml:",attr,omitempty"`
    Owner          string             `xml:",attr,omitempty"`
    Seq            int                `xml:",attr,omitempty"`
}

type ListBaseType struct {
    ListToken string `xml:",attr,omitempty"`
    Owner     string `xml:",attr,omitempty"`
    Seq       int    `xml:",attr,omitempty"`
}

type MetaBaseType struct {
}

type OwnerSimpleType string

type OwnerTextSimpleType string

type AssociatedObjectBaseType struct {
    Refs            string `xml:",attr,omitempty"`
    ApproxInd       bool   `xml:",attr,omitempty"`
    RefundAllInd    bool   `xml:",attr,omitempty"`
    ExemptAllInd    bool   `xml:",attr,omitempty"`
    AllGuaranteeInd bool   `xml:",attr,omitempty"`
    DisclosureInd   bool   `xml:",attr,omitempty"`
    BrowserInd      bool   `xml:",attr,omitempty"`
}

type ObjAssociationAttrGroup struct {
    Refs string `xml:",attr,omitempty"`
}

type TokenID_SimpleType string

type AugmentationPoint AugPointInfoType

type AugmentationType struct {
    Augmentations []*Augmentations `xml:"http://www.iata.org/IATA/EDIST Augmentations,omitempty"`
}

type Augmentations struct {
    Refs string `xml:",attr,omitempty"`
}

type AugPointType struct {
    Key   string `xml:",attr,omitempty"`
    Owner string `xml:",attr,omitempty"`
    Seq   int    `xml:",attr,omitempty"`
}

type AugPointAssocType struct {
    AugPointAssoc []*AugPointAssoc `xml:"http://www.iata.org/IATA/EDIST AugPointAssoc,omitempty"`
    Owner         string           `xml:",attr,omitempty"`
}

type AugPointAssoc struct {
    KeyRef string `xml:",attr,omitempty"`
    Owner  string `xml:",attr,omitempty"`
    Seq    int    `xml:",attr,omitempty"`
}

type AugPointListType struct {
    ListBaseType
    List []*List `xml:"http://www.iata.org/IATA/EDIST List,omitempty"`
}

type List struct {
    AugPointAssoc []*AugPointAssoc_1 `xml:"http://www.iata.org/IATA/EDIST AugPointAssoc,omitempty"`
    NamedAssocs   *NamedAssocType    `xml:"http://www.iata.org/IATA/EDIST NamedAssocs,omitempty"`
    ListKey       string             `xml:",attr,omitempty"`
    ListName      string             `xml:",attr,omitempty"`
    Owner         string             `xml:",attr,omitempty"`
    Seq           int                `xml:",attr,omitempty"`
}

type AugPointAssoc_1 struct {
    KeyRef string `xml:",attr,omitempty"`
    Owner  string `xml:",attr,omitempty"`
    Seq    int    `xml:",attr,omitempty"`
}

type AugPointInfoType struct {
    AugPoint []*AugPointType   `xml:"http://www.iata.org/IATA/EDIST AugPoint,omitempty"`
    Lists    *AugPointListType `xml:"http://www.iata.org/IATA/EDIST Lists,omitempty"`
}

type NamedAssocType struct {
    NamedAssoc []*NamedAssoc `xml:"http://www.iata.org/IATA/EDIST NamedAssoc,omitempty"`
}

type NamedAssoc struct {
    Group       []*Group       `xml:"http://www.iata.org/IATA/EDIST Group,omitempty"`
    List        []*List_1      `xml:"http://www.iata.org/IATA/EDIST List,omitempty"`
    UniqueKeyID []*UniqueKeyID `xml:"http://www.iata.org/IATA/EDIST UniqueKeyID,omitempty"`
    Target      string         `xml:",attr,omitempty"`
    KeyRef      string         `xml:",attr,omitempty"`
    From        string         `xml:",attr,omitempty"`
    Seq         int            `xml:",attr,omitempty"`
}

type Group struct {
    GroupKeyRef string `xml:",attr,omitempty"`
    TokenRef    string `xml:",attr,omitempty"`
    Seq         int    `xml:",attr,omitempty"`
}

type List_1 struct {
    ListKeyRef string `xml:",attr,omitempty"`
    TokenRef   string `xml:",attr,omitempty"`
    Seq        int    `xml:",attr,omitempty"`
}

type UniqueKeyID struct {
    UniqueID_Ref string `xml:",attr,omitempty"`
    Seq          int    `xml:",attr,omitempty"`
}

type DataListObjAttrGroup struct {
    Refs    string `xml:",attr,omitempty"`
    ListKey string `xml:",attr,omitempty"`
}

type DataListObjectBaseType struct {
    Refs            string `xml:",attr,omitempty"`
    ListKey         string `xml:",attr,omitempty"`
    DisplayAllInd   bool   `xml:",attr,omitempty"`
    NetFareInd      bool   `xml:",attr,omitempty"`
    ReissueOnlyInd  bool   `xml:",attr,omitempty"`
    AutoExchangeInd bool   `xml:",attr,omitempty"`
    BundleInd       bool   `xml:",attr,omitempty"`
    MaximumTryInd   bool   `xml:",attr,omitempty"`
    NotProcessedInd bool   `xml:",attr,omitempty"`
    RetryInd        bool   `xml:",attr,omitempty"`
}

type IdentifierObjAttrGroup struct {
    Refs          string `xml:",attr,omitempty"`
    IdentifierKey string `xml:",attr,omitempty"`
}

type IdentifierObjectBaseType struct {
    Refs          string `xml:",attr,omitempty"`
    IdentifierKey string `xml:",attr,omitempty"`
}

type MetadataObjAttrGroup struct {
    Metarefs    string `xml:",attr,omitempty"`
    Refs        string `xml:",attr,omitempty"`
    MetadataKey string `xml:",attr,omitempty"`
}

type MetadataObjectBaseType struct {
    AugmentationPoint *AugPointInfoType `xml:"http://www.iata.org/IATA/EDIST AugmentationPoint,omitempty"`
    Refs              string            `xml:",attr,omitempty"`
    MetadataKey       string            `xml:",attr,omitempty"`
}

type MetadataKey KeyIdentifier

type KeyObjectBaseType struct {
    Refs      string `xml:",attr,omitempty"`
    ObjectKey string `xml:",attr,omitempty"`
    PricedInd bool   `xml:",attr,omitempty"`
}

type KeyWithMetaObjectBaseType struct {
    Refs                string `xml:",attr,omitempty"`
    ObjectKey           string `xml:",attr,omitempty"`
    DisplayAllInd       bool   `xml:",attr,omitempty"`
    AllExemptInd        bool   `xml:",attr,omitempty"`
    PricingInd          bool   `xml:",attr,omitempty"`
    ConnectionInd       bool   `xml:",attr,omitempty"`
    E_TicketInd         bool   `xml:",attr,omitempty"`
    TicketlessInd       bool   `xml:",attr,omitempty"`
    AssociateInd        bool   `xml:",attr,omitempty"`
    TaxOnEMD_Ind        bool   `xml:",attr,omitempty"`
    StopOverInd         bool   `xml:",attr,omitempty"`
    TaxInd              bool   `xml:",attr,omitempty"`
    OtherChargeInd      bool   `xml:",attr,omitempty"`
    PoolingInd          bool   `xml:",attr,omitempty"`
    SpecialItemsInd     bool   `xml:",attr,omitempty"`
    TaxesInd            bool   `xml:",attr,omitempty"`
    NetFareInd          bool   `xml:",attr,omitempty"`
    ReissueOnlyInd      bool   `xml:",attr,omitempty"`
    AutoExchangeInd     bool   `xml:",attr,omitempty"`
    AlertInd            bool   `xml:",attr,omitempty"`
    AuthenticationInd   bool   `xml:",attr,omitempty"`
    MarketingInd        bool   `xml:",attr,omitempty"`
    MultiPayFormInd     bool   `xml:",attr,omitempty"`
    NoticeInd           bool   `xml:",attr,omitempty"`
    PartialInd          bool   `xml:",attr,omitempty"`
    WarningInd          bool   `xml:",attr,omitempty"`
    FailedPaymentInd    bool   `xml:",attr,omitempty"`
    PartialPaymentInd   bool   `xml:",attr,omitempty"`
    VerificationInd     bool   `xml:",attr,omitempty"`
    PriceVarianceInd    bool   `xml:",attr,omitempty"`
    InvGuaranteedInd    bool   `xml:",attr,omitempty"`
    WaitlistInd         bool   `xml:",attr,omitempty"`
    BundleInd           bool   `xml:",attr,omitempty"`
    InvGuarRequestedInd bool   `xml:",attr,omitempty"`
    ExitRowInd          bool   `xml:",attr,omitempty"`
    PremiumInd          bool   `xml:",attr,omitempty"`
    UpperDeckInd        bool   `xml:",attr,omitempty"`
    TaxIncludedInd      bool   `xml:",attr,omitempty"`
    FeeIncludedInd      bool   `xml:",attr,omitempty"`
    LeadPricingInd      bool   `xml:",attr,omitempty"`
    Timestamp           string `xml:",attr,omitempty"`
    MetadataToken       string `xml:",attr,omitempty"`
}

type KeyWithMetaOfferBaseType struct {
    Refs                string `xml:",attr,omitempty"`
    OfferMetaReferences string `xml:",attr,omitempty"`
}

type ObjectKeyAttrGroup struct {
    Refs      string `xml:",attr,omitempty"`
    ObjectKey string `xml:",attr,omitempty"`
}

type ObjectKeyMetaAttrGroup struct {
    Refs      string `xml:",attr,omitempty"`
    ObjectKey string `xml:",attr,omitempty"`
}

type OfferKeyMetaAttrGroup struct {
    Refs                string `xml:",attr,omitempty"`
    OfferMetaReferences string `xml:",attr,omitempty"`
}

type ObjectPolicyMetaAttrGroup struct {
    Refs                 string `xml:",attr,omitempty"`
    ObjectMetaReferences string `xml:",attr,omitempty"`
}

type AttrCoreDateGrpType struct {
    DateTime  string `xml:",attr,omitempty"`
    ShortDate string `xml:",attr,omitempty"`
    Timestamp string `xml:",attr,omitempty"`
    Time      string `xml:",attr,omitempty"`
}

type AllDateGrpType struct {
    MonthYear   string `xml:",attr,omitempty"`
    Year        string `xml:",attr,omitempty"`
    YearMonth   string `xml:",attr,omitempty"`
    DayNumber   string `xml:",attr,omitempty"`
    MonthNumber string `xml:",attr,omitempty"`
}

type CoreDateGrpType struct {
    DateTime  string `xml:",attr,omitempty"`
    ShortDate string `xml:",attr,omitempty"`
    Timestamp string `xml:",attr,omitempty"`
    Time      string `xml:",attr,omitempty"`
}

type DayOfWeekIndType struct {
    MondayInd    bool `xml:",attr,omitempty"`
    TuesdayInd   bool `xml:",attr,omitempty"`
    WednesdayInd bool `xml:",attr,omitempty"`
    ThursdayInd  bool `xml:",attr,omitempty"`
    FridayInd    bool `xml:",attr,omitempty"`
    SaturdayInd  bool `xml:",attr,omitempty"`
    SundayInd    bool `xml:",attr,omitempty"`
}

type TrxProcessObjAttrGroup struct {
    Refs       string `xml:",attr,omitempty"`
    TrxItemKey string `xml:",attr,omitempty"`
}

type TrxProcessObjectBaseType struct {
    Refs            string `xml:",attr,omitempty"`
    TrxItemKey      string `xml:",attr,omitempty"`
    AuthResponseInd bool   `xml:",attr,omitempty"`
    MaximumTryInd   bool   `xml:",attr,omitempty"`
    NotProcessedInd bool   `xml:",attr,omitempty"`
    RetryInd        bool   `xml:",attr,omitempty"`
    AuthRequestInd  bool   `xml:",attr,omitempty"`
}

type PreferenceAttrGroup struct {
    PreferencesLevel   string `xml:",attr,omitempty"`
    PreferencesContext string `xml:",attr,omitempty"`
}

type DomainListType string

type EncSchemeSimpleType string

type EncSchemeListType string

type InstanceClassRefSimpleType string

type KeyIdentifier string

type ListApplicationSimpleType string

type MultiAssocSimpleType InstanceClassRefSimpleType

type SingleAssocSimpleType string

type UniqueID_SimpleType string

type UniqueIDContextType struct {
    Refs  string `xml:",attr,omitempty"`
    Owner string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type UriContextType struct {
    Refs    string `xml:",attr,omitempty"`
    Context string `xml:",attr,omitempty"`
    Value   string `xml:",chardata"`
}

type CorePeriodGrpType struct {
    Date      *DatePeriodType     `xml:"http://www.iata.org/IATA/EDIST Date,omitempty"`
    DateTime  *DateTimePeriodType `xml:"http://www.iata.org/IATA/EDIST DateTime,omitempty"`
    TimeStamp *TimeStamp          `xml:"http://www.iata.org/IATA/EDIST TimeStamp,omitempty"`
}

type TimeStamp struct {
    Effective  string `xml:",attr,omitempty"`
    Expiration string `xml:",attr,omitempty"`
}

type DateTimePeriodType struct {
    Effective  string `xml:",attr,omitempty"`
    Expiration string `xml:",attr,omitempty"`
}

type DateTime DateTimeRepType

type DatePeriodType struct {
    Effective  string `xml:",attr,omitempty"`
    Expiration string `xml:",attr,omitempty"`
}

type Day DayRepType

type MonthYear MonthYearRepType

type Month MonthRepType

type ShortDate DateRepType

type TimestampType struct {
    TimestampRepType
}

type Timestamp TimestampType

type Year YearRepType

type YearMonth YearMonthRepType

type DatePeriod DatePeriodRepType

type DateTimePeriod DateTimePeriodRepType

type DayPeriod DayPeriodRepType

type DayRepType struct {
    Name  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type DateRepType string

type DateTimeRepType string

type DatePeriodRepType struct {
    Effective  string `xml:"http://www.iata.org/IATA/EDIST Effective,omitempty"`
    Expiration string `xml:"http://www.iata.org/IATA/EDIST Expiration,omitempty"`
}

type DateTimePeriodRepType struct {
    Effective  string `xml:"http://www.iata.org/IATA/EDIST Effective,omitempty"`
    Expiration string `xml:"http://www.iata.org/IATA/EDIST Expiration,omitempty"`
}

type DayPeriodRepType struct {
    Effective  *DayRepType `xml:"http://www.iata.org/IATA/EDIST Effective,omitempty"`
    Expiration *DayRepType `xml:"http://www.iata.org/IATA/EDIST Expiration,omitempty"`
}

type MonthPeriod MonthPeriodRepType

type MonthYearRepType ShortYearMonthSimpleType

type MonthRepType struct {
    Name  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type MonthPeriodRepType struct {
    Effective  *MonthRepType `xml:"http://www.iata.org/IATA/EDIST Effective,omitempty"`
    Expiration *MonthRepType `xml:"http://www.iata.org/IATA/EDIST Expiration,omitempty"`
}

type QuarterPeriod QuarterPeriodRepType

type QuarterPeriodRepType struct {
    Effective  string `xml:"http://www.iata.org/IATA/EDIST Effective,omitempty"`
    Expiration string `xml:"http://www.iata.org/IATA/EDIST Expiration,omitempty"`
}

type TimePeriod TimePeriodRepType

type TimePeriodRepType struct {
    Effective  string `xml:"http://www.iata.org/IATA/EDIST Effective,omitempty"`
    Expiration string `xml:"http://www.iata.org/IATA/EDIST Expiration,omitempty"`
}

type TimestampRepType string

type YearPeriod YearPeriodRepType

type YearMonthPeriod YearMonthPeriodRepType

type YearRepType string

type YearMonthRepType string

type YearPeriodRepType struct {
    Effective  string `xml:"http://www.iata.org/IATA/EDIST Effective,omitempty"`
    Expiration string `xml:"http://www.iata.org/IATA/EDIST Expiration,omitempty"`
}

type YearMonthPeriodRepType struct {
    Effective  string `xml:"http://www.iata.org/IATA/EDIST Effective,omitempty"`
    Expiration string `xml:"http://www.iata.org/IATA/EDIST Expiration,omitempty"`
}

type AVS_ActionSimpleType string

type CVC_SubmissionListType string

type CVC_SubmissionSimpleType string

type CustomerAuthStatusListType string

type CustomerAuthStatusSimpleType string

type EnableAVS_SimpleType string

type EnrollStatusListType string

type EnrollStatusSimpleType string

type FactorAuthListType string

type FactorAuthSimpleType string

type OTP_CodeFormatListType string

type OTP_CodeFormatSimpleType string

type SecureActionListType string

type SecureActionSimpleType string

type SecureTransactionListType string

type SecureTransactionSimpleType string

type SecureTrxClientListType string

type SecureTrxClientSimpleType string

type AdvPurchListType string

type AdvPurchaseSimpleType string

type AgeUnitSimpleType string

type ApplicationSimpleType string

type AirRowNbrSimpleType string

type AircraftEquipSimpleType string

type AirlineDesigSimpleType string

type AirportCodeRoleSimpleType string

type AirportCitySimpleType string

type BagAppListType string

type BagAppSimpleType string

type BagDimensionListType string

type BagDimensionSimpleType string

type BagDiscloseRuleListType string

type BagDiscloseRuleSimpleType string

type BagRestrictionListType string

type BagRestrictionSimpleType string

type CodesetValueSimpleType string

type OtherCodeSimpleType string

type ConnectMethodListType string

type ConnectMethodSimpleType string

type ContactListType string

type ContactAppSimpleType string

type EmailListType string

type EmailAppSimpleType string

type ContextSimpleType string

type CountrySimpleType string

type CouponNumberSimpleType string

type CouponMediaListType string

type CouponMediaSimpleType string

type CouponPurposeListType string

type CpnCancelFunctionListType string

type CpnCancelFunctionSimpleType string

type FareWaiverRuleListType string

type FareWaiverRuleSimpleType string

type AmountCurrencyGroup struct {
    Code string `xml:",attr,omitempty"`
}

type AmountCurrencyOptGroup struct {
    Code string `xml:",attr,omitempty"`
}

type CurrencyListType string

type CurrencyAppSimpleType string

type CurrencySimpleType string

type DateSimpleType string

type DayOfWeekMaskType string

type DayOfWeekSimpleType string

type MonthNumberSimpleType string

type ShortYearMonthSimpleType string

type TimeSimpleType string

type OverviewSimpleType string

type DescriptionSimpleType string

type HintSimpleType string

type ShortDescSimpleType string

type DomainSimpleType string

type DistanceUnitListType string

type DistanceUnitSimpleType string

type ImageUnitListType string

type ImageUnitSimpleType string

type LanguageAppListType string

type LanguageAppSimpleType string

type MediaAppListType string

type MediaAppSimpleType string

type MediaCategoryListType string

type MediaCategorySimpleType string

type RenderingTypeListType string

type RenderingTypeSimpleType string

type MetaSrchLinkAppListType string

type MetaSrchLinkAppSimpleType string

type UTM_AppListType string

type UTM_AppSimpleType string

type PaxSimpleType string

type ApprovalCodeSimpleType string

type PayCardTypeSimpleType string

type PayCardCodeSimpleType string

type PayCardNmbrSimpleType string

type PayCardCVV_SimpleType string

type PayCardDateSimpleType string

type PayFormListType string

type PayFormSimpleType string

type PaymentFormListType string

type PaymentFormSimpleType string

type PaymentStatusListType string

type PaymentStatusSimpleType string

type MaskedCardNmbrSimpleType string

type PenaltyAppListType string

type PenaltyAppSimpleType string

type PenaltyListType string

type PenaltySimpleType string

type WholePercentageSimpleType float64

type PreferencesLevelListType string

type PreferencesLevelSimpleType string

type ProperNameSimpleType string

type PropertyNameSimpleType_ string

type ProximityAppListType string

type ProximityAppSimpleType string

type PseudoCitySimpleType string

type RedempUnitListType string

type RedemptionUnitSimpleType string

type RuleStatusListType string

type RuleStatusSimpleType string

type SpecialSrvcSimpleType string

type SSR_SimpleType string

type IATA_NbrSimpleType string

type ICAO_LocSimpleType string

type TargetSystemListType string

type TargetSystemSimpleType string

type TraceIP_SimpleType string

type TicketDesigAppListType string

type TicketDesigAppSimpleType string

type TicketNumberSimpleType string

type TimeLimitTypeListType string

type TimeLimitTypeSimpleType string

type TravelerGenderSimpleType string

type TripPurposeListType string

type TripPurposeSimpleType string

type TrxProcessingListType string

type TrxProcessingSimpleType string

type IdentifierSimpleType string

type UniqueStringID_SimpleType string

type SizeUnitSimpleType string

type WeightUnitSimpleType string

type ZoneCodeSimpleType string

type PolicyTypeSimpleType string

type PolicyTypeListType string

type AlphaNumericStringLength1to2 string

type AlphaNumericStringLength1to3 string

type AlphaLength3 string

type AirlineType string

type AlphaNumericStringLength1to71 string

type AlphaNumericStringLength1to4 string

type Numeric0to99 int

type NumericStringLength1to19 string

type OrderItemActionSimpleType string

type OrderItemActionListType string

type Address struct {
    AddressCore   *AddressCoreType   `xml:"http://www.iata.org/IATA/EDIST AddressCore,omitempty"`
    AddressDetail *AddressDetailType `xml:"http://www.iata.org/IATA/EDIST AddressDetail,omitempty"`
}

type AddressCore AddressCoreType

type AddressCoreType struct {
    KeyWithMetaObjectBaseType
    Address *Address_1 `xml:"http://www.iata.org/IATA/EDIST Address,omitempty"`
}

type Address_1 struct {
    PaymentAddress    *PaymentAddrType    `xml:"http://www.iata.org/IATA/EDIST PaymentAddress,omitempty"`
    SimpleAddress     *SimpleAddrType     `xml:"http://www.iata.org/IATA/EDIST SimpleAddress,omitempty"`
    StructuredAddress *StructuredAddrType `xml:"http://www.iata.org/IATA/EDIST StructuredAddress,omitempty"`
}

type AddressDetail AddressDetailType

type AddressDetailType struct {
    AddressCoreType
    Directions *Directions `xml:"http://www.iata.org/IATA/EDIST Directions,omitempty"`
}

type Directions struct {
    Direction []*DirectionsType `xml:"http://www.iata.org/IATA/EDIST Direction,omitempty"`
}

type PaymentAddress PaymentAddrType

type PaymentAddrType struct {
    Street       []string `xml:"http://www.iata.org/IATA/EDIST Street,omitempty"`
    PO_Box       string   `xml:"http://www.iata.org/IATA/EDIST PO_Box,omitempty"`
    BuildingRoom string   `xml:"http://www.iata.org/IATA/EDIST BuildingRoom,omitempty"`
    City         string   `xml:"http://www.iata.org/IATA/EDIST City,omitempty"`
    StateProv    string   `xml:"http://www.iata.org/IATA/EDIST StateProv,omitempty"`
    PostalCode   string   `xml:"http://www.iata.org/IATA/EDIST PostalCode,omitempty"`
    Country      string   `xml:"http://www.iata.org/IATA/EDIST Country,omitempty"`
    Refs         string   `xml:",attr,omitempty"`
}

type SimpleAddress SimpleAddrType

type SimpleAddrType struct {
    AddressLine []string `xml:"http://www.iata.org/IATA/EDIST AddressLine,omitempty"`
    Refs        string   `xml:",attr,omitempty"`
}

type StructuredAddress StructuredAddrType

type StructuredAddrType struct {
    Application  string           `xml:"http://www.iata.org/IATA/EDIST Application,omitempty"`
    Street       []string         `xml:"http://www.iata.org/IATA/EDIST Street,omitempty"`
    BuildingRoom string           `xml:"http://www.iata.org/IATA/EDIST BuildingRoom,omitempty"`
    PO_Box       string           `xml:"http://www.iata.org/IATA/EDIST PO_Box,omitempty"`
    CityName     string           `xml:"http://www.iata.org/IATA/EDIST CityName,omitempty"`
    StateProv    string           `xml:"http://www.iata.org/IATA/EDIST StateProv,omitempty"`
    PostalCode   string           `xml:"http://www.iata.org/IATA/EDIST PostalCode,omitempty"`
    CountryCode  *CountryCodeType `xml:"http://www.iata.org/IATA/EDIST CountryCode,omitempty"`
    County       string           `xml:"http://www.iata.org/IATA/EDIST County,omitempty"`
    Refs         string           `xml:",attr,omitempty"`
}

type AirPassCore AirPassCoreType

type AirPassDetail AirPassDetailType

type AirPassCoreType struct {
    AirlineID   *AirlineID_Type `xml:"http://www.iata.org/IATA/EDIST AirlineID,omitempty"`
    Certificate []*Certificate  `xml:"http://www.iata.org/IATA/EDIST Certificate,omitempty"`
    Refs        string          `xml:",attr,omitempty"`
}

type Certificate struct {
    Number          *Number          `xml:"http://www.iata.org/IATA/EDIST Number,omitempty"`
    Application     string           `xml:"http://www.iata.org/IATA/EDIST Application,omitempty"`
    EffectivePeriod *EffectivePeriod `xml:"http://www.iata.org/IATA/EDIST EffectivePeriod,omitempty"`
    Refs            string           `xml:",attr,omitempty"`
}

type Number struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type EffectivePeriod struct {
    YearPeriod      *YearPeriodRepType      `xml:"http://www.iata.org/IATA/EDIST YearPeriod,omitempty"`
    YearMonthPeriod *YearMonthPeriodRepType `xml:"http://www.iata.org/IATA/EDIST YearMonthPeriod,omitempty"`
    TimePeriod      *TimePeriodRepType      `xml:"http://www.iata.org/IATA/EDIST TimePeriod,omitempty"`
    QuarterPeriod   *QuarterPeriodRepType   `xml:"http://www.iata.org/IATA/EDIST QuarterPeriod,omitempty"`
    MonthPeriod     *MonthPeriodRepType     `xml:"http://www.iata.org/IATA/EDIST MonthPeriod,omitempty"`
    DayPeriod       *DayPeriodRepType       `xml:"http://www.iata.org/IATA/EDIST DayPeriod,omitempty"`
    DateTimePeriod  *DateTimePeriodRepType  `xml:"http://www.iata.org/IATA/EDIST DateTimePeriod,omitempty"`
    DatePeriod      *DatePeriodRepType      `xml:"http://www.iata.org/IATA/EDIST DatePeriod,omitempty"`
}

type AirPassDetailType struct {
    AirPassCoreType
    ProgramName string  `xml:"http://www.iata.org/IATA/EDIST ProgramName,omitempty"`
    ProgramCode string  `xml:"http://www.iata.org/IATA/EDIST ProgramCode,omitempty"`
    Holder      *Holder `xml:"http://www.iata.org/IATA/EDIST Holder,omitempty"`
}

type Holder struct {
    AgencyID  *UniqueIDContextType   `xml:"http://www.iata.org/IATA/EDIST AgencyID,omitempty"`
    PartnerID *PartnerCompanyID_Type `xml:"http://www.iata.org/IATA/EDIST PartnerID,omitempty"`
}

type AirportCode struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type Attributes AttributesType

type AttributesType struct {
    Group    *Group_1  `xml:"http://www.iata.org/IATA/EDIST Group,omitempty"`
    SubGroup *SubGroup `xml:"http://www.iata.org/IATA/EDIST SubGroup,omitempty"`
    Desc1    *Desc1    `xml:"http://www.iata.org/IATA/EDIST Desc1,omitempty"`
    Desc2    *Desc2    `xml:"http://www.iata.org/IATA/EDIST Desc2,omitempty"`
    Refs     string    `xml:",attr,omitempty"`
}

type Group_1 struct {
    Code string `xml:"http://www.iata.org/IATA/EDIST Code,omitempty"`
    Text string `xml:"http://www.iata.org/IATA/EDIST Text,omitempty"`
}

type SubGroup struct {
    Code string `xml:"http://www.iata.org/IATA/EDIST Code,omitempty"`
    Text string `xml:"http://www.iata.org/IATA/EDIST Text,omitempty"`
}

type Desc1 struct {
    Code string `xml:"http://www.iata.org/IATA/EDIST Code,omitempty"`
    Text string `xml:"http://www.iata.org/IATA/EDIST Text,omitempty"`
}

type Desc2 struct {
    Code string `xml:"http://www.iata.org/IATA/EDIST Code,omitempty"`
    Text string `xml:"http://www.iata.org/IATA/EDIST Text,omitempty"`
}

type S5Type struct {
    UID  string `xml:"http://www.iata.org/IATA/EDIST UID,omitempty"`
    Refs string `xml:",attr,omitempty"`
}

type S7Type struct {
    UID         string `xml:"http://www.iata.org/IATA/EDIST UID,omitempty"`
    Seq         int    `xml:"http://www.iata.org/IATA/EDIST Seq,omitempty"`
    ExcessFirst int    `xml:"http://www.iata.org/IATA/EDIST ExcessFirst,omitempty"`
    ExcessLast  int    `xml:"http://www.iata.org/IATA/EDIST ExcessLast,omitempty"`
    Refs        string `xml:",attr,omitempty"`
}

type AddlBaggageInfoType struct {
    AllowableBag         []*AllowableBag         `xml:"http://www.iata.org/IATA/EDIST AllowableBag,omitempty"`
    BagCharge            []*BagCharge            `xml:"http://www.iata.org/IATA/EDIST BagCharge,omitempty"`
    CarryOnAllowableBag  []*CarryOnAllowableBag  `xml:"http://www.iata.org/IATA/EDIST CarryOnAllowableBag,omitempty"`
    CarryOnChargeableBag []*CarryOnChargeableBag `xml:"http://www.iata.org/IATA/EDIST CarryOnChargeableBag,omitempty"`
    CarryOnFree          *CarryOnFree            `xml:"http://www.iata.org/IATA/EDIST CarryOnFree,omitempty"`
    CheckedFree          *CheckedFree            `xml:"http://www.iata.org/IATA/EDIST CheckedFree,omitempty"`
    CheckedChargeableBag []*CheckedChargeableBag `xml:"http://www.iata.org/IATA/EDIST CheckedChargeableBag,omitempty"`
    MaxFreeBagDim        string                  `xml:",attr,omitempty"`
}

type AllowableBag struct {
    Type   string `xml:",attr,omitempty"`
    Number int    `xml:",attr,omitempty"`
}

type BagCharge struct {
    Type         string `xml:",attr,omitempty"`
    Amount       string `xml:",attr,omitempty"`
    CurrencyCode string `xml:",attr,omitempty"`
}

type CarryOnAllowableBag struct {
    DeterminingCarrierCode string `xml:",attr,omitempty"`
    Type                   string `xml:",attr,omitempty"`
    Number                 int    `xml:",attr,omitempty"`
}

type CarryOnChargeableBag struct {
    Embargo      []*Embargo `xml:"http://www.iata.org/IATA/EDIST Embargo,omitempty"`
    Type         string     `xml:",attr,omitempty"`
    Amount       string     `xml:",attr,omitempty"`
    CurrencyCode string     `xml:",attr,omitempty"`
    MaxBagDim    string     `xml:",attr,omitempty"`
    MaxBagWght   string     `xml:",attr,omitempty"`
}

type Embargo struct {
    Info string `xml:",attr,omitempty"`
}

type CarryOnFree struct {
    MaxBagDim  string `xml:",attr,omitempty"`
    MaxBagWght string `xml:",attr,omitempty"`
}

type CheckedFree struct {
    MaxBagDim  string `xml:",attr,omitempty"`
    MaxBagWght string `xml:",attr,omitempty"`
}

type CheckedChargeableBag struct {
    Embargo      []*Embargo_1 `xml:"http://www.iata.org/IATA/EDIST Embargo,omitempty"`
    Type         string       `xml:",attr,omitempty"`
    Amount       string       `xml:",attr,omitempty"`
    CurrencyCode string       `xml:",attr,omitempty"`
    MaxBagDim    string       `xml:",attr,omitempty"`
    MaxBagWght   string       `xml:",attr,omitempty"`
}

type Embargo_1 struct {
    Info string `xml:",attr,omitempty"`
}

type BagAllowanceDescType struct {
    ApplicableParty string        `xml:"http://www.iata.org/IATA/EDIST ApplicableParty,omitempty"`
    ApplicableBag   string        `xml:"http://www.iata.org/IATA/EDIST ApplicableBag,omitempty"`
    Descriptions    *Descriptions `xml:"http://www.iata.org/IATA/EDIST Descriptions,omitempty"`
    Refs            string        `xml:",attr,omitempty"`
    Concept         string        `xml:",attr,omitempty"`
}

type BagAllowanceDimensionType struct {
    ApplicableParty string        `xml:"http://www.iata.org/IATA/EDIST ApplicableParty,omitempty"`
    DimensionUOM    string        `xml:"http://www.iata.org/IATA/EDIST DimensionUOM,omitempty"`
    MaxLinear       *MaxLinear    `xml:"http://www.iata.org/IATA/EDIST MaxLinear,omitempty"`
    MinLinear       *MinLinear    `xml:"http://www.iata.org/IATA/EDIST MinLinear,omitempty"`
    Dimensions      []*Dimensions `xml:"http://www.iata.org/IATA/EDIST Dimensions,omitempty"`
    ApplicableBag   string        `xml:"http://www.iata.org/IATA/EDIST ApplicableBag,omitempty"`
    Descriptions    *Descriptions `xml:"http://www.iata.org/IATA/EDIST Descriptions,omitempty"`
    Refs            string        `xml:",attr,omitempty"`
}

type MaxLinear struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type MinLinear struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type Dimensions struct {
    Category string  `xml:"http://www.iata.org/IATA/EDIST Category,omitempty"`
    MaxValue float64 `xml:"http://www.iata.org/IATA/EDIST MaxValue,omitempty"`
    MinValue float64 `xml:"http://www.iata.org/IATA/EDIST MinValue,omitempty"`
    Refs     string  `xml:",attr,omitempty"`
}

type BagAllowancePieceType struct {
    ApplicableParty           string               `xml:"http://www.iata.org/IATA/EDIST ApplicableParty,omitempty"`
    TotalQuantity             int                  `xml:"http://www.iata.org/IATA/EDIST TotalQuantity,omitempty"`
    BagType                   string               `xml:"http://www.iata.org/IATA/EDIST BagType,omitempty"`
    ApplicableBag             string               `xml:"http://www.iata.org/IATA/EDIST ApplicableBag,omitempty"`
    Descriptions              *Descriptions        `xml:"http://www.iata.org/IATA/EDIST Descriptions,omitempty"`
    PieceMeasurements         []*PieceMeasurements `xml:"http://www.iata.org/IATA/EDIST PieceMeasurements,omitempty"`
    PieceAllowanceCombination string               `xml:",attr,omitempty"`
    Refs                      string               `xml:",attr,omitempty"`
}

type PieceMeasurements struct {
    PieceWeightAllowance    []*BagAllowanceWeightType    `xml:"http://www.iata.org/IATA/EDIST PieceWeightAllowance,omitempty"`
    PieceDimensionAllowance []*BagAllowanceDimensionType `xml:"http://www.iata.org/IATA/EDIST PieceDimensionAllowance,omitempty"`
    Quantity                int                          `xml:",attr,omitempty"`
}

type BagAllowanceWeightType struct {
    ApplicableParty string           `xml:"http://www.iata.org/IATA/EDIST ApplicableParty,omitempty"`
    MaximumWeight   []*MaximumWeight `xml:"http://www.iata.org/IATA/EDIST MaximumWeight,omitempty"`
    ApplicableBag   string           `xml:"http://www.iata.org/IATA/EDIST ApplicableBag,omitempty"`
    Descriptions    *Descriptions    `xml:"http://www.iata.org/IATA/EDIST Descriptions,omitempty"`
    Refs            string           `xml:",attr,omitempty"`
}

type MaximumWeight struct {
    Value                  float64 `xml:",chardata"`
    UOM                    string  `xml:"http://www.iata.org/IATA/EDIST UOM,omitempty"`
    SpreadOverBagsQuantity int     `xml:"http://www.iata.org/IATA/EDIST SpreadOverBagsQuantity,omitempty"`
    Refs                   string  `xml:",attr,omitempty"`
}

type BagDisclosureType struct {
    BagRule      string        `xml:"http://www.iata.org/IATA/EDIST BagRule,omitempty"`
    Descriptions *Descriptions `xml:"http://www.iata.org/IATA/EDIST Descriptions,omitempty"`
    Remarks      *RemarkType   `xml:"http://www.iata.org/IATA/EDIST Remarks,omitempty"`
    Refs         string        `xml:",attr,omitempty"`
}

type AllowanceDescription BagAllowanceDescType

type CarryOnAllowanceType struct {
    CarryOnBag []*CarryOnBag `xml:"http://www.iata.org/IATA/EDIST CarryOnBag,omitempty"`
}

type CarryOnBag struct {
    WeightAllowance           *WeightAllowance               `xml:"http://www.iata.org/IATA/EDIST WeightAllowance,omitempty"`
    PieceAllowance            []*BagAllowancePieceType       `xml:"http://www.iata.org/IATA/EDIST PieceAllowance,omitempty"`
    DimensionAllowance        *BagAllowanceDimensionType     `xml:"http://www.iata.org/IATA/EDIST DimensionAllowance,omitempty"`
    AllowanceDescription      *BagAllowanceDescType          `xml:"http://www.iata.org/IATA/EDIST AllowanceDescription,omitempty"`
    BaggageDeterminingCarrier *BaggageDeterminingCarrierType `xml:"http://www.iata.org/IATA/EDIST BaggageDeterminingCarrier,omitempty"`
    BDC_AnalysisResult        *CodesetType                   `xml:"http://www.iata.org/IATA/EDIST BDC_AnalysisResult,omitempty"`
    Refs                      string                         `xml:",attr,omitempty"`
}

type CarryOnBags CarryOnAllowanceType

type CheckedBagAllowanceType struct {
    CheckedBag []*CheckedBag `xml:"http://www.iata.org/IATA/EDIST CheckedBag,omitempty"`
}

type CheckedBag struct {
    WeightAllowance           *WeightAllowance               `xml:"http://www.iata.org/IATA/EDIST WeightAllowance,omitempty"`
    PieceAllowance            []*BagAllowancePieceType       `xml:"http://www.iata.org/IATA/EDIST PieceAllowance,omitempty"`
    DimensionAllowance        *BagAllowanceDimensionType     `xml:"http://www.iata.org/IATA/EDIST DimensionAllowance,omitempty"`
    AllowanceDescription      *BagAllowanceDescType          `xml:"http://www.iata.org/IATA/EDIST AllowanceDescription,omitempty"`
    BaggageDeterminingCarrier *BaggageDeterminingCarrierType `xml:"http://www.iata.org/IATA/EDIST BaggageDeterminingCarrier,omitempty"`
    BDC_AnalysisResult        *CodesetType                   `xml:"http://www.iata.org/IATA/EDIST BDC_AnalysisResult,omitempty"`
    Refs                      string                         `xml:",attr,omitempty"`
}

type CheckedBags CheckedBagAllowanceType

type DimensionAllowance BagAllowanceDimensionType

type PieceAllowance BagAllowancePieceType

type WeightAllowance struct {
    BagAllowanceWeightType
    DimensionAllowance []*BagAllowanceDimensionType `xml:"http://www.iata.org/IATA/EDIST DimensionAllowance,omitempty"`
}

type BookingReference BookingReferenceType

type BookingReferences struct {
    BookingReference []*BookingReferenceType `xml:"http://www.iata.org/IATA/EDIST BookingReference,omitempty"`
}

type BookingReferenceType struct {
    Type      *CodesetType `xml:"http://www.iata.org/IATA/EDIST Type,omitempty"`
    ID        string       `xml:"http://www.iata.org/IATA/EDIST ID,omitempty"`
    AirlineID *AirlineID   `xml:"http://www.iata.org/IATA/EDIST AirlineID,omitempty"`
    OtherID   *OtherID     `xml:"http://www.iata.org/IATA/EDIST OtherID,omitempty"`
    Refs      string       `xml:",attr,omitempty"`
}

type AirlineID struct {
    AirlineID_Type
    Name string `xml:",attr,omitempty"`
}

type OtherID struct {
    Refs                 string `xml:",attr,omitempty"`
    ObjectMetaReferences string `xml:",attr,omitempty"`
    Name                 string `xml:",attr,omitempty"`
    Value                string `xml:",chardata"`
}

type CityCode CityCodeType

type CityCodeType struct {
    Refs                 string `xml:",attr,omitempty"`
    ObjectMetaReferences string `xml:",attr,omitempty"`
    Value                string `xml:",chardata"`
}

type CityName string

type CodesetType struct {
    Code       string `xml:"http://www.iata.org/IATA/EDIST Code,omitempty"`
    Definition string `xml:"http://www.iata.org/IATA/EDIST Definition,omitempty"`
    TableName  string `xml:"http://www.iata.org/IATA/EDIST TableName,omitempty"`
    Link       string `xml:"http://www.iata.org/IATA/EDIST Link,omitempty"`
    Refs       string `xml:",attr,omitempty"`
}

type AddressContact AddressContactType

type AddressContactType struct {
    StructuredAddrType
}

type Contacts struct {
    Contact []*Contact `xml:"http://www.iata.org/IATA/EDIST Contact,omitempty"`
}

type Contact struct {
    AddressContact     *StructuredAddrType     `xml:"http://www.iata.org/IATA/EDIST AddressContact,omitempty"`
    EmailContact       *EmailType              `xml:"http://www.iata.org/IATA/EDIST EmailContact,omitempty"`
    OtherContactMethod *OtherContactMethodType `xml:"http://www.iata.org/IATA/EDIST OtherContactMethod,omitempty"`
    PhoneContact       *PhoneType              `xml:"http://www.iata.org/IATA/EDIST PhoneContact,omitempty"`
    Name               *Name                   `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
    ContactType        string                  `xml:",attr,omitempty"`
}

type Name struct {
    Surname              *Surname  `xml:"http://www.iata.org/IATA/EDIST Surname,omitempty"`
    Given                []*Given  `xml:"http://www.iata.org/IATA/EDIST Given,omitempty"`
    Title                string    `xml:"http://www.iata.org/IATA/EDIST Title,omitempty"`
    SurnameSuffix        string    `xml:"http://www.iata.org/IATA/EDIST SurnameSuffix,omitempty"`
    Middle               []*Middle `xml:"http://www.iata.org/IATA/EDIST Middle,omitempty"`
    Refs                 string    `xml:",attr,omitempty"`
    ObjectMetaReferences string    `xml:",attr,omitempty"`
}

type Surname struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type Given struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type Middle struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type ContactCoreType struct {
    Refs string `xml:",attr,omitempty"`
}

type EmailType struct {
    Application string        `xml:"http://www.iata.org/IATA/EDIST Application,omitempty"`
    Address     *EmailID_Type `xml:"http://www.iata.org/IATA/EDIST Address,omitempty"`
    Refs        string        `xml:",attr,omitempty"`
}

type EmailContact EmailType

type EmailAddress EmailType

type EmailID_Type struct {
    Refs                 string `xml:",attr,omitempty"`
    ObjectMetaReferences string `xml:",attr,omitempty"`
    Value                string `xml:",chardata"`
}

type PhoneContact PhoneContactType

type PhoneNumber PhoneType

type PhoneContactType struct {
    PhoneType
}

type PhoneType struct {
    Application string      `xml:"http://www.iata.org/IATA/EDIST Application,omitempty"`
    Number      []*Number_1 `xml:"http://www.iata.org/IATA/EDIST Number,omitempty"`
    Refs        string      `xml:",attr,omitempty"`
}

type Number_1 struct {
    Refs        string `xml:",attr,omitempty"`
    CountryCode string `xml:",attr,omitempty"`
    AreaCode    string `xml:",attr,omitempty"`
    Extension   string `xml:",attr,omitempty"`
    Value       string `xml:",chardata"`
}

type OtherContactMethod OtherContactMethodType

type OtherContactMethodType struct {
    ContactCoreType
    Name      string `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
    Value     string `xml:",chardata"`
    GroupName string `xml:"http://www.iata.org/IATA/EDIST GroupName,omitempty"`
}

type Commission CommissionType

type CommissionType struct {
    Amount     *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST Amount,omitempty"`
    Percentage float64                `xml:"http://www.iata.org/IATA/EDIST Percentage,omitempty"`
    Code       string                 `xml:"http://www.iata.org/IATA/EDIST Code,omitempty"`
    Remarks    *RemarkType            `xml:"http://www.iata.org/IATA/EDIST Remarks,omitempty"`
    Refs       string                 `xml:",attr,omitempty"`
}

type CountryCode CountryCodeType

type CountryCodeType struct {
    Refs                 string `xml:",attr,omitempty"`
    ObjectMetaReferences string `xml:",attr,omitempty"`
    Value                string `xml:",chardata"`
}

type CountryQueryType struct {
    CountryCodes       *CountryCodes           `xml:"http://www.iata.org/IATA/EDIST CountryCodes,omitempty"`
    Keywords           *Keywords               `xml:"http://www.iata.org/IATA/EDIST Keywords,omitempty"`
    Proximity          []*CountryProximityType `xml:"http://www.iata.org/IATA/EDIST Proximity,omitempty"`
    PreferencesLevel   string                  `xml:",attr,omitempty"`
    PreferencesContext string                  `xml:",attr,omitempty"`
    Refs               string                  `xml:",attr,omitempty"`
}

type CountryCodes struct {
    CountryCode []*CountryCodeType `xml:"http://www.iata.org/IATA/EDIST CountryCode,omitempty"`
}

type Keywords struct {
    KeyWord []*KeyWordType `xml:"http://www.iata.org/IATA/EDIST KeyWord,omitempty"`
}

type CountryProximityType struct {
    LocationProximityType
    CountryCode *CountryCodeType `xml:"http://www.iata.org/IATA/EDIST CountryCode,omitempty"`
}

type CurrencyAmount CurrencyAmountOptType

type CurrCode struct {
    Id          string `xml:",attr,omitempty"`
    Metadata    string `xml:",attr,omitempty"`
    Refs        string `xml:",attr,omitempty"`
    Application string `xml:",attr,omitempty"`
    Value       string `xml:",chardata"`
}

type CurrencyCodeType struct {
    Refs                 string `xml:",attr,omitempty"`
    ObjectMetaReferences string `xml:",attr,omitempty"`
    Value                string `xml:",chardata"`
}

type CurrencyAmountEncodedType struct {
    Refs  string  `xml:",attr,omitempty"`
    Code  string  `xml:",attr,omitempty"`
    Value float64 `xml:",chardata"`
}

type CurrencyAmountOptType struct {
    Code    string  `xml:",attr,omitempty"`
    Taxable bool    `xml:",attr,omitempty"`
    Value   float64 `xml:",chardata"`
}

type EncodedCurrencyAmount CurrencyAmountEncodedType

type ISO_CurrencyCode CurrencyCodeType

type MessageCurrencies struct {
    CurrCode []*CurrencyCodeType `xml:"http://www.iata.org/IATA/EDIST CurrCode,omitempty"`
}

type Descriptions struct {
    Description []*Description `xml:"http://www.iata.org/IATA/EDIST Description,omitempty"`
}

type Description struct {
    DescriptionType
    OriginDestinationReference []string `xml:"http://www.iata.org/IATA/EDIST OriginDestinationReference,omitempty"`
}

type DescriptionReferences string

type DescriptionType struct {
    Text        *Text    `xml:"http://www.iata.org/IATA/EDIST Text,omitempty"`
    MarkupStyle string   `xml:"http://www.iata.org/IATA/EDIST MarkupStyle,omitempty"`
    Link        string   `xml:"http://www.iata.org/IATA/EDIST Link,omitempty"`
    Media       []*Media `xml:"http://www.iata.org/IATA/EDIST Media,omitempty"`
    Refs        string   `xml:",attr,omitempty"`
    ObjectKey   string   `xml:",attr,omitempty"`
}

type Text struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type Media struct {
    ObjectID     *MediaID_Type `xml:"http://www.iata.org/IATA/EDIST ObjectID,omitempty"`
    MediaLink    *MediaLink    `xml:"http://www.iata.org/IATA/EDIST MediaLink,omitempty"`
    AttachmentID *MediaID_Type `xml:"http://www.iata.org/IATA/EDIST AttachmentID,omitempty"`
}

type MediaID MediaID_Type

type MediaID_Type struct {
    UniqueIDContextType
}

type MediaLink struct {
    Refs                 string `xml:",attr,omitempty"`
    ObjectMetaReferences string `xml:",attr,omitempty"`
    Value                string `xml:",chardata"`
}

type MediaAttachment MediaAttachmentType

type MediaAttachmentType struct {
    ImageID       *ImageID                   `xml:"http://www.iata.org/IATA/EDIST ImageID,omitempty"`
    AttachmentURI *AttachmentURI             `xml:"http://www.iata.org/IATA/EDIST AttachmentURI,omitempty"`
    Description   string                     `xml:"http://www.iata.org/IATA/EDIST Description,omitempty"`
    Mimetype      string                     `xml:"http://www.iata.org/IATA/EDIST Mimetype,omitempty"`
    FileKbSize    string                     `xml:"http://www.iata.org/IATA/EDIST FileKbSize,omitempty"`
    Rendering     *RenderingInstructionsType `xml:"http://www.iata.org/IATA/EDIST Rendering,omitempty"`
    Content       string                     `xml:"http://www.iata.org/IATA/EDIST Content,omitempty"`
    Refs          string                     `xml:",attr,omitempty"`
}

type AttachmentURI struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type RenderingInstructionsType struct {
    Method         string                  `xml:"http://www.iata.org/IATA/EDIST Method,omitempty"`
    OutputFormat   string                  `xml:"http://www.iata.org/IATA/EDIST OutputFormat,omitempty"`
    AttachmentURI  string                  `xml:"http://www.iata.org/IATA/EDIST AttachmentURI,omitempty"`
    AttachmentSize string                  `xml:"http://www.iata.org/IATA/EDIST AttachmentSize,omitempty"`
    Parameters     *RenderingParameterType `xml:"http://www.iata.org/IATA/EDIST Parameters,omitempty"`
    Overview       string                  `xml:"http://www.iata.org/IATA/EDIST Overview,omitempty"`
    Refs           string                  `xml:",attr,omitempty"`
}

type RenderingParameterType struct {
    Parameter []*Parameter `xml:"http://www.iata.org/IATA/EDIST Parameter,omitempty"`
    Refs      string       `xml:",attr,omitempty"`
}

type Parameter struct {
    Name  string `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
    Value string `xml:",chardata"`
}

type Directions_1 struct {
    Direction []*DirectionsType `xml:"http://www.iata.org/IATA/EDIST Direction,omitempty"`
}

type Direction DirectionsType

type DirectionsType struct {
    Text string `xml:"http://www.iata.org/IATA/EDIST Text,omitempty"`
    Link string `xml:"http://www.iata.org/IATA/EDIST Link,omitempty"`
    Name string `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
    From string `xml:"http://www.iata.org/IATA/EDIST From,omitempty"`
    To   string `xml:"http://www.iata.org/IATA/EDIST To,omitempty"`
    Refs string `xml:",attr,omitempty"`
}

type Disclosures DisclosureType

type DisclosureType struct {
    Description []*DescriptionType `xml:"http://www.iata.org/IATA/EDIST Description,omitempty"`
    Refs        string             `xml:",attr,omitempty"`
}

type DiscountAmount CurrencyAmountOptType

type DiscountPercent int

type DiscountType struct {
    DiscountAmount  *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST DiscountAmount,omitempty"`
    DiscountPercent int                    `xml:"http://www.iata.org/IATA/EDIST DiscountPercent,omitempty"`
    Application     string                 `xml:"http://www.iata.org/IATA/EDIST Application,omitempty"`
    Description     string                 `xml:"http://www.iata.org/IATA/EDIST Description,omitempty"`
    Refs            string                 `xml:",attr,omitempty"`
}

type FQTV_ProgramCore FQTV_ProgramCoreType

type FQTV_ProgramSummary FQTV_ProgramSummaryType

type FQTV_ProgramDetail FQTV_ProgramDetailType

type FQTV_Alliance FQTV_AllianceType

type FQTV_ProgramID FQTV_ProgramIDType

type FQTV_ProgramIDType struct {
    UniqueIDContextType
}

type FQTV_ProgramCoreType struct {
    FQTV_ProgramID *UniqueIDContextType `xml:"http://www.iata.org/IATA/EDIST FQTV_ProgramID,omitempty"`
    ProviderID     *ProviderID          `xml:"http://www.iata.org/IATA/EDIST ProviderID,omitempty"`
    Account        *Account             `xml:"http://www.iata.org/IATA/EDIST Account,omitempty"`
    Tier           *Tier                `xml:"http://www.iata.org/IATA/EDIST Tier,omitempty"`
    Refs           string               `xml:",attr,omitempty"`
}

type ProviderID struct {
    Refs                 string `xml:",attr,omitempty"`
    ObjectMetaReferences string `xml:",attr,omitempty"`
    Value                string `xml:",chardata"`
}

type Account struct {
    SignInID *SignInID `xml:"http://www.iata.org/IATA/EDIST SignInID,omitempty"`
    Number   *Number_2 `xml:"http://www.iata.org/IATA/EDIST Number,omitempty"`
    Refs     string    `xml:",attr,omitempty"`
}

type SignInID struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type Number_2 struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type Tier struct {
    TierName string `xml:"http://www.iata.org/IATA/EDIST TierName,omitempty"`
    TierCode string `xml:"http://www.iata.org/IATA/EDIST TierCode,omitempty"`
    Priority string `xml:"http://www.iata.org/IATA/EDIST Priority,omitempty"`
}

type FQTV_ProgramSummaryType struct {
    FQTV_ProgramCoreType
    URL  string `xml:"http://www.iata.org/IATA/EDIST URL,omitempty"`
    Name string `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
}

type FQTV_ProgramDetailType struct {
    FQTV_ProgramSummaryType
    Alliances *Alliances `xml:"http://www.iata.org/IATA/EDIST Alliances,omitempty"`
}

type Alliances struct {
    Alliance []*FQTV_AllianceType `xml:"http://www.iata.org/IATA/EDIST Alliance,omitempty"`
}

type TravelerFQTV_Information TravelerFQTV_Type

type TravelerFQTV_Type struct {
    AirlineID *AirlineID_Type `xml:"http://www.iata.org/IATA/EDIST AirlineID,omitempty"`
    Account   *Account_1      `xml:"http://www.iata.org/IATA/EDIST Account,omitempty"`
    ProgramID string          `xml:"http://www.iata.org/IATA/EDIST ProgramID,omitempty"`
    Refs      string          `xml:",attr,omitempty"`
}

type Account_1 struct {
    SignInID   *SignInID_1 `xml:"http://www.iata.org/IATA/EDIST SignInID,omitempty"`
    Number     *Number_3   `xml:"http://www.iata.org/IATA/EDIST Number,omitempty"`
    FQTVStatus string      `xml:"http://www.iata.org/IATA/EDIST FQTVStatus,omitempty"`
    Refs       string      `xml:",attr,omitempty"`
}

type SignInID_1 struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type Number_3 struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type FQTV_AllianceType struct {
    Code         *UniqueIDContextType `xml:"http://www.iata.org/IATA/EDIST Code,omitempty"`
    URL          string               `xml:"http://www.iata.org/IATA/EDIST URL,omitempty"`
    Name         string               `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
    Participants *Participants        `xml:"http://www.iata.org/IATA/EDIST Participants,omitempty"`
    Refs         string               `xml:",attr,omitempty"`
}

type Participants struct {
    Participant []*Participant `xml:"http://www.iata.org/IATA/EDIST Participant,omitempty"`
}

type Participant struct {
    ParticipatingAirline *AllianceAirlinePartyType `xml:"http://www.iata.org/IATA/EDIST ParticipatingAirline,omitempty"`
    ParticipatingProgram *AllianceProgramType      `xml:"http://www.iata.org/IATA/EDIST ParticipatingProgram,omitempty"`
}

type AllianceAirlinePartyType struct {
    AirlineID   *AirlineID_Type `xml:"http://www.iata.org/IATA/EDIST AirlineID,omitempty"`
    ProgramName string          `xml:"http://www.iata.org/IATA/EDIST ProgramName,omitempty"`
    Refs        string          `xml:",attr,omitempty"`
}

type AllianceProgramType struct {
    ProgramName string          `xml:"http://www.iata.org/IATA/EDIST ProgramName,omitempty"`
    AirlineID   *AirlineID_Type `xml:"http://www.iata.org/IATA/EDIST AirlineID,omitempty"`
    Refs        string          `xml:",attr,omitempty"`
}

type ParticipatingProgram AllianceProgramType

type ParticipatingAirline AllianceAirlinePartyType

type InstantPurchase InstantPurchaseType

type InstantPurchaseType struct {
    CodesetType
}

type KeyWord KeyWordType

type KeyWordType struct {
    Word      string   `xml:"http://www.iata.org/IATA/EDIST Word,omitempty"`
    Value     []string `xml:",chardata"`
    Refs      string   `xml:",attr,omitempty"`
    ObjectKey string   `xml:",attr,omitempty"`
}

type LanguageCode LanguageCodeType

type Languages struct {
    LanguageCode *LanguageCodeType `xml:"http://www.iata.org/IATA/EDIST LanguageCode,omitempty"`
}

type LanguageCodeType struct {
    Refs      string `xml:",attr,omitempty"`
    ObjectKey string `xml:",attr,omitempty"`
    Value     string `xml:",chardata"`
}

type CampaignReferral CampaignReferralType

type CampaignReferralType struct {
    TrxProcessObjectBaseType
    UTM          *UTM          `xml:"http://www.iata.org/IATA/EDIST UTM,omitempty"`
    Localization *Localization `xml:"http://www.iata.org/IATA/EDIST Localization,omitempty"`
}

type UTM struct {
    ReferrerURL string                     `xml:"http://www.iata.org/IATA/EDIST ReferrerURL,omitempty"`
    Other_UTMs  *CampaignUTM_ParameterType `xml:"http://www.iata.org/IATA/EDIST Other_UTMs,omitempty"`
    Campaign    string                     `xml:",attr,omitempty"`
    Source      string                     `xml:",attr,omitempty"`
    Medium      string                     `xml:",attr,omitempty"`
    Content     string                     `xml:",attr,omitempty"`
    Term        string                     `xml:",attr,omitempty"`
}

type Localization struct {
    CountryCode  string `xml:",attr,omitempty"`
    CurrencyCode string `xml:",attr,omitempty"`
    LanguageCode string `xml:",attr,omitempty"`
}

type CampaignUTM_ParameterType struct {
    Other_UTM []*Other_UTM `xml:"http://www.iata.org/IATA/EDIST Other_UTM,omitempty"`
}

type Other_UTM struct {
    Name       string `xml:",attr,omitempty"`
    Value      string `xml:",attr,omitempty"`
    Definition string `xml:",attr,omitempty"`
    Method     string `xml:",attr,omitempty"`
}

type NodePath NodePathType

type NodePathType struct {
    Path    string `xml:"http://www.iata.org/IATA/EDIST Path,omitempty"`
    TagName string `xml:"http://www.iata.org/IATA/EDIST TagName,omitempty"`
}

type BankAccount BankAccountType

type BankAccountType struct {
    Name              *Name_1        `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
    BankID            *BankID        `xml:"http://www.iata.org/IATA/EDIST BankID,omitempty"`
    AccountType       string         `xml:"http://www.iata.org/IATA/EDIST AccountType,omitempty"`
    AccountNumber     *AccountNumber `xml:"http://www.iata.org/IATA/EDIST AccountNumber,omitempty"`
    Refs              string         `xml:",attr,omitempty"`
    CkecksAcceptedInd bool           `xml:",attr,omitempty"`
}

type Name_1 struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type BankID struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type AccountNumber struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type Cash struct {
    CashInd bool `xml:",attr,omitempty"`
}

type Check struct {
    Number     int    `xml:"http://www.iata.org/IATA/EDIST Number,omitempty"`
    SignedDate string `xml:"http://www.iata.org/IATA/EDIST SignedDate,omitempty"`
    OrderTo    string `xml:"http://www.iata.org/IATA/EDIST OrderTo,omitempty"`
}

type DirectBill DirectBillType

type DirectBillType struct {
    DirectBillID string     `xml:"http://www.iata.org/IATA/EDIST DirectBillID,omitempty"`
    CompanyName  string     `xml:"http://www.iata.org/IATA/EDIST CompanyName,omitempty"`
    ContactName  string     `xml:"http://www.iata.org/IATA/EDIST ContactName,omitempty"`
    Address      *Address_2 `xml:"http://www.iata.org/IATA/EDIST Address,omitempty"`
    Refs         string     `xml:",attr,omitempty"`
}

type Address_2 struct {
    StructuredAddress *StructuredAddrType `xml:"http://www.iata.org/IATA/EDIST StructuredAddress,omitempty"`
    SimpleAddress     *SimpleAddrType     `xml:"http://www.iata.org/IATA/EDIST SimpleAddress,omitempty"`
    PaymentAddress    *PaymentAddrType    `xml:"http://www.iata.org/IATA/EDIST PaymentAddress,omitempty"`
}

type LoyaltyRedemption struct {
    RedemptionQuantity int           `xml:"http://www.iata.org/IATA/EDIST RedemptionQuantity,omitempty"`
    Certificates       *Certificates `xml:"http://www.iata.org/IATA/EDIST Certificates,omitempty"`
    MemberNumber       *MemberNumber `xml:"http://www.iata.org/IATA/EDIST MemberNumber,omitempty"`
    PromotionCode      string        `xml:"http://www.iata.org/IATA/EDIST PromotionCode,omitempty"`
    PromoVendorCode    []string      `xml:"http://www.iata.org/IATA/EDIST PromoVendorCode,omitempty"`
    Refs               string        `xml:",attr,omitempty"`
}

type Certificates struct {
    CertificateNumber []*CertificateNumber `xml:"http://www.iata.org/IATA/EDIST CertificateNumber,omitempty"`
}

type CertificateNumber struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type MemberNumber struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type MiscChargeOrder struct {
    TicketNumber string `xml:"http://www.iata.org/IATA/EDIST TicketNumber,omitempty"`
}

type Other struct {
    Remarks *RemarkType `xml:"http://www.iata.org/IATA/EDIST Remarks,omitempty"`
}

type PaymentCard PaymentCardType

type PaymentForms struct {
    PaymentForm []*PaymentForm `xml:"http://www.iata.org/IATA/EDIST PaymentForm,omitempty"`
}

type PaymentForm struct {
    BankAccount       *BankAccountType   `xml:"http://www.iata.org/IATA/EDIST BankAccount,omitempty"`
    Cash              *Cash              `xml:"http://www.iata.org/IATA/EDIST Cash,omitempty"`
    DirectBill        *DirectBillType    `xml:"http://www.iata.org/IATA/EDIST DirectBill,omitempty"`
    LoyaltyRedemption *LoyaltyRedemption `xml:"http://www.iata.org/IATA/EDIST LoyaltyRedemption,omitempty"`
    MiscChargeOrder   *MiscChargeOrder   `xml:"http://www.iata.org/IATA/EDIST MiscChargeOrder,omitempty"`
    Other             *Other             `xml:"http://www.iata.org/IATA/EDIST Other,omitempty"`
    PaymentCard       *PaymentCardType   `xml:"http://www.iata.org/IATA/EDIST PaymentCard,omitempty"`
    Voucher           *Voucher           `xml:"http://www.iata.org/IATA/EDIST Voucher,omitempty"`
    Refs              string             `xml:",attr,omitempty"`
}

type PaymentCardType struct {
    KeyWithMetaObjectBaseType
    CardType                 string                 `xml:"http://www.iata.org/IATA/EDIST CardType,omitempty"`
    CardCode                 string                 `xml:"http://www.iata.org/IATA/EDIST CardCode,omitempty"`
    CardNumber               *CardNumber            `xml:"http://www.iata.org/IATA/EDIST CardNumber,omitempty"`
    IssueNumber              int                    `xml:"http://www.iata.org/IATA/EDIST IssueNumber,omitempty"`
    SeriesCode               *SeriesCode            `xml:"http://www.iata.org/IATA/EDIST SeriesCode,omitempty"`
    CardHolderName           *CardHolderName        `xml:"http://www.iata.org/IATA/EDIST CardHolderName,omitempty"`
    CardIssuerName           *CardIssuerName        `xml:"http://www.iata.org/IATA/EDIST CardIssuerName,omitempty"`
    CardHolderBillingAddress *StructuredAddrType    `xml:"http://www.iata.org/IATA/EDIST CardHolderBillingAddress,omitempty"`
    MaskedCardNumber         *MaskedCardNumber      `xml:"http://www.iata.org/IATA/EDIST MaskedCardNumber,omitempty"`
    Amount                   *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST Amount,omitempty"`
    Surcharge                *Surcharge             `xml:"http://www.iata.org/IATA/EDIST Surcharge,omitempty"`
    EffectiveExpireDate      *EffectiveExpireDate   `xml:"http://www.iata.org/IATA/EDIST EffectiveExpireDate,omitempty"`
    AddressValidation        string                 `xml:"http://www.iata.org/IATA/EDIST AddressValidation,omitempty"`
    TokenizedCardNumber      string                 `xml:"http://www.iata.org/IATA/EDIST TokenizedCardNumber,omitempty"`
    ApprovalType             *CodesetType           `xml:"http://www.iata.org/IATA/EDIST ApprovalType,omitempty"`
}

type CardNumber struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type SeriesCode struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type CardHolderName struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type CardIssuerName struct {
    BankID string `xml:"http://www.iata.org/IATA/EDIST BankID,omitempty"`
}

type MaskedCardNumber struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type Surcharge struct {
    Amount          *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST Amount,omitempty"`
    PercentageValue float64                `xml:"http://www.iata.org/IATA/EDIST PercentageValue,omitempty"`
}

type EffectiveExpireDate struct {
    Effective  string `xml:"http://www.iata.org/IATA/EDIST Effective,omitempty"`
    Expiration string `xml:"http://www.iata.org/IATA/EDIST Expiration,omitempty"`
    Refs       string `xml:",attr,omitempty"`
}

type Voucher struct {
    Number              string                 `xml:"http://www.iata.org/IATA/EDIST Number,omitempty"`
    EffectiveExpireDate *EffectiveExpireDate_1 `xml:"http://www.iata.org/IATA/EDIST EffectiveExpireDate,omitempty"`
    Name                *Name_2                `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
}

type EffectiveExpireDate_1 struct {
    Effective  string `xml:"http://www.iata.org/IATA/EDIST Effective,omitempty"`
    Expiration string `xml:"http://www.iata.org/IATA/EDIST Expiration,omitempty"`
}

type Name_2 struct {
    Surname              *Surname_1  `xml:"http://www.iata.org/IATA/EDIST Surname,omitempty"`
    Given                []*Given_1  `xml:"http://www.iata.org/IATA/EDIST Given,omitempty"`
    Title                string      `xml:"http://www.iata.org/IATA/EDIST Title,omitempty"`
    SurnameSuffix        string      `xml:"http://www.iata.org/IATA/EDIST SurnameSuffix,omitempty"`
    Middle               []*Middle_1 `xml:"http://www.iata.org/IATA/EDIST Middle,omitempty"`
    Refs                 string      `xml:",attr,omitempty"`
    ObjectMetaReferences string      `xml:",attr,omitempty"`
}

type Surname_1 struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type Given_1 struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type Middle_1 struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type Penalty PenaltyDetailType

type PenaltyType struct {
    ApplicableFeeRemarks *RemarkType `xml:"http://www.iata.org/IATA/EDIST ApplicableFeeRemarks,omitempty"`
    Details              *Details    `xml:"http://www.iata.org/IATA/EDIST Details,omitempty"`
    CancelFeeInd         bool        `xml:",attr,omitempty"`
    ChangeFeeInd         bool        `xml:",attr,omitempty"`
    RefundableInd        bool        `xml:",attr,omitempty"`
    ReuseInd             bool        `xml:",attr,omitempty"`
    UpgradeFeeInd        bool        `xml:",attr,omitempty"`
    Refs                 string      `xml:",attr,omitempty"`
    ObjectKey            string      `xml:",attr,omitempty"`
}

type Details struct {
    Detail []*Detail `xml:"http://www.iata.org/IATA/EDIST Detail,omitempty"`
}

type Detail struct {
    Type        string       `xml:"http://www.iata.org/IATA/EDIST Type,omitempty"`
    Application *CodesetType `xml:"http://www.iata.org/IATA/EDIST Application,omitempty"`
    Amounts     *Amounts     `xml:"http://www.iata.org/IATA/EDIST Amounts,omitempty"`
    Refs        string       `xml:",attr,omitempty"`
}

type Amounts struct {
    Amount []*Amount `xml:"http://www.iata.org/IATA/EDIST Amount,omitempty"`
}

type Amount struct {
    CurrencyAmountValue  *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST CurrencyAmountValue,omitempty"`
    PercentageValue      float64                `xml:"http://www.iata.org/IATA/EDIST PercentageValue,omitempty"`
    AmountApplication    string                 `xml:"http://www.iata.org/IATA/EDIST AmountApplication,omitempty"`
    ApplicableFeeRemarks *RemarkType            `xml:"http://www.iata.org/IATA/EDIST ApplicableFeeRemarks,omitempty"`
}

type PenaltyDetailType struct {
    PenaltyType
}

type Position PositionType

type PositionType struct {
    Latitude  *Latitude  `xml:"http://www.iata.org/IATA/EDIST Latitude,omitempty"`
    Longitude *Longitude `xml:"http://www.iata.org/IATA/EDIST Longitude,omitempty"`
    Altitude  *Altitude  `xml:"http://www.iata.org/IATA/EDIST Altitude,omitempty"`
    NAC       string     `xml:"http://www.iata.org/IATA/EDIST NAC,omitempty"`
    Refs      string     `xml:",attr,omitempty"`
}

type Latitude struct {
    Sign   string `xml:",attr,omitempty"`
    Minute string `xml:",attr,omitempty"`
    Second string `xml:",attr,omitempty"`
    Value  string `xml:",chardata"`
}

type Longitude struct {
    Sign   string `xml:",attr,omitempty"`
    Minute string `xml:",attr,omitempty"`
    Second string `xml:",attr,omitempty"`
    Value  string `xml:",chardata"`
}

type Altitude struct {
    Unit    string `xml:",attr,omitempty"`
    Context string `xml:",attr,omitempty"`
    Value   string `xml:",chardata"`
}

type RadiusAttrGroup struct {
    Application string `xml:",attr,omitempty"`
    Area        int    `xml:",attr,omitempty"`
    UOM         string `xml:",attr,omitempty"`
}

type PriceCoreType struct {
    Refs            string `xml:",attr,omitempty"`
    ApproxInd       bool   `xml:",attr,omitempty"`
    RefundAllInd    bool   `xml:",attr,omitempty"`
    TaxIncludedInd  bool   `xml:",attr,omitempty"`
    OtherChargeInd  bool   `xml:",attr,omitempty"`
    AutoExchangeInd bool   `xml:",attr,omitempty"`
}

type DetailCurrencyPrice DetailCurrencyPriceType

type AwardPricing AwardPriceUnitType

type AwardPriceUnitType struct {
    Redemption *AwardRedemptionType `xml:"http://www.iata.org/IATA/EDIST Redemption,omitempty"`
}

type CombinationPricing CombinationPriceType

type CombinationPriceType struct {
    Partial *Partial `xml:"http://www.iata.org/IATA/EDIST Partial,omitempty"`
}

type Partial struct {
    Currency   *Currency            `xml:"http://www.iata.org/IATA/EDIST Currency,omitempty"`
    Redemption *AwardRedemptionType `xml:"http://www.iata.org/IATA/EDIST Redemption,omitempty"`
    Refs       string               `xml:",attr,omitempty"`
}

type Currency struct {
    EncodedCurrencyAmount *CurrencyAmountEncodedType `xml:"http://www.iata.org/IATA/EDIST EncodedCurrencyAmount,omitempty"`
    SimpleCurrencyPrice   *CurrencyAmountOptType     `xml:"http://www.iata.org/IATA/EDIST SimpleCurrencyPrice,omitempty"`
}

type DetailCurrencyPriceType struct {
    PriceCoreType
    Total      *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST Total,omitempty"`
    Equivalent string                 `xml:"http://www.iata.org/IATA/EDIST Equivalent,omitempty"`
    Details    *Details_1             `xml:"http://www.iata.org/IATA/EDIST Details,omitempty"`
    Taxes      *TaxDetailType         `xml:"http://www.iata.org/IATA/EDIST Taxes,omitempty"`
    Fees       *FeeSurchargeType      `xml:"http://www.iata.org/IATA/EDIST Fees,omitempty"`
}

type Details_1 struct {
    Detail []*Detail_1 `xml:"http://www.iata.org/IATA/EDIST Detail,omitempty"`
    Refs   string      `xml:",attr,omitempty"`
}

type Detail_1 struct {
    SubTotal    *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST SubTotal,omitempty"`
    Application string                 `xml:"http://www.iata.org/IATA/EDIST Application,omitempty"`
    Refs        string                 `xml:",attr,omitempty"`
    RefundInd   bool                   `xml:",attr,omitempty"`
}

type EncodedCurrencyPrice EncodedPriceType

type EncodedPriceType struct {
    CurrencyAmountEncodedType
}

type SimpleCurrencyPriceType struct {
    CurrencyAmountOptType
}

type SimpleCurrencyPrice SimpleCurrencyPriceType

type AwardRedemption AwardRedemptionType

type AwardRedemptionType struct {
    Unit       *Unit       `xml:"http://www.iata.org/IATA/EDIST Unit,omitempty"`
    Quantity   int         `xml:"http://www.iata.org/IATA/EDIST Quantity,omitempty"`
    Conversion *Conversion `xml:"http://www.iata.org/IATA/EDIST Conversion,omitempty"`
    Refs       string      `xml:",attr,omitempty"`
}

type Unit struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type Conversion struct {
    Amount *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST Amount,omitempty"`
    Ratio  string                 `xml:"http://www.iata.org/IATA/EDIST Ratio,omitempty"`
}

type AdjustedFixedAmount CurrencyAmountOptType

type AdjustedPercentage WholePercentageSimpleType

type CurrencyAmountValue CurrencyAmountOptType

type PercentageValue WholePercentageSimpleType

type Promos PromotionType

type PromotionType struct {
    Code         *Code       `xml:"http://www.iata.org/IATA/EDIST Code,omitempty"`
    Link         string      `xml:"http://www.iata.org/IATA/EDIST Link,omitempty"`
    Issuer       *Issuer     `xml:"http://www.iata.org/IATA/EDIST Issuer,omitempty"`
    Remarks      *RemarkType `xml:"http://www.iata.org/IATA/EDIST Remarks,omitempty"`
    VoucherOwner string      `xml:"http://www.iata.org/IATA/EDIST VoucherOwner,omitempty"`
    Refs         string      `xml:",attr,omitempty"`
}

type Code struct {
    Refs                 string `xml:",attr,omitempty"`
    ObjectMetaReferences string `xml:",attr,omitempty"`
    Value                string `xml:",chardata"`
}

type Issuer struct {
    AirlineID *AirlineID_Type        `xml:"http://www.iata.org/IATA/EDIST AirlineID,omitempty"`
    AgencyID  *UniqueIDContextType   `xml:"http://www.iata.org/IATA/EDIST AgencyID,omitempty"`
    PartnerID *PartnerCompanyID_Type `xml:"http://www.iata.org/IATA/EDIST PartnerID,omitempty"`
}

type BagDetailAssocType struct {
    BagDisclosureReferences string `xml:"http://www.iata.org/IATA/EDIST BagDisclosureReferences,omitempty"`
    CheckedBagReferences    string `xml:"http://www.iata.org/IATA/EDIST CheckedBagReferences,omitempty"`
    CarryOnReferences       string `xml:"http://www.iata.org/IATA/EDIST CarryOnReferences,omitempty"`
}

type LocationProximityType struct {
    Application string  `xml:"http://www.iata.org/IATA/EDIST Application,omitempty"`
    AreaValue   float64 `xml:"http://www.iata.org/IATA/EDIST AreaValue,omitempty"`
    UOM         string  `xml:"http://www.iata.org/IATA/EDIST UOM,omitempty"`
    Refs        string  `xml:",attr,omitempty"`
}

type Remarks RemarkType

type RemarkType struct {
    Remark []*Remark `xml:"http://www.iata.org/IATA/EDIST Remark,omitempty"`
    Refs   string    `xml:",attr,omitempty"`
}

type Remark struct {
    DisplayInd bool   `xml:",attr,omitempty"`
    Timestamp  string `xml:",attr,omitempty"`
    Value      string `xml:",chardata"`
}

type PriceVarianceRules struct {
    PriceVarianceRule []*PriceVarianceRuleType `xml:"http://www.iata.org/IATA/EDIST PriceVarianceRule,omitempty"`
}

type PriceVarianceRuleType struct {
    RuleID             string              `xml:"http://www.iata.org/IATA/EDIST RuleID,omitempty"`
    AcceptableVariance *AcceptableVariance `xml:"http://www.iata.org/IATA/EDIST AcceptableVariance,omitempty"`
    Name               string              `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
    Owner              string              `xml:"http://www.iata.org/IATA/EDIST Owner,omitempty"`
    EffectivePeriod    *EffectivePeriod_1  `xml:"http://www.iata.org/IATA/EDIST EffectivePeriod,omitempty"`
    Remarks            *RemarkType         `xml:"http://www.iata.org/IATA/EDIST Remarks,omitempty"`
    Refs               string              `xml:",attr,omitempty"`
    ObjectKey          string              `xml:",attr,omitempty"`
}

type AcceptableVariance struct {
    CurrencyAmountValue *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST CurrencyAmountValue,omitempty"`
    PercentageValue     float64                `xml:"http://www.iata.org/IATA/EDIST PercentageValue,omitempty"`
}

type EffectivePeriod_1 struct {
    DatePeriod      *DatePeriodRepType      `xml:"http://www.iata.org/IATA/EDIST DatePeriod,omitempty"`
    DateTimePeriod  *DateTimePeriodRepType  `xml:"http://www.iata.org/IATA/EDIST DateTimePeriod,omitempty"`
    QuarterPeriod   *QuarterPeriodRepType   `xml:"http://www.iata.org/IATA/EDIST QuarterPeriod,omitempty"`
    DayPeriod       *DayPeriodRepType       `xml:"http://www.iata.org/IATA/EDIST DayPeriod,omitempty"`
    MonthPeriod     *MonthPeriodRepType     `xml:"http://www.iata.org/IATA/EDIST MonthPeriod,omitempty"`
    YearPeriod      *YearPeriodRepType      `xml:"http://www.iata.org/IATA/EDIST YearPeriod,omitempty"`
    YearMonthPeriod *YearMonthPeriodRepType `xml:"http://www.iata.org/IATA/EDIST YearMonthPeriod,omitempty"`
    TimePeriod      *TimePeriodRepType      `xml:"http://www.iata.org/IATA/EDIST TimePeriod,omitempty"`
}

type Rules struct {
    Rule []*RuleType `xml:"http://www.iata.org/IATA/EDIST Rule,omitempty"`
}

type RuleSetType struct {
    RuleSetID *RuleSetID  `xml:"http://www.iata.org/IATA/EDIST RuleSetID,omitempty"`
    RuleValid *RuleValid  `xml:"http://www.iata.org/IATA/EDIST RuleValid,omitempty"`
    Owner     string      `xml:"http://www.iata.org/IATA/EDIST Owner,omitempty"`
    Rule      []*RuleType `xml:"http://www.iata.org/IATA/EDIST Rule,omitempty"`
    Refs      string      `xml:",attr,omitempty"`
}

type RuleSetID struct {
    Name  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type RuleValid struct {
    YearPeriod      *YearPeriodRepType      `xml:"http://www.iata.org/IATA/EDIST YearPeriod,omitempty"`
    YearMonthPeriod *YearMonthPeriodRepType `xml:"http://www.iata.org/IATA/EDIST YearMonthPeriod,omitempty"`
    TimePeriod      *TimePeriodRepType      `xml:"http://www.iata.org/IATA/EDIST TimePeriod,omitempty"`
    QuarterPeriod   *QuarterPeriodRepType   `xml:"http://www.iata.org/IATA/EDIST QuarterPeriod,omitempty"`
    MonthPeriod     *MonthPeriodRepType     `xml:"http://www.iata.org/IATA/EDIST MonthPeriod,omitempty"`
    DayPeriod       *DayPeriodRepType       `xml:"http://www.iata.org/IATA/EDIST DayPeriod,omitempty"`
    DateTimePeriod  *DateTimePeriodRepType  `xml:"http://www.iata.org/IATA/EDIST DateTimePeriod,omitempty"`
    DatePeriod      *DatePeriodRepType      `xml:"http://www.iata.org/IATA/EDIST DatePeriod,omitempty"`
}

type RuleType struct {
    RuleID          string             `xml:"http://www.iata.org/IATA/EDIST RuleID,omitempty"`
    Value           *Value             `xml:"http://www.iata.org/IATA/EDIST Value,omitempty"`
    Name            string             `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
    Owner           string             `xml:"http://www.iata.org/IATA/EDIST Owner,omitempty"`
    EffectivePeriod *EffectivePeriod_2 `xml:"http://www.iata.org/IATA/EDIST EffectivePeriod,omitempty"`
    Refs            string             `xml:",attr,omitempty"`
    ObjectKey       string             `xml:",attr,omitempty"`
}

type Value struct {
    Text              string            `xml:"http://www.iata.org/IATA/EDIST Text,omitempty"`
    AugmentationPoint *AugPointInfoType `xml:"http://www.iata.org/IATA/EDIST AugmentationPoint,omitempty"`
}

type EffectivePeriod_2 struct {
    YearPeriod      *YearPeriodRepType      `xml:"http://www.iata.org/IATA/EDIST YearPeriod,omitempty"`
    YearMonthPeriod *YearMonthPeriodRepType `xml:"http://www.iata.org/IATA/EDIST YearMonthPeriod,omitempty"`
    TimePeriod      *TimePeriodRepType      `xml:"http://www.iata.org/IATA/EDIST TimePeriod,omitempty"`
    QuarterPeriod   *QuarterPeriodRepType   `xml:"http://www.iata.org/IATA/EDIST QuarterPeriod,omitempty"`
    MonthPeriod     *MonthPeriodRepType     `xml:"http://www.iata.org/IATA/EDIST MonthPeriod,omitempty"`
    DayPeriod       *DayPeriodRepType       `xml:"http://www.iata.org/IATA/EDIST DayPeriod,omitempty"`
    DateTimePeriod  *DateTimePeriodRepType  `xml:"http://www.iata.org/IATA/EDIST DateTimePeriod,omitempty"`
    DatePeriod      *DatePeriodRepType      `xml:"http://www.iata.org/IATA/EDIST DatePeriod,omitempty"`
}

type SpecialType struct {
    Code        string `xml:"http://www.iata.org/IATA/EDIST Code,omitempty"`
    Description string `xml:"http://www.iata.org/IATA/EDIST Description,omitempty"`
    DescContext string `xml:"http://www.iata.org/IATA/EDIST DescContext,omitempty"`
    Refs        string `xml:",attr,omitempty"`
}

type ShoppingResponseID ShoppingResponseID_Type

type ShoppingResponseID_Type struct {
    Owner      string      `xml:"http://www.iata.org/IATA/EDIST Owner,omitempty"`
    ResponseID *ResponseID `xml:"http://www.iata.org/IATA/EDIST ResponseID,omitempty"`
    Refs       string      `xml:",attr,omitempty"`
}

type ResponseID struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type ShoppingResponseOrderType struct {
    Owner      string        `xml:"http://www.iata.org/IATA/EDIST Owner,omitempty"`
    ResponseID *ResponseID_1 `xml:"http://www.iata.org/IATA/EDIST ResponseID,omitempty"`
    Offers     *Offers       `xml:"http://www.iata.org/IATA/EDIST Offers,omitempty"`
    Refs       string        `xml:",attr,omitempty"`
}

type ResponseID_1 struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type Offers struct {
    Offer []*Offer `xml:"http://www.iata.org/IATA/EDIST Offer,omitempty"`
}

type Offer struct {
    OfferID    *ItemID_Type  `xml:"http://www.iata.org/IATA/EDIST OfferID,omitempty"`
    OfferItems *OfferItems   `xml:"http://www.iata.org/IATA/EDIST OfferItems,omitempty"`
    TotalPrice *TotalPrice_1 `xml:"http://www.iata.org/IATA/EDIST TotalPrice,omitempty"`
}

type OfferItems struct {
    OfferItem []*OfferItem `xml:"http://www.iata.org/IATA/EDIST OfferItem,omitempty"`
}

type OfferItem struct {
    OfferItemID        *ItemID_Type          `xml:"http://www.iata.org/IATA/EDIST OfferItemID,omitempty"`
    Passengers         *Passengers           `xml:"http://www.iata.org/IATA/EDIST Passengers,omitempty"`
    TotalPrice         *TotalPrice           `xml:"http://www.iata.org/IATA/EDIST TotalPrice,omitempty"`
    ApplicableFlight   *FlightInfoAssocType  `xml:"http://www.iata.org/IATA/EDIST ApplicableFlight,omitempty"`
    IncludedService    *ServiceInfoAssocType `xml:"http://www.iata.org/IATA/EDIST IncludedService,omitempty"`
    AssociatedServices *AssociatedServices   `xml:"http://www.iata.org/IATA/EDIST AssociatedServices,omitempty"`
    Details            *Details_2            `xml:"http://www.iata.org/IATA/EDIST Details,omitempty"`
    Refs               string                `xml:",attr,omitempty"`
}

type Passengers struct {
    PassengerReference string `xml:"http://www.iata.org/IATA/EDIST PassengerReference,omitempty"`
}

type TotalPrice struct {
    AwardPricing         *AwardPriceUnitType        `xml:"http://www.iata.org/IATA/EDIST AwardPricing,omitempty"`
    CombinationPricing   *CombinationPriceType      `xml:"http://www.iata.org/IATA/EDIST CombinationPricing,omitempty"`
    SimpleCurrencyPrice  *CurrencyAmountOptType     `xml:"http://www.iata.org/IATA/EDIST SimpleCurrencyPrice,omitempty"`
    DetailCurrencyPrice  *DetailCurrencyPriceType   `xml:"http://www.iata.org/IATA/EDIST DetailCurrencyPrice,omitempty"`
    EncodedCurrencyPrice *CurrencyAmountEncodedType `xml:"http://www.iata.org/IATA/EDIST EncodedCurrencyPrice,omitempty"`
    Refs                 string                     `xml:",attr,omitempty"`
}

type AssociatedServices struct {
    AssociatedService []*AssociatedService `xml:"http://www.iata.org/IATA/EDIST AssociatedService,omitempty"`
}

type AssociatedService struct {
    ServiceID  *ServiceID_Type `xml:"http://www.iata.org/IATA/EDIST ServiceID,omitempty"`
    Passengers *Passengers_1   `xml:"http://www.iata.org/IATA/EDIST Passengers,omitempty"`
    Refs       string          `xml:",attr,omitempty"`
}

type Passengers_1 struct {
    PassengerReference string `xml:"http://www.iata.org/IATA/EDIST PassengerReference,omitempty"`
}

type Details_2 struct {
    InventoryGuarantee *InventoryGuarantee `xml:"http://www.iata.org/IATA/EDIST InventoryGuarantee,omitempty"`
    WaitListInd        bool                `xml:",attr,omitempty"`
}

type InventoryGuarantee struct {
    InvGuaranteeID               string           `xml:"http://www.iata.org/IATA/EDIST InvGuaranteeID,omitempty"`
    InventoryGuaranteeTimeLimits *CoreDateGrpType `xml:"http://www.iata.org/IATA/EDIST InventoryGuaranteeTimeLimits,omitempty"`
}

type TotalPrice_1 struct {
    AwardPricing         *AwardPriceUnitType        `xml:"http://www.iata.org/IATA/EDIST AwardPricing,omitempty"`
    CombinationPricing   *CombinationPriceType      `xml:"http://www.iata.org/IATA/EDIST CombinationPricing,omitempty"`
    SimpleCurrencyPrice  *CurrencyAmountOptType     `xml:"http://www.iata.org/IATA/EDIST SimpleCurrencyPrice,omitempty"`
    DetailCurrencyPrice  *DetailCurrencyPriceType   `xml:"http://www.iata.org/IATA/EDIST DetailCurrencyPrice,omitempty"`
    EncodedCurrencyPrice *CurrencyAmountEncodedType `xml:"http://www.iata.org/IATA/EDIST EncodedCurrencyPrice,omitempty"`
    Refs                 string                     `xml:",attr,omitempty"`
}

type StateProvCode StateProvCodeType

type StateProvQuery StateProvQueryType

type StateCode StateProvCodeType

type StateProvCodeType CodesetValueSimpleType

type StateProvQueryType struct {
    StateCodes         *StateCodes               `xml:"http://www.iata.org/IATA/EDIST StateCodes,omitempty"`
    Keywords           *Keywords_1               `xml:"http://www.iata.org/IATA/EDIST Keywords,omitempty"`
    Proximity          []*StateProvProximityType `xml:"http://www.iata.org/IATA/EDIST Proximity,omitempty"`
    PreferencesLevel   string                    `xml:",attr,omitempty"`
    PreferencesContext string                    `xml:",attr,omitempty"`
}

type StateCodes struct {
    StateCode []*StateCode_1 `xml:"http://www.iata.org/IATA/EDIST StateCode,omitempty"`
}

type StateCode_1 struct {
    StateProvCodeType
    PreferencesLevel   string `xml:",attr,omitempty"`
    PreferencesContext string `xml:",attr,omitempty"`
}

type Keywords_1 struct {
    KeyWord []*KeyWord_1 `xml:"http://www.iata.org/IATA/EDIST KeyWord,omitempty"`
}

type KeyWord_1 struct {
    KeyWordType
    PreferencesLevel   string `xml:",attr,omitempty"`
    PreferencesContext string `xml:",attr,omitempty"`
}

type StateProvProximityType struct {
    LocationProximityType
    StateCode *StateCode_2 `xml:"http://www.iata.org/IATA/EDIST StateCode,omitempty"`
}

type StateCode_2 struct {
    StateProvCodeType
    Application        string `xml:",attr,omitempty"`
    Area               int    `xml:",attr,omitempty"`
    UOM                string `xml:",attr,omitempty"`
    PreferencesLevel   string `xml:",attr,omitempty"`
    PreferencesContext string `xml:",attr,omitempty"`
}

type FeeSurchargeType struct {
    Total     *Total     `xml:"http://www.iata.org/IATA/EDIST Total,omitempty"`
    Breakdown *Breakdown `xml:"http://www.iata.org/IATA/EDIST Breakdown,omitempty"`
    Refs      string     `xml:",attr,omitempty"`
}

type Total struct {
    CurrencyAmountOptType
    RefundInd bool `xml:",attr,omitempty"`
}

type Breakdown struct {
    Fee []*Fee `xml:"http://www.iata.org/IATA/EDIST Fee,omitempty"`
}

type Fee struct {
    Amount      *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST Amount,omitempty"`
    LocalAmount *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST LocalAmount,omitempty"`
    Designator  string                 `xml:"http://www.iata.org/IATA/EDIST Designator,omitempty"`
    Description string                 `xml:"http://www.iata.org/IATA/EDIST Description,omitempty"`
    Nature      string                 `xml:"http://www.iata.org/IATA/EDIST Nature,omitempty"`
    Refs        string                 `xml:",attr,omitempty"`
    ApproxInd   bool                   `xml:",attr,omitempty"`
    RefundInd   bool                   `xml:",attr,omitempty"`
}

type TaxBreakdown TaxDetailType

type TaxExemption TaxExemptionType

type TaxCoreType struct {
    ApproxInd     bool   `xml:",attr,omitempty"`
    CollectionInd bool   `xml:",attr,omitempty"`
    RefundAllInd  bool   `xml:",attr,omitempty"`
    Refs          string `xml:",attr,omitempty"`
}

type TaxDetailType struct {
    TaxCoreType
    Total     *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST Total,omitempty"`
    Breakdown *Breakdown_1           `xml:"http://www.iata.org/IATA/EDIST Breakdown,omitempty"`
}

type Breakdown_1 struct {
    Tax  []*Tax `xml:"http://www.iata.org/IATA/EDIST Tax,omitempty"`
    Refs string `xml:",attr,omitempty"`
}

type Tax struct {
    Qualifier        string                 `xml:"http://www.iata.org/IATA/EDIST Qualifier,omitempty"`
    Amount           *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST Amount,omitempty"`
    Nation           string                 `xml:"http://www.iata.org/IATA/EDIST Nation,omitempty"`
    TaxCode          string                 `xml:"http://www.iata.org/IATA/EDIST TaxCode,omitempty"`
    TaxType          string                 `xml:"http://www.iata.org/IATA/EDIST TaxType,omitempty"`
    CollectionPoint  []*CollectionPoint     `xml:"http://www.iata.org/IATA/EDIST CollectionPoint,omitempty"`
    LocalAmount      *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST LocalAmount,omitempty"`
    Description      string                 `xml:"http://www.iata.org/IATA/EDIST Description,omitempty"`
    Conversion       *Conversion_1          `xml:"http://www.iata.org/IATA/EDIST Conversion,omitempty"`
    FiledAmount      *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST FiledAmount,omitempty"`
    FileTaxType      string                 `xml:"http://www.iata.org/IATA/EDIST FileTaxType,omitempty"`
    AddlTaxType      string                 `xml:"http://www.iata.org/IATA/EDIST AddlTaxType,omitempty"`
    AddlFiledTaxType string                 `xml:"http://www.iata.org/IATA/EDIST AddlFiledTaxType,omitempty"`
    Refs             string                 `xml:",attr,omitempty"`
    ApproxInd        bool                   `xml:",attr,omitempty"`
    CollectionInd    bool                   `xml:",attr,omitempty"`
    RefundInd        bool                   `xml:",attr,omitempty"`
}

type CollectionPoint struct {
    CurrCode      *CurrCode    `xml:"http://www.iata.org/IATA/EDIST CurrCode,omitempty"`
    AirportAmount string       `xml:"http://www.iata.org/IATA/EDIST AirportAmount,omitempty"`
    AirportCode   *AirportCode `xml:"http://www.iata.org/IATA/EDIST AirportCode,omitempty"`
}

type Conversion_1 struct {
    CurrencyAmount []*CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST CurrencyAmount,omitempty"`
    ConversionRate string                   `xml:"http://www.iata.org/IATA/EDIST ConversionRate,omitempty"`
}

type TaxExemptionType struct {
    Total       *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST Total,omitempty"`
    Entities    *Entities              `xml:"http://www.iata.org/IATA/EDIST Entities,omitempty"`
    Territories *Territories           `xml:"http://www.iata.org/IATA/EDIST Territories,omitempty"`
    Countries   *Countries             `xml:"http://www.iata.org/IATA/EDIST Countries,omitempty"`
    Tax         []*Tax_1               `xml:"http://www.iata.org/IATA/EDIST Tax,omitempty"`
    Refs        string                 `xml:",attr,omitempty"`
}

type Entities struct {
    Entity []string `xml:"http://www.iata.org/IATA/EDIST Entity,omitempty"`
}

type Territories struct {
    Territory []string `xml:"http://www.iata.org/IATA/EDIST Territory,omitempty"`
}

type Countries struct {
    CountryCode []*CountryCodeType `xml:"http://www.iata.org/IATA/EDIST CountryCode,omitempty"`
}

type Tax_1 struct {
    Designator    string                 `xml:"http://www.iata.org/IATA/EDIST Designator,omitempty"`
    Amount        *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST Amount,omitempty"`
    CountryCode   *CountryCodeType       `xml:"http://www.iata.org/IATA/EDIST CountryCode,omitempty"`
    Nature        string                 `xml:"http://www.iata.org/IATA/EDIST Nature,omitempty"`
    LocalAmount   *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST LocalAmount,omitempty"`
    Description   string                 `xml:"http://www.iata.org/IATA/EDIST Description,omitempty"`
    Refs          string                 `xml:",attr,omitempty"`
    ApproxInd     bool                   `xml:",attr,omitempty"`
    CollectionInd bool                   `xml:",attr,omitempty"`
    RefundInd     bool                   `xml:",attr,omitempty"`
}

type TicketDesig TicketDesignatorType

type TicketAutoExchangeType struct {
    Penalty        *Penalty_1             `xml:"http://www.iata.org/IATA/EDIST Penalty,omitempty"`
    OriginalAmount *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST OriginalAmount,omitempty"`
    CurrCode       *CurrCode              `xml:"http://www.iata.org/IATA/EDIST CurrCode,omitempty"`
    TicketNumbers  *TicketNumbers         `xml:"http://www.iata.org/IATA/EDIST TicketNumbers,omitempty"`
    Refs           string                 `xml:",attr,omitempty"`
}

type Penalty_1 struct {
    Amount     *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST Amount,omitempty"`
    Percentage float64                `xml:"http://www.iata.org/IATA/EDIST percentage,omitempty"`
    Commission *CommissionType        `xml:"http://www.iata.org/IATA/EDIST Commission,omitempty"`
}

type TicketNumbers struct {
    TicketNumber []*TicketNumber `xml:"http://www.iata.org/IATA/EDIST TicketNumber,omitempty"`
}

type TicketNumber struct {
    PTC   string `xml:",attr,omitempty"`
    Value int    `xml:",chardata"`
}

type TicketDesignatorType struct {
    Application string `xml:",attr,omitempty"`
    Value       string `xml:",chardata"`
}

type BilateralTimeLimits BilateralTimeLimitsType

type DepositTimeLimit DepositTimeLimitType

type InventoryGuaranteeTimeLimits CoreDateGrpType

type NamingTimeLimit NamingTimeLimitType

type PaymentTimeLimit PaymentTimeLimitType

type PriceGuaranteeTimeLimits PriceGuaranteeTimeLimitType

type TicketingTimeLimits TicketingTimeLimitType

type BilateralTimeLimitsType struct {
    BilateralTimeLimit []*BilateralTimeLimit `xml:"http://www.iata.org/IATA/EDIST BilateralTimeLimit,omitempty"`
}

type BilateralTimeLimit struct {
    Name        string `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
    Description string `xml:"http://www.iata.org/IATA/EDIST Description,omitempty"`
    DateTime    string `xml:",attr,omitempty"`
    ShortDate   string `xml:",attr,omitempty"`
    Timestamp   string `xml:",attr,omitempty"`
    Time        string `xml:",attr,omitempty"`
    Refs        string `xml:",attr,omitempty"`
}

type DepositTimeLimitType struct {
    Refs      string `xml:",attr,omitempty"`
    Timestamp string `xml:",attr,omitempty"`
}

type NamingTimeLimitType struct {
    Refs      string `xml:",attr,omitempty"`
    Timestamp string `xml:",attr,omitempty"`
}

type PaymentTimeLimitType struct {
    Refs      string `xml:",attr,omitempty"`
    Timestamp string `xml:",attr,omitempty"`
}

type PriceGuaranteeTimeLimitType struct {
    Refs      string `xml:",attr,omitempty"`
    Timestamp string `xml:",attr,omitempty"`
}

type TicketingTimeLimitType struct {
    Refs      string `xml:",attr,omitempty"`
    Timestamp string `xml:",attr,omitempty"`
}

type InstrCommissionType struct {
    CommissionType
}

type InstrClassUpgradeType struct {
    Classes *Classes `xml:"http://www.iata.org/IATA/EDIST Classes,omitempty"`
}

type Classes struct {
    ClassOfService []*FlightCOS_CoreType `xml:"http://www.iata.org/IATA/EDIST ClassOfService,omitempty"`
}

type InstrRemarkType struct {
    RemarkType
}

type InstrSpecialBookingType struct {
    CodesetType
}

type TravelerCoreType struct {
    KeyObjectBaseType
    PTC                    *PTC   `xml:"http://www.iata.org/IATA/EDIST PTC,omitempty"`
    ResidenceCode          string `xml:"http://www.iata.org/IATA/EDIST ResidenceCode,omitempty"`
    PassengerAssociation   string `xml:"http://www.iata.org/IATA/EDIST PassengerAssociation,omitempty"`
    Age                    *Age   `xml:"http://www.iata.org/IATA/EDIST Age,omitempty"`
    CitizenshipCountryCode string `xml:"http://www.iata.org/IATA/EDIST CitizenshipCountryCode,omitempty"`
}

type PTC struct {
    Quantity int    `xml:",attr,omitempty"`
    Value    string `xml:",chardata"`
}

type Age struct {
    Value     *Value_1 `xml:",chardata"`
    BirthDate string   `xml:"http://www.iata.org/IATA/EDIST BirthDate,omitempty"`
    Refs      string   `xml:",attr,omitempty"`
}

type Value_1 struct {
    UOM   string `xml:",attr,omitempty"`
    Value int    `xml:",chardata"`
}

type TravelerSummaryType struct {
    TravelerCoreType
    Name      *Name_3    `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
    ProfileID *ProfileID `xml:"http://www.iata.org/IATA/EDIST ProfileID,omitempty"`
}

type Name_3 struct {
    Surname              *Surname_2  `xml:"http://www.iata.org/IATA/EDIST Surname,omitempty"`
    Given                []*Given_2  `xml:"http://www.iata.org/IATA/EDIST Given,omitempty"`
    Title                string      `xml:"http://www.iata.org/IATA/EDIST Title,omitempty"`
    SurnameSuffix        string      `xml:"http://www.iata.org/IATA/EDIST SurnameSuffix,omitempty"`
    Middle               []*Middle_2 `xml:"http://www.iata.org/IATA/EDIST Middle,omitempty"`
    Refs                 string      `xml:",attr,omitempty"`
    ObjectMetaReferences string      `xml:",attr,omitempty"`
}

type Surname_2 struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type Given_2 struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type Middle_2 struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type ProfileID struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type TravelerDetailType struct {
    TravelerSummaryType
    Gender    *Gender              `xml:"http://www.iata.org/IATA/EDIST Gender,omitempty"`
    FQTVs     []*TravelerFQTV_Type `xml:"http://www.iata.org/IATA/EDIST FQTVs,omitempty"`
    FOIDs     *TravelerFOID_Type   `xml:"http://www.iata.org/IATA/EDIST FOIDs,omitempty"`
    Contacts  *Contacts            `xml:"http://www.iata.org/IATA/EDIST Contacts,omitempty"`
    Languages *Languages           `xml:"http://www.iata.org/IATA/EDIST Languages,omitempty"`
    Remarks   *RemarkType          `xml:"http://www.iata.org/IATA/EDIST Remarks,omitempty"`
}

type Gender struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type TravelerFOID_Type struct {
    FOID []*FOID `xml:"http://www.iata.org/IATA/EDIST FOID,omitempty"`
    Refs string  `xml:",attr,omitempty"`
}

type FOID struct {
    Type   *CodesetType `xml:"http://www.iata.org/IATA/EDIST Type,omitempty"`
    ID     string       `xml:"http://www.iata.org/IATA/EDIST ID,omitempty"`
    Issuer string       `xml:"http://www.iata.org/IATA/EDIST Issuer,omitempty"`
}

type AnonymousTraveler AnonymousTravelerType

type AnonymousTravelerType struct {
    TravelerCoreType
}

type RecognizedTraveler TravelerDetailType

type DetailPassenger PassengerDetailType

type Passenger struct {
    PassengerDetailType
    AdditionalRoles *AdditionalRoles `xml:"http://www.iata.org/IATA/EDIST AdditionalRoles,omitempty"`
}

type AdditionalRoles struct {
    PrimaryContactInd bool `xml:",attr,omitempty"`
    PaymentContactInd bool `xml:",attr,omitempty"`
}

type PassengerSummaryType struct {
    TravelerSummaryType
    Contacts        *Contacts        `xml:"http://www.iata.org/IATA/EDIST Contacts,omitempty"`
    FQTVs           *FQTVs           `xml:"http://www.iata.org/IATA/EDIST FQTVs,omitempty"`
    Gender          *Gender_1        `xml:"http://www.iata.org/IATA/EDIST Gender,omitempty"`
    Remarks         *RemarkType      `xml:"http://www.iata.org/IATA/EDIST Remarks,omitempty"`
    PassengerIDInfo *PassengerIDInfo `xml:"http://www.iata.org/IATA/EDIST PassengerIDInfo,omitempty"`
    ConsentInd      bool             `xml:",attr,omitempty"`
}

type FQTVs struct {
    FQTV_Alliance            []*FQTV_AllianceType       `xml:"http://www.iata.org/IATA/EDIST FQTV_Alliance,omitempty"`
    FQTV_ProgramCore         []*FQTV_ProgramCoreType    `xml:"http://www.iata.org/IATA/EDIST FQTV_ProgramCore,omitempty"`
    FQTV_ProgramDetail       []*FQTV_ProgramDetailType  `xml:"http://www.iata.org/IATA/EDIST FQTV_ProgramDetail,omitempty"`
    FQTV_ProgramSummary      []*FQTV_ProgramSummaryType `xml:"http://www.iata.org/IATA/EDIST FQTV_ProgramSummary,omitempty"`
    TravelerFQTV_Information []*TravelerFQTV_Type       `xml:"http://www.iata.org/IATA/EDIST TravelerFQTV_Information,omitempty"`
    Refs                     string                     `xml:",attr,omitempty"`
}

type Gender_1 struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type PassengerIDInfo struct {
    FOID              *FOID_1              `xml:"http://www.iata.org/IATA/EDIST FOID,omitempty"`
    PassengerDocument []*PassengerDocument `xml:"http://www.iata.org/IATA/EDIST PassengerDocument,omitempty"`
    AllowDocumentInd  bool                 `xml:",attr,omitempty"`
}

type FOID_1 struct {
    Type string `xml:"http://www.iata.org/IATA/EDIST Type,omitempty"`
    ID   string `xml:"http://www.iata.org/IATA/EDIST ID,omitempty"`
}

type ID struct {
    VendorCode string `xml:",attr,omitempty"`
    Value      string `xml:",chardata"`
}

type PassengerDocument struct {
    Type                  string `xml:"http://www.iata.org/IATA/EDIST Type,omitempty"`
    ID                    string `xml:"http://www.iata.org/IATA/EDIST ID,omitempty"`
    BirthCountry          string `xml:"http://www.iata.org/IATA/EDIST BirthCountry,omitempty"`
    DateOfIssue           string `xml:"http://www.iata.org/IATA/EDIST DateOfIssue,omitempty"`
    DateOfExpiration      string `xml:"http://www.iata.org/IATA/EDIST DateOfExpiration,omitempty"`
    CountryOfIssuance     string `xml:"http://www.iata.org/IATA/EDIST CountryOfIssuance,omitempty"`
    ApplicabilityLocation string `xml:"http://www.iata.org/IATA/EDIST ApplicabilityLocation,omitempty"`
    CountryOfResidence    string `xml:"http://www.iata.org/IATA/EDIST CountryOfResidence,omitempty"`
}

type PassengerDetailType struct {
    PassengerSummaryType
}

type PrimaryContact struct {
    Name    *Name_4      `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
    Contact []*Contact_1 `xml:"http://www.iata.org/IATA/EDIST Contact,omitempty"`
}

type Name_4 struct {
    Surname              *Surname_3  `xml:"http://www.iata.org/IATA/EDIST Surname,omitempty"`
    Given                []*Given_3  `xml:"http://www.iata.org/IATA/EDIST Given,omitempty"`
    Middle               []*Middle_3 `xml:"http://www.iata.org/IATA/EDIST Middle,omitempty"`
    Refs                 string      `xml:",attr,omitempty"`
    ObjectMetaReferences string      `xml:",attr,omitempty"`
}

type Surname_3 struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type Given_3 struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type Middle_3 struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type Contact_1 struct {
    KeyObjectBaseType
    PhoneContact       *PhoneType              `xml:"http://www.iata.org/IATA/EDIST PhoneContact,omitempty"`
    OtherContactMethod *OtherContactMethodType `xml:"http://www.iata.org/IATA/EDIST OtherContactMethod,omitempty"`
    EmailContact       *EmailType              `xml:"http://www.iata.org/IATA/EDIST EmailContact,omitempty"`
    AddressContact     *StructuredAddrType     `xml:"http://www.iata.org/IATA/EDIST AddressContact,omitempty"`
}

type SummaryPassenger PassengerSummaryType

type GroupType struct {
    Name           string    `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
    PassengerCount int       `xml:"http://www.iata.org/IATA/EDIST PassengerCount,omitempty"`
    Contacts       *Contacts `xml:"http://www.iata.org/IATA/EDIST Contacts,omitempty"`
    Refs           string    `xml:",attr,omitempty"`
    ObjectKey      string    `xml:",attr,omitempty"`
}

type PartnerTypeListType string

type PartnerTypeSimpleType string

type AgencyCatListType string

type AgencyCategorySimpleType string

type AirlineID_1 AirlineID_Type

type RetailerID RetailerID_Type

type AirlineCoreRepType struct {
    SupplierCoreRepType
    AirlineID *AirlineID_Type `xml:"http://www.iata.org/IATA/EDIST AirlineID,omitempty"`
    Name      string          `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
}

type MarketingCarrierType struct {
    AirlineCoreRepType
    FlightNumber *FlightNumber `xml:"http://www.iata.org/IATA/EDIST FlightNumber,omitempty"`
}

type OperatingCarrierType struct {
    AirlineCoreRepType
    FlightNumber *FlightNumber `xml:"http://www.iata.org/IATA/EDIST FlightNumber,omitempty"`
}

type MarketingCarrierFlightType struct {
    AirlineID        *AirlineID_Type `xml:"http://www.iata.org/IATA/EDIST AirlineID,omitempty"`
    Name             string          `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
    FlightNumber     *FlightNumber   `xml:"http://www.iata.org/IATA/EDIST FlightNumber,omitempty"`
    ResBookDesigCode string          `xml:"http://www.iata.org/IATA/EDIST ResBookDesigCode,omitempty"`
    Refs             string          `xml:",attr,omitempty"`
}

type MarketingCarrierAirline MarketingCarrierType

type OperatingCarrierAirline OperatingCarrierType

type OperatingCarrierFlightType struct {
    AirlineID        *AirlineID_Type `xml:"http://www.iata.org/IATA/EDIST AirlineID,omitempty"`
    Name             string          `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
    FlightNumber     *FlightNumber   `xml:"http://www.iata.org/IATA/EDIST FlightNumber,omitempty"`
    ResBookDesigCode string          `xml:"http://www.iata.org/IATA/EDIST ResBookDesigCode,omitempty"`
    Refs             string          `xml:",attr,omitempty"`
}

type RetailerType struct {
    SupplierCoreRepType
    RetailerID *UniqueIDContextType `xml:"http://www.iata.org/IATA/EDIST RetailerID,omitempty"`
    Contacts   *Contacts            `xml:"http://www.iata.org/IATA/EDIST Contacts,omitempty"`
}

type RetailPartner RetailerType

type SupplierCoreRepType struct {
    ActorObjectType
    OtherIDs *OtherIDs `xml:"http://www.iata.org/IATA/EDIST OtherIDs,omitempty"`
}

type OtherIDs struct {
    OtherID []*OtherID_1 `xml:"http://www.iata.org/IATA/EDIST OtherID,omitempty"`
}

type OtherID_1 struct {
    Description string `xml:",attr,omitempty"`
    Value       string `xml:",chardata"`
}

type AirlineID_Type struct {
    Refs                 string `xml:",attr,omitempty"`
    ObjectMetaReferences string `xml:",attr,omitempty"`
    Value                string `xml:",chardata"`
}

type RetailerID_Type struct {
    UniqueIDContextType
}

type ValidatingCarrier AirlineDesigSimpleType

type AggregatorID AggregatorID_Type

type Aggregator AggregatorType

type AggregatorType struct {
    IntermediaryCoreRepType
    RoleOfIntermediaryReference *RoleOfIntermediaryReferenceType `xml:"http://www.iata.org/IATA/EDIST RoleOfIntermediaryReference,omitempty"`
    AggregatorID                *UniqueIDContextType             `xml:"http://www.iata.org/IATA/EDIST AggregatorID,omitempty"`
    ConnectMethod               string                           `xml:",attr,omitempty"`
}

type EnabledSystem EnabledSystemType

type EnabledSystemType struct {
    IntermediaryCoreRepType
    SystemID *UniqueIDContextType `xml:"http://www.iata.org/IATA/EDIST SystemID,omitempty"`
}

type IntermediaryCoreRepType struct {
    ActorObjectType
    Name     string       `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
    Category *CodesetType `xml:"http://www.iata.org/IATA/EDIST Category,omitempty"`
    Contact  []*Contact_2 `xml:"http://www.iata.org/IATA/EDIST Contact,omitempty"`
}

type Contact_2 struct {
    AddressContact     *StructuredAddrType     `xml:"http://www.iata.org/IATA/EDIST AddressContact,omitempty"`
    EmailContact       *EmailType              `xml:"http://www.iata.org/IATA/EDIST EmailContact,omitempty"`
    OtherContactMethod *OtherContactMethodType `xml:"http://www.iata.org/IATA/EDIST OtherContactMethod,omitempty"`
    PhoneContact       *PhoneType              `xml:"http://www.iata.org/IATA/EDIST PhoneContact,omitempty"`
}

type RoleOfIntermediaryReferenceType struct {
    Supplier_ref string `xml:",attr,omitempty"`
}

type RoleOfIntermediaryReference RoleOfIntermediaryReferenceType

type SystemID SystemID_Type

type AggregatorID_Type struct {
    UniqueIDContextType
}

type SystemID_Type struct {
    UniqueIDContextType
}

type AgentUser AgentUserType

type AgencyID AgencyID_Type

type AgentUserID AgentUserID_Type

type AgencyCoreRepType struct {
    SellerCoreRepType
    OtherIDs    *OtherIDs_1 `xml:"http://www.iata.org/IATA/EDIST OtherIDs,omitempty"`
    PseudoCity  *PseudoCity `xml:"http://www.iata.org/IATA/EDIST PseudoCity,omitempty"`
    IATA_Number string      `xml:"http://www.iata.org/IATA/EDIST IATA_Number,omitempty"`
}

type OtherIDs_1 struct {
    OtherID []*OtherID_2 `xml:"http://www.iata.org/IATA/EDIST OtherID,omitempty"`
}

type OtherID_2 struct {
    Refs        string `xml:",attr,omitempty"`
    Description string `xml:",attr,omitempty"`
    Value       string `xml:",chardata"`
}

type PseudoCity struct {
    Owner string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type AgentUserType struct {
    AgencyCoreRepType
    AgentUserID *UniqueIDContextType `xml:"http://www.iata.org/IATA/EDIST AgentUserID,omitempty"`
    UserRole    string               `xml:"http://www.iata.org/IATA/EDIST UserRole,omitempty"`
}

type SellerCoreRepType struct {
    ActorObjectType
    Name     string    `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
    Type     string    `xml:"http://www.iata.org/IATA/EDIST Type,omitempty"`
    Contacts *Contacts `xml:"http://www.iata.org/IATA/EDIST Contacts,omitempty"`
}

type TravelAgency struct {
    TravelAgencyType
    AgentUserID *AgentUserID_Type `xml:"http://www.iata.org/IATA/EDIST AgentUserID,omitempty"`
    UserRole    string            `xml:",attr,omitempty"`
}

type TravelAgencyType struct {
    AgencyCoreRepType
    AgencyID *UniqueIDContextType `xml:"http://www.iata.org/IATA/EDIST AgencyID,omitempty"`
}

type AgencyID_Type struct {
    UniqueIDContextType
}

type AgentUserID_Type struct {
    UniqueIDContextType
    Name string `xml:",attr,omitempty"`
}

type PartnerID PartnerCompanyID_Type

type FulfillmentPartner FulfillmentPartnerType

type FulfillmentPartnerType struct {
    PartnerCoreRepType
    Fulfillments *Fulfillments `xml:"http://www.iata.org/IATA/EDIST Fulfillments,omitempty"`
}

type Fulfillments struct {
    Fulfillment []*Fulfillment `xml:"http://www.iata.org/IATA/EDIST Fulfillment,omitempty"`
}

type Fulfillment struct {
    OfferValidDates *OfferValidDates `xml:"http://www.iata.org/IATA/EDIST OfferValidDates,omitempty"`
    Location        *Location        `xml:"http://www.iata.org/IATA/EDIST Location,omitempty"`
    Refs            string           `xml:",attr,omitempty"`
}

type OfferValidDates struct {
    Start *CoreDateGrpType `xml:"http://www.iata.org/IATA/EDIST Start,omitempty"`
    End   *CoreDateGrpType `xml:"http://www.iata.org/IATA/EDIST End,omitempty"`
}

type Location struct {
    AirportCode       *AirportCode        `xml:"http://www.iata.org/IATA/EDIST AirportCode,omitempty"`
    StructuredAddress *StructuredAddrType `xml:"http://www.iata.org/IATA/EDIST StructuredAddress,omitempty"`
}

type PartnerCoreRepType struct {
    ActorObjectType
    PartnerID *PartnerCompanyID_Type `xml:"http://www.iata.org/IATA/EDIST PartnerID,omitempty"`
    Name      string                 `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
    Type      string                 `xml:"http://www.iata.org/IATA/EDIST Type,omitempty"`
}

type PartnerCompanyID_Type struct {
    Refs                 string `xml:",attr,omitempty"`
    ObjectMetaReferences string `xml:",attr,omitempty"`
    Value                string `xml:",chardata"`
}

type Device DeviceType

type DeviceType struct {
    Type        *Type      `xml:"http://www.iata.org/IATA/EDIST Type,omitempty"`
    PhoneNumber *PhoneType `xml:"http://www.iata.org/IATA/EDIST PhoneNumber,omitempty"`
    IP_Address  string     `xml:"http://www.iata.org/IATA/EDIST IP_Address,omitempty"`
    MAC_Address string     `xml:"http://www.iata.org/IATA/EDIST MAC_Address,omitempty"`
    Name        string     `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
}

type Type struct {
    CodesetType
    Position *PositionType `xml:"http://www.iata.org/IATA/EDIST Position,omitempty"`
}

type MessageParamsBaseType struct {
    Languages *Languages_1 `xml:"http://www.iata.org/IATA/EDIST Languages,omitempty"`
    CurrCodes *CurrCodes   `xml:"http://www.iata.org/IATA/EDIST CurrCodes,omitempty"`
    Refs      string       `xml:",attr,omitempty"`
}

type Languages_1 struct {
    LanguageCode []*LanguageCodeType `xml:"http://www.iata.org/IATA/EDIST LanguageCode,omitempty"`
}

type CurrCodes struct {
    CurrCode []*CurrCode `xml:"http://www.iata.org/IATA/EDIST CurrCode,omitempty"`
}

type Document MsgDocumentType

type MsgDocumentType struct {
    Metadata         *MetaBaseType `xml:"http://www.iata.org/IATA/EDIST Metadata,omitempty"`
    Name             string        `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
    ReferenceVersion string        `xml:"http://www.iata.org/IATA/EDIST ReferenceVersion,omitempty"`
    Id               string        `xml:",attr,omitempty"`
    Refs             string        `xml:",attr,omitempty"`
}

type DefaultPolicy PolicyDefaultType

type Policies struct {
    Policy []*Policy `xml:"http://www.iata.org/IATA/EDIST Policy,omitempty"`
}

type Policy struct {
    PolicyType
    DefaultPolicy      *PolicyDefaultType      `xml:"http://www.iata.org/IATA/EDIST DefaultPolicy,omitempty"`
    PolicyAugmentation *PolicyAugmentationType `xml:"http://www.iata.org/IATA/EDIST PolicyAugmentation,omitempty"`
}

type PolicyAugmentation PolicyAugmentationType

type PolicyDefaultType struct {
    Policy []*Policy_1 `xml:"http://www.iata.org/IATA/EDIST Policy,omitempty"`
}

type Policy_1 struct {
    DataPolicyInd bool          `xml:"http://www.iata.org/IATA/EDIST DataPolicyInd,omitempty"`
    PolicyID      string        `xml:"http://www.iata.org/IATA/EDIST PolicyID,omitempty"`
    Type          string        `xml:"http://www.iata.org/IATA/EDIST Type,omitempty"`
    Description   string        `xml:"http://www.iata.org/IATA/EDIST Description,omitempty"`
    PolicyLink    string        `xml:"http://www.iata.org/IATA/EDIST PolicyLink,omitempty"`
    NodePath      *NodePathType `xml:"http://www.iata.org/IATA/EDIST NodePath,omitempty"`
    Refs          string        `xml:",attr,omitempty"`
}

type PolicyAugmentationType struct {
    Policy []*Policy_2 `xml:"http://www.iata.org/IATA/EDIST Policy,omitempty"`
}

type Policy_2 struct {
    Refs string `xml:",attr,omitempty"`
}

type PolicyLink string

type PointOfSale PointOfSaleType

type PointOfSaleType struct {
    Location      *Location_1  `xml:"http://www.iata.org/IATA/EDIST Location,omitempty"`
    RequestTime   *RequestTime `xml:"http://www.iata.org/IATA/EDIST RequestTime,omitempty"`
    TouchPoint    *TouchPoint  `xml:"http://www.iata.org/IATA/EDIST TouchPoint,omitempty"`
    Id            string       `xml:",attr,omitempty"`
    Refs          string       `xml:",attr,omitempty"`
    AgentDutyCode string       `xml:",attr,omitempty"`
}

type Location_1 struct {
    CountryCode *CountryCodeType `xml:"http://www.iata.org/IATA/EDIST CountryCode,omitempty"`
    CityCode    *CityCodeType    `xml:"http://www.iata.org/IATA/EDIST CityCode,omitempty"`
}

type RequestTime struct {
    Zone  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type TouchPoint struct {
    Device *Device_1    `xml:"http://www.iata.org/IATA/EDIST Device,omitempty"`
    Event  *CodesetType `xml:"http://www.iata.org/IATA/EDIST Event,omitempty"`
}

type Device_1 struct {
    CodesetType
    Position *PositionType `xml:"http://www.iata.org/IATA/EDIST Position,omitempty"`
}

type AgentUserMsgPartyCoreType struct {
    AgentUserType
}

type AggregatorMsgPartyCoreType struct {
    AggregatorType
    AgentUserID *AgentUserID_Type `xml:"http://www.iata.org/IATA/EDIST AgentUserID,omitempty"`
}

type AirlineMsgPartyCoreType struct {
    AirlineCoreRepType
    AgentUser   *AgentUserType  `xml:"http://www.iata.org/IATA/EDIST AgentUser,omitempty"`
    Disclosures *DisclosureType `xml:"http://www.iata.org/IATA/EDIST Disclosures,omitempty"`
}

type EnabledSysMsgPartyCoreType struct {
    EnabledSystemType
}

type MsgPartiesType struct {
    Sender       *Sender         `xml:"http://www.iata.org/IATA/EDIST Sender,omitempty"`
    Participants *Participants_1 `xml:"http://www.iata.org/IATA/EDIST Participants,omitempty"`
    Recipient    *Recipient      `xml:"http://www.iata.org/IATA/EDIST Recipient,omitempty"`
}

type Sender struct {
    AgentUserSender        *AgentUserType              `xml:"http://www.iata.org/IATA/EDIST AgentUserSender,omitempty"`
    AggregatorSender       *AggregatorMsgPartyCoreType `xml:"http://www.iata.org/IATA/EDIST AggregatorSender,omitempty"`
    EnabledSystemSender    *EnabledSystemType          `xml:"http://www.iata.org/IATA/EDIST EnabledSystemSender,omitempty"`
    MarketingCarrierSender *AirlineMsgPartyCoreType    `xml:"http://www.iata.org/IATA/EDIST MarketingCarrierSender,omitempty"`
    ORA_Sender             *AirlineMsgPartyCoreType    `xml:"http://www.iata.org/IATA/EDIST ORA_Sender,omitempty"`
    OperatingCarrierSender *AirlineMsgPartyCoreType    `xml:"http://www.iata.org/IATA/EDIST OperatingCarrierSender,omitempty"`
    POA_Sender             *AirlineMsgPartyCoreType    `xml:"http://www.iata.org/IATA/EDIST POA_Sender,omitempty"`
    RetailPartnerSender    *RetailerMsgPartyCoreType   `xml:"http://www.iata.org/IATA/EDIST RetailPartnerSender,omitempty"`
    TravelAgencySender     *TrvlAgencyMsgPartyCoreType `xml:"http://www.iata.org/IATA/EDIST TravelAgencySender,omitempty"`
    CorporateSender        *CorporateSender            `xml:"http://www.iata.org/IATA/EDIST CorporateSender,omitempty"`
}

type Participants_1 struct {
    Participant []*Participant_1 `xml:"http://www.iata.org/IATA/EDIST Participant,omitempty"`
}

type Participant_1 struct {
    AggregatorParticipant       *AggregatorParticipantType       `xml:"http://www.iata.org/IATA/EDIST AggregatorParticipant,omitempty"`
    EnabledSystemParticipant    *EnabledSysParticipantType       `xml:"http://www.iata.org/IATA/EDIST EnabledSystemParticipant,omitempty"`
    MarketingCarrierParticipant *MarketingCarrierParticipantType `xml:"http://www.iata.org/IATA/EDIST MarketingCarrierParticipant,omitempty"`
    ORA_Participant             *ORA_AirlineParticipantType      `xml:"http://www.iata.org/IATA/EDIST ORA_Participant,omitempty"`
    OperatingCarrierParticipant *OperatingCarrierParticipantType `xml:"http://www.iata.org/IATA/EDIST OperatingCarrierParticipant,omitempty"`
    POA_Participant             *POA_AirlineParticipantType      `xml:"http://www.iata.org/IATA/EDIST POA_Participant,omitempty"`
    RetailSupplierParticipant   *RetailSupplierParticipantType   `xml:"http://www.iata.org/IATA/EDIST RetailSupplierParticipant,omitempty"`
    TravelAgencyParticipant     *TravelAgencyParticipantType     `xml:"http://www.iata.org/IATA/EDIST TravelAgencyParticipant,omitempty"`
    CorporateParticipant        *CorporateParticipant            `xml:"http://www.iata.org/IATA/EDIST CorporateParticipant,omitempty"`
}

type Recipient struct {
    AgentUserRecipient        *TrvlAgencyMsgPartyCoreType `xml:"http://www.iata.org/IATA/EDIST AgentUserRecipient,omitempty"`
    AggregatorRecipient       *AggregatorMsgPartyCoreType `xml:"http://www.iata.org/IATA/EDIST AggregatorRecipient,omitempty"`
    EnabledSystemRecipient    *EnabledSystemType          `xml:"http://www.iata.org/IATA/EDIST EnabledSystemRecipient,omitempty"`
    MarketingCarrierRecipient *AirlineMsgPartyCoreType    `xml:"http://www.iata.org/IATA/EDIST MarketingCarrierRecipient,omitempty"`
    ORA_Recipient             *AirlineMsgPartyCoreType    `xml:"http://www.iata.org/IATA/EDIST ORA_Recipient,omitempty"`
    OperatingCarrierRecipient *AirlineMsgPartyCoreType    `xml:"http://www.iata.org/IATA/EDIST OperatingCarrierRecipient,omitempty"`
    POA_Recipient             *AirlineMsgPartyCoreType    `xml:"http://www.iata.org/IATA/EDIST POA_Recipient,omitempty"`
    RetailPartnerRecipient    *RetailerMsgPartyCoreType   `xml:"http://www.iata.org/IATA/EDIST RetailPartnerRecipient,omitempty"`
    TravelAgencyRecipient     *TrvlAgencyMsgPartyCoreType `xml:"http://www.iata.org/IATA/EDIST TravelAgencyRecipient,omitempty"`
}

type Party MsgPartiesType

type RetailerMsgPartyCoreType struct {
    RetailerType
    AgentUser *AgentUserType `xml:"http://www.iata.org/IATA/EDIST AgentUser,omitempty"`
}

type TrvlAgencyMsgPartyCoreType struct {
    TravelAgencyType
    AgentUser *AgentUserType `xml:"http://www.iata.org/IATA/EDIST AgentUser,omitempty"`
}

type ORA_Sender ORA_AirlineSenderType

type POA_Sender POA_AirlineSenderType

type AggregatorSender AggregatorSenderType

type AgentUserSender AgentUserSenderType

type AggregatorSenderType struct {
    AggregatorMsgPartyCoreType
}

type AgentUserSenderType struct {
    AgentUserMsgPartyCoreType
}

type EnabledSystemSender EnabledSysSenderType

type EnabledSysSenderType struct {
    EnabledSysMsgPartyCoreType
}

type MarketingCarrierSenderType struct {
    AirlineMsgPartyCoreType
}

type MarketingCarrierSender MarketingCarrierSenderType

type OperatingCarrierSenderType struct {
    AirlineMsgPartyCoreType
}

type ORA_AirlineSenderType struct {
    AirlineMsgPartyCoreType
}

type OperatingCarrierSender OperatingCarrierSenderType

type POA_AirlineSenderType struct {
    AirlineMsgPartyCoreType
}

type RetailPartnerSender RetailSupplierSenderType

type RetailSupplierSenderType struct {
    RetailerMsgPartyCoreType
}

type TravelAgencySender TravelAgencySenderType

type TravelAgencySenderType struct {
    TrvlAgencyMsgPartyCoreType
}

type CorporateSender struct {
    Name          string `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
    ID            string `xml:"http://www.iata.org/IATA/EDIST ID,omitempty"`
    CorporateCode string `xml:"http://www.iata.org/IATA/EDIST CorporateCode,omitempty"`
    IATA_Number   string `xml:"http://www.iata.org/IATA/EDIST IATA_Number,omitempty"`
}

type CorporateParticipant struct {
    Name          string `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
    ID            string `xml:"http://www.iata.org/IATA/EDIST ID,omitempty"`
    CorporateCode string `xml:"http://www.iata.org/IATA/EDIST CorporateCode,omitempty"`
    IATA_Number   string `xml:"http://www.iata.org/IATA/EDIST IATA_Number,omitempty"`
}

type AggregatorRecipient AggregatorRecipientType

type AgentUserRecipient AgentUserRecipientType

type AgentUserRecipientType struct {
    TrvlAgencyMsgPartyCoreType
}

type AggregatorRecipientType struct {
    AggregatorMsgPartyCoreType
}

type EnabledSystemRecipient EnabledSysRecipientType

type EnabledSysRecipientType struct {
    EnabledSysMsgPartyCoreType
}

type MarketingCarrierRecipient MarketingCarrierRecipientType

type MarketingCarrierRecipientType struct {
    AirlineMsgPartyCoreType
}

type OperatingCarrierRecipient OperatingCarrierRecipientType

type ORA_Recipient ORA_AirlineRecipientType

type OperatingCarrierRecipientType struct {
    AirlineMsgPartyCoreType
}

type ORA_AirlineRecipientType struct {
    AirlineMsgPartyCoreType
}

type POA_AirlineRecipientType struct {
    AirlineMsgPartyCoreType
}

type POA_Recipient POA_AirlineRecipientType

type RetailPartnerRecipient RetailSupplierRecipientType

type RetailSupplierRecipientType struct {
    RetailerMsgPartyCoreType
}

type TravelAgencyRecipient TravelAgencyRecipientType

type TravelAgencyRecipientType struct {
    TrvlAgencyMsgPartyCoreType
}

type AggregatorParticipant AggregatorParticipantType

type AggregatorParticipantType struct {
    AggregatorMsgPartyCoreType
    SequenceNumber int `xml:",attr,omitempty"`
}

type EnabledSysParticipantType struct {
    EnabledSysMsgPartyCoreType
    SequenceNumber int `xml:",attr,omitempty"`
}

type EnabledSystemParticipant EnabledSysParticipantType

type MarketingCarrierParticipant MarketingCarrierParticipantType

type MarketingCarrierParticipantType struct {
    AirlineMsgPartyCoreType
    SequenceNumber int `xml:",attr,omitempty"`
}

type OperatingCarrierParticipant OperatingCarrierParticipantType

type ORA_AirlineParticipantType struct {
    AirlineMsgPartyCoreType
    SequenceNumber int `xml:",attr,omitempty"`
}

type OperatingCarrierParticipantType struct {
    AirlineMsgPartyCoreType
    SequenceNumber int `xml:",attr,omitempty"`
}

type ORA_Participant ORA_AirlineParticipantType

type POA_Participant POA_AirlineParticipantType

type POA_AirlineParticipantType struct {
    AirlineMsgPartyCoreType
    SequenceNumber int `xml:",attr,omitempty"`
}

type RetailSupplierParticipant RetailSupplierParticipantType

type RetailSupplierParticipantType struct {
    RetailerMsgPartyCoreType
    SequenceNumber int `xml:",attr,omitempty"`
}

type TravelAgencyParticipant TravelAgencyParticipantType

type TravelAgencyParticipantType struct {
    TrvlAgencyMsgPartyCoreType
    SequenceNumber string `xml:",attr,omitempty"`
}

type MarketingInfoType struct {
    Message []*Message `xml:"http://www.iata.org/IATA/EDIST Message,omitempty"`
    Refs    string     `xml:",attr,omitempty"`
}

type Message struct {
    DescriptionType
    Associations *MarketMsgAssocType `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
}

type MarketMsgAssocSimpleType string

type MarketMsgBagAssocListType string

type MarketMsgAssocType struct {
    Association []*Association `xml:"http://www.iata.org/IATA/EDIST Association,omitempty"`
    Refs        string         `xml:",attr,omitempty"`
}

type Association struct {
    Type           string `xml:"http://www.iata.org/IATA/EDIST Type,omitempty"`
    ReferenceValue string `xml:"http://www.iata.org/IATA/EDIST ReferenceValue,omitempty"`
}

type Errors ErrorsType

type OtherInfo ProcessingResultType

type ErrorsType struct {
    Error []*ErrorType `xml:"http://www.iata.org/IATA/EDIST Error,omitempty"`
}

type ErrorType struct {
    FreeTextType
    Type      string        `xml:",attr,omitempty"`
    ShortText string        `xml:",attr,omitempty"`
    Code      string        `xml:",attr,omitempty"`
    DocURL    string        `xml:",attr,omitempty"`
    Status    string        `xml:",attr,omitempty"`
    Tag       string        `xml:",attr,omitempty"`
    RecordID  string        `xml:",attr,omitempty"`
    Owner     string        `xml:",attr,omitempty"`
    NodeList  string        `xml:",attr,omitempty"`
}

type IATA_CodeType string

type StringLength1to64 string

type ErrorWarningAttributeGroup struct {
    ShortText string `xml:",attr,omitempty"`
    Code      string `xml:",attr,omitempty"`
    DocURL    string `xml:",attr,omitempty"`
    Status    string `xml:",attr,omitempty"`
    Tag       string `xml:",attr,omitempty"`
    RecordID  string `xml:",attr,omitempty"`
    Owner     string `xml:",attr,omitempty"`
}

type StringLength1to32 string

type FreeTextType struct {
    Language string `xml:",attr,omitempty"`
    Value    string `xml:",chardata"`
}

type LanguageGroup struct {
    Language string `xml:",attr,omitempty"`
}

type ProcessingErrorType struct {
    Error    []*Error `xml:"http://www.iata.org/IATA/EDIST Error,omitempty"`
    RetryInd bool     `xml:",attr,omitempty"`
}

type Error struct {
    Type        string        `xml:"http://www.iata.org/IATA/EDIST Type,omitempty"`
    Code        string        `xml:"http://www.iata.org/IATA/EDIST Code,omitempty"`
    Description string        `xml:"http://www.iata.org/IATA/EDIST Description,omitempty"`
    InfoURL     string        `xml:"http://www.iata.org/IATA/EDIST InfoURL,omitempty"`
    NodePath    *NodePathType `xml:"http://www.iata.org/IATA/EDIST NodePath,omitempty"`
    Refs        string        `xml:",attr,omitempty"`
}

type ProcessingResultType struct {
    Marketing *MarketingInfoType `xml:"http://www.iata.org/IATA/EDIST Marketing,omitempty"`
    Refs      string             `xml:",attr,omitempty"`
}

type TrxProcessStatus TrxProcessingSimpleType

type TrxPayloadType CodesetType

type Alert AlertType

type Alerts AlertsType

type AlertType struct {
    InventoryDiscrepancyAlert *InvDiscrepencyAlertType `xml:"http://www.iata.org/IATA/EDIST InventoryDiscrepancyAlert,omitempty"`
    PIN_AuthenticationAlert   *PIN_AuthTravelerType    `xml:"http://www.iata.org/IATA/EDIST PIN_AuthenticationAlert,omitempty"`
    SecurePaymentAlert        *SecurePaymentAlertType  `xml:"http://www.iata.org/IATA/EDIST SecurePaymentAlert,omitempty"`
}

type AlertsType struct {
    Alert []*AlertType `xml:"http://www.iata.org/IATA/EDIST Alert,omitempty"`
}

type Failed struct {
    Code         *CodesetType              `xml:"http://www.iata.org/IATA/EDIST Code,omitempty"`
    Associations *OrderItemAssociationType `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
    Remarks      *RemarkType               `xml:"http://www.iata.org/IATA/EDIST Remarks,omitempty"`
    FailedInd    bool                      `xml:",attr,omitempty"`
}

type InventoryDiscrepancyAlert InvDiscrepencyAlertType

type InvDiscrepencyAlertType struct {
    Code            *CodesetType          `xml:"http://www.iata.org/IATA/EDIST Code,omitempty"`
    Associations    *MultiAssociationType `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
    AlternateOffers *AlternateOffers      `xml:"http://www.iata.org/IATA/EDIST AlternateOffers,omitempty"`
    Remarks         *RemarkType           `xml:"http://www.iata.org/IATA/EDIST Remarks,omitempty"`
    NoInventoryInd  bool                  `xml:",attr,omitempty"`
    Refs            string                `xml:",attr,omitempty"`
}

type AlternateOffers struct {
    TotalOfferQuantity int               `xml:"http://www.iata.org/IATA/EDIST TotalOfferQuantity,omitempty"`
    Owner              *AirlineID_Type   `xml:"http://www.iata.org/IATA/EDIST Owner,omitempty"`
    AlternateOffer     []*AlternateOffer `xml:"http://www.iata.org/IATA/EDIST AlternateOffer,omitempty"`
}

type AlternateOffer struct {
    OfferType
    AltBaggageOffer      *AltBaggageOfferType      `xml:"http://www.iata.org/IATA/EDIST AltBaggageOffer,omitempty"`
    AltOtherOffer        *AltOtherOfferType        `xml:"http://www.iata.org/IATA/EDIST AltOtherOffer,omitempty"`
    AltPricedFlightOffer *AltPricedFlightOfferType `xml:"http://www.iata.org/IATA/EDIST AltPricedFlightOffer,omitempty"`
    AltSeatOffer         *AltSeatOfferType         `xml:"http://www.iata.org/IATA/EDIST AltSeatOffer,omitempty"`
}

type MultiAssociationType struct {
    OfferAssociations *OfferAssociationsType    `xml:"http://www.iata.org/IATA/EDIST OfferAssociations,omitempty"`
    OrderAssociations *OrderItemAssociationType `xml:"http://www.iata.org/IATA/EDIST OrderAssociations,omitempty"`
}

type PIN_AuthenticationAlert PIN_AuthTravelerType

type PIN_AuthTravelerType struct {
    TrxProcessObjectBaseType
    AuthRequest  *AuthRequest         `xml:"http://www.iata.org/IATA/EDIST AuthRequest,omitempty"`
    AuthResponse *PinPhraseAnswerType `xml:"http://www.iata.org/IATA/EDIST AuthResponse,omitempty"`
}

type AuthRequest struct {
    PinPhraseQuestionType
    AuthenticationInd bool `xml:",attr,omitempty"`
    MaximumTrxInd     bool `xml:",attr,omitempty"`
    RetryInd          bool `xml:",attr,omitempty"`
}

type PinPhraseAnswerType struct {
    Status                *Status                `xml:"http://www.iata.org/IATA/EDIST Status,omitempty"`
    PIN                   string                 `xml:"http://www.iata.org/IATA/EDIST PIN,omitempty"`
    PoolQuestion          string                 `xml:"http://www.iata.org/IATA/EDIST PoolQuestion,omitempty"`
    AuthAccount           *AuthAccountType       `xml:"http://www.iata.org/IATA/EDIST AuthAccount,omitempty"`
    Device                *Device_2              `xml:"http://www.iata.org/IATA/EDIST Device,omitempty"`
    OriginalTransactionID *OriginalTransactionID `xml:"http://www.iata.org/IATA/EDIST OriginalTransactionID,omitempty"`
}

type Status struct {
    Context string `xml:",attr,omitempty"`
    Value   string `xml:",chardata"`
}

type Device_2 struct {
    DeviceType
    Position *PositionType `xml:"http://www.iata.org/IATA/EDIST Position,omitempty"`
}

type OriginalTransactionID struct {
    Source string `xml:",attr,omitempty"`
    Value  string `xml:",chardata"`
}

type PaymentIssueAlertType struct {
    TrxProcessObjectBaseType
    Payment []*Payment `xml:"http://www.iata.org/IATA/EDIST Payment,omitempty"`
}

type Payment struct {
    TrxProcessStatus string       `xml:"http://www.iata.org/IATA/EDIST TrxProcessStatus,omitempty"`
    InfoURL          string       `xml:"http://www.iata.org/IATA/EDIST InfoURL,omitempty"`
    DataType         *CodesetType `xml:"http://www.iata.org/IATA/EDIST DataType,omitempty"`
    Status           *Status_1    `xml:"http://www.iata.org/IATA/EDIST Status,omitempty"`
    Failed           *Failed_1    `xml:"http://www.iata.org/IATA/EDIST Failed,omitempty"`
    Incomplete       *Incomplete  `xml:"http://www.iata.org/IATA/EDIST Incomplete,omitempty"`
    Refs             string       `xml:",attr,omitempty"`
    ListKey          string       `xml:",attr,omitempty"`
}

type Status_1 struct {
    Context string `xml:",attr,omitempty"`
    Value   string `xml:",chardata"`
}

type Failed_1 struct {
    Code         *CodesetType              `xml:"http://www.iata.org/IATA/EDIST Code,omitempty"`
    Associations *OrderItemAssociationType `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
    Remarks      *RemarkType               `xml:"http://www.iata.org/IATA/EDIST Remarks,omitempty"`
    FailedInd    bool                      `xml:",attr,omitempty"`
}

type Incomplete struct {
    Code             *CodesetType              `xml:"http://www.iata.org/IATA/EDIST Code,omitempty"`
    Associations     *OrderItemAssociationType `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
    Remarks          *RemarkType               `xml:"http://www.iata.org/IATA/EDIST Remarks,omitempty"`
    PriceVarianceInd bool                      `xml:",attr,omitempty"`
}

type PinPhraseQuestionType struct {
    Status                *Status_2                `xml:"http://www.iata.org/IATA/EDIST Status,omitempty"`
    Challenge             *Challenge               `xml:"http://www.iata.org/IATA/EDIST Challenge,omitempty"`
    Associations          *MultiAssociationType    `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
    AuthAccount           *AuthAccountType         `xml:"http://www.iata.org/IATA/EDIST AuthAccount,omitempty"`
    Device                *Device_3                `xml:"http://www.iata.org/IATA/EDIST Device,omitempty"`
    OriginalTransactionID *OriginalTransactionID_1 `xml:"http://www.iata.org/IATA/EDIST OriginalTransactionID,omitempty"`
}

type Status_2 struct {
    Context string `xml:",attr,omitempty"`
    Value   string `xml:",chardata"`
}

type Challenge struct {
    SourceURL  string      `xml:"http://www.iata.org/IATA/EDIST SourceURL,omitempty"`
    Parameters *Parameters `xml:"http://www.iata.org/IATA/EDIST Parameters,omitempty"`
}

type Parameters struct {
    ChallengeQuestion string     `xml:"http://www.iata.org/IATA/EDIST ChallengeQuestion,omitempty"`
    PhrasePrompt      string     `xml:"http://www.iata.org/IATA/EDIST PhrasePrompt,omitempty"`
    Positions         *Positions `xml:"http://www.iata.org/IATA/EDIST Positions,omitempty"`
}

type Positions struct {
    Position []int `xml:"http://www.iata.org/IATA/EDIST Position,omitempty"`
}

type Device_3 struct {
    DeviceType
    Position *PositionType `xml:"http://www.iata.org/IATA/EDIST Position,omitempty"`
}

type OriginalTransactionID_1 struct {
    Source string `xml:",attr,omitempty"`
    Value  string `xml:",chardata"`
}

type SecurePaymentAlert SecurePaymentAlertType

type SecurePaymentAlertType struct {
    TrxProcessObjectBaseType
    PayerAuth        *PayerAuth            `xml:"http://www.iata.org/IATA/EDIST PayerAuth,omitempty"`
    TransactionType  *TransactionType      `xml:"http://www.iata.org/IATA/EDIST TransactionType,omitempty"`
    EnrollmentStatus *EnrollmentStatus     `xml:"http://www.iata.org/IATA/EDIST EnrollmentStatus,omitempty"`
    Airline          *Airline              `xml:"http://www.iata.org/IATA/EDIST Airline,omitempty"`
    Reference        *Reference            `xml:"http://www.iata.org/IATA/EDIST Reference,omitempty"`
    URLs             *SecurePaymentUrlType `xml:"http://www.iata.org/IATA/EDIST URLs,omitempty"`
    Details          *Details_3            `xml:"http://www.iata.org/IATA/EDIST Details,omitempty"`
    ProcessingInfos  *ProcessingInfos      `xml:"http://www.iata.org/IATA/EDIST ProcessingInfos,omitempty"`
}

type PayerAuth struct {
    PAReq string `xml:"http://www.iata.org/IATA/EDIST PAReq,omitempty"`
    PARes string `xml:"http://www.iata.org/IATA/EDIST PARes,omitempty"`
}

type TransactionType struct {
    Context string `xml:",attr,omitempty"`
    Value   string `xml:",chardata"`
}

type EnrollmentStatus struct {
    Context string `xml:",attr,omitempty"`
    Value   string `xml:",chardata"`
}

type Airline struct {
    ID          *ID_1   `xml:"http://www.iata.org/IATA/EDIST ID,omitempty"`
    Name        *Name_5 `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
    CountryCode string  `xml:"http://www.iata.org/IATA/EDIST CountryCode,omitempty"`
}

type ID_1 struct {
    Context string `xml:",attr,omitempty"`
    Value   string `xml:",chardata"`
}

type Name_5 struct {
    Context string `xml:",attr,omitempty"`
    Value   string `xml:",chardata"`
}

type Reference struct {
    ACS_TxnReference      *ACS_TxnReference        `xml:"http://www.iata.org/IATA/EDIST ACS_TxnReference,omitempty"`
    SPM_TxnReference      *SPM_TxnReference        `xml:"http://www.iata.org/IATA/EDIST SPM_TxnReference,omitempty"`
    OriginalTransactionID *OriginalTransactionID_2 `xml:"http://www.iata.org/IATA/EDIST OriginalTransactionID,omitempty"`
    TrxTimestamp          string                   `xml:"http://www.iata.org/IATA/EDIST TrxTimestamp,omitempty"`
    TxnDescription        string                   `xml:"http://www.iata.org/IATA/EDIST TxnDescription,omitempty"`
    TxnDatas              *TxnDatas                `xml:"http://www.iata.org/IATA/EDIST TxnDatas,omitempty"`
}

type OriginalTransactionID_2 struct {
    Source string `xml:",attr,omitempty"`
    Value  string `xml:",chardata"`
}

type TxnDatas struct {
    TxnDate []string `xml:"http://www.iata.org/IATA/EDIST TxnDate,omitempty"`
}

type Details_3 struct {
    TrxTimestamp    string                    `xml:"http://www.iata.org/IATA/EDIST TrxTimestamp,omitempty"`
    ClientType      *ClientType               `xml:"http://www.iata.org/IATA/EDIST ClientType,omitempty"`
    CustomerDevice  *CustomerDevice           `xml:"http://www.iata.org/IATA/EDIST CustomerDevice,omitempty"`
    Currency        *Currency_1               `xml:"http://www.iata.org/IATA/EDIST Currency,omitempty"`
    ReservationInfo *SecurePaymentPaxInfoType `xml:"http://www.iata.org/IATA/EDIST ReservationInfo,omitempty"`
    TxnDatas        *TxnDatas_1               `xml:"http://www.iata.org/IATA/EDIST TxnDatas,omitempty"`
}

type ClientType struct {
    Context string `xml:",attr,omitempty"`
    Value   string `xml:",chardata"`
}

type CustomerDevice struct {
    DeviceType
    BowserAcceptHeader     string `xml:"http://www.iata.org/IATA/EDIST BowserAcceptHeader,omitempty"`
    BrowserUserAgentHeader string `xml:"http://www.iata.org/IATA/EDIST BrowserUserAgentHeader,omitempty"`
    DeviceCategoryCode     string `xml:"http://www.iata.org/IATA/EDIST DeviceCategoryCode,omitempty"`
}

type Currency_1 struct {
    InputCurrCode      string `xml:"http://www.iata.org/IATA/EDIST InputCurrCode,omitempty"`
    SettlementCurrCode string `xml:"http://www.iata.org/IATA/EDIST SettlementCurrCode,omitempty"`
}

type TxnDatas_1 struct {
    TxnDate []string `xml:"http://www.iata.org/IATA/EDIST TxnDate,omitempty"`
}

type ProcessingInfos struct {
    ProcessingInfo []*ProcessingInfo `xml:"http://www.iata.org/IATA/EDIST ProcessingInfo,omitempty"`
}

type ProcessingInfo struct {
    AddrVerification   *AddrVerification   `xml:"http://www.iata.org/IATA/EDIST AddrVerification,omitempty"`
    CAVV               string              `xml:"http://www.iata.org/IATA/EDIST CAVV,omitempty"`
    CustomerAuthStatus *CustomerAuthStatus `xml:"http://www.iata.org/IATA/EDIST CustomerAuthStatus,omitempty"`
    ECI                string              `xml:"http://www.iata.org/IATA/EDIST ECI,omitempty"`
}

type AddrVerification struct {
    Code       string `xml:",attr,omitempty"`
    Text       string `xml:",attr,omitempty"`
    InvalidInd bool   `xml:",attr,omitempty"`
    NoMatchInd bool   `xml:",attr,omitempty"`
}

type CustomerAuthStatus struct {
    Context string `xml:",attr,omitempty"`
    Value   string `xml:",chardata"`
}

type ACS_TxnReference struct {
    Context string `xml:",attr,omitempty"`
    Value   string `xml:",chardata"`
}

type MerchantName string

type ReservationInfo SecurePaymentPaxInfoType

type SPM_TxnReference struct {
    Context string `xml:",attr,omitempty"`
    Value   string `xml:",chardata"`
}

type SecurePaymentPaxInfoType struct {
    Carriers         *Carriers         `xml:"http://www.iata.org/IATA/EDIST Carriers,omitempty"`
    DateTimes        *DateTimes        `xml:"http://www.iata.org/IATA/EDIST DateTimes,omitempty"`
    ClassesOfService *ClassesOfService `xml:"http://www.iata.org/IATA/EDIST ClassesOfService,omitempty"`
    StopLocations    *StopLocations    `xml:"http://www.iata.org/IATA/EDIST StopLocations,omitempty"`
    FareBasisCodes   *FareBasisCodes   `xml:"http://www.iata.org/IATA/EDIST FareBasisCodes,omitempty"`
    FilghtNumbers    *FilghtNumbers    `xml:"http://www.iata.org/IATA/EDIST FilghtNumbers,omitempty"`
    PassengerName    string            `xml:"http://www.iata.org/IATA/EDIST PassengerName,omitempty"`
    ResidenceCode    string            `xml:"http://www.iata.org/IATA/EDIST ResidenceCode,omitempty"`
    PassengerTktNbr  string            `xml:"http://www.iata.org/IATA/EDIST PassengerTktNbr,omitempty"`
    AgencyInfo       string            `xml:"http://www.iata.org/IATA/EDIST AgencyInfo,omitempty"`
    Refs             string            `xml:",attr,omitempty"`
}

type Carriers struct {
    Carrier []*Carrier `xml:"http://www.iata.org/IATA/EDIST Carrier,omitempty"`
}

type Carrier struct {
    AirlineID_Type
    Application string `xml:",attr,omitempty"`
}

type DateTimes struct {
    DateTime []*DateTime_1 `xml:"http://www.iata.org/IATA/EDIST DateTime,omitempty"`
}

type DateTime_1 struct {
    Date        string `xml:",attr,omitempty"`
    Time        string `xml:",attr,omitempty"`
    Application string `xml:",attr,omitempty"`
}

type ClassesOfService struct {
    ClassOfService []*FlightCOS_CoreType `xml:"http://www.iata.org/IATA/EDIST ClassOfService,omitempty"`
}

type StopLocations struct {
    StopLocation []*StopLocation `xml:"http://www.iata.org/IATA/EDIST StopLocation,omitempty"`
}

type StopLocation struct {
    AirportCode *AirportCode `xml:"http://www.iata.org/IATA/EDIST AirportCode,omitempty"`
}

type FareBasisCodes struct {
    FareBasisCode []*FareBasisCodeType `xml:"http://www.iata.org/IATA/EDIST FareBasisCode,omitempty"`
}

type FilghtNumbers struct {
    FlightNumber []*FlightNumber `xml:"http://www.iata.org/IATA/EDIST FlightNumber,omitempty"`
}

type SecurePaymentUrlType struct {
    ACS_URL     string `xml:"http://www.iata.org/IATA/EDIST ACS_URL,omitempty"`
    FailURL     string `xml:"http://www.iata.org/IATA/EDIST FailURL,omitempty"`
    MerchantURL string `xml:"http://www.iata.org/IATA/EDIST MerchantURL,omitempty"`
    TermURL     string `xml:"http://www.iata.org/IATA/EDIST TermURL,omitempty"`
}

type AuthAccountType struct {
    AccountID   string `xml:"http://www.iata.org/IATA/EDIST AccountID,omitempty"`
    AccountName string `xml:"http://www.iata.org/IATA/EDIST AccountName,omitempty"`
    FirstName   string `xml:"http://www.iata.org/IATA/EDIST FirstName,omitempty"`
    LastName    string `xml:"http://www.iata.org/IATA/EDIST LastName,omitempty"`
}

type MarketingMessages struct {
    MarketMessage []*MarketMessage `xml:"http://www.iata.org/IATA/EDIST MarketMessage,omitempty"`
}

type MarketMessage struct {
    DescriptionType
    Associations *MultiAssociationType `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
}

type Warnings struct {
    Warning []*Warning `xml:"http://www.iata.org/IATA/EDIST Warning,omitempty"`
}

type Warning struct {
    Type         *CodesetType          `xml:"http://www.iata.org/IATA/EDIST Type,omitempty"`
    Remarks      *RemarkType           `xml:"http://www.iata.org/IATA/EDIST Remarks,omitempty"`
    Associations *MultiAssociationType `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
    Refs         string                `xml:",attr,omitempty"`
}

type Notices struct {
    Notice []*Notice `xml:"http://www.iata.org/IATA/EDIST Notice,omitempty"`
}

type Notice struct {
    TaxExemptionNotice      *TaxExemptionNoticeType      `xml:"http://www.iata.org/IATA/EDIST TaxExemptionNotice,omitempty"`
    ServiceFilterNotice     *ServiceFilterNoticeType     `xml:"http://www.iata.org/IATA/EDIST ServiceFilterNotice,omitempty"`
    PricingParametersNotice *PricingParametersNoticeType `xml:"http://www.iata.org/IATA/EDIST PricingParametersNotice,omitempty"`
    PriceVarianceNotice     *PriceVarianceRuleNoticeType `xml:"http://www.iata.org/IATA/EDIST PriceVarianceNotice,omitempty"`
    PaymentRulesNotice      *PaymentRulesNoticeType      `xml:"http://www.iata.org/IATA/EDIST PaymentRulesNotice,omitempty"`
    InventoryGuaranteeNotif *InvGuaranteeNoticeType      `xml:"http://www.iata.org/IATA/EDIST InventoryGuaranteeNotif,omitempty"`
}

type NoticesGrp string

type InventoryGuaranteeNotif InvGuaranteeNoticeType

type PaymentRulesNotice PaymentRulesNoticeType

type PriceVarianceNotice PriceVarianceRuleNoticeType

type PricingParametersNotice PricingParametersNoticeType

type ServiceFilterNotice ServiceFilterNoticeType

type TaxExemptionNotice TaxExemptionNoticeType

type InvGuaranteeNoticeType struct {
    KeyWithMetaObjectBaseType
    Query   *Query   `xml:"http://www.iata.org/IATA/EDIST Query,omitempty"`
    Results *Results `xml:"http://www.iata.org/IATA/EDIST Results,omitempty"`
}

type Query struct {
    GuaranteeRequestInd bool `xml:",attr,omitempty"`
}

type Results struct {
    InventoryGuarantee *InventoryGuarantee_1 `xml:"http://www.iata.org/IATA/EDIST InventoryGuarantee,omitempty"`
    NoGuaranteeInd     bool                  `xml:",attr,omitempty"`
}

type InventoryGuarantee_1 struct {
    InvGuaranteeID               string                `xml:"http://www.iata.org/IATA/EDIST InvGuaranteeID,omitempty"`
    InventoryGuaranteeTimeLimits *CoreDateGrpType      `xml:"http://www.iata.org/IATA/EDIST InventoryGuaranteeTimeLimits,omitempty"`
    Associations                 *MultiAssociationType `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
}

type InfoPolicyNoticeType struct {
    Policy           []*PolicyType `xml:"http://www.iata.org/IATA/EDIST Policy,omitempty"`
    PolicyDefinedInd bool          `xml:",attr,omitempty"`
}

type LocalizationNoticeType struct {
    TrxProcessObjectBaseType
    Languages  *Languages  `xml:"http://www.iata.org/IATA/EDIST Languages,omitempty"`
    Currencies *Currencies `xml:"http://www.iata.org/IATA/EDIST Currencies,omitempty"`
}

type Currencies struct {
    CurrCode []*CurrCode `xml:"http://www.iata.org/IATA/EDIST CurrCode,omitempty"`
}

type NoticeBaseType struct {
    Localization *LocalizationNoticeType `xml:"http://www.iata.org/IATA/EDIST Localization,omitempty"`
    InfoPolicies *InfoPolicyNoticeType   `xml:"http://www.iata.org/IATA/EDIST InfoPolicies,omitempty"`
}

type PaymentRulesNoticeType struct {
    TrxProcessObjectBaseType
    FormOfPayment *FormOfPayment `xml:"http://www.iata.org/IATA/EDIST FormOfPayment,omitempty"`
}

type FormOfPayment struct {
    Query   *Query_1   `xml:"http://www.iata.org/IATA/EDIST Query,omitempty"`
    Results *Results_1 `xml:"http://www.iata.org/IATA/EDIST Results,omitempty"`
}

type Query_1 struct {
    ProceedOnFailureInd bool `xml:",attr,omitempty"`
}

type Results_1 struct {
    Code              *CodesetType              `xml:"http://www.iata.org/IATA/EDIST Code,omitempty"`
    Associations      *OrderItemAssociationType `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
    PaymentFailureInd bool                      `xml:",attr,omitempty"`
}

type PriceVarianceRuleNoticeType struct {
    TrxProcessObjectBaseType
    Query   *Query_2   `xml:"http://www.iata.org/IATA/EDIST Query,omitempty"`
    Results *Results_2 `xml:"http://www.iata.org/IATA/EDIST Results,omitempty"`
}

type Query_2 struct {
    PriceVarianceRule []*PriceVarianceRule `xml:"http://www.iata.org/IATA/EDIST PriceVarianceRule,omitempty"`
    VarianceRuleInd   bool                 `xml:",attr,omitempty"`
}

type PriceVarianceRule struct {
    SequenceNbr        int                       `xml:"http://www.iata.org/IATA/EDIST SequenceNbr,omitempty"`
    RuleID             *RuleID                   `xml:"http://www.iata.org/IATA/EDIST RuleID,omitempty"`
    AcceptableVariance *AcceptableVariance_1     `xml:"http://www.iata.org/IATA/EDIST AcceptableVariance,omitempty"`
    Name               string                    `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
    Owner              string                    `xml:"http://www.iata.org/IATA/EDIST Owner,omitempty"`
    RuleValid          *RuleValid_1              `xml:"http://www.iata.org/IATA/EDIST RuleValid,omitempty"`
    Currencies         *Currencies_1             `xml:"http://www.iata.org/IATA/EDIST Currencies,omitempty"`
    Associations       *OrderItemAssociationType `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
    Remarks            *RemarkType               `xml:"http://www.iata.org/IATA/EDIST Remarks,omitempty"`
}

type RuleID struct {
    Context string `xml:",attr,omitempty"`
    Name    string `xml:",attr,omitempty"`
    Value   string `xml:",chardata"`
}

type AcceptableVariance_1 struct {
    CurrencyAmountValue *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST CurrencyAmountValue,omitempty"`
    PercentageValue     float64                `xml:"http://www.iata.org/IATA/EDIST PercentageValue,omitempty"`
}

type RuleValid_1 struct {
    DatePeriod      *DatePeriodRepType      `xml:"http://www.iata.org/IATA/EDIST DatePeriod,omitempty"`
    DateTimePeriod  *DateTimePeriodRepType  `xml:"http://www.iata.org/IATA/EDIST DateTimePeriod,omitempty"`
    DayPeriod       *DayPeriodRepType       `xml:"http://www.iata.org/IATA/EDIST DayPeriod,omitempty"`
    MonthPeriod     *MonthPeriodRepType     `xml:"http://www.iata.org/IATA/EDIST MonthPeriod,omitempty"`
    QuarterPeriod   *QuarterPeriodRepType   `xml:"http://www.iata.org/IATA/EDIST QuarterPeriod,omitempty"`
    TimePeriod      *TimePeriodRepType      `xml:"http://www.iata.org/IATA/EDIST TimePeriod,omitempty"`
    YearMonthPeriod *YearMonthPeriodRepType `xml:"http://www.iata.org/IATA/EDIST YearMonthPeriod,omitempty"`
    YearPeriod      *YearPeriodRepType      `xml:"http://www.iata.org/IATA/EDIST YearPeriod,omitempty"`
}

type Currencies_1 struct {
    InputCode      []*CurrencyCodeType `xml:"http://www.iata.org/IATA/EDIST InputCode,omitempty"`
    SettlementCode []*CurrencyCodeType `xml:"http://www.iata.org/IATA/EDIST SettlementCode,omitempty"`
}

type Results_2 struct {
    PriceVariance      []*PriceVariance `xml:"http://www.iata.org/IATA/EDIST PriceVariance,omitempty"`
    VarianceAppliedInd bool             `xml:",attr,omitempty"`
}

type PriceVariance struct {
    RuleID       *RuleID_1                 `xml:"http://www.iata.org/IATA/EDIST RuleID,omitempty"`
    Amount       *Amount_1                 `xml:"http://www.iata.org/IATA/EDIST Amount,omitempty"`
    Associations *OrderItemAssociationType `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
}

type RuleID_1 struct {
    Context string `xml:",attr,omitempty"`
    Name    string `xml:",attr,omitempty"`
    Value   string `xml:",chardata"`
}

type Amount_1 struct {
    CurrencyAmountValue *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST CurrencyAmountValue,omitempty"`
    PercentageValue     float64                `xml:"http://www.iata.org/IATA/EDIST PercentageValue,omitempty"`
}

type PricingParametersNoticeType struct {
    TrxProcessObjectBaseType
    Query   *Query_3   `xml:"http://www.iata.org/IATA/EDIST Query,omitempty"`
    Results *Results_3 `xml:"http://www.iata.org/IATA/EDIST Results,omitempty"`
}

type Query_3 struct {
    AutoExchangeReqInd bool `xml:",attr,omitempty"`
    IncludeAwardReqInd bool `xml:",attr,omitempty"`
    AwardOnlyReqInd    bool `xml:",attr,omitempty"`
    SimpleReqInd       bool `xml:",attr,omitempty"`
}

type Results_3 struct {
    AutoExchangeInd  bool `xml:",attr,omitempty"`
    AwardIncludedInd bool `xml:",attr,omitempty"`
    AwardOnlyInd     bool `xml:",attr,omitempty"`
    SimpleInd        bool `xml:",attr,omitempty"`
}

type ServiceFilterNoticeType struct {
    TrxProcessObjectBaseType
    Query   *Query_4   `xml:"http://www.iata.org/IATA/EDIST Query,omitempty"`
    Results *Results_4 `xml:"http://www.iata.org/IATA/EDIST Results,omitempty"`
}

type Query_4 struct {
    ServiceFilter []*ServiceFilterType `xml:"http://www.iata.org/IATA/EDIST ServiceFilter,omitempty"`
}

type Results_4 struct {
    AppliedFilters       *AppliedFilters `xml:"http://www.iata.org/IATA/EDIST AppliedFilters,omitempty"`
    SrvcFilterAppliedInd bool            `xml:",attr,omitempty"`
}

type AppliedFilters struct {
    AppliedFilter []*AppliedFilter `xml:"http://www.iata.org/IATA/EDIST AppliedFilter,omitempty"`
}

type AppliedFilter struct {
    ServiceFilterType
    Associations *Associations `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
}

type Associations struct {
    Offer *Offer_1                  `xml:"http://www.iata.org/IATA/EDIST Offer,omitempty"`
    Order *OrderItemAssociationType `xml:"http://www.iata.org/IATA/EDIST Order,omitempty"`
}

type Offer_1 struct {
    Shopper          *ShopperInfoAssocType     `xml:"http://www.iata.org/IATA/EDIST Shopper,omitempty"`
    Flight           *FlightInfoAssocType      `xml:"http://www.iata.org/IATA/EDIST Flight,omitempty"`
    PriceClass       *PriceClass               `xml:"http://www.iata.org/IATA/EDIST PriceClass,omitempty"`
    BagDetails       *BagDetailAssocType       `xml:"http://www.iata.org/IATA/EDIST BagDetails,omitempty"`
    OfferDetails     *OfferDetailInfoAssocType `xml:"http://www.iata.org/IATA/EDIST OfferDetails,omitempty"`
    OtherAssociation []*OtherAssociation       `xml:"http://www.iata.org/IATA/EDIST OtherAssociation,omitempty"`
}

type PriceClass struct {
    PriceClassReference string `xml:"http://www.iata.org/IATA/EDIST PriceClassReference,omitempty"`
}

type OtherAssociation struct {
    Type     string `xml:",attr,omitempty"`
    RefValue string `xml:",attr,omitempty"`
}

type TaxExemptionNoticeType struct {
    TrxProcessObjectBaseType
    Query   *Query_5   `xml:"http://www.iata.org/IATA/EDIST Query,omitempty"`
    Results *Results_5 `xml:"http://www.iata.org/IATA/EDIST Results,omitempty"`
}

type Query_5 struct {
    Named        *Named       `xml:"http://www.iata.org/IATA/EDIST Named,omitempty"`
    Rules        *RuleSetType `xml:"http://www.iata.org/IATA/EDIST Rules,omitempty"`
    ExemptAllInd bool         `xml:",attr,omitempty"`
}

type Named struct {
    Countries   *Countries_1   `xml:"http://www.iata.org/IATA/EDIST Countries,omitempty"`
    Entities    *Entities_1    `xml:"http://www.iata.org/IATA/EDIST Entities,omitempty"`
    Territories *Territories_1 `xml:"http://www.iata.org/IATA/EDIST Territories,omitempty"`
    TaxCodes    *TaxCodes      `xml:"http://www.iata.org/IATA/EDIST TaxCodes,omitempty"`
}

type Countries_1 struct {
    CountryCode []*CountryCodeType `xml:"http://www.iata.org/IATA/EDIST CountryCode,omitempty"`
}

type Entities_1 struct {
    Entity []string `xml:"http://www.iata.org/IATA/EDIST Entity,omitempty"`
}

type Territories_1 struct {
    Territory []string `xml:"http://www.iata.org/IATA/EDIST Territory,omitempty"`
}

type TaxCodes struct {
    TaxCode []*TaxCode `xml:"http://www.iata.org/IATA/EDIST TaxCode,omitempty"`
}

type TaxCode struct {
    Designator string `xml:"http://www.iata.org/IATA/EDIST Designator,omitempty"`
    Nature     string `xml:"http://www.iata.org/IATA/EDIST Nature,omitempty"`
}

type Results_5 struct {
    TaxExemption        []*TaxExemption_1 `xml:"http://www.iata.org/IATA/EDIST TaxExemption,omitempty"`
    ExemptionAppliedInd bool              `xml:",attr,omitempty"`
}

type TaxExemption_1 struct {
    RuleID       *RuleID_2                 `xml:"http://www.iata.org/IATA/EDIST RuleID,omitempty"`
    Associations *OrderItemAssociationType `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
}

type RuleID_2 struct {
    Context string `xml:",attr,omitempty"`
    Name    string `xml:",attr,omitempty"`
    Value   string `xml:",chardata"`
}

type EncSchemeType struct {
    Detail      *Detail_2 `xml:"http://www.iata.org/IATA/EDIST Detail,omitempty"`
    Metadata    *Metadata `xml:"http://www.iata.org/IATA/EDIST Metadata,omitempty"`
    Key         string    `xml:",attr,omitempty"`
    OwnerCode   string    `xml:",attr,omitempty"`
    OwnerType   string    `xml:",attr,omitempty"`
    OwnerName   string    `xml:",attr,omitempty"`
    SchemeToken string    `xml:",attr,omitempty"`
}

type Detail_2 struct {
    SupportedDomain []*SupportedDomain `xml:"http://www.iata.org/IATA/EDIST SupportedDomain,omitempty"`
    SchemeName      string             `xml:",attr,omitempty"`
    SchemePurpose   string             `xml:",attr,omitempty"`
    Scheme_URI      string             `xml:",attr,omitempty"`
    SchemeVersion   string             `xml:",attr,omitempty"`
}

type SupportedDomain struct {
    Name    string `xml:",attr,omitempty"`
    Version string `xml:",attr,omitempty"`
}

type Metadata struct {
    MetaBaseType
    Remarks   []*RemarkType `xml:"http://www.iata.org/IATA/EDIST Remarks,omitempty"`
    Timestamp string        `xml:",attr,omitempty"`
}

type PolicyType struct {
    CoreBaseType
    Nodes        *Nodes          `xml:"http://www.iata.org/IATA/EDIST Nodes,omitempty"`
    Descriptions *Descriptions_1 `xml:"http://www.iata.org/IATA/EDIST Descriptions,omitempty"`
    Metadata     *Metadata_1     `xml:"http://www.iata.org/IATA/EDIST Metadata,omitempty"`
    PolicyType   string          `xml:",attr,omitempty"`
    Version      string          `xml:",attr,omitempty"`
}

type Nodes struct {
    Node []*NodePathType `xml:"http://www.iata.org/IATA/EDIST Node,omitempty"`
}

type Descriptions_1 struct {
    Description []*LinkDescriptionType `xml:"http://www.iata.org/IATA/EDIST Description,omitempty"`
}

type Metadata_1 struct {
    MetaBaseType
    Remarks        []*RemarkType        `xml:"http://www.iata.org/IATA/EDIST Remarks,omitempty"`
    EncodingScheme *EncSchemeType       `xml:"http://www.iata.org/IATA/EDIST EncodingScheme,omitempty"`
    ValidPeriod    []*CorePeriodGrpType `xml:"http://www.iata.org/IATA/EDIST ValidPeriod,omitempty"`
    PolicyCode     string               `xml:",attr,omitempty"`
}

type DataListType struct {
    AnonymousTravelerList   *AnonymousTravelerList       `xml:"http://www.iata.org/IATA/EDIST AnonymousTravelerList,omitempty"`
    RecognizedTravelerList  *RecognizedTravelerList      `xml:"http://www.iata.org/IATA/EDIST RecognizedTravelerList,omitempty"`
    BagDisclosureList       *ListOfBagDisclosureType     `xml:"http://www.iata.org/IATA/EDIST BagDisclosureList,omitempty"`
    CarryOnAllowanceList    *ListOfCarryOnAllowanceType  `xml:"http://www.iata.org/IATA/EDIST CarryOnAllowanceList,omitempty"`
    CheckedBagAllowanceList *ListOfBagAllowanceType      `xml:"http://www.iata.org/IATA/EDIST CheckedBagAllowanceList,omitempty"`
    ClassOfServiceList      *ListOfClassOfServiceType    `xml:"http://www.iata.org/IATA/EDIST ClassOfServiceList,omitempty"`
    ContentSourceList       *ListOfContentSourceType     `xml:"http://www.iata.org/IATA/EDIST ContentSourceList,omitempty"`
    DescriptionList         *DescriptionList             `xml:"http://www.iata.org/IATA/EDIST DescriptionList,omitempty"`
    DisclosureList          *ListOfDisclosureType        `xml:"http://www.iata.org/IATA/EDIST DisclosureList,omitempty"`
    FareList                *FareList                    `xml:"http://www.iata.org/IATA/EDIST FareList,omitempty"`
    FlightSegmentList       *FlightSegmentList           `xml:"http://www.iata.org/IATA/EDIST FlightSegmentList,omitempty"`
    FlightList              *FlightList                  `xml:"http://www.iata.org/IATA/EDIST FlightList,omitempty"`
    OriginDestinationList   *OriginDestinationList       `xml:"http://www.iata.org/IATA/EDIST OriginDestinationList,omitempty"`
    InstructionsList        *ListOfOfferInstructionsType `xml:"http://www.iata.org/IATA/EDIST InstructionsList,omitempty"`
    MediaList               *ListOfMediaType             `xml:"http://www.iata.org/IATA/EDIST MediaList,omitempty"`
    PenaltyList             *ListOfOfferPenaltyType      `xml:"http://www.iata.org/IATA/EDIST PenaltyList,omitempty"`
    PriceClassList          *ListOfPriceClassType        `xml:"http://www.iata.org/IATA/EDIST PriceClassList,omitempty"`
    ServiceBundleList       *ListOfServiceBundleType     `xml:"http://www.iata.org/IATA/EDIST ServiceBundleList,omitempty"`
    ServiceList             *ServiceList                 `xml:"http://www.iata.org/IATA/EDIST ServiceList,omitempty"`
    TermsList               *ListOfOfferTermsType        `xml:"http://www.iata.org/IATA/EDIST TermsList,omitempty"`
    SeatList                *SeatList                    `xml:"http://www.iata.org/IATA/EDIST SeatList,omitempty"`
}

type DescriptionList struct {
    Description *Description `xml:"http://www.iata.org/IATA/EDIST Description,omitempty"`
}

type FlightSegmentList struct {
    FlightSegment []*ListOfFlightSegmentType `xml:"http://www.iata.org/IATA/EDIST FlightSegment,omitempty"`
}

type FlightList struct {
    Flight []*Flight `xml:"http://www.iata.org/IATA/EDIST Flight,omitempty"`
}

type Flight struct {
    Journey           *TotalJourneyType  `xml:"http://www.iata.org/IATA/EDIST Journey,omitempty"`
    SegmentReferences *SegmentReferences `xml:"http://www.iata.org/IATA/EDIST SegmentReferences,omitempty"`
    Settlement        *Settlement        `xml:"http://www.iata.org/IATA/EDIST Settlement,omitempty"`
    Refs              string             `xml:",attr,omitempty"`
    FlightKey         string             `xml:",attr,omitempty"`
}

type Settlement struct {
    Method                   *CodesetType           `xml:"http://www.iata.org/IATA/EDIST Method,omitempty"`
    InterlineSettlementValue *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST InterlineSettlementValue,omitempty"`
}

type OriginDestinationList struct {
    OriginDestination []*OriginDestination `xml:"http://www.iata.org/IATA/EDIST OriginDestination,omitempty"`
}

type SeatList struct {
    Seats []*ListOfSeatType `xml:"http://www.iata.org/IATA/EDIST Seats,omitempty"`
}

type BagDisclosureList ListOfBagDisclosureType

type CheckedBagAllowanceList ListOfBagAllowanceType

type CarryOnAllowanceList ListOfCarryOnAllowanceType

type ListOfBagAllowanceType struct {
    CheckedBagAllowance []*CheckedBagAllowance `xml:"http://www.iata.org/IATA/EDIST CheckedBagAllowance,omitempty"`
}

type CheckedBagAllowance struct {
    AllowanceDescription      *BagAllowanceDescType          `xml:"http://www.iata.org/IATA/EDIST AllowanceDescription,omitempty"`
    DimensionAllowance        *BagAllowanceDimensionType     `xml:"http://www.iata.org/IATA/EDIST DimensionAllowance,omitempty"`
    PieceAllowance            []*BagAllowancePieceType       `xml:"http://www.iata.org/IATA/EDIST PieceAllowance,omitempty"`
    WeightAllowance           *WeightAllowance               `xml:"http://www.iata.org/IATA/EDIST WeightAllowance,omitempty"`
    BaggageDeterminingCarrier *BaggageDeterminingCarrierType `xml:"http://www.iata.org/IATA/EDIST BaggageDeterminingCarrier,omitempty"`
    Refs                      string                         `xml:",attr,omitempty"`
    ListKey                   string                         `xml:",attr,omitempty"`
}

type ListOfBagDisclosureType struct {
    BagDisclosure []*BagDisclosure `xml:"http://www.iata.org/IATA/EDIST BagDisclosure,omitempty"`
}

type BagDisclosure struct {
    BagRule                   string                         `xml:"http://www.iata.org/IATA/EDIST BagRule,omitempty"`
    Descriptions              *Descriptions                  `xml:"http://www.iata.org/IATA/EDIST Descriptions,omitempty"`
    BaggageDeterminingCarrier *BaggageDeterminingCarrierType `xml:"http://www.iata.org/IATA/EDIST BaggageDeterminingCarrier,omitempty"`
    Refs                      string                         `xml:",attr,omitempty"`
    ListKey                   string                         `xml:",attr,omitempty"`
    CheckInTFC_ApplicableInd  bool                           `xml:",attr,omitempty"`
    DeferralInd               bool                           `xml:",attr,omitempty"`
    CommercialAgreementID     string                         `xml:",attr,omitempty"`
    FixedPrePaidInd           bool                           `xml:",attr,omitempty"`
}

type ListOfCarryOnAllowanceType struct {
    CarryOnAllowance []*CarryOnAllowance `xml:"http://www.iata.org/IATA/EDIST CarryOnAllowance,omitempty"`
}

type CarryOnAllowance struct {
    AllowanceDescription      *BagAllowanceDescType          `xml:"http://www.iata.org/IATA/EDIST AllowanceDescription,omitempty"`
    DimensionAllowance        *BagAllowanceDimensionType     `xml:"http://www.iata.org/IATA/EDIST DimensionAllowance,omitempty"`
    PieceAllowance            []*BagAllowancePieceType       `xml:"http://www.iata.org/IATA/EDIST PieceAllowance,omitempty"`
    WeightAllowance           *WeightAllowance               `xml:"http://www.iata.org/IATA/EDIST WeightAllowance,omitempty"`
    BaggageDeterminingCarrier *BaggageDeterminingCarrierType `xml:"http://www.iata.org/IATA/EDIST BaggageDeterminingCarrier,omitempty"`
    Refs                      string                         `xml:",attr,omitempty"`
    ListKey                   string                         `xml:",attr,omitempty"`
}

type CompanyList string

type ContentSourceList ListOfContentSourceType

type ListOfContentSourceType struct {
    ContentSource []*ContentSource `xml:"http://www.iata.org/IATA/EDIST ContentSource,omitempty"`
}

type ContentSource struct {
    NodePath  string          `xml:"http://www.iata.org/IATA/EDIST NodePath,omitempty"`
    AirlineID *AirlineID_Type `xml:"http://www.iata.org/IATA/EDIST AirlineID,omitempty"`
    OtherID   *OtherID_3      `xml:"http://www.iata.org/IATA/EDIST OtherID,omitempty"`
    Refs      string          `xml:",attr,omitempty"`
    ListKey   string          `xml:",attr,omitempty"`
}

type OtherID_3 struct {
    Name  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type LinkDescriptionType struct {
    Links     *Links `xml:"http://www.iata.org/IATA/EDIST Links,omitempty"`
    Texts     *Texts `xml:"http://www.iata.org/IATA/EDIST Texts,omitempty"`
    LangCode  string `xml:",attr,omitempty"`
    Timestamp string `xml:",attr,omitempty"`
    Title     string `xml:",attr,omitempty"`
}

type Links struct {
    Link []*LinkFormattedType `xml:"http://www.iata.org/IATA/EDIST Link,omitempty"`
}

type Texts struct {
    Text []string `xml:"http://www.iata.org/IATA/EDIST Text,omitempty"`
}

type LinkFormattedType struct {
    CoreBaseType
    Link []*Link `xml:"http://www.iata.org/IATA/EDIST Link,omitempty"`
}

type Link struct {
    CoreBaseType
    Context string `xml:",attr,omitempty"`
    Text    string `xml:",attr,omitempty"`
    Value   string `xml:",attr,omitempty"`
}

type DisclosureList ListOfDisclosureType

type ListOfDisclosureType struct {
    Disclosures []*Disclosures_1 `xml:"http://www.iata.org/IATA/EDIST Disclosures,omitempty"`
}

type Disclosures_1 struct {
    Description []*DescriptionType `xml:"http://www.iata.org/IATA/EDIST Description,omitempty"`
    Refs        string             `xml:",attr,omitempty"`
    ListKey     string             `xml:",attr,omitempty"`
}

type FareList struct {
    FareGroup []*FareGroup `xml:"http://www.iata.org/IATA/EDIST FareGroup,omitempty"`
}

type FareGroup struct {
    Fare          *Fare          `xml:"http://www.iata.org/IATA/EDIST Fare,omitempty"`
    FareBasisCode *FareBasisCode `xml:"http://www.iata.org/IATA/EDIST FareBasisCode,omitempty"`
    Refs          string         `xml:",attr,omitempty"`
    ListKey       string         `xml:",attr,omitempty"`
}

type Fare struct {
    FareCode   *CodesetType    `xml:"http://www.iata.org/IATA/EDIST FareCode,omitempty"`
    FareDetail *FareDetailType `xml:"http://www.iata.org/IATA/EDIST FareDetail,omitempty"`
}

type FareBasisCode struct {
    Code        string `xml:"http://www.iata.org/IATA/EDIST Code,omitempty"`
    Application string `xml:"http://www.iata.org/IATA/EDIST Application,omitempty"`
}

type ClassOfServiceList ListOfClassOfServiceType

type ListOfFlightSegmentType struct {
    Departure           *FlightDepartureType        `xml:"http://www.iata.org/IATA/EDIST Departure,omitempty"`
    Arrival             *FlightArrivalType          `xml:"http://www.iata.org/IATA/EDIST Arrival,omitempty"`
    MarketingCarrier    *MarketingCarrierFlightType `xml:"http://www.iata.org/IATA/EDIST MarketingCarrier,omitempty"`
    OperatingCarrier    *OperatingCarrier           `xml:"http://www.iata.org/IATA/EDIST OperatingCarrier,omitempty"`
    Equipment           *AircraftSummaryType        `xml:"http://www.iata.org/IATA/EDIST Equipment,omitempty"`
    ClassOfService      *FlightCOS_CoreType         `xml:"http://www.iata.org/IATA/EDIST ClassOfService,omitempty"`
    FlightDetail        *FlightDetailType           `xml:"http://www.iata.org/IATA/EDIST FlightDetail,omitempty"`
    OnTimePerformance   *OnTimePerformance          `xml:"http://www.iata.org/IATA/EDIST OnTimePerformance,omitempty"`
    Settlement          *Settlement_1               `xml:"http://www.iata.org/IATA/EDIST Settlement,omitempty"`
    Refs                string                      `xml:",attr,omitempty"`
    SegmentKey          string                      `xml:",attr,omitempty"`
    ConnectInd          bool                        `xml:",attr,omitempty"`
    ElectronicTicketInd bool                        `xml:",attr,omitempty"`
    TicketlessInd       bool                        `xml:",attr,omitempty"`
    SecureFlight        bool                        `xml:",attr,omitempty"`
}

type OperatingCarrier struct {
    OperatingCarrierFlightType
    Disclosures *DisclosureType `xml:"http://www.iata.org/IATA/EDIST Disclosures,omitempty"`
}

type Settlement_1 struct {
    Method                   *CodesetType           `xml:"http://www.iata.org/IATA/EDIST Method,omitempty"`
    InterlineSettlementValue *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST InterlineSettlementValue,omitempty"`
}

type ListOfClassOfServiceType struct {
    ServiceClass []*ServiceClass `xml:"http://www.iata.org/IATA/EDIST ServiceClass,omitempty"`
}

type ServiceClass struct {
    Code          string         `xml:"http://www.iata.org/IATA/EDIST Code,omitempty"`
    MarketingName string         `xml:"http://www.iata.org/IATA/EDIST MarketingName,omitempty"`
    Associations  *COS_AssocType `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
    Refs          string         `xml:",attr,omitempty"`
    ListKey       string         `xml:",attr,omitempty"`
}

type ListOfMediaType struct {
    Media []*Media_1 `xml:"http://www.iata.org/IATA/EDIST Media,omitempty"`
}

type Media_1 struct {
    ObjectID     *MediaID_Type `xml:"http://www.iata.org/IATA/EDIST ObjectID,omitempty"`
    MediaLink    *MediaLink    `xml:"http://www.iata.org/IATA/EDIST MediaLink,omitempty"`
    AttachmentID *MediaID_Type `xml:"http://www.iata.org/IATA/EDIST AttachmentID,omitempty"`
    Descriptions *Descriptions `xml:"http://www.iata.org/IATA/EDIST Descriptions,omitempty"`
    Refs         string        `xml:",attr,omitempty"`
    ListKey      string        `xml:",attr,omitempty"`
}

type MediaList ListOfMediaType

type InstructionsList ListOfOfferInstructionsType

type ListOfOfferInstructionsType struct {
    Instruction []*Instruction `xml:"http://www.iata.org/IATA/EDIST Instruction,omitempty"`
}

type Instruction struct {
    OfferInstructionType
    Refs    string `xml:",attr,omitempty"`
    ListKey string `xml:",attr,omitempty"`
}

type ListOfOfferPenaltyType struct {
    Penalty []*OfferPenaltyType `xml:"http://www.iata.org/IATA/EDIST Penalty,omitempty"`
}

type ListOfOfferTermsType struct {
    Term    []*OfferTermsType `xml:"http://www.iata.org/IATA/EDIST Term,omitempty"`
    Refs    string            `xml:",attr,omitempty"`
    ListKey string            `xml:",attr,omitempty"`
}

type PenaltyList ListOfOfferPenaltyType

type TermsList ListOfOfferTermsType

type ListOfPriceClassType struct {
    PriceClass []*PriceClassType `xml:"http://www.iata.org/IATA/EDIST PriceClass,omitempty"`
}

type PriceClassList ListOfPriceClassType

type ListOfServiceType struct {
    Service []*Service `xml:"http://www.iata.org/IATA/EDIST Service,omitempty"`
}

type Service struct {
    ServiceID       *ServiceID_Type         `xml:"http://www.iata.org/IATA/EDIST ServiceID,omitempty"`
    Name            string                  `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
    Encoding        *ServiceEncodingType    `xml:"http://www.iata.org/IATA/EDIST Encoding,omitempty"`
    FeeMethod       string                  `xml:"http://www.iata.org/IATA/EDIST FeeMethod,omitempty"`
    SVCDescriptions *ServiceDescriptionType `xml:"http://www.iata.org/IATA/EDIST SVCDescriptions,omitempty"`
    Settlement      *Settlement_2           `xml:"http://www.iata.org/IATA/EDIST Settlement,omitempty"`
    Price           *ServicePriceType       `xml:"http://www.iata.org/IATA/EDIST Price,omitempty"`
    Associations    *ServiceAssocType       `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
    Refs            string                  `xml:",attr,omitempty"`
    ListKey         string                  `xml:",attr,omitempty"`
}

type Settlement_2 struct {
    Method                   *CodesetType           `xml:"http://www.iata.org/IATA/EDIST Method,omitempty"`
    InterlineSettlementValue *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST InterlineSettlementValue,omitempty"`
}

type ListOfServiceBundleType struct {
    ServiceBundle []*ServiceBundle `xml:"http://www.iata.org/IATA/EDIST ServiceBundle,omitempty"`
}

type ServiceBundle struct {
    ItemCount    int                 `xml:"http://www.iata.org/IATA/EDIST ItemCount,omitempty"`
    Associations *Associations_1     `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
    Selection    *Selection          `xml:"http://www.iata.org/IATA/EDIST Selection,omitempty"`
    BundleID     *BundleID           `xml:"http://www.iata.org/IATA/EDIST BundleID,omitempty"`
    Price        []*ServicePriceType `xml:"http://www.iata.org/IATA/EDIST Price,omitempty"`
    BundleName   string              `xml:"http://www.iata.org/IATA/EDIST BundleName,omitempty"`
    Refs         string              `xml:",attr,omitempty"`
    ListKey      string              `xml:",attr,omitempty"`
}

type Associations_1 struct {
    ServiceReference []string          `xml:"http://www.iata.org/IATA/EDIST ServiceReference,omitempty"`
    ServiceID        []*ServiceID_Type `xml:"http://www.iata.org/IATA/EDIST ServiceID,omitempty"`
}

type Selection struct {
    MinimumQuantity int `xml:"http://www.iata.org/IATA/EDIST MinimumQuantity,omitempty"`
    MaximumQuantity int `xml:"http://www.iata.org/IATA/EDIST MaximumQuantity,omitempty"`
}

type BundleID struct {
    Refs      string `xml:",attr,omitempty"`
    ObjectKey string `xml:",attr,omitempty"`
    Value     string `xml:",chardata"`
}

type ServiceList struct {
    Service []*ServiceDetailType `xml:"http://www.iata.org/IATA/EDIST Service,omitempty"`
}

type ServiceBundleList ListOfServiceBundleType

type AnonymousTravelerList struct {
    AnonymousTraveler []*TravelerCoreType `xml:"http://www.iata.org/IATA/EDIST AnonymousTraveler,omitempty"`
}

type RecognizedTravelerList struct {
    RecognizedTraveler []*TravelerDetailType `xml:"http://www.iata.org/IATA/EDIST RecognizedTraveler,omitempty"`
}

type FareAmountType string

type FareIndicatorListType string

type FareIndicatorSimpleType string

type FareBasisListType string

type FareBasisAppSimpleType string

type FareCode FareCodeType

type FareBasisCode_1 FareBasisCodeType

type FareCodeType struct {
    CodesetType
}

type FareBasisCodeType struct {
    KeyWithMetaObjectBaseType
    Code        string `xml:"http://www.iata.org/IATA/EDIST Code,omitempty"`
    Application string `xml:"http://www.iata.org/IATA/EDIST Application,omitempty"`
}

type Fare_1 FareType

type FareType struct {
    FareCode   *CodesetType    `xml:"http://www.iata.org/IATA/EDIST FareCode,omitempty"`
    FareDetail *FareDetailType `xml:"http://www.iata.org/IATA/EDIST FareDetail,omitempty"`
    Refs       string          `xml:",attr,omitempty"`
}

type FareDetail FareDetailType

type FareDetailType struct {
    FareIndicatorCode   *FareIndicatorCode   `xml:"http://www.iata.org/IATA/EDIST FareIndicatorCode,omitempty"`
    FareComponent       []*FareComponentType `xml:"http://www.iata.org/IATA/EDIST FareComponent,omitempty"`
    PriceClassReference string               `xml:"http://www.iata.org/IATA/EDIST PriceClassReference,omitempty"`
    FlightMileage       *FlightMileageType   `xml:"http://www.iata.org/IATA/EDIST FlightMileage,omitempty"`
    TourCode            string               `xml:"http://www.iata.org/IATA/EDIST TourCode,omitempty"`
    Remarks             *RemarkType          `xml:"http://www.iata.org/IATA/EDIST Remarks,omitempty"`
    Refs                string               `xml:",attr,omitempty"`
}

type FareIndicatorCode struct {
    CodesetType
    FiledFareInd bool `xml:",attr,omitempty"`
}

type AdvancePurchase struct {
    Context string `xml:",attr,omitempty"`
    Value   string `xml:",chardata"`
}

type AdvanceTicketing struct {
    AdvanceReservation *AdvanceReservation `xml:"http://www.iata.org/IATA/EDIST AdvanceReservation,omitempty"`
    AdvanceDeparture   *AdvanceDeparture   `xml:"http://www.iata.org/IATA/EDIST AdvanceDeparture,omitempty"`
}

type AdvanceReservation struct {
    Context string `xml:",attr,omitempty"`
    Value   string `xml:",chardata"`
}

type AdvanceDeparture struct {
    Context string `xml:",attr,omitempty"`
    Value   string `xml:",chardata"`
}

type CorporateFare struct {
    KeyObjectBaseType
    CompanyIndex *CompanyIndex `xml:"http://www.iata.org/IATA/EDIST CompanyIndex,omitempty"`
    Contract     *Contract     `xml:"http://www.iata.org/IATA/EDIST Contract,omitempty"`
    Account      *Account_2    `xml:"http://www.iata.org/IATA/EDIST Account,omitempty"`
}

type CompanyIndex struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type Contract struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type Account_2 struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type FareCalculation string

type FareRules FareRulesType

type FareRulesType struct {
    Penalty       *PenaltyType   `xml:"http://www.iata.org/IATA/EDIST Penalty,omitempty"`
    CorporateFare *CorporateFare `xml:"http://www.iata.org/IATA/EDIST CorporateFare,omitempty"`
    AdvanceStay   *AdvanceStay   `xml:"http://www.iata.org/IATA/EDIST AdvanceStay,omitempty"`
    Ticketing     *Ticketing     `xml:"http://www.iata.org/IATA/EDIST Ticketing,omitempty"`
    Remarks       *RemarkType    `xml:"http://www.iata.org/IATA/EDIST Remarks,omitempty"`
    Refs          string         `xml:",attr,omitempty"`
}

type AdvanceStay struct {
    AdvancePurchase  *AdvancePurchase  `xml:"http://www.iata.org/IATA/EDIST AdvancePurchase,omitempty"`
    AdvanceTicketing *AdvanceTicketing `xml:"http://www.iata.org/IATA/EDIST AdvanceTicketing,omitempty"`
    MinimumStay      *MinimumStay      `xml:"http://www.iata.org/IATA/EDIST MinimumStay,omitempty"`
    MaximumStay      *MaximumStay      `xml:"http://www.iata.org/IATA/EDIST MaximumStay,omitempty"`
}

type Ticketing struct {
    TicketlessInd   bool          `xml:"http://www.iata.org/IATA/EDIST TicketlessInd,omitempty"`
    InstantPurchase *CodesetType  `xml:"http://www.iata.org/IATA/EDIST InstantPurchase,omitempty"`
    Endorsements    *Endorsements `xml:"http://www.iata.org/IATA/EDIST Endorsements,omitempty"`
}

type Endorsements struct {
    Endorsement []string `xml:"http://www.iata.org/IATA/EDIST Endorsement,omitempty"`
}

type MinimumStay struct {
    DayQuantity *DayQuantity `xml:"http://www.iata.org/IATA/EDIST DayQuantity,omitempty"`
    DayOfWeek   int          `xml:"http://www.iata.org/IATA/EDIST DayOfWeek,omitempty"`
}

type DayQuantity struct {
    Context string `xml:",attr,omitempty"`
    Value   int    `xml:",chardata"`
}

type MaximumStay struct {
    DayQuantity *DayQuantity_1 `xml:"http://www.iata.org/IATA/EDIST DayQuantity,omitempty"`
    DayOfWeek   int            `xml:"http://www.iata.org/IATA/EDIST DayOfWeek,omitempty"`
}

type DayQuantity_1 struct {
    Context string `xml:",attr,omitempty"`
    Value   int    `xml:",chardata"`
}

type TourCode ContextSimpleType

type FareComponent FareComponentType

type FareComponentType struct {
    Parameters       *Parameters_1         `xml:"http://www.iata.org/IATA/EDIST Parameters,omitempty"`
    SegmentReference *SegmentReference     `xml:"http://www.iata.org/IATA/EDIST SegmentReference,omitempty"`
    PriceBreakdown   *PriceBreakdown       `xml:"http://www.iata.org/IATA/EDIST PriceBreakdown,omitempty"`
    FareBasis        *FareBasis            `xml:"http://www.iata.org/IATA/EDIST FareBasis,omitempty"`
    TicketDesig      *TicketDesignatorType `xml:"http://www.iata.org/IATA/EDIST TicketDesig,omitempty"`
    FareRules        *FareRulesType        `xml:"http://www.iata.org/IATA/EDIST FareRules,omitempty"`
    Refs             string                `xml:",attr,omitempty"`
}

type Parameters_1 struct {
    Quantity int `xml:",attr,omitempty"`
}

type PriceBreakdown struct {
    Price *FarePriceDetailType `xml:"http://www.iata.org/IATA/EDIST Price,omitempty"`
}

type FareBasis struct {
    FareBasisCode     *FareBasisCodeType `xml:"http://www.iata.org/IATA/EDIST FareBasisCode,omitempty"`
    FareRulesRemarks  *FareRulesRemarks  `xml:"http://www.iata.org/IATA/EDIST FareRulesRemarks,omitempty"`
    FareBasisCityPair string             `xml:"http://www.iata.org/IATA/EDIST FareBasisCityPair,omitempty"`
    RBD               string             `xml:"http://www.iata.org/IATA/EDIST RBD,omitempty"`
}

type FareRulesRemarks struct {
    FareRulesRemark []*FareRulesRemark `xml:"http://www.iata.org/IATA/EDIST FareRulesRemark,omitempty"`
}

type FareRulesRemark struct {
    KeyWithMetaObjectBaseType
    Category *CodesetType `xml:"http://www.iata.org/IATA/EDIST Category,omitempty"`
    Text     string       `xml:"http://www.iata.org/IATA/EDIST Text,omitempty"`
}

type FareBasisCityPair ContextSimpleType

type PricedMarketName ProperNameSimpleType

type PricedMileageZone ProperNameSimpleType

type FareFiledIn FareFilingType

type FareFilingType struct {
    BaseAmount     *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST BaseAmount,omitempty"`
    NUC_Amount     int                    `xml:"http://www.iata.org/IATA/EDIST NUC_Amount,omitempty"`
    ExchangeRate   string                 `xml:"http://www.iata.org/IATA/EDIST ExchangeRate,omitempty"`
    TicketBulkMask string                 `xml:"http://www.iata.org/IATA/EDIST TicketBulkMask,omitempty"`
    FiledFare      *FileFareType          `xml:"http://www.iata.org/IATA/EDIST FiledFare,omitempty"`
    Refs           string                 `xml:",attr,omitempty"`
}

type FileFareType struct {
    FareIndicatorCode string `xml:"http://www.iata.org/IATA/EDIST FareIndicatorCode,omitempty"`
    Refs              string `xml:",attr,omitempty"`
    Cat35NetFareInd   bool   `xml:",attr,omitempty"`
}

type FiledFare FileFareType

type FareCacheKey struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type FareInd CodesetType

type FareQualifierCodes struct {
    FareQualifierCode []*FareQualifierType `xml:"http://www.iata.org/IATA/EDIST FareQualifierCode,omitempty"`
}

type FareReferenceKey struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type FareRuleCategory CodesetType

type FareTextInd string

type FarePriceBaseType struct {
    BaseAmount         *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST BaseAmount,omitempty"`
    FareFiledIn        *FareFilingType        `xml:"http://www.iata.org/IATA/EDIST FareFiledIn,omitempty"`
    Surcharges         *Surcharges            `xml:"http://www.iata.org/IATA/EDIST Surcharges,omitempty"`
    AwardPricing       *AwardPriceUnitType    `xml:"http://www.iata.org/IATA/EDIST AwardPricing,omitempty"`
    CombinationPricing *CombinationPriceType  `xml:"http://www.iata.org/IATA/EDIST CombinationPricing,omitempty"`
}

type Surcharges struct {
    Surcharge []*FeeSurchargeType `xml:"http://www.iata.org/IATA/EDIST Surcharge,omitempty"`
}

type FarePriceDetailType struct {
    FarePriceBaseType
    Taxes         *TaxDetailType    `xml:"http://www.iata.org/IATA/EDIST Taxes,omitempty"`
    TaxExemption  *TaxExemptionType `xml:"http://www.iata.org/IATA/EDIST TaxExemption,omitempty"`
    RequestedDate string            `xml:"http://www.iata.org/IATA/EDIST RequestedDate,omitempty"`
    Alignment     *CodesetType      `xml:"http://www.iata.org/IATA/EDIST Alignment,omitempty"`
}

type OrderFareRulesType struct {
    Penalty       *PenaltyType   `xml:"http://www.iata.org/IATA/EDIST Penalty,omitempty"`
    CorporateFare *CorporateFare `xml:"http://www.iata.org/IATA/EDIST CorporateFare,omitempty"`
    AdvanceStay   *AdvanceStay_1 `xml:"http://www.iata.org/IATA/EDIST AdvanceStay,omitempty"`
    Remarks       *RemarkType    `xml:"http://www.iata.org/IATA/EDIST Remarks,omitempty"`
}

type AdvanceStay_1 struct {
    AdvancePurchase  *AdvancePurchase  `xml:"http://www.iata.org/IATA/EDIST AdvancePurchase,omitempty"`
    AdvanceTicketing *AdvanceTicketing `xml:"http://www.iata.org/IATA/EDIST AdvanceTicketing,omitempty"`
    MinimumStay      *MinimumStay      `xml:"http://www.iata.org/IATA/EDIST MinimumStay,omitempty"`
    MaximumStay      *MaximumStay      `xml:"http://www.iata.org/IATA/EDIST MaximumStay,omitempty"`
}

type COS_AssocListItemType string

type COS_AssocListType string

type Flight_COS_SimpleType string

type FlightDurationListType string

type FlightDurationAppSimpleType string

type FlightMileageListType string

type FlightMileageAppSimpleType string

type COS_AssocType struct {
    COS_OtherAssocType
}

type COS_OtherAssocType struct {
    OtherAssociation []*OtherAssociation_1 `xml:"http://www.iata.org/IATA/EDIST OtherAssociation,omitempty"`
}

type OtherAssociation_1 struct {
    Type           string `xml:"http://www.iata.org/IATA/EDIST Type,omitempty"`
    ReferenceValue string `xml:"http://www.iata.org/IATA/EDIST ReferenceValue,omitempty"`
}

type FlightNumber struct {
    OperationalSuffix string `xml:",attr,omitempty"`
    Value             string `xml:",chardata"`
}

type FlightNumberType string

type ClassOfService FlightCOS_CoreType

type FlightCOS_CoreType struct {
    Code          *Code_1        `xml:"http://www.iata.org/IATA/EDIST Code,omitempty"`
    MarketingName *MarketingName `xml:"http://www.iata.org/IATA/EDIST MarketingName,omitempty"`
    Refs          string         `xml:",attr,omitempty"`
}

type Code_1 struct {
    SeatsLeft int    `xml:",attr,omitempty"`
    Value     string `xml:",chardata"`
}

type MarketingName struct {
    CabinDesignator string `xml:",attr,omitempty"`
    Value           string `xml:",chardata"`
}

type StopLocations_1 StopLocationType

type StopLocationType struct {
    StopLocation     []*StopLocation_1 `xml:"http://www.iata.org/IATA/EDIST StopLocation,omitempty"`
    ChangeOfGaugeInd bool              `xml:",attr,omitempty"`
}

type StopLocation_1 struct {
    AirportCode   *AirportCode         `xml:"http://www.iata.org/IATA/EDIST AirportCode,omitempty"`
    Name          string               `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
    ArrivalDate   string               `xml:"http://www.iata.org/IATA/EDIST ArrivalDate,omitempty"`
    ArrivalTime   string               `xml:"http://www.iata.org/IATA/EDIST ArrivalTime,omitempty"`
    DepartureDate string               `xml:"http://www.iata.org/IATA/EDIST DepartureDate,omitempty"`
    DepartureTime string               `xml:"http://www.iata.org/IATA/EDIST DepartureTime,omitempty"`
    Equipment     *AircraftSummaryType `xml:"http://www.iata.org/IATA/EDIST Equipment,omitempty"`
    GroundTime    string               `xml:"http://www.iata.org/IATA/EDIST GroundTime,omitempty"`
    Refs          string               `xml:",attr,omitempty"`
}

type Arrival FlightArrivalType

type ArrivalCode struct {
    Application string `xml:",attr,omitempty"`
    Area        int    `xml:",attr,omitempty"`
    UOM         string `xml:",attr,omitempty"`
    Value       string `xml:",chardata"`
}

type Departure FlightDepartureType

type DepartureCode struct {
    Application string `xml:",attr,omitempty"`
    Area        int    `xml:",attr,omitempty"`
    UOM         string `xml:",attr,omitempty"`
    Value       string `xml:",chardata"`
}

type DepartureDate string

type FlightDepartureType struct {
    AirportCode *AirportCode_1 `xml:"http://www.iata.org/IATA/EDIST AirportCode,omitempty"`
    Date        string         `xml:"http://www.iata.org/IATA/EDIST Date,omitempty"`
    Time        string         `xml:"http://www.iata.org/IATA/EDIST Time,omitempty"`
    AirportName string         `xml:"http://www.iata.org/IATA/EDIST AirportName,omitempty"`
    Terminal    *Terminal      `xml:"http://www.iata.org/IATA/EDIST Terminal,omitempty"`
    Refs        string         `xml:",attr,omitempty"`
}

type AirportCode_1 struct {
    Application string `xml:",attr,omitempty"`
    Area        int    `xml:",attr,omitempty"`
    UOM         string `xml:",attr,omitempty"`
    Value       string `xml:",chardata"`
}

type Terminal struct {
    Name string `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
    Gate string `xml:"http://www.iata.org/IATA/EDIST Gate,omitempty"`
}

type FlightArrivalType struct {
    AirportCode *AirportCode_2 `xml:"http://www.iata.org/IATA/EDIST AirportCode,omitempty"`
    Date        string         `xml:"http://www.iata.org/IATA/EDIST Date,omitempty"`
    Time        string         `xml:"http://www.iata.org/IATA/EDIST Time,omitempty"`
    ChangeOfDay int            `xml:"http://www.iata.org/IATA/EDIST ChangeOfDay,omitempty"`
    AirportName string         `xml:"http://www.iata.org/IATA/EDIST AirportName,omitempty"`
    Terminal    *Terminal_1    `xml:"http://www.iata.org/IATA/EDIST Terminal,omitempty"`
    Refs        string         `xml:",attr,omitempty"`
}

type AirportCode_2 struct {
    Application string `xml:",attr,omitempty"`
    Area        int    `xml:",attr,omitempty"`
    UOM         string `xml:",attr,omitempty"`
    Value       string `xml:",chardata"`
}

type Terminal_1 struct {
    Name string `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
    Gate string `xml:"http://www.iata.org/IATA/EDIST Gate,omitempty"`
}

type FlightType struct {
    OriginDestinationKey string            `xml:"http://www.iata.org/IATA/EDIST OriginDestinationKey,omitempty"`
    TotalJourney         *TotalJourneyType `xml:"http://www.iata.org/IATA/EDIST TotalJourney,omitempty"`
    Flight               []*Flight_1       `xml:"http://www.iata.org/IATA/EDIST Flight,omitempty"`
    Refs                 string            `xml:",attr,omitempty"`
}

type Flight_1 struct {
    SegmentKey       string                      `xml:"http://www.iata.org/IATA/EDIST SegmentKey,omitempty"`
    Indicators       *Indicators                 `xml:"http://www.iata.org/IATA/EDIST Indicators,omitempty"`
    Status           *Status_3                   `xml:"http://www.iata.org/IATA/EDIST Status,omitempty"`
    Departure        *FlightDepartureType        `xml:"http://www.iata.org/IATA/EDIST Departure,omitempty"`
    Arrival          *FlightArrivalType          `xml:"http://www.iata.org/IATA/EDIST Arrival,omitempty"`
    MarketingCarrier *MarketingCarrierFlightType `xml:"http://www.iata.org/IATA/EDIST MarketingCarrier,omitempty"`
    OperatingCarrier *OperatingCarrier_1         `xml:"http://www.iata.org/IATA/EDIST OperatingCarrier,omitempty"`
    Equipment        *AircraftSummaryType        `xml:"http://www.iata.org/IATA/EDIST Equipment,omitempty"`
    CabinType        *CabinType                  `xml:"http://www.iata.org/IATA/EDIST CabinType,omitempty"`
    ClassOfService   *FlightCOS_CoreType         `xml:"http://www.iata.org/IATA/EDIST ClassOfService,omitempty"`
    Details          *FlightDetailType           `xml:"http://www.iata.org/IATA/EDIST Details,omitempty"`
    Refs             string                      `xml:",attr,omitempty"`
}

type Indicators struct {
    PricingIndicator    bool `xml:",attr,omitempty"`
    ConnectionIndicator bool `xml:",attr,omitempty"`
    E_TicketIndicator   bool `xml:",attr,omitempty"`
    TicketlessIndicator bool `xml:",attr,omitempty"`
}

type Status_3 struct {
    StatusCode        *CodesetType `xml:"http://www.iata.org/IATA/EDIST StatusCode,omitempty"`
    ChangeOfGaugeInd  bool         `xml:",attr,omitempty"`
    ScheduleChangeInd bool         `xml:",attr,omitempty"`
}

type OperatingCarrier_1 struct {
    OperatingCarrierFlightType
    Disclosures *DisclosureType `xml:"http://www.iata.org/IATA/EDIST Disclosures,omitempty"`
}

type FlightDetailType struct {
    FlightSegmentType    *CodesetType        `xml:"http://www.iata.org/IATA/EDIST FlightSegmentType,omitempty"`
    FlightDistance       *FlightDistanceType `xml:"http://www.iata.org/IATA/EDIST FlightDistance,omitempty"`
    FlightDuration       *FlightDurationType `xml:"http://www.iata.org/IATA/EDIST FlightDuration,omitempty"`
    Stops                *Stops              `xml:"http://www.iata.org/IATA/EDIST Stops,omitempty"`
    ResDateTime          *ResDateTime        `xml:"http://www.iata.org/IATA/EDIST ResDateTime,omitempty"`
    TourOperatorFlightID string              `xml:"http://www.iata.org/IATA/EDIST TourOperatorFlightID,omitempty"`
    Refs                 string              `xml:",attr,omitempty"`
}

type Stops struct {
    StopQuantity  int               `xml:"http://www.iata.org/IATA/EDIST StopQuantity,omitempty"`
    StopLocations *StopLocationType `xml:"http://www.iata.org/IATA/EDIST StopLocations,omitempty"`
}

type FlightOnlyType struct {
    SegmentKey       string                      `xml:"http://www.iata.org/IATA/EDIST SegmentKey,omitempty"`
    Departure        *FlightDepartureType        `xml:"http://www.iata.org/IATA/EDIST Departure,omitempty"`
    Arrival          *FlightArrivalType          `xml:"http://www.iata.org/IATA/EDIST Arrival,omitempty"`
    MarketingCarrier *MarketingCarrierFlightType `xml:"http://www.iata.org/IATA/EDIST MarketingCarrier,omitempty"`
    Equipment        []*AircraftSummaryType      `xml:"http://www.iata.org/IATA/EDIST Equipment,omitempty"`
    OperatingCarrier *OperatingCarrier_2         `xml:"http://www.iata.org/IATA/EDIST OperatingCarrier,omitempty"`
    CabinType        *CabinType                  `xml:"http://www.iata.org/IATA/EDIST CabinType,omitempty"`
    ClassOfService   *FlightCOS_CoreType         `xml:"http://www.iata.org/IATA/EDIST ClassOfService,omitempty"`
    Details          *FlightDetailType           `xml:"http://www.iata.org/IATA/EDIST Details,omitempty"`
    Refs             string                      `xml:",attr,omitempty"`
}

type OperatingCarrier_2 struct {
    OperatingCarrierFlightType
    Disclosures *DisclosureType `xml:"http://www.iata.org/IATA/EDIST Disclosures,omitempty"`
}

type FlightDistance FlightDistanceType

type FlightDistanceType struct {
    Value       int    `xml:",chardata"`
    UOM         string `xml:"http://www.iata.org/IATA/EDIST UOM,omitempty"`
    Application string `xml:"http://www.iata.org/IATA/EDIST Application,omitempty"`
    Refs        string `xml:",attr,omitempty"`
}

type FlightDuration FlightDurationType

type FlightDurationType struct {
    Value       string `xml:",chardata"`
    Application string `xml:"http://www.iata.org/IATA/EDIST Application,omitempty"`
    Refs        string `xml:",attr,omitempty"`
}

type TotalJourney TotalJourneyType

type TotalJourneyType struct {
    Time     string    `xml:"http://www.iata.org/IATA/EDIST Time,omitempty"`
    Distance *Distance `xml:"http://www.iata.org/IATA/EDIST Distance,omitempty"`
    Refs     string    `xml:",attr,omitempty"`
}

type Distance struct {
    Value int    `xml:",chardata"`
    UOM   string `xml:"http://www.iata.org/IATA/EDIST UOM,omitempty"`
}

type FlightStatus FlightStatusType

type FlightStatusType struct {
    CodesetType
}

type FlightMealsType struct {
    CodesetType
}

type MealsServed FlightMealsType

type Frequency FlightFrequencyType

type FlightFrequencyType DayOfWeekMaskType

type FlightPerformanceType struct {
    Low            float64 `xml:"http://www.iata.org/IATA/EDIST Low,omitempty"`
    High           float64 `xml:"http://www.iata.org/IATA/EDIST High,omitempty"`
    NewFlightInd   bool    `xml:",attr,omitempty"`
    NoDataInd      bool    `xml:",attr,omitempty"`
    NotRequiredInd bool    `xml:",attr,omitempty"`
}

type OnTimePerformance struct {
    Percent             float64 `xml:",attr,omitempty"`
    Period              string  `xml:",attr,omitempty"`
    Type                string  `xml:",attr,omitempty"`
    LatePercent         float64 `xml:",attr,omitempty"`
    CancelledPercent    float64 `xml:",attr,omitempty"`
    SpecialHighlightInd bool    `xml:",attr,omitempty"`
}

type Percentage float64

type FlightMileage FlightMileageType

type FlightMileageType struct {
    Value       int    `xml:",chardata"`
    Application string `xml:"http://www.iata.org/IATA/EDIST Application,omitempty"`
}

type FlightDisplayPricingMethod string

type ResDateTime struct {
    Timestamp string `xml:"http://www.iata.org/IATA/EDIST Timestamp,omitempty"`
    Date      *Date  `xml:"http://www.iata.org/IATA/EDIST Date,omitempty"`
}

type Date struct {
    Time  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type FlightSegmentType CodesetType

type AircraftCode AircraftCodeType

type AircraftCodeType struct {
    Refs                 string `xml:",attr,omitempty"`
    ObjectMetaReferences string `xml:",attr,omitempty"`
    Value                string `xml:",chardata"`
}

type AircraftCore AircraftCoreType

type AircraftSummary AircraftSummaryType

type AircraftCoreType struct {
    AircraftCode     *AircraftCodeType `xml:"http://www.iata.org/IATA/EDIST AircraftCode,omitempty"`
    ChangeOfGaugeInd bool              `xml:",attr,omitempty"`
    Refs             string            `xml:",attr,omitempty"`
}

type AircraftSummaryType struct {
    AircraftCoreType
    Name             string `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
    AirlineEquipCode string `xml:"http://www.iata.org/IATA/EDIST AirlineEquipCode,omitempty"`
}

type Equipment AircraftSummaryType

type AircraftCodeQuery AircraftCodeQueryType

type AircraftFeaturesQuery AircraftFeaturesQueryType

type AircraftCodeQueryType struct {
    AircraftCode []*AircraftCodeType `xml:"http://www.iata.org/IATA/EDIST AircraftCode,omitempty"`
    Refs         string              `xml:",attr,omitempty"`
}

type AircraftTailNmbrQueryType struct {
    TailNumber []string `xml:"http://www.iata.org/IATA/EDIST TailNumber,omitempty"`
    Refs       string   `xml:",attr,omitempty"`
}

type AircraftFeaturesQueryType struct {
    Feature []*Feature `xml:"http://www.iata.org/IATA/EDIST Feature,omitempty"`
    Refs    string     `xml:",attr,omitempty"`
}

type Feature struct {
    CodesetType
    PreferencesLevel   string `xml:",attr,omitempty"`
    PreferencesContext string `xml:",attr,omitempty"`
}

type AircraftCabin AircraftCabinType

type AircraftCabinType struct {
    CodesetType
}

type CabinType struct {
    CodesetType
    Name                        string   `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
    OriginDestinationReferences []string `xml:"http://www.iata.org/IATA/EDIST OriginDestinationReferences,omitempty"`
}

type FlightCabin_CoreType struct {
    CabinDesignator *CabinDesignator `xml:"http://www.iata.org/IATA/EDIST CabinDesignator,omitempty"`
    MarketingName   string           `xml:"http://www.iata.org/IATA/EDIST MarketingName,omitempty"`
    Refs            string           `xml:",attr,omitempty"`
}

type CabinDesignator struct {
    AllCabins bool   `xml:",attr,omitempty"`
    Value     string `xml:",chardata"`
}

type SimpleAircraftCabinType struct {
    FlightCabin_CoreType
}

type Itinerary ItineraryType

type ItineraryType struct {
    Flight []*FlightType `xml:"http://www.iata.org/IATA/EDIST Flight,omitempty"`
    Refs   string        `xml:",attr,omitempty"`
}

type OriginDestination struct {
    OriginDestinationType
    TotalJourney     *TotalJourneyType `xml:"http://www.iata.org/IATA/EDIST TotalJourney,omitempty"`
    FlightReferences *FlightReferences `xml:"http://www.iata.org/IATA/EDIST FlightReferences,omitempty"`
    CheckedBag       *CheckedBag_1     `xml:"http://www.iata.org/IATA/EDIST CheckedBag,omitempty"`
    CarryOnBag       *CarryOnBag_1     `xml:"http://www.iata.org/IATA/EDIST CarryOnBag,omitempty"`
}

type CheckedBag_1 struct {
    BaggageDeterminingCarrier *BaggageDeterminingCarrierType `xml:"http://www.iata.org/IATA/EDIST BaggageDeterminingCarrier,omitempty"`
    BDC_AnalysisResult        *CodesetType                   `xml:"http://www.iata.org/IATA/EDIST BDC_AnalysisResult,omitempty"`
}

type CarryOnBag_1 struct {
    BaggageDeterminingCarrier *BaggageDeterminingCarrierType `xml:"http://www.iata.org/IATA/EDIST BaggageDeterminingCarrier,omitempty"`
    BDC_AnalysisResult        *CodesetType                   `xml:"http://www.iata.org/IATA/EDIST BDC_AnalysisResult,omitempty"`
}

type OriginDestinationType struct {
    DepartureCode        *DepartureCode `xml:"http://www.iata.org/IATA/EDIST DepartureCode,omitempty"`
    ArrivalCode          *ArrivalCode   `xml:"http://www.iata.org/IATA/EDIST ArrivalCode,omitempty"`
    Refs                 string         `xml:",attr,omitempty"`
    OriginDestinationKey string         `xml:",attr,omitempty"`
}

type OfferItemID ItemID_Type

type OrderItemID ItemID_Type

type ItemID_Type struct {
    Refs         string `xml:",attr,omitempty"`
    ObjectKey    string `xml:",attr,omitempty"`
    Owner        string `xml:",attr,omitempty"`
    WebAddressID string `xml:",attr,omitempty"`
    OwnerType    string `xml:",attr,omitempty"`
    Value        string `xml:",chardata"`
}

type AircraftKey string

type AircraftReference string

type AirportKey string

type AirportReference string

type BagDetailAssociation struct {
    BagDisclosureReferences string `xml:"http://www.iata.org/IATA/EDIST BagDisclosureReferences,omitempty"`
    CheckedBagReferences    string `xml:"http://www.iata.org/IATA/EDIST CheckedBagReferences,omitempty"`
    CarryOnReferences       string `xml:"http://www.iata.org/IATA/EDIST CarryOnReferences,omitempty"`
}

type BagDisclosureKey string

type BagDisclosureReference string

type BagDisclosureReferences InstanceClassRefSimpleType

type CheckedBagKey string

type CheckedBagReference string

type CheckedBagReferences InstanceClassRefSimpleType

type CarryOnKey string

type CarryOnReference string

type CarryOnReferences InstanceClassRefSimpleType

type ContentSourceKey string

type ContentSourceReference string

type ContentSourceReferences InstanceClassRefSimpleType

type ListKey string

type DisclosureKey string

type DisclosureReference string

type DisclosureReferences InstanceClassRefSimpleType

type EquivalentID_Key string

type EquivalentReference string

type ExitPosKey string

type ExitPosReference string

type ExitPosReferences InstanceClassRefSimpleType

type FareKey string

type FareReference InstanceClassRefSimpleType

type FareBasisKey string

type FareBasisReference string

type FareBasisReferences InstanceClassRefSimpleType

type MetadataReferences InstanceClassRefSimpleType

type ServiceClassKey string

type ServiceClassReference string

type ServiceClassReferences InstanceClassRefSimpleType

type OriginDestinationKey string

type OriginDestinationReference string

type OriginDestinationReferences InstanceClassRefSimpleType

type FlightReferences struct {
    OnPoint  string `xml:",attr,omitempty"`
    OffPoint string `xml:",attr,omitempty"`
    Value    string `xml:",chardata"`
}

type FlightSegmentReferences InstanceClassRefSimpleType

type FlightSegmentReference struct {
    Cabin                *SimpleAircraftCabinType `xml:"http://www.iata.org/IATA/EDIST Cabin,omitempty"`
    ClassOfService       *FlightCOS_CoreType      `xml:"http://www.iata.org/IATA/EDIST ClassOfService,omitempty"`
    BagDetailAssociation *BagDetailAssociation    `xml:"http://www.iata.org/IATA/EDIST BagDetailAssociation,omitempty"`
    MarriedSegmentGroup  int                      `xml:"http://www.iata.org/IATA/EDIST MarriedSegmentGroup,omitempty"`
    Ref                  string                   `xml:"ref,attr,omitempty"`
}

type SegmentKey string

type SegmentReference struct {
    ON_Point  string `xml:",attr,omitempty"`
    OFF_Point string `xml:",attr,omitempty"`
    Value     string `xml:",chardata"`
}

type SegmentReferences struct {
    OnPoint  string `xml:",attr,omitempty"`
    OffPoint string `xml:",attr,omitempty"`
    Value    string `xml:",chardata"`
}

type GroupReference string

type MediaItemKey string

type MediaGroupKey string

type MediaGroupreference string

type MediaItemReference string

type MediaItemReferences InstanceClassRefSimpleType

type OfferKey string

type OfferReference string

type OfferReferences InstanceClassRefSimpleType

type OfferInstructionKey string

type OfferInstructionReference string

type OfferInstructionReferences string

type OfferPenaltyKey string

type OfferPenaltyReferences string

type OfferPenaltyReference InstanceClassRefSimpleType

type OfferTermsKey string

type OfferTermReference string

type OfferTermReferences InstanceClassRefSimpleType

type OrderKey string

type OrderReference string

type OrderReferences InstanceClassRefSimpleType

type OrderItemKey string

type OrderItemReference string

type OrderItemReferences InstanceClassRefSimpleType

type OrderInstructionKey string

type OrderInstructionReference string

type OrderInstructionReferences InstanceClassRefSimpleType

type OrderPenaltyKey string

type OrderPenaltyReferences string

type OrderPenaltyReference InstanceClassRefSimpleType

type PassengerKey string

type PassengerReference string

type PassengerReferences InstanceClassRefSimpleType

type PriceClassKey string

type PriceClassReference string

type PriceClassReferences InstanceClassRefSimpleType

type SeatKey string

type SeatReference string

type SeatReferences InstanceClassRefSimpleType

type SeatDisplayKey string

type SeatDisplayRef string

type SeatDisplayReferences InstanceClassRefSimpleType

type ComponentKey string

type ComponentReference string

type ComponentReferences InstanceClassRefSimpleType

type SeatMsgKey string

type SeatMsgReference string

type SeatMsgReferences InstanceClassRefSimpleType

type ServiceKey string

type ServiceReference string

type ServiceReferences InstanceClassRefSimpleType

type BundleKey string

type BundleReference string

type BundleReferences InstanceClassRefSimpleType

type TravelerKey string

type TravelerReference string

type TravelerReferences InstanceClassRefSimpleType

type StateProvKey string

type StateProvReference InstanceClassRefSimpleType

type StateProvReferences InstanceClassRefSimpleType

type WingPosKey string

type WingPosReference string

type WingPosReferences InstanceClassRefSimpleType

type AddressMetadatas struct {
    AddressMetadata []*AddressMetadataType `xml:"http://www.iata.org/IATA/EDIST AddressMetadata,omitempty"`
}

type AddressMetadata AddressMetadataType

type AircraftMetadatas struct {
    AircraftMetadata []*AircraftMetadataType `xml:"http://www.iata.org/IATA/EDIST AircraftMetadata,omitempty"`
}

type AircraftMetadata AircraftMetadataType

type AirportMetadatas struct {
    AirportMetadata []*AirportMetadataType `xml:"http://www.iata.org/IATA/EDIST AirportMetadata,omitempty"`
}

type AirportMetadata AirportMetadataType

type CityMetadatas struct {
    CityMetadata []*CityMetadataType `xml:"http://www.iata.org/IATA/EDIST CityMetadata,omitempty"`
}

type CityMetadata CityMetadataType

type CodesetMetadatas struct {
    CodesetMetadata []*CodesetMetadataType `xml:"http://www.iata.org/IATA/EDIST CodesetMetadata,omitempty"`
}

type CodesetMetadata CodesetMetadataType

type ContactMetadatas struct {
    ContactMetadata []*MetadataObjectBaseType `xml:"http://www.iata.org/IATA/EDIST ContactMetadata,omitempty"`
}

type ContactMetadata ContactMetadataType

type ContentMetadatas struct {
    ContentMetadata []*MetadataObjectBaseType `xml:"http://www.iata.org/IATA/EDIST ContentMetadata,omitempty"`
}

type ContentMetadata ContentMetadataType

type CountryMetadatas struct {
    CountryMetadata []*CountryMetadataType `xml:"http://www.iata.org/IATA/EDIST CountryMetadata,omitempty"`
}

type CountryMetadata CountryMetadataType

type CurrencyMetadatas struct {
    CurrencyMetadata []*CurrencyMetadataType `xml:"http://www.iata.org/IATA/EDIST CurrencyMetadata,omitempty"`
}

type CurrencyMetadata CurrencyMetadataType

type DescriptionMetadatas struct {
    DescriptionMetadata []*DescriptionMetadataType `xml:"http://www.iata.org/IATA/EDIST DescriptionMetadata,omitempty"`
}

type DescriptionMetadata DescriptionMetadataType

type EquivalentID_Metadatas struct {
    EquivalentID_Metadata []*EquivalentID_MetadataType `xml:"http://www.iata.org/IATA/EDIST EquivalentID_Metadata,omitempty"`
}

type EquivalentID_Metadata EquivalentID_MetadataType

type LanguageMetadatas struct {
    LanguageMetadata []*LanguageMetadataType `xml:"http://www.iata.org/IATA/EDIST LanguageMetadata,omitempty"`
}

type LanguageMetadata LanguageMetadataType

type MediaMetadatas struct {
    MediaMetadata []*MediaMetadataType `xml:"http://www.iata.org/IATA/EDIST MediaMetadata,omitempty"`
}

type MediaMetadata MediaMetadataType

type PaymentCardMetadatas struct {
    PaymentCardMetadata []*PaymentCardMetadataType `xml:"http://www.iata.org/IATA/EDIST PaymentCardMetadata,omitempty"`
}

type PaymentCardMetadata PaymentCardMetadataType

type PaymentFormMetadatas struct {
    PaymentFormMetadata []*PaymentFormMetadataType `xml:"http://www.iata.org/IATA/EDIST PaymentFormMetadata,omitempty"`
}

type PaymentFormMetadata PaymentFormMetadataType

type PriceMetadatas struct {
    PriceMetadata []*MetadataObjectBaseType `xml:"http://www.iata.org/IATA/EDIST PriceMetadata,omitempty"`
}

type PriceMetadata PriceMetadataType

type RuleMetadatas struct {
    RuleMetadata []*RuleMetadataType `xml:"http://www.iata.org/IATA/EDIST RuleMetadata,omitempty"`
}

type RuleMetadata RuleMetadataType

type StateProvMetadatas struct {
    StateProvMetadata []*StateProvMetadataType `xml:"http://www.iata.org/IATA/EDIST StateProvMetadata,omitempty"`
}

type StateProvMetadata StateProvMetadataType

type ZoneMetadatas struct {
    ZoneMetadata []*ZoneMetadataType `xml:"http://www.iata.org/IATA/EDIST ZoneMetadata,omitempty"`
}

type ZoneMetadata ZoneMetadataType

type AddressMetadataType struct {
    MetadataObjectBaseType
    Position      *PositionType  `xml:"http://www.iata.org/IATA/EDIST Position,omitempty"`
    AddressType   string         `xml:"http://www.iata.org/IATA/EDIST AddressType,omitempty"`
    AddressFields *AddressFields `xml:"http://www.iata.org/IATA/EDIST AddressFields,omitempty"`
}

type AddressFields struct {
    FieldName []*FieldName `xml:"http://www.iata.org/IATA/EDIST FieldName,omitempty"`
}

type FieldName struct {
    Mandatory bool   `xml:",attr,omitempty"`
    Value     string `xml:",chardata"`
}

type AircraftMetadataType struct {
    MetadataObjectBaseType
    TailNumber string `xml:"http://www.iata.org/IATA/EDIST TailNumber,omitempty"`
    Name       string `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
}

type AirportMetadataType struct {
    MetadataObjectBaseType
    Contacts   *Contacts     `xml:"http://www.iata.org/IATA/EDIST Contacts,omitempty"`
    Directions *Directions_2 `xml:"http://www.iata.org/IATA/EDIST Directions,omitempty"`
    Position   *PositionType `xml:"http://www.iata.org/IATA/EDIST Position,omitempty"`
}

type Directions_2 struct {
    Direction []*DirectionsType `xml:"http://www.iata.org/IATA/EDIST Direction,omitempty"`
}

type CityMetadataType struct {
    MetadataObjectBaseType
    Directions *Directions_1 `xml:"http://www.iata.org/IATA/EDIST Directions,omitempty"`
    Position   *PositionType `xml:"http://www.iata.org/IATA/EDIST Position,omitempty"`
}

type CodesetMetadataType struct {
    MetadataObjectBaseType
    Source        *Source        `xml:"http://www.iata.org/IATA/EDIST Source,omitempty"`
    OtherLanguage *OtherLanguage `xml:"http://www.iata.org/IATA/EDIST OtherLanguage,omitempty"`
}

type Source struct {
    OwnerID *OwnerID `xml:"http://www.iata.org/IATA/EDIST OwnerID,omitempty"`
    File    string   `xml:"http://www.iata.org/IATA/EDIST File,omitempty"`
    Version string   `xml:"http://www.iata.org/IATA/EDIST Version,omitempty"`
}

type OwnerID struct {
    Name  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type OtherLanguage struct {
    LanguageCode *LanguageCodeType `xml:"http://www.iata.org/IATA/EDIST LanguageCode,omitempty"`
    Description  string            `xml:"http://www.iata.org/IATA/EDIST Description,omitempty"`
}

type ContactMetadataType struct {
    MetadataObjectBaseType
}

type ContentMetadataType struct {
    MetadataObjectBaseType
}

type CountryMetadataType struct {
    MetadataObjectBaseType
    ICAO_Code string        `xml:"http://www.iata.org/IATA/EDIST ICAO_Code,omitempty"`
    Name      string        `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
    Position  *PositionType `xml:"http://www.iata.org/IATA/EDIST Position,omitempty"`
}

type CurrencyMetadataType struct {
    MetadataObjectBaseType
    Application string `xml:"http://www.iata.org/IATA/EDIST Application,omitempty"`
    Decimals    int    `xml:"http://www.iata.org/IATA/EDIST Decimals,omitempty"`
    Name        string `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
}

type DescriptionMetadataType struct {
    MetadataObjectBaseType
    Application string `xml:"http://www.iata.org/IATA/EDIST Application,omitempty"`
    Topic       string `xml:"http://www.iata.org/IATA/EDIST Topic,omitempty"`
    Hint        string `xml:"http://www.iata.org/IATA/EDIST Hint,omitempty"`
    Copyright   string `xml:"http://www.iata.org/IATA/EDIST Copyright,omitempty"`
    Sequence    int    `xml:"http://www.iata.org/IATA/EDIST Sequence,omitempty"`
}

type EquivalentID_MetadataType struct {
    MetadataObjectBaseType
    EquivID []*EquivID `xml:"http://www.iata.org/IATA/EDIST EquivID,omitempty"`
}

type EquivID struct {
    EquivalentID_Key string `xml:"http://www.iata.org/IATA/EDIST EquivalentID_Key,omitempty"`
    ID_Value         string `xml:"http://www.iata.org/IATA/EDIST ID_Value,omitempty"`
    Owner            string `xml:"http://www.iata.org/IATA/EDIST Owner,omitempty"`
    Metarefs         string `xml:",attr,omitempty"`
    Refs             string `xml:",attr,omitempty"`
    MetadataKey      string `xml:",attr,omitempty"`
}

type LanguageMetadataType struct {
    MetadataObjectBaseType
    Application string `xml:"http://www.iata.org/IATA/EDIST Application,omitempty"`
    Code_ISO    string `xml:"http://www.iata.org/IATA/EDIST Code_ISO,omitempty"`
    Code_NLS    string `xml:"http://www.iata.org/IATA/EDIST Code_NLS,omitempty"`
    Name        string `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
}

type PaymentCardMetadataType struct {
    CardCode    string       `xml:"http://www.iata.org/IATA/EDIST CardCode,omitempty"`
    CardName    string       `xml:"http://www.iata.org/IATA/EDIST CardName,omitempty"`
    CardType    string       `xml:"http://www.iata.org/IATA/EDIST CardType,omitempty"`
    CardFields  *CardFields  `xml:"http://www.iata.org/IATA/EDIST CardFields,omitempty"`
    Surcharge   *Surcharge_1 `xml:"http://www.iata.org/IATA/EDIST Surcharge,omitempty"`
    Refs        string       `xml:",attr,omitempty"`
    MetadataKey string       `xml:",attr,omitempty"`
}

type CardFields struct {
    FieldName []*FieldName_1 `xml:"http://www.iata.org/IATA/EDIST FieldName,omitempty"`
}

type FieldName_1 struct {
    Mandatory bool   `xml:",attr,omitempty"`
    Value     string `xml:",chardata"`
}

type Surcharge_1 struct {
    Amount          *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST Amount,omitempty"`
    PercentageValue float64                `xml:"http://www.iata.org/IATA/EDIST PercentageValue,omitempty"`
}

type PaymentFormMetadataType struct {
    MetadataObjectBaseType
    CustomerFileReference string `xml:"http://www.iata.org/IATA/EDIST CustomerFileReference,omitempty"`
    ExtendedPaymentCode   int    `xml:"http://www.iata.org/IATA/EDIST ExtendedPaymentCode,omitempty"`
    Text                  string `xml:"http://www.iata.org/IATA/EDIST Text,omitempty"`
    CorporateContractCode string `xml:"http://www.iata.org/IATA/EDIST CorporateContractCode,omitempty"`
}

type PenaltyMetadataType struct {
    MetadataObjectBaseType
}

type PriceMetadataType struct {
    MetadataObjectBaseType
}

type RuleMetadataType struct {
    MetadataObjectBaseType
    RuleID  string      `xml:"http://www.iata.org/IATA/EDIST RuleID,omitempty"`
    Name    string      `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
    Status  string      `xml:"http://www.iata.org/IATA/EDIST Status,omitempty"`
    Values  *Values     `xml:"http://www.iata.org/IATA/EDIST Values,omitempty"`
    Remarks *RemarkType `xml:"http://www.iata.org/IATA/EDIST Remarks,omitempty"`
}

type Values struct {
    Value []*Value_2 `xml:"http://www.iata.org/IATA/EDIST Value,omitempty"`
}

type Value_2 struct {
    NodePath    []*NodePathType `xml:"http://www.iata.org/IATA/EDIST NodePath,omitempty"`
    Instruction string          `xml:"http://www.iata.org/IATA/EDIST Instruction,omitempty"`
}

type StateProvMetadataType struct {
    MetadataObjectBaseType
    Position *PositionType `xml:"http://www.iata.org/IATA/EDIST Position,omitempty"`
}

type ZoneMetadataType struct {
    MetadataObjectBaseType
    Position *PositionType `xml:"http://www.iata.org/IATA/EDIST Position,omitempty"`
}

type Baggage struct {
    CheckedBagMetadatas        *CheckedBagMetadatas        `xml:"http://www.iata.org/IATA/EDIST CheckedBagMetadatas,omitempty"`
    CarryOnBagMetadatas        *CarryOnBagMetadatas        `xml:"http://www.iata.org/IATA/EDIST CarryOnBagMetadatas,omitempty"`
    BaggageDisclosureMetadatas *BaggageDisclosureMetadatas `xml:"http://www.iata.org/IATA/EDIST BaggageDisclosureMetadatas,omitempty"`
    BaggageDetailMetadata      *BaggageDetailMetadataType  `xml:"http://www.iata.org/IATA/EDIST BaggageDetailMetadata,omitempty"`
    BaggageQueryMetadata       *BaggageQueryMetadataType   `xml:"http://www.iata.org/IATA/EDIST BaggageQueryMetadata,omitempty"`
}

type Flight_2 struct {
    FlightMetadatas   *FlightMetadatas       `xml:"http://www.iata.org/IATA/EDIST FlightMetadatas,omitempty"`
    ItineraryMetadata *ItineraryMetadataType `xml:"http://www.iata.org/IATA/EDIST ItineraryMetadata,omitempty"`
}

type Location_2 struct {
    DirectionMetadatas *DirectionMetadatas `xml:"http://www.iata.org/IATA/EDIST DirectionMetadatas,omitempty"`
}

type Offer_2 struct {
    DisclosureMetadatas       *DisclosureMetadatas       `xml:"http://www.iata.org/IATA/EDIST DisclosureMetadatas,omitempty"`
    OfferMetadatas            *OfferMetadatas            `xml:"http://www.iata.org/IATA/EDIST OfferMetadatas,omitempty"`
    OfferInstructionMetadatas *OfferInstructionMetadatas `xml:"http://www.iata.org/IATA/EDIST OfferInstructionMetadatas,omitempty"`
    OfferPenaltyMetadatas     *OfferPenaltyMetadatas     `xml:"http://www.iata.org/IATA/EDIST OfferPenaltyMetadatas,omitempty"`
    OfferTermsMetadatas       *OfferTermsMetadatas       `xml:"http://www.iata.org/IATA/EDIST OfferTermsMetadatas,omitempty"`
}

type Pricing struct {
    DiscountMetadatas *DiscountMetadatas `xml:"http://www.iata.org/IATA/EDIST DiscountMetadatas,omitempty"`
}

type Qualifier struct {
    BaggagePricingQualifier *BaggagePricingQualifierType `xml:"http://www.iata.org/IATA/EDIST BaggagePricingQualifier,omitempty"`
    ExistingOrderQualifier  *ExistingOrderQualifier      `xml:"http://www.iata.org/IATA/EDIST ExistingOrderQualifier,omitempty"`
    PaymentCardQualifiers   *CardQualifierType           `xml:"http://www.iata.org/IATA/EDIST PaymentCardQualifiers,omitempty"`
    ProgramQualifiers       *ProgramQualifiers           `xml:"http://www.iata.org/IATA/EDIST ProgramQualifiers,omitempty"`
    PromotionQualifiers     *PromotionQualifiers         `xml:"http://www.iata.org/IATA/EDIST PromotionQualifiers,omitempty"`
    SeatQualifier           *SeatQualifier               `xml:"http://www.iata.org/IATA/EDIST SeatQualifier,omitempty"`
    ServiceQualifier        *ServiceQualifierPriceType   `xml:"http://www.iata.org/IATA/EDIST ServiceQualifier,omitempty"`
    SocialMediaQualifiers   *SocialMediaQualifierType    `xml:"http://www.iata.org/IATA/EDIST SocialMediaQualifiers,omitempty"`
    SpecialFareQualifiers   *FareQualifierType           `xml:"http://www.iata.org/IATA/EDIST SpecialFareQualifiers,omitempty"`
    SpecialNeedQualifiers   *SpecialType                 `xml:"http://www.iata.org/IATA/EDIST SpecialNeedQualifiers,omitempty"`
    TripPurposeQualifier    string                       `xml:"http://www.iata.org/IATA/EDIST TripPurposeQualifier,omitempty"`
}

type Seat struct {
    SeatMetadatas    *SeatMetadatas    `xml:"http://www.iata.org/IATA/EDIST SeatMetadatas,omitempty"`
    SeatMapMetadatas *SeatMapMetadatas `xml:"http://www.iata.org/IATA/EDIST SeatMapMetadatas,omitempty"`
}

type ShopMetadataGroup struct {
    Baggage   *Baggage    `xml:"http://www.iata.org/IATA/EDIST Baggage,omitempty"`
    Fare      *FareType   `xml:"http://www.iata.org/IATA/EDIST Fare,omitempty"`
    Flight    *Flight_2   `xml:"http://www.iata.org/IATA/EDIST Flight,omitempty"`
    Location  *Location_2 `xml:"http://www.iata.org/IATA/EDIST Location,omitempty"`
    Offer     *Offer_2    `xml:"http://www.iata.org/IATA/EDIST Offer,omitempty"`
    Pricing   *Pricing    `xml:"http://www.iata.org/IATA/EDIST Pricing,omitempty"`
    Qualifier *Qualifier  `xml:"http://www.iata.org/IATA/EDIST Qualifier,omitempty"`
    Seat      *Seat       `xml:"http://www.iata.org/IATA/EDIST Seat,omitempty"`
}

type Traveler struct {
    TravelerMetadata *TravelerMetadataType `xml:"http://www.iata.org/IATA/EDIST TravelerMetadata,omitempty"`
}

type BaggageDetailMetadata BaggageDetailMetadataType

type BaggageDisclosureMetadatas struct {
    BaggageDisclosureMetadata []*MetadataObjectBaseType `xml:"http://www.iata.org/IATA/EDIST BaggageDisclosureMetadata,omitempty"`
}

type BaggageDisclosureMetadata BaggageDisclosureMetadataType

type BaggageQueryMetadata BaggageQueryMetadataType

type CarryOnBagMetadatas struct {
    CarryOnBagMetadata []*MetadataObjectBaseType `xml:"http://www.iata.org/IATA/EDIST CarryOnBagMetadata,omitempty"`
}

type CarryOnBagMetadata BaggageCarryOnMetadataType

type CheckedBagMetadatas struct {
    CheckedBagMetadata []*MetadataObjectBaseType `xml:"http://www.iata.org/IATA/EDIST CheckedBagMetadata,omitempty"`
}

type CheckedBagMetadata BaggageCheckedMetadataType

type FlightMetadatas struct {
    FlightMetadata []*FlightMetadataType `xml:"http://www.iata.org/IATA/EDIST FlightMetadata,omitempty"`
}

type FlightMetadata FlightMetadataType

type ItineraryMetadata ItineraryMetadataType

type DirectionMetadatas struct {
    DirectionMetadata []*MetadataObjectBaseType `xml:"http://www.iata.org/IATA/EDIST DirectionMetadata,omitempty"`
}

type DirectionMetadata DirectionsMetadataType

type DisclosureMetadatas struct {
    DisclosureMetadata []*DisclosureMetadataType `xml:"http://www.iata.org/IATA/EDIST DisclosureMetadata,omitempty"`
}

type DisclosureMetadata DisclosureMetadataType

type OfferInstructionMetadatas struct {
    OfferInstructionMetadata []*MetadataObjectBaseType `xml:"http://www.iata.org/IATA/EDIST OfferInstructionMetadata,omitempty"`
}

type OfferInstructionMetadata OfferInstructionMetadataType

type OfferMetadatas struct {
    OfferMetadata []*OfferItemMetadataType `xml:"http://www.iata.org/IATA/EDIST OfferMetadata,omitempty"`
}

type OfferMetadata OfferItemMetadataType

type OfferPenaltyMetadatas struct {
    OfferPenaltyMetadata []*MetadataObjectBaseType `xml:"http://www.iata.org/IATA/EDIST OfferPenaltyMetadata,omitempty"`
}

type OfferPenaltyMetadata PenaltyMetadataType

type OfferTermsMetadatas struct {
    OfferTermsMetadata []*MetadataObjectBaseType `xml:"http://www.iata.org/IATA/EDIST OfferTermsMetadata,omitempty"`
}

type OfferTermsMetadata OfferTermsMetadataType

type DiscountMetadatas struct {
    DiscountMetadata []*DisclosureMetadataType `xml:"http://www.iata.org/IATA/EDIST DiscountMetadata,omitempty"`
}

type DiscountMetadata DisclosureMetadataType

type SeatMetadatas struct {
    SeatMetadata []*SeatMetadataType `xml:"http://www.iata.org/IATA/EDIST SeatMetadata,omitempty"`
}

type SeatMetadata SeatMetadataType

type SeatMapMetadatas struct {
    SeatMapMetadata []*MetadataObjectBaseType `xml:"http://www.iata.org/IATA/EDIST SeatMapMetadata,omitempty"`
}

type SeatMapMetadata SeatMapMetadataType

type PenaltyMetadatas struct {
    PenaltyMetadata []*MetadataObjectBaseType `xml:"http://www.iata.org/IATA/EDIST PenaltyMetadata,omitempty"`
}

type PenaltyMetadata PenaltyMetadataType

type BaggageCarryOnMetadataType struct {
    MetadataObjectBaseType
}

type BaggageCheckedMetadataType struct {
    MetadataObjectBaseType
}

type BaggageDetailMetadataType struct {
    MetadataObjectBaseType
    Attributes *AttributesType `xml:"http://www.iata.org/IATA/EDIST Attributes,omitempty"`
    FareTariff *FareTariff     `xml:"http://www.iata.org/IATA/EDIST FareTariff,omitempty"`
    FareRule   string          `xml:"http://www.iata.org/IATA/EDIST FareRule,omitempty"`
    FareCat    string          `xml:"http://www.iata.org/IATA/EDIST FareCat,omitempty"`
}

type FareTariff struct {
    FareType string `xml:",attr,omitempty"`
    Value    string `xml:",chardata"`
}

type BaggageDisclosureMetadataType struct {
    MetadataObjectBaseType
}

type BaggageQueryMetadataType struct {
    MetadataObjectBaseType
    TicketIssuePlace   string                 `xml:"http://www.iata.org/IATA/EDIST TicketIssuePlace,omitempty"`
    TicketIssueCountry string                 `xml:"http://www.iata.org/IATA/EDIST TicketIssueCountry,omitempty"`
    TravelerCount      int                    `xml:"http://www.iata.org/IATA/EDIST TravelerCount,omitempty"`
    TotalPrice         *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST TotalPrice,omitempty"`
}

type DirectionsMetadataType struct {
    MetadataObjectBaseType
}

type DisclosureMetadataType struct {
    MetadataObjectBaseType
    Timestamp string `xml:"http://www.iata.org/IATA/EDIST Timestamp,omitempty"`
}

type FlightMetadataType struct {
    MetadataObjectBaseType
    ActionCode        string             `xml:"http://www.iata.org/IATA/EDIST ActionCode,omitempty"`
    BindingKey        string             `xml:"http://www.iata.org/IATA/EDIST BindingKey,omitempty"`
    FlightStatus      *CodesetType       `xml:"http://www.iata.org/IATA/EDIST FlightStatus,omitempty"`
    Frequency         string             `xml:"http://www.iata.org/IATA/EDIST Frequency,omitempty"`
    InstantPurchase   *CodesetType       `xml:"http://www.iata.org/IATA/EDIST InstantPurchase,omitempty"`
    Meals             *Meals             `xml:"http://www.iata.org/IATA/EDIST Meals,omitempty"`
    OnTimePerformance *OnTimePerformance `xml:"http://www.iata.org/IATA/EDIST OnTimePerformance,omitempty"`
    Remarks           *RemarkType        `xml:"http://www.iata.org/IATA/EDIST Remarks,omitempty"`
}

type Meals struct {
    Meal []*FlightMealsType `xml:"http://www.iata.org/IATA/EDIST Meal,omitempty"`
}

type ItineraryMetadataType struct {
    MetadataObjectBaseType
    ActionCode string `xml:"http://www.iata.org/IATA/EDIST ActionCode,omitempty"`
}

type MediaMetadataType struct {
    MetadataObjectBaseType
    Application string        `xml:"http://www.iata.org/IATA/EDIST Application,omitempty"`
    Caption     string        `xml:"http://www.iata.org/IATA/EDIST Caption,omitempty"`
    Hint        string        `xml:"http://www.iata.org/IATA/EDIST Hint,omitempty"`
    Dimensions  *Dimensions_1 `xml:"http://www.iata.org/IATA/EDIST Dimensions,omitempty"`
    MediaSource *MediaSource  `xml:"http://www.iata.org/IATA/EDIST MediaSource,omitempty"`
}

type Dimensions_1 struct {
    UOM    string `xml:"http://www.iata.org/IATA/EDIST UOM,omitempty"`
    Height string `xml:"http://www.iata.org/IATA/EDIST Height,omitempty"`
    Width  string `xml:"http://www.iata.org/IATA/EDIST Width,omitempty"`
    Format string `xml:"http://www.iata.org/IATA/EDIST Format,omitempty"`
}

type MediaSource struct {
    Code      string `xml:"http://www.iata.org/IATA/EDIST Code,omitempty"`
    Version   string `xml:"http://www.iata.org/IATA/EDIST Version,omitempty"`
    FileName  string `xml:"http://www.iata.org/IATA/EDIST FileName,omitempty"`
    Symbology string `xml:"http://www.iata.org/IATA/EDIST Symbology,omitempty"`
    Copyright string `xml:"http://www.iata.org/IATA/EDIST Copyright,omitempty"`
}

type OfferItemMetadataType struct {
    MetadataObjectBaseType
    ATPCO      *ATPCO              `xml:"http://www.iata.org/IATA/EDIST ATPCO,omitempty"`
    Incentives *OfferIncentiveType `xml:"http://www.iata.org/IATA/EDIST Incentives,omitempty"`
    Matches    *Matches            `xml:"http://www.iata.org/IATA/EDIST Matches,omitempty"`
    Terms      *Terms              `xml:"http://www.iata.org/IATA/EDIST Terms,omitempty"`
    TimeLimits *TimeLimits         `xml:"http://www.iata.org/IATA/EDIST TimeLimits,omitempty"`
    Rule       *Rule               `xml:"http://www.iata.org/IATA/EDIST Rule,omitempty"`
    Status     *OfferStatusType    `xml:"http://www.iata.org/IATA/EDIST Status,omitempty"`
}

type ATPCO struct {
    Attributes        *AttributesType   `xml:"http://www.iata.org/IATA/EDIST Attributes,omitempty"`
    AugmentationPoint *AugPointInfoType `xml:"http://www.iata.org/IATA/EDIST AugmentationPoint,omitempty"`
}

type Matches struct {
    Match []*Match `xml:"http://www.iata.org/IATA/EDIST Match,omitempty"`
}

type Match struct {
    AugmentationPoint *AugPointInfoType `xml:"http://www.iata.org/IATA/EDIST AugmentationPoint,omitempty"`
}

type Terms struct {
    AugmentationPoint *AugPointInfoType `xml:"http://www.iata.org/IATA/EDIST AugmentationPoint,omitempty"`
}

type TimeLimits struct {
    AugmentationPoint *AugPointInfoType `xml:"http://www.iata.org/IATA/EDIST AugmentationPoint,omitempty"`
}

type Rule struct {
    AugmentationPoint *AugPointInfoType `xml:"http://www.iata.org/IATA/EDIST AugmentationPoint,omitempty"`
}

type OfferInstructionMetadataType struct {
    MetadataObjectBaseType
}

type OfferTermsMetadataType struct {
    MetadataObjectBaseType
}

type SeatMetadataType struct {
    MetadataObjectBaseType
    SeatStatus *CodesetType `xml:"http://www.iata.org/IATA/EDIST SeatStatus,omitempty"`
}

type SeatMapMetadataType struct {
    MetadataObjectBaseType
}

type PassengerMetadata PassengerMetadataType

type TravelerMetadata TravelerMetadataType

type PassengerMetadataType struct {
    MetadataObjectBaseType
    NameDetail *NameDetail `xml:"http://www.iata.org/IATA/EDIST NameDetail,omitempty"`
}

type NameDetail struct {
    GivenNamePrefix []string `xml:"http://www.iata.org/IATA/EDIST GivenNamePrefix,omitempty"`
    TitleSuffix     []string `xml:"http://www.iata.org/IATA/EDIST TitleSuffix,omitempty"`
    SurnamePrefix   string   `xml:"http://www.iata.org/IATA/EDIST SurnamePrefix,omitempty"`
    SurnameSuffix   []string `xml:"http://www.iata.org/IATA/EDIST SurnameSuffix,omitempty"`
}

type TravelerMetadataType struct {
    MetadataObjectBaseType
    NameDetail *NameDetail_1 `xml:"http://www.iata.org/IATA/EDIST NameDetail,omitempty"`
}

type NameDetail_1 struct {
    GivenNamePrefix []string `xml:"http://www.iata.org/IATA/EDIST GivenNamePrefix,omitempty"`
    TitleSuffix     []string `xml:"http://www.iata.org/IATA/EDIST TitleSuffix,omitempty"`
    SurnamePrefix   string   `xml:"http://www.iata.org/IATA/EDIST SurnamePrefix,omitempty"`
    SurnameSuffix   []string `xml:"http://www.iata.org/IATA/EDIST SurnameSuffix,omitempty"`
}

type MatchAppListType string

type MatchAppSimpleType string

type MatchResultsListType string

type MatchResultsSimpleType string

type OtherOfferAssocSimpleType string

type OtherAssocListType string

type ApplicableFlight FlightInfoAssocType

type AssociatedService_1 ServiceInfoAssocType

type AssociatedTraveler TravelerInfoAssocType

type FlightInfoAssocType struct {
    AllSegmentInd               bool                      `xml:"http://www.iata.org/IATA/EDIST AllSegmentInd,omitempty"`
    FlightSegmentReference      []*FlightSegmentReference `xml:"http://www.iata.org/IATA/EDIST FlightSegmentReference,omitempty"`
    AllOriginDestinationInd     bool                      `xml:"http://www.iata.org/IATA/EDIST AllOriginDestinationInd,omitempty"`
    OriginDestinationReferences string                    `xml:"http://www.iata.org/IATA/EDIST OriginDestinationReferences,omitempty"`
    AllFlightInd                bool                      `xml:"http://www.iata.org/IATA/EDIST AllFlightInd,omitempty"`
    FlightReferences            *FlightReferences         `xml:"http://www.iata.org/IATA/EDIST FlightReferences,omitempty"`
}

type BagOfferAssocType struct {
    AssociatedTraveler     *TravelerInfoAssocType    `xml:"http://www.iata.org/IATA/EDIST AssociatedTraveler,omitempty"`
    ApplicableFlight       *FlightInfoAssocType      `xml:"http://www.iata.org/IATA/EDIST ApplicableFlight,omitempty"`
    OfferDetailAssociation *OfferDetailInfoAssocType `xml:"http://www.iata.org/IATA/EDIST OfferDetailAssociation,omitempty"`
    BagDetailAssociation   *BagDetailAssociation     `xml:"http://www.iata.org/IATA/EDIST BagDetailAssociation,omitempty"`
    AssociatedService      *ServiceInfoAssocType     `xml:"http://www.iata.org/IATA/EDIST AssociatedService,omitempty"`
    OtherAssociation       *OtherOfferAssocType      `xml:"http://www.iata.org/IATA/EDIST OtherAssociation,omitempty"`
}

type IncludedService ServiceInfoAssocType

type MediaAssocType struct {
    MediaGroupreference string        `xml:"http://www.iata.org/IATA/EDIST MediaGroupreference,omitempty"`
    MediaItems          []*MediaItems `xml:"http://www.iata.org/IATA/EDIST MediaItems,omitempty"`
}

type MediaItems struct {
    MediaItemReference string `xml:"http://www.iata.org/IATA/EDIST MediaItemReference,omitempty"`
    MediaLink          string `xml:"http://www.iata.org/IATA/EDIST MediaLink,omitempty"`
}

type OfferDetailAssociation OfferDetailInfoAssocType

type OtherAssociation_2 OtherOfferAssocType

type OfferAssociations OfferAssociationsType

type OfferAssociationsType struct {
    Shopper      *ShopperInfoAssocType     `xml:"http://www.iata.org/IATA/EDIST Shopper,omitempty"`
    Flight       *FlightInfoAssocType      `xml:"http://www.iata.org/IATA/EDIST Flight,omitempty"`
    PriceClass   *PriceClass_1             `xml:"http://www.iata.org/IATA/EDIST PriceClass,omitempty"`
    BagDetails   *BagDetailAssocType       `xml:"http://www.iata.org/IATA/EDIST BagDetails,omitempty"`
    OfferDetails *OfferDetailInfoAssocType `xml:"http://www.iata.org/IATA/EDIST OfferDetails,omitempty"`
    Services     *Services                 `xml:"http://www.iata.org/IATA/EDIST Services,omitempty"`
    Media        *MediaAssocType           `xml:"http://www.iata.org/IATA/EDIST Media,omitempty"`
    Other        *OtherOfferAssocType      `xml:"http://www.iata.org/IATA/EDIST Other,omitempty"`
}

type PriceClass_1 struct {
    PriceClassReference string `xml:"http://www.iata.org/IATA/EDIST PriceClassReference,omitempty"`
}

type Services struct {
    Service []*Service_1 `xml:"http://www.iata.org/IATA/EDIST Service,omitempty"`
}

type Service_1 struct {
    BundleReference   string `xml:"http://www.iata.org/IATA/EDIST BundleReference,omitempty"`
    ServiceReferences string `xml:"http://www.iata.org/IATA/EDIST ServiceReferences,omitempty"`
}

type OfferDetailInfoAssocType struct {
    OfferInstructionReferences string `xml:"http://www.iata.org/IATA/EDIST OfferInstructionReferences,omitempty"`
    OfferPenaltyReferences     string `xml:"http://www.iata.org/IATA/EDIST OfferPenaltyReferences,omitempty"`
    OfferTermReferences        string `xml:"http://www.iata.org/IATA/EDIST OfferTermReferences,omitempty"`
}

type OtherOfferAssocType struct {
    OtherAssociation []*OtherAssociation_3 `xml:"http://www.iata.org/IATA/EDIST OtherAssociation,omitempty"`
}

type OtherAssociation_3 struct {
    Type           string `xml:"http://www.iata.org/IATA/EDIST Type,omitempty"`
    ReferenceValue string `xml:"http://www.iata.org/IATA/EDIST ReferenceValue,omitempty"`
}

type PricedFlightOfferAssocType struct {
    AssociatedTraveler     *TravelerInfoAssocType    `xml:"http://www.iata.org/IATA/EDIST AssociatedTraveler,omitempty"`
    ApplicableFlight       *FlightInfoAssocType      `xml:"http://www.iata.org/IATA/EDIST ApplicableFlight,omitempty"`
    PriceClass             *PriceClass_2             `xml:"http://www.iata.org/IATA/EDIST PriceClass,omitempty"`
    OfferDetailAssociation *OfferDetailInfoAssocType `xml:"http://www.iata.org/IATA/EDIST OfferDetailAssociation,omitempty"`
    IncludedService        *ServiceInfoAssocType     `xml:"http://www.iata.org/IATA/EDIST IncludedService,omitempty"`
    AssociatedService      *ServiceInfoAssocType     `xml:"http://www.iata.org/IATA/EDIST AssociatedService,omitempty"`
    OtherAssociation       *OtherOfferAssocType      `xml:"http://www.iata.org/IATA/EDIST OtherAssociation,omitempty"`
}

type PriceClass_2 struct {
    PriceClassReference string `xml:"http://www.iata.org/IATA/EDIST PriceClassReference,omitempty"`
}

type SeatOfferAssocType struct {
    AssociatedTraveler     *TravelerInfoAssocType    `xml:"http://www.iata.org/IATA/EDIST AssociatedTraveler,omitempty"`
    ApplicableFlight       *FlightInfoAssocType      `xml:"http://www.iata.org/IATA/EDIST ApplicableFlight,omitempty"`
    AssociatedService      *ServiceInfoAssocType     `xml:"http://www.iata.org/IATA/EDIST AssociatedService,omitempty"`
    OfferDetailAssociation *OfferDetailInfoAssocType `xml:"http://www.iata.org/IATA/EDIST OfferDetailAssociation,omitempty"`
    OtherAssociation       *OtherOfferAssocType      `xml:"http://www.iata.org/IATA/EDIST OtherAssociation,omitempty"`
}

type ServiceInfoAssocType struct {
    BundleReference   string `xml:"http://www.iata.org/IATA/EDIST BundleReference,omitempty"`
    ServiceReferences string `xml:"http://www.iata.org/IATA/EDIST ServiceReferences,omitempty"`
}

type SimpleFlightAssocType struct {
    OriginDestinationReference string            `xml:"http://www.iata.org/IATA/EDIST OriginDestinationReference,omitempty"`
    FlightReferences           *FlightReferences `xml:"http://www.iata.org/IATA/EDIST FlightReferences,omitempty"`
    FlightSegmentReferences    string            `xml:"http://www.iata.org/IATA/EDIST FlightSegmentReferences,omitempty"`
}

type ShopperInfoAssocType struct {
    TravelerReference string `xml:"http://www.iata.org/IATA/EDIST TravelerReference,omitempty"`
    AllShopperInd     bool   `xml:",attr,omitempty"`
}

type TravelerInfoAssocType struct {
    AllTravelerInd     bool   `xml:"http://www.iata.org/IATA/EDIST AllTravelerInd,omitempty"`
    TravelerReferences string `xml:"http://www.iata.org/IATA/EDIST TravelerReferences,omitempty"`
}

type BaggageOffer BaggageOfferType

type FarePriceDetail FarePriceDetailType

type FareLeadPrices OfferPriceLeadType

type OtherOffer OtherOfferType

type PricedOffer PricedFlightOfferType

type SeatOffer SeatOfferType

type SeatCharacteristics SeatCharacteristicType

type BaggageOfferType struct {
    TotalPrice        *TotalPrice_2      `xml:"http://www.iata.org/IATA/EDIST TotalPrice,omitempty"`
    Associations      *BagOfferAssocType `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
    ValidatingCarrier string             `xml:"http://www.iata.org/IATA/EDIST ValidatingCarrier,omitempty"`
    BagDetails        *BagDetails        `xml:"http://www.iata.org/IATA/EDIST BagDetails,omitempty"`
}

type TotalPrice_2 struct {
    SimpleCurrencyPrice  *CurrencyAmountOptType     `xml:"http://www.iata.org/IATA/EDIST SimpleCurrencyPrice,omitempty"`
    EncodedCurrencyPrice *CurrencyAmountEncodedType `xml:"http://www.iata.org/IATA/EDIST EncodedCurrencyPrice,omitempty"`
    DetailCurrencyPrice  *DetailCurrencyPriceType   `xml:"http://www.iata.org/IATA/EDIST DetailCurrencyPrice,omitempty"`
    CombinationPricing   *CombinationPriceType      `xml:"http://www.iata.org/IATA/EDIST CombinationPricing,omitempty"`
    AwardPricing         *AwardPriceUnitType        `xml:"http://www.iata.org/IATA/EDIST AwardPricing,omitempty"`
}

type BagDetails struct {
    BagDetail []*BagDetail `xml:"http://www.iata.org/IATA/EDIST BagDetail,omitempty"`
}

type BagDetail struct {
    ValidatingCarrier string                   `xml:"http://www.iata.org/IATA/EDIST ValidatingCarrier,omitempty"`
    Associations      *BagOfferAssocType       `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
    CheckedBags       *CheckedBagAllowanceType `xml:"http://www.iata.org/IATA/EDIST CheckedBags,omitempty"`
    CarryOnBags       *CarryOnAllowanceType    `xml:"http://www.iata.org/IATA/EDIST CarryOnBags,omitempty"`
    Disclosure        *BagDisclosureType       `xml:"http://www.iata.org/IATA/EDIST Disclosure,omitempty"`
    Price             *Price                   `xml:"http://www.iata.org/IATA/EDIST Price,omitempty"`
}

type Price struct {
    SimpleCurrencyPrice  *CurrencyAmountOptType     `xml:"http://www.iata.org/IATA/EDIST SimpleCurrencyPrice,omitempty"`
    EncodedCurrencyPrice *CurrencyAmountEncodedType `xml:"http://www.iata.org/IATA/EDIST EncodedCurrencyPrice,omitempty"`
    DetailCurrencyPrice  *DetailCurrencyPriceType   `xml:"http://www.iata.org/IATA/EDIST DetailCurrencyPrice,omitempty"`
    CombinationPricing   *CombinationPriceType      `xml:"http://www.iata.org/IATA/EDIST CombinationPricing,omitempty"`
    AwardPricing         *AwardPriceUnitType        `xml:"http://www.iata.org/IATA/EDIST AwardPricing,omitempty"`
}

type OtherOfferType struct {
    OfferPrice  *OfferPrice         `xml:"http://www.iata.org/IATA/EDIST OfferPrice,omitempty"`
    Association *SeatOfferAssocType `xml:"http://www.iata.org/IATA/EDIST Association,omitempty"`
}

type OfferPrice struct {
    SimpleCurrencyPrice  *CurrencyAmountOptType     `xml:"http://www.iata.org/IATA/EDIST SimpleCurrencyPrice,omitempty"`
    EncodedCurrencyPrice *CurrencyAmountEncodedType `xml:"http://www.iata.org/IATA/EDIST EncodedCurrencyPrice,omitempty"`
    DetailCurrencyPrice  *DetailCurrencyPriceType   `xml:"http://www.iata.org/IATA/EDIST DetailCurrencyPrice,omitempty"`
    CombinationPricing   *CombinationPriceType      `xml:"http://www.iata.org/IATA/EDIST CombinationPricing,omitempty"`
    AwardPricing         *AwardPriceUnitType        `xml:"http://www.iata.org/IATA/EDIST AwardPricing,omitempty"`
}

type PricedFlightOfferType struct {
    KeyWithMetaOfferBaseType
    OfferPrice   []*OfferPriceLeadType         `xml:"http://www.iata.org/IATA/EDIST OfferPrice,omitempty"`
    Associations []*PricedFlightOfferAssocType `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
}

type SeatOfferType struct {
    SeatDetailType
    OfferPrice  *OfferPrice_1       `xml:"http://www.iata.org/IATA/EDIST OfferPrice,omitempty"`
    Association *SeatOfferAssocType `xml:"http://www.iata.org/IATA/EDIST Association,omitempty"`
}

type OfferPrice_1 struct {
    SimpleCurrencyPrice  *CurrencyAmountOptType     `xml:"http://www.iata.org/IATA/EDIST SimpleCurrencyPrice,omitempty"`
    EncodedCurrencyPrice *CurrencyAmountEncodedType `xml:"http://www.iata.org/IATA/EDIST EncodedCurrencyPrice,omitempty"`
    DetailCurrencyPrice  *DetailCurrencyPriceType   `xml:"http://www.iata.org/IATA/EDIST DetailCurrencyPrice,omitempty"`
    CombinationPricing   *CombinationPriceType      `xml:"http://www.iata.org/IATA/EDIST CombinationPricing,omitempty"`
    AwardPricing         *AwardPriceUnitType        `xml:"http://www.iata.org/IATA/EDIST AwardPricing,omitempty"`
}

type OfferType struct {
    OfferItemDetailType
}

type OfferItemCoreType struct {
    Parameters            *Parameters_2          `xml:"http://www.iata.org/IATA/EDIST Parameters,omitempty"`
    OfferID               *ItemID_Type           `xml:"http://www.iata.org/IATA/EDIST OfferID,omitempty"`
    ValidatingCarrier     string                 `xml:"http://www.iata.org/IATA/EDIST ValidatingCarrier,omitempty"`
    TimeLimits            *OfferTimeLimitSetType `xml:"http://www.iata.org/IATA/EDIST TimeLimits,omitempty"`
    TotalPrice            *TotalPrice_3          `xml:"http://www.iata.org/IATA/EDIST TotalPrice,omitempty"`
    DescriptionReferences string                 `xml:"http://www.iata.org/IATA/EDIST DescriptionReferences,omitempty"`
    Disclosure            *DisclosureType        `xml:"http://www.iata.org/IATA/EDIST Disclosure,omitempty"`
    Penalty               *OfferPenaltyType      `xml:"http://www.iata.org/IATA/EDIST Penalty,omitempty"`
    RequestedDateInd      bool                   `xml:",attr,omitempty"`
    Refs                  string                 `xml:",attr,omitempty"`
}

type Parameters_2 struct {
    TotalItemQuantity int                       `xml:"http://www.iata.org/IATA/EDIST TotalItemQuantity,omitempty"`
    PTC_Priced        []*PTC_QuantityPricedType `xml:"http://www.iata.org/IATA/EDIST PTC_Priced,omitempty"`
    ApplyToAllInd     bool                      `xml:",attr,omitempty"`
    RedemptionOnlyInd bool                      `xml:",attr,omitempty"`
}

type TotalPrice_3 struct {
    AwardPricing         *AwardPriceUnitType        `xml:"http://www.iata.org/IATA/EDIST AwardPricing,omitempty"`
    CombinationPricing   *CombinationPriceType      `xml:"http://www.iata.org/IATA/EDIST CombinationPricing,omitempty"`
    SimpleCurrencyPrice  *CurrencyAmountOptType     `xml:"http://www.iata.org/IATA/EDIST SimpleCurrencyPrice,omitempty"`
    DetailCurrencyPrice  *DetailCurrencyPriceType   `xml:"http://www.iata.org/IATA/EDIST DetailCurrencyPrice,omitempty"`
    EncodedCurrencyPrice *CurrencyAmountEncodedType `xml:"http://www.iata.org/IATA/EDIST EncodedCurrencyPrice,omitempty"`
    Refs                 string                     `xml:",attr,omitempty"`
}

type OfferItemDetailType struct {
    OfferItemCoreType
    Match *OfferMatchType `xml:"http://www.iata.org/IATA/EDIST Match,omitempty"`
}

type BaggageOfferCoreType struct {
    TotalPrice        *TotalPrice_4 `xml:"http://www.iata.org/IATA/EDIST TotalPrice,omitempty"`
    ValidatingCarrier string        `xml:"http://www.iata.org/IATA/EDIST ValidatingCarrier,omitempty"`
    BagDetails        *BagDetails_1 `xml:"http://www.iata.org/IATA/EDIST BagDetails,omitempty"`
}

type TotalPrice_4 struct {
    AwardPricing         *AwardPriceUnitType        `xml:"http://www.iata.org/IATA/EDIST AwardPricing,omitempty"`
    CombinationPricing   *CombinationPriceType      `xml:"http://www.iata.org/IATA/EDIST CombinationPricing,omitempty"`
    SimpleCurrencyPrice  *CurrencyAmountOptType     `xml:"http://www.iata.org/IATA/EDIST SimpleCurrencyPrice,omitempty"`
    DetailCurrencyPrice  *DetailCurrencyPriceType   `xml:"http://www.iata.org/IATA/EDIST DetailCurrencyPrice,omitempty"`
    EncodedCurrencyPrice *CurrencyAmountEncodedType `xml:"http://www.iata.org/IATA/EDIST EncodedCurrencyPrice,omitempty"`
}

type BagDetails_1 struct {
    BagDetail []*BagDetail_1 `xml:"http://www.iata.org/IATA/EDIST BagDetail,omitempty"`
}

type BagDetail_1 struct {
    ValidatingCarrier string                   `xml:"http://www.iata.org/IATA/EDIST ValidatingCarrier,omitempty"`
    Associations      *BagOfferAssocType       `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
    CheckedBags       *CheckedBagAllowanceType `xml:"http://www.iata.org/IATA/EDIST CheckedBags,omitempty"`
    CarryOnBags       *CarryOnAllowanceType    `xml:"http://www.iata.org/IATA/EDIST CarryOnBags,omitempty"`
    Disclosure        *BagDisclosureType       `xml:"http://www.iata.org/IATA/EDIST Disclosure,omitempty"`
    Price             *Price_1                 `xml:"http://www.iata.org/IATA/EDIST Price,omitempty"`
}

type Price_1 struct {
    AwardPricing         *AwardPriceUnitType      `xml:"http://www.iata.org/IATA/EDIST AwardPricing,omitempty"`
    CombinationPricing   *CombinationPriceType    `xml:"http://www.iata.org/IATA/EDIST CombinationPricing,omitempty"`
    DetailCurrencyPrice  *DetailCurrencyPriceType `xml:"http://www.iata.org/IATA/EDIST DetailCurrencyPrice,omitempty"`
    EncodedCurrencyPrice *EncodedPriceType        `xml:"http://www.iata.org/IATA/EDIST EncodedCurrencyPrice,omitempty"`
    SimpleCurrencyPrice  *SimpleCurrencyPriceType `xml:"http://www.iata.org/IATA/EDIST SimpleCurrencyPrice,omitempty"`
}

type OtherOfferCoreType struct {
    OfferPrice *OfferPrice_2 `xml:"http://www.iata.org/IATA/EDIST OfferPrice,omitempty"`
}

type OfferPrice_2 struct {
    AwardPricing         *AwardPriceUnitType      `xml:"http://www.iata.org/IATA/EDIST AwardPricing,omitempty"`
    CombinationPricing   *CombinationPriceType    `xml:"http://www.iata.org/IATA/EDIST CombinationPricing,omitempty"`
    DetailCurrencyPrice  *DetailCurrencyPriceType `xml:"http://www.iata.org/IATA/EDIST DetailCurrencyPrice,omitempty"`
    EncodedCurrencyPrice *EncodedPriceType        `xml:"http://www.iata.org/IATA/EDIST EncodedCurrencyPrice,omitempty"`
    SimpleCurrencyPrice  *SimpleCurrencyPriceType `xml:"http://www.iata.org/IATA/EDIST SimpleCurrencyPrice,omitempty"`
}

type PricedFlightOfferCoreType struct {
    OfferPrice        *OfferPriceLeadType `xml:"http://www.iata.org/IATA/EDIST OfferPrice,omitempty"`
    FareDetail        *FareDetailType     `xml:"http://www.iata.org/IATA/EDIST FareDetail,omitempty"`
    LeadPricedInd     bool                `xml:",attr,omitempty"`
    LeadPricingIncInd bool                `xml:",attr,omitempty"`
    Refs              string              `xml:",attr,omitempty"`
}

type SeatOfferCoreType struct {
    SeatDetailType
    OfferPrice *OfferPrice_3 `xml:"http://www.iata.org/IATA/EDIST OfferPrice,omitempty"`
}

type OfferPrice_3 struct {
    AwardPricing         *AwardPriceUnitType        `xml:"http://www.iata.org/IATA/EDIST AwardPricing,omitempty"`
    CombinationPricing   *CombinationPriceType      `xml:"http://www.iata.org/IATA/EDIST CombinationPricing,omitempty"`
    DetailCurrencyPrice  *DetailCurrencyPriceType   `xml:"http://www.iata.org/IATA/EDIST DetailCurrencyPrice,omitempty"`
    EncodedCurrencyPrice *CurrencyAmountEncodedType `xml:"http://www.iata.org/IATA/EDIST EncodedCurrencyPrice,omitempty"`
    SimpleCurrencyPrice  *CurrencyAmountOptType     `xml:"http://www.iata.org/IATA/EDIST SimpleCurrencyPrice,omitempty"`
}

type OfferInstructions OfferInstructionType

type OfferInstructionType struct {
    ClassOfServiceUpgrade     *OfferClassUpgradeType    `xml:"http://www.iata.org/IATA/EDIST ClassOfServiceUpgrade,omitempty"`
    FreeFormTextInstruction   *FreeFormInstructionsType `xml:"http://www.iata.org/IATA/EDIST FreeFormTextInstruction,omitempty"`
    SpecialBookingInstruction *SpecialBookingInstrType  `xml:"http://www.iata.org/IATA/EDIST SpecialBookingInstruction,omitempty"`
}

type ClassOfServiceUpgrade OfferClassUpgradeType

type FreeFormTextInstruction FreeFormInstructionsType

type FreeFormInstructionsType struct {
    InstrRemarkType
}

type OfferClassUpgradeType struct {
    InstrClassUpgradeType
}

type SpecialBookingInstrType struct {
    InstrSpecialBookingType
}

type InvGuaranteeType struct {
    InvGuaranteeTimeLimit *CoreDateGrpType `xml:"http://www.iata.org/IATA/EDIST InvGuaranteeTimeLimit,omitempty"`
    InvGuaranteeID        string           `xml:"http://www.iata.org/IATA/EDIST InvGuaranteeID,omitempty"`
    OfferReferences       string           `xml:"http://www.iata.org/IATA/EDIST OfferReferences,omitempty"`
    Refs                  string           `xml:",attr,omitempty"`
}

type InventoryGuarantee_2 InvGuaranteeType

type OfferPriceLeadType struct {
    RequestedDate             *RequestedDate  `xml:"http://www.iata.org/IATA/EDIST RequestedDate,omitempty"`
    FareDetail                *FareDetailType `xml:"http://www.iata.org/IATA/EDIST FareDetail,omitempty"`
    OfferItemID               string          `xml:",attr,omitempty"`
    ModificationProhibitedInd bool            `xml:",attr,omitempty"`
    Refs                      string          `xml:",attr,omitempty"`
}

type RequestedDate struct {
    OfferPriceLeadDetailType
    Associations []*PricedFlightOfferAssocType `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
}

type OfferPriceLeadDetailType struct {
    PriceDetail *PriceDetail `xml:"http://www.iata.org/IATA/EDIST PriceDetail,omitempty"`
}

type PriceDetail struct {
    TotalAmount                *TotalAmount           `xml:"http://www.iata.org/IATA/EDIST TotalAmount,omitempty"`
    BaseAmount                 *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST BaseAmount,omitempty"`
    FareFiledIn                *FareFilingType        `xml:"http://www.iata.org/IATA/EDIST FareFiledIn,omitempty"`
    Discount                   *DiscountType          `xml:"http://www.iata.org/IATA/EDIST Discount,omitempty"`
    Surcharges                 *Surcharges_1          `xml:"http://www.iata.org/IATA/EDIST Surcharges,omitempty"`
    Taxes                      *TaxDetailType         `xml:"http://www.iata.org/IATA/EDIST Taxes,omitempty"`
    TaxExemption               *TaxExemptionType      `xml:"http://www.iata.org/IATA/EDIST TaxExemption,omitempty"`
    AwardPricing               *AwardPriceUnitType    `xml:"http://www.iata.org/IATA/EDIST AwardPricing,omitempty"`
    CombinationPricing         *CombinationPriceType  `xml:"http://www.iata.org/IATA/EDIST CombinationPricing,omitempty"`
    OriginDestinationReference string                 `xml:"http://www.iata.org/IATA/EDIST OriginDestinationReference,omitempty"`
    SegmentReferences          *SegmentReferences     `xml:"http://www.iata.org/IATA/EDIST SegmentReferences,omitempty"`
    Fees                       *FeeSurchargeType      `xml:"http://www.iata.org/IATA/EDIST Fees,omitempty"`
}

type TotalAmount struct {
    AwardPricing         *AwardPriceUnitType        `xml:"http://www.iata.org/IATA/EDIST AwardPricing,omitempty"`
    CombinationPricing   *CombinationPriceType      `xml:"http://www.iata.org/IATA/EDIST CombinationPricing,omitempty"`
    DetailCurrencyPrice  *DetailCurrencyPriceType   `xml:"http://www.iata.org/IATA/EDIST DetailCurrencyPrice,omitempty"`
    EncodedCurrencyPrice *CurrencyAmountEncodedType `xml:"http://www.iata.org/IATA/EDIST EncodedCurrencyPrice,omitempty"`
    SimpleCurrencyPrice  *CurrencyAmountOptType     `xml:"http://www.iata.org/IATA/EDIST SimpleCurrencyPrice,omitempty"`
}

type Surcharges_1 struct {
    Surcharge []*FeeSurchargeType `xml:"http://www.iata.org/IATA/EDIST Surcharge,omitempty"`
}

type LeadPrice struct {
    LeadDate                   string                 `xml:"http://www.iata.org/IATA/EDIST LeadDate,omitempty"`
    RequestedDate              string                 `xml:"http://www.iata.org/IATA/EDIST RequestedDate,omitempty"`
    OriginDestinationReference string                 `xml:"http://www.iata.org/IATA/EDIST OriginDestinationReference,omitempty"`
    SegmentReference           *SegmentReference      `xml:"http://www.iata.org/IATA/EDIST SegmentReference,omitempty"`
    TotalAmount                *TotalAmount_1         `xml:"http://www.iata.org/IATA/EDIST TotalAmount,omitempty"`
    BaseAmount                 *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST BaseAmount,omitempty"`
    FareFiledIn                *FareFilingType        `xml:"http://www.iata.org/IATA/EDIST FareFiledIn,omitempty"`
    Surcharges                 *Surcharges_2          `xml:"http://www.iata.org/IATA/EDIST Surcharges,omitempty"`
    Taxes                      *TaxDetailType         `xml:"http://www.iata.org/IATA/EDIST Taxes,omitempty"`
    TaxExemption               *TaxExemptionType      `xml:"http://www.iata.org/IATA/EDIST TaxExemption,omitempty"`
    AwardPricing               *AwardPriceUnitType    `xml:"http://www.iata.org/IATA/EDIST AwardPricing,omitempty"`
    CombinationPricing         *CombinationPriceType  `xml:"http://www.iata.org/IATA/EDIST CombinationPricing,omitempty"`
}

type TotalAmount_1 struct {
    AwardPricing         *AwardPriceUnitType        `xml:"http://www.iata.org/IATA/EDIST AwardPricing,omitempty"`
    CombinationPricing   *CombinationPriceType      `xml:"http://www.iata.org/IATA/EDIST CombinationPricing,omitempty"`
    SimpleCurrencyPrice  *CurrencyAmountOptType     `xml:"http://www.iata.org/IATA/EDIST SimpleCurrencyPrice,omitempty"`
    DetailCurrencyPrice  *DetailCurrencyPriceType   `xml:"http://www.iata.org/IATA/EDIST DetailCurrencyPrice,omitempty"`
    EncodedCurrencyPrice *CurrencyAmountEncodedType `xml:"http://www.iata.org/IATA/EDIST EncodedCurrencyPrice,omitempty"`
}

type Surcharges_2 struct {
    Surcharge []*FeeSurchargeType `xml:"http://www.iata.org/IATA/EDIST Surcharge,omitempty"`
}

type Match_1 OfferMatchType

type OfferMatchType struct {
    Application string  `xml:"http://www.iata.org/IATA/EDIST Application,omitempty"`
    MatchResult string  `xml:"http://www.iata.org/IATA/EDIST MatchResult,omitempty"`
    Percentage  float64 `xml:"http://www.iata.org/IATA/EDIST Percentage,omitempty"`
    Refs        string  `xml:",attr,omitempty"`
}

type PTC_Quantity PTC_QuantityType

type PTC_Priced PTC_QuantityPricedType

type PTC_QuantityType struct {
    Quantity int    `xml:",attr,omitempty"`
    Value    string `xml:",chardata"`
}

type PTC_QuantityPricedType struct {
    Requested *PTC_QuantityType `xml:"http://www.iata.org/IATA/EDIST Requested,omitempty"`
    Priced    *PTC_QuantityType `xml:"http://www.iata.org/IATA/EDIST Priced,omitempty"`
    Refs      string            `xml:",attr,omitempty"`
}

type OfferPenalty OfferPenaltyType

type OfferPenaltyType struct {
    PenaltyType
}

type OfferIncentiveType struct {
    Incentive []*Incentive `xml:"http://www.iata.org/IATA/EDIST Incentive,omitempty"`
}

type Incentive struct {
    OfferCodeID       string            `xml:"http://www.iata.org/IATA/EDIST OfferCodeID,omitempty"`
    ExpirationDate    *CoreDateGrpType  `xml:"http://www.iata.org/IATA/EDIST ExpirationDate,omitempty"`
    OfferSubCode      string            `xml:"http://www.iata.org/IATA/EDIST OfferSubCode,omitempty"`
    AvailableUnits    int               `xml:"http://www.iata.org/IATA/EDIST AvailableUnits,omitempty"`
    DiscountLevel     *DiscountLevel    `xml:"http://www.iata.org/IATA/EDIST DiscountLevel,omitempty"`
    AugmentationPoint *AugPointInfoType `xml:"http://www.iata.org/IATA/EDIST AugmentationPoint,omitempty"`
    Refs              string            `xml:",attr,omitempty"`
}

type DiscountLevel struct {
    DiscountAmount  *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST DiscountAmount,omitempty"`
    DiscountPercent int                    `xml:"http://www.iata.org/IATA/EDIST DiscountPercent,omitempty"`
}

type AllOffersSnapshot AirlineOffersSnapshotType

type AirlineOfferSnapshot AirlineOffersSnapshotType

type AllOffersSnapshotType struct {
    TravelerQuantity   int                    `xml:"http://www.iata.org/IATA/EDIST TravelerQuantity,omitempty"`
    AssociatedTraveler *TravelerInfoAssocType `xml:"http://www.iata.org/IATA/EDIST AssociatedTraveler,omitempty"`
    SubTotal           *SubTotal              `xml:"http://www.iata.org/IATA/EDIST SubTotal,omitempty"`
    Highest            *Highest               `xml:"http://www.iata.org/IATA/EDIST Highest,omitempty"`
    Lowest             *Lowest                `xml:"http://www.iata.org/IATA/EDIST Lowest,omitempty"`
    Refs               string                 `xml:",attr,omitempty"`
}

type SubTotal struct {
    SimpleCurrencyPrice  *CurrencyAmountOptType     `xml:"http://www.iata.org/IATA/EDIST SimpleCurrencyPrice,omitempty"`
    EncodedCurrencyPrice *CurrencyAmountEncodedType `xml:"http://www.iata.org/IATA/EDIST EncodedCurrencyPrice,omitempty"`
    DetailCurrencyPrice  *DetailCurrencyPriceType   `xml:"http://www.iata.org/IATA/EDIST DetailCurrencyPrice,omitempty"`
    CombinationPricing   *CombinationPriceType      `xml:"http://www.iata.org/IATA/EDIST CombinationPricing,omitempty"`
    AwardPricing         *AwardPriceUnitType        `xml:"http://www.iata.org/IATA/EDIST AwardPricing,omitempty"`
    Refs                 string                     `xml:",attr,omitempty"`
}

type Highest struct {
    SimpleCurrencyPrice  *CurrencyAmountOptType     `xml:"http://www.iata.org/IATA/EDIST SimpleCurrencyPrice,omitempty"`
    EncodedCurrencyPrice *CurrencyAmountEncodedType `xml:"http://www.iata.org/IATA/EDIST EncodedCurrencyPrice,omitempty"`
    DetailCurrencyPrice  *DetailCurrencyPriceType   `xml:"http://www.iata.org/IATA/EDIST DetailCurrencyPrice,omitempty"`
    CombinationPricing   *CombinationPriceType      `xml:"http://www.iata.org/IATA/EDIST CombinationPricing,omitempty"`
    AwardPricing         *AwardPriceUnitType        `xml:"http://www.iata.org/IATA/EDIST AwardPricing,omitempty"`
    Refs                 string                     `xml:",attr,omitempty"`
}

type Lowest struct {
    SimpleCurrencyPrice  *CurrencyAmountOptType     `xml:"http://www.iata.org/IATA/EDIST SimpleCurrencyPrice,omitempty"`
    EncodedCurrencyPrice *CurrencyAmountEncodedType `xml:"http://www.iata.org/IATA/EDIST EncodedCurrencyPrice,omitempty"`
    DetailCurrencyPrice  *DetailCurrencyPriceType   `xml:"http://www.iata.org/IATA/EDIST DetailCurrencyPrice,omitempty"`
    CombinationPricing   *CombinationPriceType      `xml:"http://www.iata.org/IATA/EDIST CombinationPricing,omitempty"`
    AwardPricing         *AwardPriceUnitType        `xml:"http://www.iata.org/IATA/EDIST AwardPricing,omitempty"`
    Refs                 string                     `xml:",attr,omitempty"`
}

type AirlineOffersSnapshotType struct {
    TravelerQuantity     int                    `xml:"http://www.iata.org/IATA/EDIST TravelerQuantity,omitempty"`
    AssociatedTraveler   *TravelerInfoAssocType `xml:"http://www.iata.org/IATA/EDIST AssociatedTraveler,omitempty"`
    SubTotal             *SubTotal_1            `xml:"http://www.iata.org/IATA/EDIST SubTotal,omitempty"`
    Highest              *Highest_1             `xml:"http://www.iata.org/IATA/EDIST Highest,omitempty"`
    Lowest               *Lowest_1              `xml:"http://www.iata.org/IATA/EDIST Lowest,omitempty"`
    MatchedOfferQuantity int                    `xml:"http://www.iata.org/IATA/EDIST MatchedOfferQuantity,omitempty"`
    Refs                 string                 `xml:",attr,omitempty"`
}

type SubTotal_1 struct {
    SimpleCurrencyPrice  *CurrencyAmountOptType     `xml:"http://www.iata.org/IATA/EDIST SimpleCurrencyPrice,omitempty"`
    EncodedCurrencyPrice *CurrencyAmountEncodedType `xml:"http://www.iata.org/IATA/EDIST EncodedCurrencyPrice,omitempty"`
    DetailCurrencyPrice  *DetailCurrencyPriceType   `xml:"http://www.iata.org/IATA/EDIST DetailCurrencyPrice,omitempty"`
    CombinationPricing   *CombinationPriceType      `xml:"http://www.iata.org/IATA/EDIST CombinationPricing,omitempty"`
    AwardPricing         *AwardPriceUnitType        `xml:"http://www.iata.org/IATA/EDIST AwardPricing,omitempty"`
    Refs                 string                     `xml:",attr,omitempty"`
}

type Highest_1 struct {
    EncodedCurrencyPrice *CurrencyAmountEncodedType `xml:"http://www.iata.org/IATA/EDIST EncodedCurrencyPrice,omitempty"`
    AwardPricing         *AwardPriceUnitType        `xml:"http://www.iata.org/IATA/EDIST AwardPricing,omitempty"`
    Refs                 string                     `xml:",attr,omitempty"`
}

type Lowest_1 struct {
    EncodedCurrencyPrice *CurrencyAmountEncodedType `xml:"http://www.iata.org/IATA/EDIST EncodedCurrencyPrice,omitempty"`
    AwardPricing         *AwardPriceUnitType        `xml:"http://www.iata.org/IATA/EDIST AwardPricing,omitempty"`
    Refs                 string                     `xml:",attr,omitempty"`
}

type OfferItemStatus OfferStatusType

type OfferStatusType struct {
    Status            string            `xml:"http://www.iata.org/IATA/EDIST Status,omitempty"`
    AugmentationPoint *AugPointInfoType `xml:"http://www.iata.org/IATA/EDIST AugmentationPoint,omitempty"`
}

type OfferTermsType struct {
    AvailablePeriod  *OfferValidPeriodType   `xml:"http://www.iata.org/IATA/EDIST AvailablePeriod,omitempty"`
    Descriptions     *Descriptions           `xml:"http://www.iata.org/IATA/EDIST Descriptions,omitempty"`
    GroupSize        *OfferGroupSizeType     `xml:"http://www.iata.org/IATA/EDIST GroupSize,omitempty"`
    OrderingQuantity *OfferOrderQuantityType `xml:"http://www.iata.org/IATA/EDIST OrderingQuantity,omitempty"`
    Refs             string                  `xml:",attr,omitempty"`
}

type OfferGroupSizeType struct {
    Minimum int `xml:"http://www.iata.org/IATA/EDIST Minimum,omitempty"`
    Maximum int `xml:"http://www.iata.org/IATA/EDIST Maximum,omitempty"`
}

type OfferValidPeriodType struct {
    Earliest *CoreDateGrpType `xml:"http://www.iata.org/IATA/EDIST Earliest,omitempty"`
    Latest   *CoreDateGrpType `xml:"http://www.iata.org/IATA/EDIST Latest,omitempty"`
}

type OfferOrderQuantityType struct {
    Minimum int `xml:"http://www.iata.org/IATA/EDIST Minimum,omitempty"`
    Maximum int `xml:"http://www.iata.org/IATA/EDIST Maximum,omitempty"`
}

type OfferTimeLimitSetType struct {
    OfferExpiration *OfferExpiration `xml:"http://www.iata.org/IATA/EDIST OfferExpiration,omitempty"`
    Payment         *CoreDateGrpType `xml:"http://www.iata.org/IATA/EDIST Payment,omitempty"`
    OtherLimits     *OtherLimits     `xml:"http://www.iata.org/IATA/EDIST OtherLimits,omitempty"`
    Refs            string           `xml:",attr,omitempty"`
}

type OfferExpiration struct {
    CoreDateGrpType
    Refs      string `xml:",attr,omitempty"`
    ObjectKey string `xml:",attr,omitempty"`
}

type OtherLimits struct {
    OtherLimit []*OtherLimit `xml:"http://www.iata.org/IATA/EDIST OtherLimit,omitempty"`
}

type OtherLimit struct {
    PriceGuaranteeTimeLimit *KeyWithMetaObjectBaseType `xml:"http://www.iata.org/IATA/EDIST PriceGuaranteeTimeLimit,omitempty"`
    TicketByTimeLimit       *TicketByTimeLimitType     `xml:"http://www.iata.org/IATA/EDIST TicketByTimeLimit,omitempty"`
    Refs                    string                     `xml:",attr,omitempty"`
    ObjectKey               string                     `xml:",attr,omitempty"`
}

type OfferItemTimeLimitSetType struct {
    TimeLimit []*TimeLimit `xml:"http://www.iata.org/IATA/EDIST TimeLimit,omitempty"`
}

type TimeLimit struct {
    PriceGuaranteeTimeLimit *KeyWithMetaObjectBaseType `xml:"http://www.iata.org/IATA/EDIST PriceGuaranteeTimeLimit,omitempty"`
    TicketByTimeLimit       *TicketByTimeLimitType     `xml:"http://www.iata.org/IATA/EDIST TicketByTimeLimit,omitempty"`
    Refs                    string                     `xml:",attr,omitempty"`
}

type PriceGuaranteeTimeLimit PriceGuarTimeLimitType

type PriceGuarTimeLimitType struct {
    KeyWithMetaObjectBaseType
}

type TicketByTimeLimit TicketByTimeLimitType

type TicketByTimeLimitType struct {
    TicketBy string `xml:"http://www.iata.org/IATA/EDIST TicketBy,omitempty"`
    Refs     string `xml:",attr,omitempty"`
}

type OtherOrderAssocSimpleType string

type OtherOrderAssocListType string

type Associations_2 OrderItemAssociationType

type OtherOrderAssocType struct {
    OtherAssociation []*OtherAssociation_4 `xml:"http://www.iata.org/IATA/EDIST OtherAssociation,omitempty"`
}

type OtherAssociation_4 struct {
    Type           string `xml:"http://www.iata.org/IATA/EDIST Type,omitempty"`
    ReferenceValue string `xml:"http://www.iata.org/IATA/EDIST ReferenceValue,omitempty"`
}

type OrderAssociations OrderItemAssociationType

type OrderItemAssociationType struct {
    Passengers        *Passengers_2        `xml:"http://www.iata.org/IATA/EDIST Passengers,omitempty"`
    Group             *GroupType           `xml:"http://www.iata.org/IATA/EDIST Group,omitempty"`
    Flight            *Flight_3            `xml:"http://www.iata.org/IATA/EDIST Flight,omitempty"`
    OrderItems        *OrderItems          `xml:"http://www.iata.org/IATA/EDIST OrderItems,omitempty"`
    OfferItems        *OfferItems_1        `xml:"http://www.iata.org/IATA/EDIST OfferItems,omitempty"`
    Services          *Services_1          `xml:"http://www.iata.org/IATA/EDIST Services,omitempty"`
    OtherAssociations *OtherOrderAssocType `xml:"http://www.iata.org/IATA/EDIST OtherAssociations,omitempty"`
    AllPassengersInd  bool                 `xml:",attr,omitempty"`
    WholeItineraryInd bool                 `xml:",attr,omitempty"`
}

type Passengers_2 struct {
    PassengerReferences string `xml:"http://www.iata.org/IATA/EDIST PassengerReferences,omitempty"`
}

type Flight_3 struct {
    AllFlightInd                bool               `xml:"http://www.iata.org/IATA/EDIST AllFlightInd,omitempty"`
    AllOriginDestinationInd     bool               `xml:"http://www.iata.org/IATA/EDIST AllOriginDestinationInd,omitempty"`
    AllSegmentInd               bool               `xml:"http://www.iata.org/IATA/EDIST AllSegmentInd,omitempty"`
    OriginDestinationReferences string             `xml:"http://www.iata.org/IATA/EDIST OriginDestinationReferences,omitempty"`
    SegmentReferences           *SegmentReferences `xml:"http://www.iata.org/IATA/EDIST SegmentReferences,omitempty"`
}

type OrderItems struct {
    OrderItemID []*ItemID_Type `xml:"http://www.iata.org/IATA/EDIST OrderItemID,omitempty"`
}

type OfferItems_1 struct {
    OfferItemID []*ItemID_Type `xml:"http://www.iata.org/IATA/EDIST OfferItemID,omitempty"`
}

type Services_1 struct {
    ServiceID []*ServiceID_Type `xml:"http://www.iata.org/IATA/EDIST ServiceID,omitempty"`
}

type OrderItemDetails struct {
    OrderItemDetail     []*OrderItemDetail `xml:"http://www.iata.org/IATA/EDIST OrderItemDetail,omitempty"`
    OrderItemSettlement *SettlementType    `xml:"http://www.iata.org/IATA/EDIST OrderItemSettlement,omitempty"`
}

type OrderItemDetail struct {
    OrderCommision    *CommissionType    `xml:"http://www.iata.org/IATA/EDIST OrderCommision,omitempty"`
    OrderInstructions *OrderInstructions `xml:"http://www.iata.org/IATA/EDIST OrderInstructions,omitempty"`
}

type OrderInstructions struct {
    OrderInstructionType
    ClassOfServiceUpgrade     *InstrClassUpgradeType   `xml:"http://www.iata.org/IATA/EDIST ClassOfServiceUpgrade,omitempty"`
    FreeFormTextInstruction   *RemarkType              `xml:"http://www.iata.org/IATA/EDIST FreeFormTextInstruction,omitempty"`
    SpecialBookingInstruction *SpecialBookingInstrType `xml:"http://www.iata.org/IATA/EDIST SpecialBookingInstruction,omitempty"`
}

type OrderInstructionType struct {
    Refs string `xml:",attr,omitempty"`
}

type OrderCommision OrderCommissionType

type OrderCommissionType struct {
    InstrCommissionType
}

type OrderQualiferType struct {
    OrderKeys *OrderKeysType `xml:"http://www.iata.org/IATA/EDIST OrderKeys,omitempty"`
}

type OrderKeys OrderKeysType

type OrderKeysType struct {
    OrderID       *OrderID_Type  `xml:"http://www.iata.org/IATA/EDIST OrderID,omitempty"`
    AssociatedIDs *AssociatedIDs `xml:"http://www.iata.org/IATA/EDIST AssociatedIDs,omitempty"`
    Refs          string         `xml:",attr,omitempty"`
}

type AssociatedIDs struct {
    AssociatedID []*AssociatedID `xml:"http://www.iata.org/IATA/EDIST AssociatedID,omitempty"`
}

type AssociatedID struct {
    OrderItemID *ItemID_Type    `xml:"http://www.iata.org/IATA/EDIST OrderItemID,omitempty"`
    OfferItemID *ItemID_Type    `xml:"http://www.iata.org/IATA/EDIST OfferItemID,omitempty"`
    ServiceID   *ServiceID_Type `xml:"http://www.iata.org/IATA/EDIST ServiceID,omitempty"`
}

type OrderID OrderID_Type

type OrderID_Type struct {
    Refs                 string `xml:",attr,omitempty"`
    ObjectMetaReferences string `xml:",attr,omitempty"`
    Owner                string `xml:",attr,omitempty"`
    WebAddressID         string `xml:",attr,omitempty"`
    OwnerType            string `xml:",attr,omitempty"`
    Value                string `xml:",chardata"`
}

type OrderCoreType struct {
    OrderID                *OrderID_Type      `xml:"http://www.iata.org/IATA/EDIST OrderID,omitempty"`
    BookingReferences      *BookingReferences `xml:"http://www.iata.org/IATA/EDIST BookingReferences,omitempty"`
    OriginalOrder          *OriginalOrder     `xml:"http://www.iata.org/IATA/EDIST OriginalOrder,omitempty"`
    TotalOrderPrice        *TotalOrderPrice   `xml:"http://www.iata.org/IATA/EDIST TotalOrderPrice,omitempty"`
    Status                 *Status_4          `xml:"http://www.iata.org/IATA/EDIST Status,omitempty"`
    Payments               *Payments          `xml:"http://www.iata.org/IATA/EDIST Payments,omitempty"`
    TimeLimits             *TimeLimits_1      `xml:"http://www.iata.org/IATA/EDIST TimeLimits,omitempty"`
    OrderKeys              *OrderKeysType     `xml:"http://www.iata.org/IATA/EDIST OrderKeys,omitempty"`
    TotalOrderItemQuantity int                `xml:"http://www.iata.org/IATA/EDIST TotalOrderItemQuantity,omitempty"`
    Refs                   string             `xml:",attr,omitempty"`
}

type OriginalOrder struct {
    OriginalOrderID *OrderID_Type `xml:"http://www.iata.org/IATA/EDIST OriginalOrderID,omitempty"`
    PriceQuotes     *PriceQuotes  `xml:"http://www.iata.org/IATA/EDIST PriceQuotes,omitempty"`
}

type PriceQuotes struct {
    PriceQuote []*PriceQuote `xml:"http://www.iata.org/IATA/EDIST PriceQuote,omitempty"`
}

type PriceQuote struct {
    Amount                  *Amount_2                  `xml:"http://www.iata.org/IATA/EDIST Amount,omitempty"`
    Source                  *Source_1                  `xml:"http://www.iata.org/IATA/EDIST Source,omitempty"`
    Commission              *CommissionType            `xml:"http://www.iata.org/IATA/EDIST Commission,omitempty"`
    PriceGuaranteeTimeLimit *KeyWithMetaObjectBaseType `xml:"http://www.iata.org/IATA/EDIST PriceGuaranteeTimeLimit,omitempty"`
    CreatedDate             string                     `xml:",attr,omitempty"`
}

type Amount_2 struct {
    CurrencyAmount *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST CurrencyAmount,omitempty"`
}

type Source_1 struct {
    Owner     string `xml:",attr,omitempty"`
    Reference string `xml:",attr,omitempty"`
}

type TotalOrderPrice struct {
    AwardPricing         *AwardPriceUnitType        `xml:"http://www.iata.org/IATA/EDIST AwardPricing,omitempty"`
    CombinationPricing   *CombinationPriceType      `xml:"http://www.iata.org/IATA/EDIST CombinationPricing,omitempty"`
    DetailCurrencyPrice  *DetailCurrencyPriceType   `xml:"http://www.iata.org/IATA/EDIST DetailCurrencyPrice,omitempty"`
    EncodedCurrencyPrice *CurrencyAmountEncodedType `xml:"http://www.iata.org/IATA/EDIST EncodedCurrencyPrice,omitempty"`
    SimpleCurrencyPrice  *CurrencyAmountOptType     `xml:"http://www.iata.org/IATA/EDIST SimpleCurrencyPrice,omitempty"`
}

type Status_4 struct {
    StatusCode        *CodesetType `xml:"http://www.iata.org/IATA/EDIST StatusCode,omitempty"`
    ChangeOfGaugeInd  bool         `xml:",attr,omitempty"`
    ScheduleChangeInd bool         `xml:",attr,omitempty"`
}

type Payments struct {
    Payment []*PaymentProcessType `xml:"http://www.iata.org/IATA/EDIST Payment,omitempty"`
}

type TimeLimits_1 struct {
    PaymentTimeLimit    *PaymentTimeLimit_1      `xml:"http://www.iata.org/IATA/EDIST PaymentTimeLimit,omitempty"`
    DepositTimeLimit    *DepositTimeLimitType    `xml:"http://www.iata.org/IATA/EDIST DepositTimeLimit,omitempty"`
    NamingTimeLimit     *NamingTimeLimitType     `xml:"http://www.iata.org/IATA/EDIST NamingTimeLimit,omitempty"`
    BilateralTimeLimits *BilateralTimeLimitsType `xml:"http://www.iata.org/IATA/EDIST BilateralTimeLimits,omitempty"`
}

type PaymentTimeLimit_1 struct {
    CoreDateGrpType
    Refs string `xml:",attr,omitempty"`
}

type OrderCoreChangeType struct {
    OrderID                *OrderID_Type            `xml:"http://www.iata.org/IATA/EDIST OrderID,omitempty"`
    TotalOrderItemQuantity int                      `xml:"http://www.iata.org/IATA/EDIST TotalOrderItemQuantity,omitempty"`
    TotalOrderPrice        *DetailCurrencyPriceType `xml:"http://www.iata.org/IATA/EDIST TotalOrderPrice,omitempty"`
    TimeLimits             *OrderTimeLimitsType     `xml:"http://www.iata.org/IATA/EDIST TimeLimits,omitempty"`
    Associations           *Associations_3          `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
    Details                *Details_4               `xml:"http://www.iata.org/IATA/EDIST Details,omitempty"`
    ActionType             *ActionType_1            `xml:"http://www.iata.org/IATA/EDIST ActionType,omitempty"`
    Refs                   string                   `xml:",attr,omitempty"`
}

type Associations_3 struct {
    OrderItemID      []*ItemID_Type        `xml:"http://www.iata.org/IATA/EDIST OrderItemID,omitempty"`
    Passengers       *Passengers_3         `xml:"http://www.iata.org/IATA/EDIST Passengers,omitempty"`
    OtherAssociation []*OtherAssociation_5 `xml:"http://www.iata.org/IATA/EDIST OtherAssociation,omitempty"`
}

type Passengers_3 struct {
    PassengerReferences string `xml:"http://www.iata.org/IATA/EDIST PassengerReferences,omitempty"`
    GroupReference      string `xml:"http://www.iata.org/IATA/EDIST GroupReference,omitempty"`
}

type OtherAssociation_5 struct {
    Type           string `xml:",attr,omitempty"`
    ReferenceValue string `xml:",attr,omitempty"`
}

type Details_4 struct {
    OriginalOrderID  *OrderID_Type     `xml:"http://www.iata.org/IATA/EDIST OriginalOrderID,omitempty"`
    ShoppingResponse *ShoppingResponse `xml:"http://www.iata.org/IATA/EDIST ShoppingResponse,omitempty"`
    Amendments       *Amendments       `xml:"http://www.iata.org/IATA/EDIST Amendments,omitempty"`
}

type ShoppingResponse struct {
    Owner      string         `xml:"http://www.iata.org/IATA/EDIST Owner,omitempty"`
    ResponseID *ResponseID_2  `xml:"http://www.iata.org/IATA/EDIST ResponseID,omitempty"`
    OfferID    []*ItemID_Type `xml:"http://www.iata.org/IATA/EDIST OfferID,omitempty"`
    OfferItems *OfferItems_2  `xml:"http://www.iata.org/IATA/EDIST OfferItems,omitempty"`
    Refs       string         `xml:",attr,omitempty"`
}

type ResponseID_2 struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type OfferItems_2 struct {
    OfferItem []*OfferItem_1 `xml:"http://www.iata.org/IATA/EDIST OfferItem,omitempty"`
}

type OfferItem_1 struct {
    OfferItemID *ItemID_Type      `xml:"http://www.iata.org/IATA/EDIST OfferItemID,omitempty"`
    ServiceID   []*ServiceID_Type `xml:"http://www.iata.org/IATA/EDIST ServiceID,omitempty"`
    Refs        string            `xml:",attr,omitempty"`
}

type Amendments struct {
    Amendment []*Amendment `xml:"http://www.iata.org/IATA/EDIST Amendment,omitempty"`
}

type Amendment struct {
    ActionType    *ActionType    `xml:"http://www.iata.org/IATA/EDIST ActionType,omitempty"`
    TicketDocInfo *TicketDocInfo `xml:"http://www.iata.org/IATA/EDIST TicketDocInfo,omitempty"`
    OrderItem     *OrderItem     `xml:"http://www.iata.org/IATA/EDIST OrderItem,omitempty"`
}

type ActionType struct {
    Context string `xml:",attr,omitempty"`
    Value   string `xml:",chardata"`
}

type TicketDocInfo struct {
    AgentIDs           *AgentIDs            `xml:"http://www.iata.org/IATA/EDIST AgentIDs,omitempty"`
    IssuingAirlineInfo *AirlineIssuanceType `xml:"http://www.iata.org/IATA/EDIST IssuingAirlineInfo,omitempty"`
    TicketDocument     []*TicketDocument    `xml:"http://www.iata.org/IATA/EDIST TicketDocument,omitempty"`
    CarrierFeeInfo     *CarrierFeeInfoType  `xml:"http://www.iata.org/IATA/EDIST CarrierFeeInfo,omitempty"`
    OriginalIssueInfo  *OriginalIssueType   `xml:"http://www.iata.org/IATA/EDIST OriginalIssueInfo,omitempty"`
    Passengers         *Passengers_4        `xml:"http://www.iata.org/IATA/EDIST Passengers,omitempty"`
}

type AgentIDs struct {
    AgentID []*CouponAgentType `xml:"http://www.iata.org/IATA/EDIST AgentID,omitempty"`
}

type Passengers_4 struct {
    PassengerReferences string `xml:"http://www.iata.org/IATA/EDIST PassengerReferences,omitempty"`
    GroupReference      string `xml:"http://www.iata.org/IATA/EDIST GroupReference,omitempty"`
}

type OrderItem struct {
    OrderItemID      *ItemID_Type          `xml:"http://www.iata.org/IATA/EDIST OrderItemID,omitempty"`
    OfferItem        *OfferItem_2          `xml:"http://www.iata.org/IATA/EDIST OfferItem,omitempty"`
    Passengers       *Passengers_7         `xml:"http://www.iata.org/IATA/EDIST Passengers,omitempty"`
    OtherAssociation []*OtherAssociation_7 `xml:"http://www.iata.org/IATA/EDIST OtherAssociation,omitempty"`
    Refs             string                `xml:",attr,omitempty"`
}

type OfferItem_2 struct {
    OfferItemID      *ItemID_Type          `xml:"http://www.iata.org/IATA/EDIST OfferItemID,omitempty"`
    Passengers       *Passengers_5         `xml:"http://www.iata.org/IATA/EDIST Passengers,omitempty"`
    Services         *Services_2           `xml:"http://www.iata.org/IATA/EDIST Services,omitempty"`
    OtherAssociation []*OtherAssociation_6 `xml:"http://www.iata.org/IATA/EDIST OtherAssociation,omitempty"`
    Refs             string                `xml:",attr,omitempty"`
}

type Passengers_5 struct {
    PassengerReferences string `xml:"http://www.iata.org/IATA/EDIST PassengerReferences,omitempty"`
    GroupReference      string `xml:"http://www.iata.org/IATA/EDIST GroupReference,omitempty"`
}

type Services_2 struct {
    ServiceID  []*ServiceID_Type `xml:"http://www.iata.org/IATA/EDIST ServiceID,omitempty"`
    Passengers *Passengers_6     `xml:"http://www.iata.org/IATA/EDIST Passengers,omitempty"`
}

type Passengers_6 struct {
    PassengerReferences string `xml:"http://www.iata.org/IATA/EDIST PassengerReferences,omitempty"`
    GroupReference      string `xml:"http://www.iata.org/IATA/EDIST GroupReference,omitempty"`
}

type OtherAssociation_6 struct {
    Type           string `xml:",attr,omitempty"`
    ReferenceValue string `xml:",attr,omitempty"`
}

type Passengers_7 struct {
    PassengerReferences string `xml:"http://www.iata.org/IATA/EDIST PassengerReferences,omitempty"`
    GroupReference      string `xml:"http://www.iata.org/IATA/EDIST GroupReference,omitempty"`
}

type OtherAssociation_7 struct {
    Type           string `xml:",attr,omitempty"`
    ReferenceValue string `xml:",attr,omitempty"`
}

type ActionType_1 struct {
    Context string `xml:",attr,omitempty"`
    Value   string `xml:",chardata"`
}

type OrderItemCoreType struct {
    OrderItem []*OrderItem_1 `xml:"http://www.iata.org/IATA/EDIST OrderItem,omitempty"`
}

type OrderItem_1 struct {
    OrderItemID        *ItemID_Type             `xml:"http://www.iata.org/IATA/EDIST OrderItemID,omitempty"`
    SeatItem           *SeatItemType            `xml:"http://www.iata.org/IATA/EDIST SeatItem,omitempty"`
    OtherItem          *OtherItemType           `xml:"http://www.iata.org/IATA/EDIST OtherItem,omitempty"`
    FlightItem         *FlightItemType          `xml:"http://www.iata.org/IATA/EDIST FlightItem,omitempty"`
    BaggageItem        *BaggageItemType         `xml:"http://www.iata.org/IATA/EDIST BaggageItem,omitempty"`
    TimeLimits         *OrderItemTimeLimitsType `xml:"http://www.iata.org/IATA/EDIST TimeLimits,omitempty"`
    InventoryGuarantee *InventoryGuarantee_3    `xml:"http://www.iata.org/IATA/EDIST InventoryGuarantee,omitempty"`
    Associations       *Associations_4          `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
    Disclosures        *DisclosureType          `xml:"http://www.iata.org/IATA/EDIST Disclosures,omitempty"`
    Penalty            *OrderPenaltyType        `xml:"http://www.iata.org/IATA/EDIST Penalty,omitempty"`
    OrderItemDetails   *OrderItemDetails        `xml:"http://www.iata.org/IATA/EDIST OrderItemDetails,omitempty"`
    Refs               string                   `xml:",attr,omitempty"`
    Timestamp          string                   `xml:",attr,omitempty"`
}

type InventoryGuarantee_3 struct {
    InventoryGuaranteeID         string           `xml:"http://www.iata.org/IATA/EDIST InventoryGuaranteeID,omitempty"`
    InventoryGuaranteeTimeLimits *CoreDateGrpType `xml:"http://www.iata.org/IATA/EDIST InventoryGuaranteeTimeLimits,omitempty"`
}

type Associations_4 struct {
    OrderID           *OrderID_Type         `xml:"http://www.iata.org/IATA/EDIST OrderID,omitempty"`
    OrderItemID       []*ItemID_Type        `xml:"http://www.iata.org/IATA/EDIST OrderItemID,omitempty"`
    Passengers        *Passengers_8         `xml:"http://www.iata.org/IATA/EDIST Passengers,omitempty"`
    IncludedService   *ServiceInfoAssocType `xml:"http://www.iata.org/IATA/EDIST IncludedService,omitempty"`
    AssociatedService *ServiceInfoAssocType `xml:"http://www.iata.org/IATA/EDIST AssociatedService,omitempty"`
    OtherAssociation  []*OtherAssociation_8 `xml:"http://www.iata.org/IATA/EDIST OtherAssociation,omitempty"`
}

type Passengers_8 struct {
    PassengerReferences string `xml:"http://www.iata.org/IATA/EDIST PassengerReferences,omitempty"`
    GroupReference      string `xml:"http://www.iata.org/IATA/EDIST GroupReference,omitempty"`
}

type OtherAssociation_8 struct {
    Type           string `xml:",attr,omitempty"`
    ReferenceValue string `xml:",attr,omitempty"`
}

type OrderItemDetailType struct {
    OrderItemCoreType
}

type OrderItemRepriceType struct {
    OrderItem []*OrderItem_2 `xml:"http://www.iata.org/IATA/EDIST OrderItem,omitempty"`
}

type OrderItem_2 struct {
    OrderItemID        *ItemID_Type              `xml:"http://www.iata.org/IATA/EDIST OrderItemID,omitempty"`
    Associations       *OrderItemAssociationType `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
    BaggageItem        *BaggageItemType          `xml:"http://www.iata.org/IATA/EDIST BaggageItem,omitempty"`
    FlightItem         *FlightItemType           `xml:"http://www.iata.org/IATA/EDIST FlightItem,omitempty"`
    OtherItem          *OtherItemType            `xml:"http://www.iata.org/IATA/EDIST OtherItem,omitempty"`
    SeatItem           *SeatItemType             `xml:"http://www.iata.org/IATA/EDIST SeatItem,omitempty"`
    InventoryGuarantee *InventoryGuarantee_4     `xml:"http://www.iata.org/IATA/EDIST InventoryGuarantee,omitempty"`
    Disclosures        *DisclosureType           `xml:"http://www.iata.org/IATA/EDIST Disclosures,omitempty"`
    TimeLimits         *OrderItemTimeLimitsType  `xml:"http://www.iata.org/IATA/EDIST TimeLimits,omitempty"`
    ItemStatus         *CodesetType              `xml:"http://www.iata.org/IATA/EDIST ItemStatus,omitempty"`
    Penalty            *OrderPenaltyType         `xml:"http://www.iata.org/IATA/EDIST Penalty,omitempty"`
    OrderItemDetails   *OrderItemDetails         `xml:"http://www.iata.org/IATA/EDIST OrderItemDetails,omitempty"`
    ActionType         *ActionType_2             `xml:"http://www.iata.org/IATA/EDIST ActionType,omitempty"`
    Refs               string                    `xml:",attr,omitempty"`
}

type InventoryGuarantee_4 struct {
    InvGuaranteeID               string           `xml:"http://www.iata.org/IATA/EDIST InvGuaranteeID,omitempty"`
    InventoryGuaranteeTimeLimits *CoreDateGrpType `xml:"http://www.iata.org/IATA/EDIST InventoryGuaranteeTimeLimits,omitempty"`
}

type ActionType_2 struct {
    Context string `xml:",attr,omitempty"`
    Value   string `xml:",chardata"`
}

type BaggageItem BaggageItemType

type BaggageOfferItem BaggageItemType

type BaggageItemType struct {
    Price             *Price_2      `xml:"http://www.iata.org/IATA/EDIST Price,omitempty"`
    ValidatingCarrier string        `xml:"http://www.iata.org/IATA/EDIST ValidatingCarrier,omitempty"`
    BagDetails        *BagDetails_2 `xml:"http://www.iata.org/IATA/EDIST BagDetails,omitempty"`
    Refs              string        `xml:",attr,omitempty"`
}

type Price_2 struct {
    SimpleCurrencyPrice  *CurrencyAmountOptType     `xml:"http://www.iata.org/IATA/EDIST SimpleCurrencyPrice,omitempty"`
    EncodedCurrencyPrice *CurrencyAmountEncodedType `xml:"http://www.iata.org/IATA/EDIST EncodedCurrencyPrice,omitempty"`
    DetailCurrencyPrice  *DetailCurrencyPriceType   `xml:"http://www.iata.org/IATA/EDIST DetailCurrencyPrice,omitempty"`
    CombinationPricing   *CombinationPriceType      `xml:"http://www.iata.org/IATA/EDIST CombinationPricing,omitempty"`
    AwardPricing         *AwardPriceUnitType        `xml:"http://www.iata.org/IATA/EDIST AwardPricing,omitempty"`
}

type BagDetails_2 struct {
    BagDetail []*BagDetail_2 `xml:"http://www.iata.org/IATA/EDIST BagDetail,omitempty"`
}

type BagDetail_2 struct {
    ValidatingCarrier    string                   `xml:"http://www.iata.org/IATA/EDIST ValidatingCarrier,omitempty"`
    CheckedBags          *CheckedBagAllowanceType `xml:"http://www.iata.org/IATA/EDIST CheckedBags,omitempty"`
    CarryOnBags          *CarryOnAllowanceType    `xml:"http://www.iata.org/IATA/EDIST CarryOnBags,omitempty"`
    Disclosure           *BagDisclosureType       `xml:"http://www.iata.org/IATA/EDIST Disclosure,omitempty"`
    Price                *Price_3                 `xml:"http://www.iata.org/IATA/EDIST Price,omitempty"`
    BagDetailAssociation *BagDetailAssociation    `xml:"http://www.iata.org/IATA/EDIST BagDetailAssociation,omitempty"`
}

type Price_3 struct {
    AwardPricing         *AwardPriceUnitType        `xml:"http://www.iata.org/IATA/EDIST AwardPricing,omitempty"`
    CombinationPricing   *CombinationPriceType      `xml:"http://www.iata.org/IATA/EDIST CombinationPricing,omitempty"`
    SimpleCurrencyPrice  *CurrencyAmountOptType     `xml:"http://www.iata.org/IATA/EDIST SimpleCurrencyPrice,omitempty"`
    DetailCurrencyPrice  *DetailCurrencyPriceType   `xml:"http://www.iata.org/IATA/EDIST DetailCurrencyPrice,omitempty"`
    EncodedCurrencyPrice *CurrencyAmountEncodedType `xml:"http://www.iata.org/IATA/EDIST EncodedCurrencyPrice,omitempty"`
}

type DetailedFlightItem FlightItemType

type FlightItemOfferType struct {
    Pricing           *FlightPriceType   `xml:"http://www.iata.org/IATA/EDIST Pricing,omitempty"`
    Flight            []*Flight_4        `xml:"http://www.iata.org/IATA/EDIST Flight,omitempty"`
    AircraftEquipment *AircraftEquipment `xml:"http://www.iata.org/IATA/EDIST AircraftEquipment,omitempty"`
    FareDetail        *FareDetailType    `xml:"http://www.iata.org/IATA/EDIST FareDetail,omitempty"`
    TotalJourney      *TotalJourneyType  `xml:"http://www.iata.org/IATA/EDIST TotalJourney,omitempty"`
    Passengers        *Passengers_9      `xml:"http://www.iata.org/IATA/EDIST Passengers,omitempty"`
    Details           *Details_5         `xml:"http://www.iata.org/IATA/EDIST Details,omitempty"`
    Refs              string             `xml:",attr,omitempty"`
}

type Flight_4 struct {
    SegmentKey       string                      `xml:"http://www.iata.org/IATA/EDIST SegmentKey,omitempty"`
    Departure        *FlightDepartureType        `xml:"http://www.iata.org/IATA/EDIST Departure,omitempty"`
    Arrival          *FlightArrivalType          `xml:"http://www.iata.org/IATA/EDIST Arrival,omitempty"`
    MarketingCarrier *MarketingCarrierFlightType `xml:"http://www.iata.org/IATA/EDIST MarketingCarrier,omitempty"`
    OperatingCarrier *OperatingCarrier_3         `xml:"http://www.iata.org/IATA/EDIST OperatingCarrier,omitempty"`
    Equipment        *AircraftSummaryType        `xml:"http://www.iata.org/IATA/EDIST Equipment,omitempty"`
    CabinType        *CabinType                  `xml:"http://www.iata.org/IATA/EDIST CabinType,omitempty"`
    ClassOfService   *FlightCOS_CoreType         `xml:"http://www.iata.org/IATA/EDIST ClassOfService,omitempty"`
    Details          *FlightDetailType           `xml:"http://www.iata.org/IATA/EDIST Details,omitempty"`
    Refs             string                      `xml:",attr,omitempty"`
    PricingInd       bool                        `xml:",attr,omitempty"`
    ConnectionInd    bool                        `xml:",attr,omitempty"`
    E_TicketInd      bool                        `xml:",attr,omitempty"`
    TicketlessInd    bool                        `xml:",attr,omitempty"`
}

type OperatingCarrier_3 struct {
    OperatingCarrierFlightType
    Disclosures *DisclosureType `xml:"http://www.iata.org/IATA/EDIST Disclosures,omitempty"`
}

type AircraftEquipment struct {
    AircraftCode     *AircraftCodeType `xml:"http://www.iata.org/IATA/EDIST AircraftCode,omitempty"`
    AirlineEquipCode string            `xml:"http://www.iata.org/IATA/EDIST AirlineEquipCode,omitempty"`
    Name             string            `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
}

type Passengers_9 struct {
    PassengerReference string `xml:"http://www.iata.org/IATA/EDIST PassengerReference,omitempty"`
    GroupReference     string `xml:"http://www.iata.org/IATA/EDIST GroupReference,omitempty"`
}

type Details_5 struct {
    InventoryGuarantee *InventoryGuarantee_5 `xml:"http://www.iata.org/IATA/EDIST InventoryGuarantee,omitempty"`
    WaitListInd        bool                  `xml:",attr,omitempty"`
}

type InventoryGuarantee_5 struct {
    InvGuaranteeID               string           `xml:"http://www.iata.org/IATA/EDIST InvGuaranteeID,omitempty"`
    InventoryGuaranteeTimeLimits *CoreDateGrpType `xml:"http://www.iata.org/IATA/EDIST InventoryGuaranteeTimeLimits,omitempty"`
}

type FlightItemType struct {
    Price             *FlightPriceType `xml:"http://www.iata.org/IATA/EDIST Price,omitempty"`
    OriginDestination []*FlightType    `xml:"http://www.iata.org/IATA/EDIST OriginDestination,omitempty"`
    FareDetail        *FareDetailType  `xml:"http://www.iata.org/IATA/EDIST FareDetail,omitempty"`
    Refs              string           `xml:",attr,omitempty"`
}

type FlightItem FlightItemType

type OtherItem OtherItemType

type OrderOfferItemType struct {
    OfferItemID   *ItemID_Type               `xml:"http://www.iata.org/IATA/EDIST OfferItemID,omitempty"`
    OfferItemType *OfferItemType             `xml:"http://www.iata.org/IATA/EDIST OfferItemType,omitempty"`
    TimeLimits    *OfferItemTimeLimitSetType `xml:"http://www.iata.org/IATA/EDIST TimeLimits,omitempty"`
    Refs          string                     `xml:",attr,omitempty"`
}

type OfferItemType struct {
    BaggageItem        []*BaggageItemType `xml:"http://www.iata.org/IATA/EDIST BaggageItem,omitempty"`
    DetailedFlightItem []*FlightItemType  `xml:"http://www.iata.org/IATA/EDIST DetailedFlightItem,omitempty"`
    OtherItem          []*OtherItemType   `xml:"http://www.iata.org/IATA/EDIST OtherItem,omitempty"`
    SeatItem           []*SeatItemType    `xml:"http://www.iata.org/IATA/EDIST SeatItem,omitempty"`
}

type OtherOfferItem OtherItemType

type OtherItemType struct {
    Price       *Price_4 `xml:"http://www.iata.org/IATA/EDIST Price,omitempty"`
    Description string   `xml:"http://www.iata.org/IATA/EDIST Description,omitempty"`
    Refs        string   `xml:",attr,omitempty"`
}

type Price_4 struct {
    SimpleCurrencyPrice  *CurrencyAmountOptType     `xml:"http://www.iata.org/IATA/EDIST SimpleCurrencyPrice,omitempty"`
    EncodedCurrencyPrice *CurrencyAmountEncodedType `xml:"http://www.iata.org/IATA/EDIST EncodedCurrencyPrice,omitempty"`
    DetailCurrencyPrice  *DetailCurrencyPriceType   `xml:"http://www.iata.org/IATA/EDIST DetailCurrencyPrice,omitempty"`
    CombinationPricing   *CombinationPriceType      `xml:"http://www.iata.org/IATA/EDIST CombinationPricing,omitempty"`
    AwardPricing         *AwardPriceUnitType        `xml:"http://www.iata.org/IATA/EDIST AwardPricing,omitempty"`
}

type PaymentStatus PaymentStatusType

type PaymentStatusType struct {
    StatusCode        *CodesetType       `xml:"http://www.iata.org/IATA/EDIST StatusCode,omitempty"`
    IncompletePayment *IncompletePayment `xml:"http://www.iata.org/IATA/EDIST IncompletePayment,omitempty"`
}

type IncompletePayment struct {
    StatusCode   *CodesetType              `xml:"http://www.iata.org/IATA/EDIST StatusCode,omitempty"`
    Associations *OrderItemAssociationType `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
    Remarks      *RemarkType               `xml:"http://www.iata.org/IATA/EDIST Remarks,omitempty"`
}

type SeatItem SeatItemType

type SeatOfferItem SeatItemType

type SeatItemType struct {
    Price         *OrderPriceType   `xml:"http://www.iata.org/IATA/EDIST Price,omitempty"`
    Descriptions  *Descriptions     `xml:"http://www.iata.org/IATA/EDIST Descriptions,omitempty"`
    Location      *SeatLocationType `xml:"http://www.iata.org/IATA/EDIST Location,omitempty"`
    SeatReference []string          `xml:"http://www.iata.org/IATA/EDIST SeatReference,omitempty"`
    Refs          string            `xml:",attr,omitempty"`
}

type Status_5 CodesetType

type TimeLimits_2 struct {
    PaymentTimeLimit        *PaymentTimeLimit_2        `xml:"http://www.iata.org/IATA/EDIST PaymentTimeLimit,omitempty"`
    PriceGuaranteeTimeLimit *KeyWithMetaObjectBaseType `xml:"http://www.iata.org/IATA/EDIST PriceGuaranteeTimeLimit,omitempty"`
    DepositTimeLimit        *DepositTimeLimitType      `xml:"http://www.iata.org/IATA/EDIST DepositTimeLimit,omitempty"`
    TicketingTimeLimits     *TicketingTimeLimitType    `xml:"http://www.iata.org/IATA/EDIST TicketingTimeLimits,omitempty"`
    NamingTimeLimit         *NamingTimeLimitType       `xml:"http://www.iata.org/IATA/EDIST NamingTimeLimit,omitempty"`
    BilateralTimeLimits     *BilateralTimeLimitsType   `xml:"http://www.iata.org/IATA/EDIST BilateralTimeLimits,omitempty"`
}

type PaymentTimeLimit_2 struct {
    CoreDateGrpType
    Refs string `xml:",attr,omitempty"`
}

type BankAccountMethod BankAccountMethodType

type BankAccountMethodType struct {
    Name        *Name_6 `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
    CheckNumber string  `xml:"http://www.iata.org/IATA/EDIST CheckNumber,omitempty"`
    Refs        string  `xml:",attr,omitempty"`
}

type Name_6 struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type CashMethod CashMethodType

type CashMethodType struct {
    Amount      *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST Amount,omitempty"`
    TerminalID  string                 `xml:"http://www.iata.org/IATA/EDIST TerminalID,omitempty"`
    ReceiptID   string                 `xml:"http://www.iata.org/IATA/EDIST ReceiptID,omitempty"`
    AgentUserID *AgentUserID_Type      `xml:"http://www.iata.org/IATA/EDIST AgentUserID,omitempty"`
    Address     *Address_3             `xml:"http://www.iata.org/IATA/EDIST Address,omitempty"`
    Refs        string                 `xml:",attr,omitempty"`
}

type Address_3 struct {
    SimpleAddress     *SimpleAddrType     `xml:"http://www.iata.org/IATA/EDIST SimpleAddress,omitempty"`
    StructuredAddress *StructuredAddrType `xml:"http://www.iata.org/IATA/EDIST StructuredAddress,omitempty"`
    PaymentAddress    *PaymentAddrType    `xml:"http://www.iata.org/IATA/EDIST PaymentAddress,omitempty"`
}

type DirectBillMethodType struct {
    DirectBillID string     `xml:"http://www.iata.org/IATA/EDIST DirectBillID,omitempty"`
    CompanyName  string     `xml:"http://www.iata.org/IATA/EDIST CompanyName,omitempty"`
    ContactName  string     `xml:"http://www.iata.org/IATA/EDIST ContactName,omitempty"`
    Address      *Address_4 `xml:"http://www.iata.org/IATA/EDIST Address,omitempty"`
    Refs         string     `xml:",attr,omitempty"`
}

type Address_4 struct {
    SimpleAddress     *SimpleAddrType     `xml:"http://www.iata.org/IATA/EDIST SimpleAddress,omitempty"`
    StructuredAddress *StructuredAddrType `xml:"http://www.iata.org/IATA/EDIST StructuredAddress,omitempty"`
    PaymentAddress    *PaymentAddrType    `xml:"http://www.iata.org/IATA/EDIST PaymentAddress,omitempty"`
}

type DirectBillMethod DirectBillMethodType

type MiscChargeMethod MiscChargeMethodType

type MiscChargeMethodType struct {
    TicketNumber string `xml:"http://www.iata.org/IATA/EDIST TicketNumber,omitempty"`
    Refs         string `xml:",attr,omitempty"`
}

type OtherMethod OtherMethodType

type OtherMethodType struct {
    Remarks *RemarkType `xml:"http://www.iata.org/IATA/EDIST Remarks,omitempty"`
    Refs    string      `xml:",attr,omitempty"`
}

type PaymentProcessType struct {
    Associations *OrderItemAssociationType `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
    Status       *Status_6                 `xml:"http://www.iata.org/IATA/EDIST Status,omitempty"`
    Amount       *Amount_3                 `xml:"http://www.iata.org/IATA/EDIST Amount,omitempty"`
    Method       *Method                   `xml:"http://www.iata.org/IATA/EDIST Method,omitempty"`
    Variance     *Variance                 `xml:"http://www.iata.org/IATA/EDIST Variance,omitempty"`
    Promotions   *Promotions               `xml:"http://www.iata.org/IATA/EDIST Promotions,omitempty"`
    Refs         string                    `xml:",attr,omitempty"`
}

type Status_6 struct {
    PaymentStatusType
    FailedPaymentInd  bool `xml:",attr,omitempty"`
    PartialPaymentInd bool `xml:",attr,omitempty"`
    VerificationInd   bool `xml:",attr,omitempty"`
    PriceVarianceInd  bool `xml:",attr,omitempty"`
}

type Amount_3 struct {
    AwardPricing         *AwardPriceUnitType        `xml:"http://www.iata.org/IATA/EDIST AwardPricing,omitempty"`
    CombinationPricing   *CombinationPriceType      `xml:"http://www.iata.org/IATA/EDIST CombinationPricing,omitempty"`
    SimpleCurrencyPrice  *CurrencyAmountOptType     `xml:"http://www.iata.org/IATA/EDIST SimpleCurrencyPrice,omitempty"`
    DetailCurrencyPrice  *DetailCurrencyPriceType   `xml:"http://www.iata.org/IATA/EDIST DetailCurrencyPrice,omitempty"`
    EncodedCurrencyPrice *CurrencyAmountEncodedType `xml:"http://www.iata.org/IATA/EDIST EncodedCurrencyPrice,omitempty"`
}

type Method struct {
    VoucherMethod     *VoucherMethodType     `xml:"http://www.iata.org/IATA/EDIST VoucherMethod,omitempty"`
    RedemptionMethod  *RedemptionMethodType  `xml:"http://www.iata.org/IATA/EDIST RedemptionMethod,omitempty"`
    PaymentCardMethod *PaymentCardMethodType `xml:"http://www.iata.org/IATA/EDIST PaymentCardMethod,omitempty"`
    OtherMethod       *OtherMethodType       `xml:"http://www.iata.org/IATA/EDIST OtherMethod,omitempty"`
    MiscChargeMethod  *MiscChargeMethodType  `xml:"http://www.iata.org/IATA/EDIST MiscChargeMethod,omitempty"`
    DirectBillMethod  *DirectBillMethodType  `xml:"http://www.iata.org/IATA/EDIST DirectBillMethod,omitempty"`
    CashMethod        *CashMethodType        `xml:"http://www.iata.org/IATA/EDIST CashMethod,omitempty"`
    BankAccountMethod *BankAccountMethodType `xml:"http://www.iata.org/IATA/EDIST BankAccountMethod,omitempty"`
    Check             *Check                 `xml:"http://www.iata.org/IATA/EDIST Check,omitempty"`
}

type Variance struct {
    Amount       *CurrencyAmountOptType    `xml:"http://www.iata.org/IATA/EDIST Amount,omitempty"`
    Associations *OrderItemAssociationType `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
    Rule         *PriceVarianceRuleType    `xml:"http://www.iata.org/IATA/EDIST Rule,omitempty"`
}

type Promotions struct {
    Promotion []*Promotion `xml:"http://www.iata.org/IATA/EDIST Promotion,omitempty"`
}

type Promotion struct {
    PromotionType
    Associations *OrderItemAssociationType `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
}

type PaymentCardMethodType struct {
    CardType            string                 `xml:"http://www.iata.org/IATA/EDIST CardType,omitempty"`
    CardCode            string                 `xml:"http://www.iata.org/IATA/EDIST CardCode,omitempty"`
    MaskedCardNumber    *MaskedCardNumber_1    `xml:"http://www.iata.org/IATA/EDIST MaskedCardNumber,omitempty"`
    TokenizedCardNumber string                 `xml:"http://www.iata.org/IATA/EDIST TokenizedCardNumber,omitempty"`
    Contacts            *Contacts              `xml:"http://www.iata.org/IATA/EDIST Contacts,omitempty"`
    CardHolderName      *CardHolderName_1      `xml:"http://www.iata.org/IATA/EDIST CardHolderName,omitempty"`
    CardIssuerName      *CardIssuerName_1      `xml:"http://www.iata.org/IATA/EDIST CardIssuerName,omitempty"`
    CardholderAddress   *CardholderAddress     `xml:"http://www.iata.org/IATA/EDIST CardholderAddress,omitempty"`
    EffectiveExpireDate *EffectiveExpireDate_2 `xml:"http://www.iata.org/IATA/EDIST EffectiveExpireDate,omitempty"`
    ApprovalType        *CodesetType           `xml:"http://www.iata.org/IATA/EDIST ApprovalType,omitempty"`
    Refs                string                 `xml:",attr,omitempty"`
}

type MaskedCardNumber_1 struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type CardHolderName_1 struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type CardIssuerName_1 struct {
    BankID string `xml:"http://www.iata.org/IATA/EDIST BankID,omitempty"`
}

type CardholderAddress struct {
    SimpleAddress     *SimpleAddrType     `xml:"http://www.iata.org/IATA/EDIST SimpleAddress,omitempty"`
    StructuredAddress *StructuredAddrType `xml:"http://www.iata.org/IATA/EDIST StructuredAddress,omitempty"`
    PaymentAddress    *PaymentAddrType    `xml:"http://www.iata.org/IATA/EDIST PaymentAddress,omitempty"`
    Refs              string              `xml:",attr,omitempty"`
}

type EffectiveExpireDate_2 struct {
    Effective  string `xml:"http://www.iata.org/IATA/EDIST Effective,omitempty"`
    Expiration string `xml:"http://www.iata.org/IATA/EDIST Expiration,omitempty"`
    Refs       string `xml:",attr,omitempty"`
}

type PaymentCardMethod PaymentCardMethodType

type RedemptionMethod RedemptionMethodType

type RedemptionMethodType struct {
    Certificates       *Certificates_1 `xml:"http://www.iata.org/IATA/EDIST Certificates,omitempty"`
    MemberNumber       *MemberNumber_1 `xml:"http://www.iata.org/IATA/EDIST MemberNumber,omitempty"`
    RedemptionQuantity int             `xml:"http://www.iata.org/IATA/EDIST RedemptionQuantity,omitempty"`
    PromotionCode      string          `xml:"http://www.iata.org/IATA/EDIST PromotionCode,omitempty"`
    PromoVendorCode    []string        `xml:"http://www.iata.org/IATA/EDIST PromoVendorCode,omitempty"`
    Refs               string          `xml:",attr,omitempty"`
}

type Certificates_1 struct {
    CertificateNumber []*CertificateNumber_1 `xml:"http://www.iata.org/IATA/EDIST CertificateNumber,omitempty"`
}

type CertificateNumber_1 struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type MemberNumber_1 struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type VoucherMethod VoucherMethodType

type VoucherMethodType struct {
    Number              string                 `xml:"http://www.iata.org/IATA/EDIST Number,omitempty"`
    EffectiveExpireDate *EffectiveExpireDate_3 `xml:"http://www.iata.org/IATA/EDIST EffectiveExpireDate,omitempty"`
    RemainingValue      *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST RemainingValue,omitempty"`
    Refs                string                 `xml:",attr,omitempty"`
}

type EffectiveExpireDate_3 struct {
    Effective  string `xml:"http://www.iata.org/IATA/EDIST Effective,omitempty"`
    Expiration string `xml:"http://www.iata.org/IATA/EDIST Expiration,omitempty"`
}

type AltBaggageOfferType struct {
    BaggageOfferCoreType
    Associations *MultiAssociationType `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
}

type AltOtherOfferType struct {
    OtherOfferCoreType
    Associations *MultiAssociationType `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
}

type AltPricedFlightOfferType struct {
    PricedFlightOfferCoreType
    Associations *MultiAssociationType `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
}

type AltSeatOfferType struct {
    SeatOfferCoreType
    Associations *MultiAssociationType `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
}

type AltBaggageOffer AltBaggageOfferType

type AltPricedFlightOffer AltPricedFlightOfferType

type AltOtherOffer AltOtherOfferType

type AltSeatOffer AltSeatOfferType

type OrderPenalty OrderPenaltyType

type OrderPenaltyType struct {
    PenaltyType
}

type FlightPriceType struct {
    BaseAmount   *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST BaseAmount,omitempty"`
    Surcharges   *Surcharges_3          `xml:"http://www.iata.org/IATA/EDIST Surcharges,omitempty"`
    Taxes        *TaxDetailType         `xml:"http://www.iata.org/IATA/EDIST Taxes,omitempty"`
    TaxExemption *TaxExemptionType      `xml:"http://www.iata.org/IATA/EDIST TaxExemption,omitempty"`
    Fees         *FeeSurchargeType      `xml:"http://www.iata.org/IATA/EDIST Fees,omitempty"`
    Refs         string                 `xml:",attr,omitempty"`
}

type Surcharges_3 struct {
    Surcharge []*FeeSurchargeType `xml:"http://www.iata.org/IATA/EDIST Surcharge,omitempty"`
}

type OrderPriceType struct {
    DetailCurrencyPriceType
}

type ItemStatus CodesetType

type OrderProcessResultType struct {
    MarketingMessages *MarketingMessages `xml:"http://www.iata.org/IATA/EDIST MarketingMessages,omitempty"`
    Refs              string             `xml:",attr,omitempty"`
}

type OrderTimeLimits OrderTimeLimitsType

type OrderItemTimeLimits OrderItemTimeLimitsType

type OrderTimeLimitsType struct {
    PaymentTimeLimit    *PaymentTimeLimitType    `xml:"http://www.iata.org/IATA/EDIST PaymentTimeLimit,omitempty"`
    DepositTimeLimit    *DepositTimeLimitType    `xml:"http://www.iata.org/IATA/EDIST DepositTimeLimit,omitempty"`
    NamingTimeLimit     *NamingTimeLimitType     `xml:"http://www.iata.org/IATA/EDIST NamingTimeLimit,omitempty"`
    BilateralTimeLimits *BilateralTimeLimitsType `xml:"http://www.iata.org/IATA/EDIST BilateralTimeLimits,omitempty"`
}

type OrderItemTimeLimitsType struct {
    OrderTimeLimitsType
    PriceGuaranteeTimeLimits *PriceGuaranteeTimeLimitType `xml:"http://www.iata.org/IATA/EDIST PriceGuaranteeTimeLimits,omitempty"`
    TicketingTimeLimits      *TicketingTimeLimitType      `xml:"http://www.iata.org/IATA/EDIST TicketingTimeLimits,omitempty"`
}

type CarrierFeeInfo CarrierFeeInfoType

type CarrierFeeTax TaxDetailType

type CarrierFeeInfoType struct {
    PaymentForm *AcceptedPaymentFormType `xml:"http://www.iata.org/IATA/EDIST PaymentForm,omitempty"`
    CarrierFees *CarrierFees             `xml:"http://www.iata.org/IATA/EDIST CarrierFees,omitempty"`
    Taxes       *TaxDetailType           `xml:"http://www.iata.org/IATA/EDIST Taxes,omitempty"`
}

type CarrierFees struct {
    Fee []*Fee_1 `xml:"http://www.iata.org/IATA/EDIST Fee,omitempty"`
}

type Fee_1 struct {
    Type          *CodesetType             `xml:"http://www.iata.org/IATA/EDIST Type,omitempty"`
    FeeAmount     []*FeeAmount             `xml:"http://www.iata.org/IATA/EDIST FeeAmount,omitempty"`
    Taxes         *TaxDetailType           `xml:"http://www.iata.org/IATA/EDIST Taxes,omitempty"`
    PaymentForm   *AcceptedPaymentFormType `xml:"http://www.iata.org/IATA/EDIST PaymentForm,omitempty"`
    FareComponent *FareComponent_1         `xml:"http://www.iata.org/IATA/EDIST FareComponent,omitempty"`
    AirlineID     *AirlineID_2             `xml:"http://www.iata.org/IATA/EDIST AirlineID,omitempty"`
    FareClassCode string                   `xml:"http://www.iata.org/IATA/EDIST FareClassCode,omitempty"`
    ReportingCode string                   `xml:"http://www.iata.org/IATA/EDIST ReportingCode,omitempty"`
}

type FeeAmount struct {
    Type            string                 `xml:"http://www.iata.org/IATA/EDIST Type,omitempty"`
    Amount          *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST Amount,omitempty"`
    CurrencyCode    string                 `xml:"http://www.iata.org/IATA/EDIST CurrencyCode,omitempty"`
    ApplicationCode *CodesetType           `xml:"http://www.iata.org/IATA/EDIST ApplicationCode,omitempty"`
    DepartureCode   *DepartureCode         `xml:"http://www.iata.org/IATA/EDIST DepartureCode,omitempty"`
    ArrivalCode     *ArrivalCode           `xml:"http://www.iata.org/IATA/EDIST ArrivalCode,omitempty"`
}

type FareComponent_1 struct {
    Number       int    `xml:"http://www.iata.org/IATA/EDIST Number,omitempty"`
    TariffNumber string `xml:"http://www.iata.org/IATA/EDIST TariffNumber,omitempty"`
    RuleNumber   string `xml:"http://www.iata.org/IATA/EDIST RuleNumber,omitempty"`
    RuleCode     string `xml:"http://www.iata.org/IATA/EDIST RuleCode,omitempty"`
}

type AirlineID_2 struct {
    AirlineID_Type
    Name string `xml:",attr,omitempty"`
}

type AddlReferenceID AdditionalReferenceType

type AgentID CouponAgentType

type CheckedInAirlineInfo CouponSoldAirlineType

type CouponInfo CouponInfoType

type CouponEffective CouponEffectiveType

type CouponSoldAirline CouponSoldAirlineType

type CurrentAirlineInfo CouponSoldAirlineType

type FlightSegment CouponFlightSegmentType

type FlownAirlineInfo CouponSoldAirlineType

type InConnectionWithInfo InConnectionWithType

type IssuingAirlineInfo AirlineIssuanceType

type OriginalIssueInfo OriginalIssueType

type PricingInfo PricingInfoType

type TicketDocument struct {
    CouponTicketDocType
    PresentCreditCardInd       bool `xml:",attr,omitempty"`
    PenaltyRestrictionInd      bool `xml:",attr,omitempty"`
    NonCommissionableInd       bool `xml:",attr,omitempty"`
    NonInterlineableInd        bool `xml:",attr,omitempty"`
    NonReissuableNonExchangInd bool `xml:",attr,omitempty"`
    NonRefundableInd           bool `xml:",attr,omitempty"`
    TaxOnEMD_Ind               bool `xml:",attr,omitempty"`
    ExchTicketNbrInd           bool `xml:",attr,omitempty"`
    PrimaryDocInd              bool `xml:",attr,omitempty"`
}

type TicketDocQuantity int

type Traveller CouponTravelerCoreType

type TravelerInfo CouponTravelerDetailType

type AdditionalReferenceType struct {
    Type *CodesetType `xml:"http://www.iata.org/IATA/EDIST Type,omitempty"`
    ID   string       `xml:"http://www.iata.org/IATA/EDIST ID,omitempty"`
    Refs string       `xml:",attr,omitempty"`
}

type AirlineIssuanceType struct {
    AirlineName string `xml:"http://www.iata.org/IATA/EDIST AirlineName,omitempty"`
    Place       string `xml:"http://www.iata.org/IATA/EDIST Place,omitempty"`
}

type CouponAgentType struct {
    Type *CodesetType `xml:"http://www.iata.org/IATA/EDIST Type,omitempty"`
    ID   *ID_2        `xml:"http://www.iata.org/IATA/EDIST ID,omitempty"`
}

type ID_2 struct {
    Refs                 string `xml:",attr,omitempty"`
    ObjectMetaReferences string `xml:",attr,omitempty"`
    Name                 string `xml:",attr,omitempty"`
    Value                string `xml:",chardata"`
}

type CouponInfoType struct {
    CouponItinSequenceNbr    int                     `xml:"http://www.iata.org/IATA/EDIST CouponItinSequenceNbr,omitempty"`
    CouponNumber             int                     `xml:"http://www.iata.org/IATA/EDIST CouponNumber,omitempty"`
    CouponReference          string                  `xml:"http://www.iata.org/IATA/EDIST CouponReference,omitempty"`
    FareBasisCode            *FareBasisCodeType      `xml:"http://www.iata.org/IATA/EDIST FareBasisCode,omitempty"`
    CouponMedia              string                  `xml:"http://www.iata.org/IATA/EDIST CouponMedia,omitempty"`
    CouponValid              *CouponEffectiveType    `xml:"http://www.iata.org/IATA/EDIST CouponValid,omitempty"`
    Status                   *CodesetType            `xml:"http://www.iata.org/IATA/EDIST Status,omitempty"`
    InvoluntaryIndicatorCode *CodesetType            `xml:"http://www.iata.org/IATA/EDIST InvoluntaryIndicatorCode,omitempty"`
    SettlementAuthCode       string                  `xml:"http://www.iata.org/IATA/EDIST SettlementAuthCode,omitempty"`
    AssocFareBasis           string                  `xml:"http://www.iata.org/IATA/EDIST AssocFareBasis,omitempty"`
    PromoCode                *PromotionType          `xml:"http://www.iata.org/IATA/EDIST PromoCode,omitempty"`
    DateOfService            string                  `xml:"http://www.iata.org/IATA/EDIST DateOfService,omitempty"`
    ServiceQuantity          int                     `xml:"http://www.iata.org/IATA/EDIST ServiceQuantity,omitempty"`
    ServiceReferences        string                  `xml:"http://www.iata.org/IATA/EDIST ServiceReferences,omitempty"`
    Value                    *CurrencyAmountOptType  `xml:",chardata"`
    SoldAirlineInfo          *CouponSoldAirlineType  `xml:"http://www.iata.org/IATA/EDIST SoldAirlineInfo,omitempty"`
    CurrentAirlineInfo       *CouponSoldAirlineType  `xml:"http://www.iata.org/IATA/EDIST CurrentAirlineInfo,omitempty"`
    CheckedInAirlineInfo     *CouponSoldAirlineType  `xml:"http://www.iata.org/IATA/EDIST CheckedInAirlineInfo,omitempty"`
    FlownAirlineInfo         *CouponSoldAirlineType  `xml:"http://www.iata.org/IATA/EDIST FlownAirlineInfo,omitempty"`
    InConnectionWithInfo     *InConnectionWithType   `xml:"http://www.iata.org/IATA/EDIST InConnectionWithInfo,omitempty"`
    PresentInfo              *PresentInfo            `xml:"http://www.iata.org/IATA/EDIST PresentInfo,omitempty"`
    ReasonForIssuance        *ReasonForIssuance      `xml:"http://www.iata.org/IATA/EDIST ReasonForIssuance,omitempty"`
    FiledFeeInfo             *FiledFeeInfo           `xml:"http://www.iata.org/IATA/EDIST FiledFeeInfo,omitempty"`
    ProductCharacteristic    *ProductCharacteristic  `xml:"http://www.iata.org/IATA/EDIST ProductCharacteristic,omitempty"`
    AdditionalServicesInfo   *AdditionalServicesInfo `xml:"http://www.iata.org/IATA/EDIST AdditionalServicesInfo,omitempty"`
    ExcessBaggage            *ExcessBaggage          `xml:"http://www.iata.org/IATA/EDIST ExcessBaggage,omitempty"`
    ValidatingAirline        string                  `xml:"http://www.iata.org/IATA/EDIST ValidatingAirline,omitempty"`
    Remark                   string                  `xml:"http://www.iata.org/IATA/EDIST Remark,omitempty"`
    AddlBaggageInfo          *AddlBaggageInfoType    `xml:"http://www.iata.org/IATA/EDIST AddlBaggageInfo,omitempty"`
    ResAirlineDesig          string                  `xml:"http://www.iata.org/IATA/EDIST ResAirlineDesig,omitempty"`
    ResDateOfFlight          string                  `xml:"http://www.iata.org/IATA/EDIST ResDateOfFlight,omitempty"`
    ResDesigAirportCityCode  string                  `xml:"http://www.iata.org/IATA/EDIST ResDesigAirportCityCode,omitempty"`
    ResFlightDepartureTime   string                  `xml:"http://www.iata.org/IATA/EDIST ResFlightDepartureTime,omitempty"`
    ResFlightNumber          string                  `xml:"http://www.iata.org/IATA/EDIST ResFlightNumber,omitempty"`
    ResOriginAirportCityCode string                  `xml:"http://www.iata.org/IATA/EDIST ResOriginAirportCityCode,omitempty"`
    ResBookDesign            string                  `xml:"http://www.iata.org/IATA/EDIST ResBookDesign,omitempty"`
    ResStatusCode            *CodesetType            `xml:"http://www.iata.org/IATA/EDIST ResStatusCode,omitempty"`
    ConsumedAtIssuanceInd    bool                    `xml:",attr,omitempty"`
}

type PresentInfo struct {
    To string `xml:",attr,omitempty"`
    At string `xml:",attr,omitempty"`
}

type ReasonForIssuance struct {
    RFIC        *CodesetType `xml:"http://www.iata.org/IATA/EDIST RFIC,omitempty"`
    Code        string       `xml:"http://www.iata.org/IATA/EDIST Code,omitempty"`
    Description string       `xml:"http://www.iata.org/IATA/EDIST Description,omitempty"`
}

type FiledFeeInfo struct {
    Amount   *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST Amount,omitempty"`
    BSR_Rate float64                `xml:",attr,omitempty"`
}

type ProductCharacteristic struct {
    Type        string `xml:"http://www.iata.org/IATA/EDIST Type,omitempty"`
    Description string `xml:"http://www.iata.org/IATA/EDIST Description,omitempty"`
}

type AdditionalServicesInfo struct {
    Group             string       `xml:"http://www.iata.org/IATA/EDIST Group,omitempty"`
    SubGroup          string       `xml:"http://www.iata.org/IATA/EDIST SubGroup,omitempty"`
    RFISC_DefinedType *CodesetType `xml:"http://www.iata.org/IATA/EDIST RFISC_DefinedType,omitempty"`
    ServiceType       string       `xml:"http://www.iata.org/IATA/EDIST ServiceType,omitempty"`
}

type ExcessBaggage struct {
    Amount            *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST Amount,omitempty"`
    UnitOfMeasureCode *CodesetType           `xml:"http://www.iata.org/IATA/EDIST UnitOfMeasureCode,omitempty"`
    Quantity          float64                `xml:"http://www.iata.org/IATA/EDIST Quantity,omitempty"`
    UnitOfMeasure     string                 `xml:"http://www.iata.org/IATA/EDIST UnitOfMeasure,omitempty"`
}

type CouponEffectiveType struct {
    EffectiveDatePeriod     *EffectiveDatePeriod     `xml:"http://www.iata.org/IATA/EDIST EffectiveDatePeriod,omitempty"`
    EffectiveDateTimePeriod *EffectiveDateTimePeriod `xml:"http://www.iata.org/IATA/EDIST EffectiveDateTimePeriod,omitempty"`
    Refs                    string                   `xml:",attr,omitempty"`
}

type EffectiveDatePeriod struct {
    DatePeriodRepType
    Duration string `xml:"http://www.iata.org/IATA/EDIST Duration,omitempty"`
}

type EffectiveDateTimePeriod struct {
    DateTimePeriodRepType
    Duration string `xml:"http://www.iata.org/IATA/EDIST Duration,omitempty"`
}

type CouponOrderKeyType struct {
    OrderID            *OrderID_Type            `xml:"http://www.iata.org/IATA/EDIST OrderID,omitempty"`
    OrderItemID        *ItemID_Type             `xml:"http://www.iata.org/IATA/EDIST OrderItemID,omitempty"`
    OfferItemID        *ItemID_Type             `xml:"http://www.iata.org/IATA/EDIST OfferItemID,omitempty"`
    ServiceID          []*ServiceID_Type        `xml:"http://www.iata.org/IATA/EDIST ServiceID,omitempty"`
    BookingReference   *BookingReferenceType    `xml:"http://www.iata.org/IATA/EDIST BookingReference,omitempty"`
    ShoppingResponseID *ShoppingResponseID_Type `xml:"http://www.iata.org/IATA/EDIST ShoppingResponseID,omitempty"`
}

type CouponSoldAirlineType struct {
    DepartureDateTime        *CoreDateGrpType            `xml:"http://www.iata.org/IATA/EDIST DepartureDateTime,omitempty"`
    ArrivalDateTime          *CoreDateGrpType            `xml:"http://www.iata.org/IATA/EDIST ArrivalDateTime,omitempty"`
    Stops                    *Stops_1                    `xml:"http://www.iata.org/IATA/EDIST Stops,omitempty"`
    InfoSource               string                      `xml:"http://www.iata.org/IATA/EDIST InfoSource,omitempty"`
    TourOperatorFlightID     string                      `xml:"http://www.iata.org/IATA/EDIST TourOperatorFlightID,omitempty"`
    Departure                *FlightDepartureType        `xml:"http://www.iata.org/IATA/EDIST Departure,omitempty"`
    Arrival                  *FlightArrivalType          `xml:"http://www.iata.org/IATA/EDIST Arrival,omitempty"`
    OperatingCarrier         *OperatingCarrier_4         `xml:"http://www.iata.org/IATA/EDIST OperatingCarrier,omitempty"`
    MarketingCarrier         *MarketingCarrierFlightType `xml:"http://www.iata.org/IATA/EDIST MarketingCarrier,omitempty"`
    Equipment                []*AircraftSummaryType      `xml:"http://www.iata.org/IATA/EDIST Equipment,omitempty"`
    Status                   *CodesetType                `xml:"http://www.iata.org/IATA/EDIST Status,omitempty"`
    SeatNumber               string                      `xml:"http://www.iata.org/IATA/EDIST SeatNumber,omitempty"`
    TicketedBaggageAllowance *AddlBaggageInfoType        `xml:"http://www.iata.org/IATA/EDIST TicketedBaggageAllowance,omitempty"`
    Refs                     string                      `xml:",attr,omitempty"`
    ObjectKey                string                      `xml:",attr,omitempty"`
    MetadataToken            string                      `xml:",attr,omitempty"`
}

type Stops_1 struct {
    StopQuantity  int               `xml:"http://www.iata.org/IATA/EDIST StopQuantity,omitempty"`
    StopLocations *StopLocationType `xml:"http://www.iata.org/IATA/EDIST StopLocations,omitempty"`
}

type OperatingCarrier_4 struct {
    OperatingCarrierFlightType
    Disclosures *DisclosureType `xml:"http://www.iata.org/IATA/EDIST Disclosures,omitempty"`
}

type CouponFlightSegmentType struct {
    InfoSource               string                      `xml:"http://www.iata.org/IATA/EDIST InfoSource,omitempty"`
    DepartureDateTime        *CoreDateGrpType            `xml:"http://www.iata.org/IATA/EDIST DepartureDateTime,omitempty"`
    TourOperatorFlightID     string                      `xml:"http://www.iata.org/IATA/EDIST TourOperatorFlightID,omitempty"`
    Departure                *FlightDepartureType        `xml:"http://www.iata.org/IATA/EDIST Departure,omitempty"`
    Arrival                  *FlightArrivalType          `xml:"http://www.iata.org/IATA/EDIST Arrival,omitempty"`
    OperatingCarrier         *OperatingCarrier_5         `xml:"http://www.iata.org/IATA/EDIST OperatingCarrier,omitempty"`
    MarketingCarrier         *MarketingCarrierFlightType `xml:"http://www.iata.org/IATA/EDIST MarketingCarrier,omitempty"`
    Equipment                []*AircraftSummaryType      `xml:"http://www.iata.org/IATA/EDIST Equipment,omitempty"`
    InvoluntaryIndicatorCode *CodesetType                `xml:"http://www.iata.org/IATA/EDIST InvoluntaryIndicatorCode,omitempty"`
    Refs                     string                      `xml:",attr,omitempty"`
}

type OperatingCarrier_5 struct {
    OperatingCarrierFlightType
    Disclosures *DisclosureType `xml:"http://www.iata.org/IATA/EDIST Disclosures,omitempty"`
}

type CouponTicketDocType struct {
    TicketDocNbr       string               `xml:"http://www.iata.org/IATA/EDIST TicketDocNbr,omitempty"`
    Type               *CodesetType         `xml:"http://www.iata.org/IATA/EDIST Type,omitempty"`
    InConnectionDocNbr string               `xml:"http://www.iata.org/IATA/EDIST InConnectionDocNbr,omitempty"`
    NumberofBooklets   int                  `xml:"http://www.iata.org/IATA/EDIST NumberofBooklets,omitempty"`
    DateOfIssue        string               `xml:"http://www.iata.org/IATA/EDIST DateOfIssue,omitempty"`
    TimeOfIssue        string               `xml:"http://www.iata.org/IATA/EDIST TimeOfIssue,omitempty"`
    TicketingLocation  string               `xml:"http://www.iata.org/IATA/EDIST TicketingLocation,omitempty"`
    RoutingDomIntCode  string               `xml:"http://www.iata.org/IATA/EDIST RoutingDomIntCode,omitempty"`
    FeeOwner           *AirlineID_Type      `xml:"http://www.iata.org/IATA/EDIST FeeOwner,omitempty"`
    AddlBaggageInfo    *AddlBaggageInfoType `xml:"http://www.iata.org/IATA/EDIST AddlBaggageInfo,omitempty"`
    Remark             string               `xml:"http://www.iata.org/IATA/EDIST Remark,omitempty"`
    CouponInfo         []*CouponInfoType    `xml:"http://www.iata.org/IATA/EDIST CouponInfo,omitempty"`
    PenaltyReferences  string               `xml:"http://www.iata.org/IATA/EDIST PenaltyReferences,omitempty"`
    ReportingType      string               `xml:"http://www.iata.org/IATA/EDIST ReportingType,omitempty"`
}

type CouponTravelerCoreType struct {
    Surname        *Surname_4           `xml:"http://www.iata.org/IATA/EDIST Surname,omitempty"`
    Given          *Given_4             `xml:"http://www.iata.org/IATA/EDIST Given,omitempty"`
    PTC            string               `xml:"http://www.iata.org/IATA/EDIST PTC,omitempty"`
    FQTV           []*TravelerFQTV_Type `xml:"http://www.iata.org/IATA/EDIST FQTV,omitempty"`
    BirthDate      string               `xml:"http://www.iata.org/IATA/EDIST BirthDate,omitempty"`
    InfantOnLapInd bool                 `xml:",attr,omitempty"`
}

type Surname_4 struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type Given_4 struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type CouponTravelerDetailType struct {
    CouponTravelerCoreType
    EmailContact []*EmailType `xml:"http://www.iata.org/IATA/EDIST EmailContact,omitempty"`
    PhoneContact []*PhoneType `xml:"http://www.iata.org/IATA/EDIST PhoneContact,omitempty"`
}

type HistoryCouponInfoType struct {
    CouponNumber         int                    `xml:"http://www.iata.org/IATA/EDIST CouponNumber,omitempty"`
    Status               *CodesetType           `xml:"http://www.iata.org/IATA/EDIST Status,omitempty"`
    PrevStatus           *CodesetType           `xml:"http://www.iata.org/IATA/EDIST PrevStatus,omitempty"`
    SettlementAuthCode   string                 `xml:"http://www.iata.org/IATA/EDIST SettlementAuthCode,omitempty"`
    TransactionTimeStamp string                 `xml:"http://www.iata.org/IATA/EDIST TransactionTimeStamp,omitempty"`
    SoldAirlineInfo      *CouponSoldAirlineType `xml:"http://www.iata.org/IATA/EDIST SoldAirlineInfo,omitempty"`
    CurrentAirlineInfo   *CouponSoldAirlineType `xml:"http://www.iata.org/IATA/EDIST CurrentAirlineInfo,omitempty"`
    CheckedInAirlineInfo *CouponSoldAirlineType `xml:"http://www.iata.org/IATA/EDIST CheckedInAirlineInfo,omitempty"`
    Party                *MsgPartiesType        `xml:"http://www.iata.org/IATA/EDIST Party,omitempty"`
}

type InConnectionWithType struct {
    InConnectionDocNbr string `xml:"http://www.iata.org/IATA/EDIST InConnectionDocNbr,omitempty"`
    InConnectonCpnNbr  int    `xml:"http://www.iata.org/IATA/EDIST InConnectonCpnNbr,omitempty"`
    AssociateInd       bool   `xml:",attr,omitempty"`
}

type OriginalIssueType struct {
    IssuingAgentID    string `xml:"http://www.iata.org/IATA/EDIST IssuingAgentID,omitempty"`
    DateOfIssue       string `xml:"http://www.iata.org/IATA/EDIST DateOfIssue,omitempty"`
    LocationCode      string `xml:"http://www.iata.org/IATA/EDIST LocationCode,omitempty"`
    TicketDocumentNbr string `xml:"http://www.iata.org/IATA/EDIST TicketDocumentNbr,omitempty"`
}

type PricingInfoType struct {
    Date         string           `xml:"http://www.iata.org/IATA/EDIST Date,omitempty"`
    Time         string           `xml:"http://www.iata.org/IATA/EDIST Time,omitempty"`
    LocationCode string           `xml:"http://www.iata.org/IATA/EDIST LocationCode,omitempty"`
    CountryCode  *CountryCodeType `xml:"http://www.iata.org/IATA/EDIST CountryCode,omitempty"`
}

type TicketType struct {
    TicketDocNbr string       `xml:"http://www.iata.org/IATA/EDIST TicketDocNbr,omitempty"`
    Type         *CodesetType `xml:"http://www.iata.org/IATA/EDIST Type,omitempty"`
}

type TicketDocumentType struct {
    TicketDocNbr string       `xml:"http://www.iata.org/IATA/EDIST TicketDocNbr,omitempty"`
    Type         *CodesetType `xml:"http://www.iata.org/IATA/EDIST Type,omitempty"`
    CpnNbrs      *CpnNbrs     `xml:"http://www.iata.org/IATA/EDIST CpnNbrs,omitempty"`
}

type CpnNbrs struct {
    CpnNbr []int `xml:"http://www.iata.org/IATA/EDIST CpnNbr,omitempty"`
}

type TicketDocHistoryType struct {
    TicketDocNbr string                   `xml:"http://www.iata.org/IATA/EDIST TicketDocNbr,omitempty"`
    Type         *CodesetType             `xml:"http://www.iata.org/IATA/EDIST Type,omitempty"`
    CouponInfo   []*HistoryCouponInfoType `xml:"http://www.iata.org/IATA/EDIST CouponInfo,omitempty"`
}

type BaseFare BaseFareTransactionType

type EquivFare EquivFareTransactionType

type FareInfo ET_FareInfoType

type Total_1 TotalFareTransactionType

type UnstructuredFareCalcInfo UnstructuredFareCalcType

type BaseFareTransactionType struct {
    Amount     *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST Amount,omitempty"`
    AmountType string                 `xml:"http://www.iata.org/IATA/EDIST AmountType,omitempty"`
    Purpose    string                 `xml:"http://www.iata.org/IATA/EDIST Purpose,omitempty"`
}

type EquivFareTransactionType struct {
    Amount    *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST Amount,omitempty"`
    Purpose   string                 `xml:"http://www.iata.org/IATA/EDIST Purpose,omitempty"`
    EquivRate float64                `xml:",attr,omitempty"`
}

type ET_FareInfoType struct {
    Waiver  []*Waiver  `xml:"http://www.iata.org/IATA/EDIST Waiver,omitempty"`
    RuleInd []*RuleInd `xml:"http://www.iata.org/IATA/EDIST RuleInd,omitempty"`
    Detail  *Detail_3  `xml:"http://www.iata.org/IATA/EDIST Detail,omitempty"`
    Refs    string     `xml:",attr,omitempty"`
}

type Waiver struct {
    Code string `xml:"http://www.iata.org/IATA/EDIST Code,omitempty"`
    Type string `xml:"http://www.iata.org/IATA/EDIST Type,omitempty"`
}

type RuleInd struct {
    RuleCode int `xml:"http://www.iata.org/IATA/EDIST RuleCode,omitempty"`
}

type Detail_3 struct {
    NetReportingCode  string           `xml:"http://www.iata.org/IATA/EDIST NetReportingCode,omitempty"`
    StatisticalCode   string           `xml:"http://www.iata.org/IATA/EDIST StatisticalCode,omitempty"`
    TourCode          string           `xml:"http://www.iata.org/IATA/EDIST TourCode,omitempty"`
    CountryCode       *CountryCodeType `xml:"http://www.iata.org/IATA/EDIST CountryCode,omitempty"`
    PricingSystemCode string           `xml:"http://www.iata.org/IATA/EDIST PricingSystemCode,omitempty"`
}

type TotalFareTransactionType struct {
    Amount        *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST Amount,omitempty"`
    AmountType    string                 `xml:"http://www.iata.org/IATA/EDIST AmountType,omitempty"`
    Purpose       string                 `xml:"http://www.iata.org/IATA/EDIST Purpose,omitempty"`
    EquivRate     float64                `xml:",attr,omitempty"`
    ReissueSeqNbr string                 `xml:",attr,omitempty"`
}

type UnstructuredFareCalcType struct {
    Purpose       string `xml:"http://www.iata.org/IATA/EDIST Purpose,omitempty"`
    PricingCode   string `xml:"http://www.iata.org/IATA/EDIST PricingCode,omitempty"`
    ReportingCode string `xml:"http://www.iata.org/IATA/EDIST ReportingCode,omitempty"`
    Info          string `xml:"http://www.iata.org/IATA/EDIST Info,omitempty"`
}

type PaymentForm_1 AcceptedPaymentFormType

type AcceptedPaymentFormType struct {
    Type              *CodesetType              `xml:"http://www.iata.org/IATA/EDIST Type,omitempty"`
    Amount            *SimpleCurrencyPriceType  `xml:"http://www.iata.org/IATA/EDIST Amount,omitempty"`
    Qualifier         *CodesetType              `xml:"http://www.iata.org/IATA/EDIST Qualifier,omitempty"`
    Voucher           *Voucher                  `xml:"http://www.iata.org/IATA/EDIST Voucher,omitempty"`
    PaymentCard       *PaymentCardType          `xml:"http://www.iata.org/IATA/EDIST PaymentCard,omitempty"`
    Other             *Other                    `xml:"http://www.iata.org/IATA/EDIST Other,omitempty"`
    MiscChargeOrder   *MiscChargeOrder          `xml:"http://www.iata.org/IATA/EDIST MiscChargeOrder,omitempty"`
    LoyaltyRedemption *LoyaltyRedemption        `xml:"http://www.iata.org/IATA/EDIST LoyaltyRedemption,omitempty"`
    DirectBill        *DirectBillType           `xml:"http://www.iata.org/IATA/EDIST DirectBill,omitempty"`
    Cash              *Cash                     `xml:"http://www.iata.org/IATA/EDIST Cash,omitempty"`
    BankAccount       *BankAccountType          `xml:"http://www.iata.org/IATA/EDIST BankAccount,omitempty"`
    Associations      *OrderItemAssociationType `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
    Refs              string                    `xml:",attr,omitempty"`
}

type OrderPaymentFormType struct {
    Method       *Method_1                `xml:"http://www.iata.org/IATA/EDIST Method,omitempty"`
    Amount       *SimpleCurrencyPriceType `xml:"http://www.iata.org/IATA/EDIST Amount,omitempty"`
    Associations *Associations_5          `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
    Payer        *Payer                   `xml:"http://www.iata.org/IATA/EDIST Payer,omitempty"`
    Qualifier    *CodesetType             `xml:"http://www.iata.org/IATA/EDIST Qualifier,omitempty"`
    Refs         string                   `xml:",attr,omitempty"`
}

type Method_1 struct {
    Voucher           *Voucher           `xml:"http://www.iata.org/IATA/EDIST Voucher,omitempty"`
    PaymentCard       *PaymentCardType   `xml:"http://www.iata.org/IATA/EDIST PaymentCard,omitempty"`
    Other             *Other             `xml:"http://www.iata.org/IATA/EDIST Other,omitempty"`
    MiscChargeOrder   *MiscChargeOrder   `xml:"http://www.iata.org/IATA/EDIST MiscChargeOrder,omitempty"`
    LoyaltyRedemption *LoyaltyRedemption `xml:"http://www.iata.org/IATA/EDIST LoyaltyRedemption,omitempty"`
    DirectBill        *DirectBillType    `xml:"http://www.iata.org/IATA/EDIST DirectBill,omitempty"`
    Cash              *Cash              `xml:"http://www.iata.org/IATA/EDIST Cash,omitempty"`
    BankAccount       *BankAccountType   `xml:"http://www.iata.org/IATA/EDIST BankAccount,omitempty"`
    Check             *Check             `xml:"http://www.iata.org/IATA/EDIST Check,omitempty"`
}

type Associations_5 struct {
    Passengers    *Passengers_10 `xml:"http://www.iata.org/IATA/EDIST Passengers,omitempty"`
    OfferItemSets *OfferItemSets `xml:"http://www.iata.org/IATA/EDIST OfferItemSets,omitempty"`
}

type Passengers_10 struct {
    PassengerReference string `xml:"http://www.iata.org/IATA/EDIST PassengerReference,omitempty"`
    GroupReference     string `xml:"http://www.iata.org/IATA/EDIST GroupReference,omitempty"`
}

type OfferItemSets struct {
    OfferItemSetRefs string `xml:"http://www.iata.org/IATA/EDIST OfferItemSetRefs,omitempty"`
}

type Payer struct {
    Name     *Name_7   `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
    Contacts *Contacts `xml:"http://www.iata.org/IATA/EDIST Contacts,omitempty"`
}

type Name_7 struct {
    Surname              *Surname_5  `xml:"http://www.iata.org/IATA/EDIST Surname,omitempty"`
    Given                []*Given_5  `xml:"http://www.iata.org/IATA/EDIST Given,omitempty"`
    Title                string      `xml:"http://www.iata.org/IATA/EDIST Title,omitempty"`
    SurnameSuffix        string      `xml:"http://www.iata.org/IATA/EDIST SurnameSuffix,omitempty"`
    Middle               []*Middle_4 `xml:"http://www.iata.org/IATA/EDIST Middle,omitempty"`
    Refs                 string      `xml:",attr,omitempty"`
    ObjectMetaReferences string      `xml:",attr,omitempty"`
}

type Surname_5 struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type Given_5 struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type Middle_4 struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type PaymentMethod PaymentMethodType

type PaymentMethodType struct {
    Method []*Method_2 `xml:"http://www.iata.org/IATA/EDIST Method,omitempty"`
}

type Method_2 struct {
    Qualifier         *CodesetType           `xml:"http://www.iata.org/IATA/EDIST Qualifier,omitempty"`
    VoucherMethod     *VoucherMethodType     `xml:"http://www.iata.org/IATA/EDIST VoucherMethod,omitempty"`
    RedemptionMethod  *RedemptionMethodType  `xml:"http://www.iata.org/IATA/EDIST RedemptionMethod,omitempty"`
    PaymentCardMethod *PaymentCardMethodType `xml:"http://www.iata.org/IATA/EDIST PaymentCardMethod,omitempty"`
    OtherMethod       *OtherMethodType       `xml:"http://www.iata.org/IATA/EDIST OtherMethod,omitempty"`
    MiscChargeMethod  *MiscChargeMethodType  `xml:"http://www.iata.org/IATA/EDIST MiscChargeMethod,omitempty"`
    DirectBillMethod  *DirectBillMethodType  `xml:"http://www.iata.org/IATA/EDIST DirectBillMethod,omitempty"`
    CashMethod        *CashMethodType        `xml:"http://www.iata.org/IATA/EDIST CashMethod,omitempty"`
    BankAccountMethod *BankAccountMethodType `xml:"http://www.iata.org/IATA/EDIST BankAccountMethod,omitempty"`
    Check             *Check                 `xml:"http://www.iata.org/IATA/EDIST Check,omitempty"`
    Promotions        *Promotions_1          `xml:"http://www.iata.org/IATA/EDIST Promotions,omitempty"`
    Refs              string                 `xml:",attr,omitempty"`
}

type Promotions_1 struct {
    Promotion []*Promotion_1 `xml:"http://www.iata.org/IATA/EDIST Promotion,omitempty"`
}

type Promotion_1 struct {
    PromotionType
    Associations *OrderItemAssociationType `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
}

type OrderPaymentMethodType struct {
    Type         *CodesetType             `xml:"http://www.iata.org/IATA/EDIST Type,omitempty"`
    Method       *Method_3                `xml:"http://www.iata.org/IATA/EDIST Method,omitempty"`
    Amount       *SimpleCurrencyPriceType `xml:"http://www.iata.org/IATA/EDIST Amount,omitempty"`
    Qualifier    *CodesetType             `xml:"http://www.iata.org/IATA/EDIST Qualifier,omitempty"`
    Promotions   *Promotions_2            `xml:"http://www.iata.org/IATA/EDIST Promotions,omitempty"`
    Associations *Associations_6          `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
    Description  *Description             `xml:"http://www.iata.org/IATA/EDIST Description,omitempty"`
    SplitFormInd bool                     `xml:",attr,omitempty"`
}

type Method_3 struct {
    BankAccountMethod *BankAccountMethodType `xml:"http://www.iata.org/IATA/EDIST BankAccountMethod,omitempty"`
    CashMethod        *CashMethodType        `xml:"http://www.iata.org/IATA/EDIST CashMethod,omitempty"`
    DirectBillMethod  *DirectBillMethodType  `xml:"http://www.iata.org/IATA/EDIST DirectBillMethod,omitempty"`
    MiscChargeMethod  *MiscChargeMethodType  `xml:"http://www.iata.org/IATA/EDIST MiscChargeMethod,omitempty"`
    OtherMethod       *OtherMethodType       `xml:"http://www.iata.org/IATA/EDIST OtherMethod,omitempty"`
    PaymentCardMethod *PaymentCardMethodType `xml:"http://www.iata.org/IATA/EDIST PaymentCardMethod,omitempty"`
    RedemptionMethod  *RedemptionMethodType  `xml:"http://www.iata.org/IATA/EDIST RedemptionMethod,omitempty"`
    VoucherMethod     *VoucherMethodType     `xml:"http://www.iata.org/IATA/EDIST VoucherMethod,omitempty"`
    Check             *Check                 `xml:"http://www.iata.org/IATA/EDIST Check,omitempty"`
    Refs              string                 `xml:",attr,omitempty"`
}

type Promotions_2 struct {
    Promotion []*Promotion_2 `xml:"http://www.iata.org/IATA/EDIST Promotion,omitempty"`
}

type Promotion_2 struct {
    PromotionType
    Associations *OrderItemAssociationType `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
}

type Associations_6 struct {
    OrderID          *OrderID_Type         `xml:"http://www.iata.org/IATA/EDIST OrderID,omitempty"`
    OrderItemID      []*ItemID_Type        `xml:"http://www.iata.org/IATA/EDIST OrderItemID,omitempty"`
    Passengers       *Passengers_11        `xml:"http://www.iata.org/IATA/EDIST Passengers,omitempty"`
    OtherAssociation []*OtherAssociation_9 `xml:"http://www.iata.org/IATA/EDIST OtherAssociation,omitempty"`
}

type Passengers_11 struct {
    PassengerReference string `xml:"http://www.iata.org/IATA/EDIST PassengerReference,omitempty"`
    GroupReference     string `xml:"http://www.iata.org/IATA/EDIST GroupReference,omitempty"`
}

type OtherAssociation_9 struct {
    Type     string `xml:",attr,omitempty"`
    RefValue string `xml:",attr,omitempty"`
}

type PriceClassAssocListItemType string

type PriceClassAssocListType string

type PriceClassAssocType struct {
    Association []*Association_1 `xml:"http://www.iata.org/IATA/EDIST Association,omitempty"`
}

type Association_1 struct {
    Type             string   `xml:"http://www.iata.org/IATA/EDIST Type,omitempty"`
    ReferenceValue   string   `xml:"http://www.iata.org/IATA/EDIST ReferenceValue,omitempty"`
    ServiceReference []string `xml:"http://www.iata.org/IATA/EDIST ServiceReference,omitempty"`
}

type PriceClassType struct {
    Name           string                `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
    Code           string                `xml:"http://www.iata.org/IATA/EDIST Code,omitempty"`
    Descriptions   *Descriptions         `xml:"http://www.iata.org/IATA/EDIST Descriptions,omitempty"`
    FareBasisCode  *FareBasisCodeType    `xml:"http://www.iata.org/IATA/EDIST FareBasisCode,omitempty"`
    ClassOfService []*FlightCOS_CoreType `xml:"http://www.iata.org/IATA/EDIST ClassOfService,omitempty"`
    Associations   *PriceClassAssocType  `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
    DisplayOrder   string                `xml:"http://www.iata.org/IATA/EDIST DisplayOrder,omitempty"`
    Refs           string                `xml:",attr,omitempty"`
    ObjectKey      string                `xml:",attr,omitempty"`
}

type ListOfSeatType struct {
    Location *SeatLocationType `xml:"http://www.iata.org/IATA/EDIST Location,omitempty"`
    Details  string            `xml:"http://www.iata.org/IATA/EDIST Details,omitempty"`
    Refs     string            `xml:",attr,omitempty"`
    ListKey  string            `xml:",attr,omitempty"`
}

type SeatCoreType struct {
    Location *SeatLocationType `xml:"http://www.iata.org/IATA/EDIST Location,omitempty"`
    Refs     string            `xml:",attr,omitempty"`
}

type SeatDetailType struct {
    SeatCoreType
    Details *SeatCharacteristicType `xml:"http://www.iata.org/IATA/EDIST Details,omitempty"`
}

type SeatLocationType struct {
    Column          string                    `xml:"http://www.iata.org/IATA/EDIST Column,omitempty"`
    Row             *Row                      `xml:"http://www.iata.org/IATA/EDIST Row,omitempty"`
    Characteristics *Characteristics          `xml:"http://www.iata.org/IATA/EDIST Characteristics,omitempty"`
    Associations    *OrderItemAssociationType `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
}

type Row struct {
    Number   *SeatMapRowNbrType `xml:"http://www.iata.org/IATA/EDIST Number,omitempty"`
    Type     *CodesetType       `xml:"http://www.iata.org/IATA/EDIST Type,omitempty"`
    SeatData *SeatDataType      `xml:"http://www.iata.org/IATA/EDIST SeatData,omitempty"`
    Refs     string             `xml:",attr,omitempty"`
}

type Characteristics struct {
    Characteristic []*Characteristic `xml:"http://www.iata.org/IATA/EDIST Characteristic,omitempty"`
}

type Characteristic struct {
    CodesetType
    Remarks *RemarkType `xml:"http://www.iata.org/IATA/EDIST Remarks,omitempty"`
}

type SeatCharacteristicType struct {
}

type ExitRowPosition ExitPositionType

type ExitPositionType struct {
    Rows             []*Rows           `xml:"http://www.iata.org/IATA/EDIST Rows,omitempty"`
    SegmentReference *SegmentReference `xml:"http://www.iata.org/IATA/EDIST SegmentReference,omitempty"`
    Refs             string            `xml:",attr,omitempty"`
}

type Rows struct {
    First            *First            `xml:"http://www.iata.org/IATA/EDIST First,omitempty"`
    Last             *Last             `xml:"http://www.iata.org/IATA/EDIST Last,omitempty"`
    Position         *CodesetType      `xml:"http://www.iata.org/IATA/EDIST Position,omitempty"`
    CabinType        *CabinType        `xml:"http://www.iata.org/IATA/EDIST CabinType,omitempty"`
    SegmentReference *SegmentReference `xml:"http://www.iata.org/IATA/EDIST SegmentReference,omitempty"`
    UpperDeckInd     bool              `xml:",attr,omitempty"`
    Refs             string            `xml:",attr,omitempty"`
}

type First struct {
    Columns string `xml:",attr,omitempty"`
    Value   string `xml:",chardata"`
}

type Last struct {
    Columns string `xml:",attr,omitempty"`
    Value   string `xml:",chardata"`
}

type WingPosition WingPositionType

type WingPositionType struct {
    Rows             []*Rows_1         `xml:"http://www.iata.org/IATA/EDIST Rows,omitempty"`
    SegmentReference *SegmentReference `xml:"http://www.iata.org/IATA/EDIST SegmentReference,omitempty"`
    Refs             string            `xml:",attr,omitempty"`
}

type Rows_1 struct {
    First            string            `xml:"http://www.iata.org/IATA/EDIST First,omitempty"`
    Last             string            `xml:"http://www.iata.org/IATA/EDIST Last,omitempty"`
    SegmentReference *SegmentReference `xml:"http://www.iata.org/IATA/EDIST SegmentReference,omitempty"`
    UpperDeckInd     bool              `xml:",attr,omitempty"`
    Refs             string            `xml:",attr,omitempty"`
}

type CabinCameraPosType struct {
    Row    *Row_1  `xml:"http://www.iata.org/IATA/EDIST Row,omitempty"`
    Column *Column `xml:"http://www.iata.org/IATA/EDIST Column,omitempty"`
}

type Row_1 struct {
    Position    int          `xml:"http://www.iata.org/IATA/EDIST Position,omitempty"`
    Orientation *CodesetType `xml:"http://www.iata.org/IATA/EDIST Orientation,omitempty"`
}

type Column struct {
    Position    string       `xml:"http://www.iata.org/IATA/EDIST Position,omitempty"`
    Orientation *CodesetType `xml:"http://www.iata.org/IATA/EDIST Orientation,omitempty"`
}

type SeatDisplay struct {
    SeatDisplayKey string               `xml:"http://www.iata.org/IATA/EDIST SeatDisplayKey,omitempty"`
    Columns        []*Columns           `xml:"http://www.iata.org/IATA/EDIST Columns,omitempty"`
    Rows           *Rows_2              `xml:"http://www.iata.org/IATA/EDIST Rows,omitempty"`
    Component      []*SeatComponentType `xml:"http://www.iata.org/IATA/EDIST Component,omitempty"`
    CabinType      *CabinType           `xml:"http://www.iata.org/IATA/EDIST CabinType,omitempty"`
    Refs           string               `xml:",attr,omitempty"`
}

type Columns struct {
    Position string `xml:",attr,omitempty"`
    Value    string `xml:",chardata"`
}

type Rows_2 struct {
    First           int           `xml:"http://www.iata.org/IATA/EDIST First,omitempty"`
    Last            int           `xml:"http://www.iata.org/IATA/EDIST Last,omitempty"`
    SectionSeatData *SeatDataType `xml:"http://www.iata.org/IATA/EDIST SectionSeatData,omitempty"`
}

type SeatData SeatDataType

type SeatComponentType struct {
    Locations  *Locations    `xml:"http://www.iata.org/IATA/EDIST Locations,omitempty"`
    Type       *CodesetType  `xml:"http://www.iata.org/IATA/EDIST Type,omitempty"`
    Dimensions *Dimensions_2 `xml:"http://www.iata.org/IATA/EDIST Dimensions,omitempty"`
    Refs       string        `xml:",attr,omitempty"`
}

type Locations struct {
    Location []*Location_3 `xml:"http://www.iata.org/IATA/EDIST Location,omitempty"`
}

type Location_3 struct {
    Space  *Space    `xml:"http://www.iata.org/IATA/EDIST Space,omitempty"`
    Row    *Row_2    `xml:"http://www.iata.org/IATA/EDIST Row,omitempty"`
    Column *Column_1 `xml:"http://www.iata.org/IATA/EDIST Column,omitempty"`
    Refs   string    `xml:",attr,omitempty"`
}

type Space struct {
    RowRange    []*RowRange  `xml:"http://www.iata.org/IATA/EDIST RowRange,omitempty"`
    ColumnRange *ColumnRange `xml:"http://www.iata.org/IATA/EDIST ColumnRange,omitempty"`
}

type RowRange struct {
    Begin int `xml:"http://www.iata.org/IATA/EDIST Begin,omitempty"`
    End   int `xml:"http://www.iata.org/IATA/EDIST End,omitempty"`
}

type ColumnRange struct {
    Begin string `xml:"http://www.iata.org/IATA/EDIST Begin,omitempty"`
    End   string `xml:"http://www.iata.org/IATA/EDIST End,omitempty"`
}

type Row_2 struct {
    Position    int          `xml:"http://www.iata.org/IATA/EDIST Position,omitempty"`
    Orientation *CodesetType `xml:"http://www.iata.org/IATA/EDIST Orientation,omitempty"`
}

type Column_1 struct {
    Position    string       `xml:"http://www.iata.org/IATA/EDIST Position,omitempty"`
    Orientation *CodesetType `xml:"http://www.iata.org/IATA/EDIST Orientation,omitempty"`
}

type Dimensions_2 struct {
    RowEquivalent    float64     `xml:"http://www.iata.org/IATA/EDIST RowEquivalent,omitempty"`
    ColumnEquivalent float64     `xml:"http://www.iata.org/IATA/EDIST ColumnEquivalent,omitempty"`
    ActualSize       *ActualSize `xml:"http://www.iata.org/IATA/EDIST ActualSize,omitempty"`
    Refs             string      `xml:",attr,omitempty"`
}

type ActualSize struct {
    UOM    string `xml:"http://www.iata.org/IATA/EDIST UOM,omitempty"`
    Length string `xml:"http://www.iata.org/IATA/EDIST Length,omitempty"`
    Width  string `xml:"http://www.iata.org/IATA/EDIST Width,omitempty"`
}

type SeatDataType struct {
    Descriptions *Descriptions       `xml:"http://www.iata.org/IATA/EDIST Descriptions,omitempty"`
    UOM          string              `xml:"http://www.iata.org/IATA/EDIST UOM,omitempty"`
    SeatPitchLow float64             `xml:"http://www.iata.org/IATA/EDIST SeatPitchLow,omitempty"`
    SeatWidthLow float64             `xml:"http://www.iata.org/IATA/EDIST SeatWidthLow,omitempty"`
    Keywords     *Keywords_2         `xml:"http://www.iata.org/IATA/EDIST Keywords,omitempty"`
    Marketing    *SeatMapMessageType `xml:"http://www.iata.org/IATA/EDIST Marketing,omitempty"`
}

type Keywords_2 struct {
    KeyWord []*KeyWordType `xml:"http://www.iata.org/IATA/EDIST KeyWord,omitempty"`
}

type SeatMapMessageType struct {
    Images  []*Images   `xml:"http://www.iata.org/IATA/EDIST Images,omitempty"`
    Links   []*Links_1  `xml:"http://www.iata.org/IATA/EDIST Links,omitempty"`
    Remarks *RemarkType `xml:"http://www.iata.org/IATA/EDIST Remarks,omitempty"`
    Refs    string      `xml:",attr,omitempty"`
}

type Images struct {
    ImageID  string              `xml:"http://www.iata.org/IATA/EDIST ImageID,omitempty"`
    Position *CabinCameraPosType `xml:"http://www.iata.org/IATA/EDIST Position,omitempty"`
}

type Links_1 struct {
    MediaLink *MediaLink          `xml:"http://www.iata.org/IATA/EDIST MediaLink,omitempty"`
    Position  *CabinCameraPosType `xml:"http://www.iata.org/IATA/EDIST Position,omitempty"`
}

type SeatMapRowNbrType struct {
    RowSuffix string `xml:",attr,omitempty"`
    RowPos    string `xml:",attr,omitempty"`
    Value     string `xml:",chardata"`
}

type Security SecurityType

type SecurityType struct {
    DutyCode             string          `xml:"http://www.iata.org/IATA/EDIST DutyCode,omitempty"`
    IATA                 string          `xml:"http://www.iata.org/IATA/EDIST IATA,omitempty"`
    PseudoCityCode       *PseudoCityCode `xml:"http://www.iata.org/IATA/EDIST PseudoCityCode,omitempty"`
    LNIATA               string          `xml:"http://www.iata.org/IATA/EDIST LNIATA,omitempty"`
    ERSP                 string          `xml:"http://www.iata.org/IATA/EDIST ERSP,omitempty"`
    Department           string          `xml:"http://www.iata.org/IATA/EDIST Department,omitempty"`
    DepartmentCode       string          `xml:"http://www.iata.org/IATA/EDIST DepartmentCode,omitempty"`
    AirlineSpecificCodes string          `xml:"http://www.iata.org/IATA/EDIST AirlineSpecificCodes,omitempty"`
}

type PseudoCityCode struct {
    Owner string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type SrvcCombineRuleListType string

type SrvcCombineRuleSimpleType string

type SrvceCouponListType string

type SrvceCouponSimpleType string

type ServiceAssocListItemType string

type ServiceAssocListType string

type ServiceAssocType struct {
    Traveler          *Traveler_1            `xml:"http://www.iata.org/IATA/EDIST Traveler,omitempty"`
    Flight            *Flight_5              `xml:"http://www.iata.org/IATA/EDIST Flight,omitempty"`
    Offer             *Offer_3               `xml:"http://www.iata.org/IATA/EDIST Offer,omitempty"`
    OtherAssociations *ServiceOtherAssocType `xml:"http://www.iata.org/IATA/EDIST OtherAssociations,omitempty"`
    SeatReference     string                 `xml:"http://www.iata.org/IATA/EDIST SeatReference,omitempty"`
}

type Traveler_1 struct {
    AllTravelerInd     bool   `xml:"http://www.iata.org/IATA/EDIST AllTravelerInd,omitempty"`
    TravelerReferences string `xml:"http://www.iata.org/IATA/EDIST TravelerReferences,omitempty"`
}

type Flight_5 struct {
    AllFlightInd                bool               `xml:"http://www.iata.org/IATA/EDIST AllFlightInd,omitempty"`
    AllOriginDestinationInd     bool               `xml:"http://www.iata.org/IATA/EDIST AllOriginDestinationInd,omitempty"`
    AllSegmentInd               bool               `xml:"http://www.iata.org/IATA/EDIST AllSegmentInd,omitempty"`
    OriginDestinationReferences string             `xml:"http://www.iata.org/IATA/EDIST OriginDestinationReferences,omitempty"`
    SegmentReferences           *SegmentReferences `xml:"http://www.iata.org/IATA/EDIST SegmentReferences,omitempty"`
}

type Offer_3 struct {
    OfferReferences string `xml:"http://www.iata.org/IATA/EDIST OfferReferences,omitempty"`
}

type ServiceOtherAssocType struct {
    OtherAssociation []*OtherAssociation_10 `xml:"http://www.iata.org/IATA/EDIST OtherAssociation,omitempty"`
}

type OtherAssociation_10 struct {
    Type           *Type_1 `xml:"http://www.iata.org/IATA/EDIST Type,omitempty"`
    ReferenceValue string  `xml:"http://www.iata.org/IATA/EDIST ReferenceValue,omitempty"`
}

type Type_1 struct {
    Code  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type BundleID_1 struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type RFIC RFIC_Type

type RFIC_Type struct {
    CodesetType
}

type ServiceCore ServiceCoreType

type ServiceCoreType struct {
    ServiceID                 *ServiceID_Type                `xml:"http://www.iata.org/IATA/EDIST ServiceID,omitempty"`
    Name                      string                         `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
    Encoding                  *ServiceEncodingType           `xml:"http://www.iata.org/IATA/EDIST Encoding,omitempty"`
    TimeLimits                *TimeLimits_2                  `xml:"http://www.iata.org/IATA/EDIST TimeLimits,omitempty"`
    FeeMethod                 string                         `xml:"http://www.iata.org/IATA/EDIST FeeMethod,omitempty"`
    Descriptions              *ServiceDescriptionType        `xml:"http://www.iata.org/IATA/EDIST Descriptions,omitempty"`
    Settlement                *Settlement_3                  `xml:"http://www.iata.org/IATA/EDIST Settlement,omitempty"`
    Price                     []*ServicePriceType            `xml:"http://www.iata.org/IATA/EDIST Price,omitempty"`
    BookingInstructions       *BookingInstructions           `xml:"http://www.iata.org/IATA/EDIST BookingInstructions,omitempty"`
    ValidatingCarrier         string                         `xml:"http://www.iata.org/IATA/EDIST ValidatingCarrier,omitempty"`
    Associations              []*ServiceAssocType            `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
    BaggageDeterminingCarrier *BaggageDeterminingCarrierType `xml:"http://www.iata.org/IATA/EDIST BaggageDeterminingCarrier,omitempty"`
    BDC_AnalysisResult        *CodesetType                   `xml:"http://www.iata.org/IATA/EDIST BDC_AnalysisResult,omitempty"`
    Refs                      string                         `xml:",attr,omitempty"`
    ObjectKey                 string                         `xml:",attr,omitempty"`
}

type Settlement_3 struct {
    Method                   *CodesetType           `xml:"http://www.iata.org/IATA/EDIST Method,omitempty"`
    InterlineSettlementValue *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST InterlineSettlementValue,omitempty"`
}

type BookingInstructions struct {
    SSRCode       []string       `xml:"http://www.iata.org/IATA/EDIST SSRCode,omitempty"`
    OSIText       []string       `xml:"http://www.iata.org/IATA/EDIST OSIText,omitempty"`
    Method        string         `xml:"http://www.iata.org/IATA/EDIST Method,omitempty"`
    UpgradeMethod *UpgradeMethod `xml:"http://www.iata.org/IATA/EDIST UpgradeMethod,omitempty"`
    Text          []string       `xml:"http://www.iata.org/IATA/EDIST Text,omitempty"`
    Equipment     string         `xml:"http://www.iata.org/IATA/EDIST Equipment,omitempty"`
}

type UpgradeMethod struct {
    NewClass string `xml:",attr,omitempty"`
    Value    string `xml:",chardata"`
}

type ServiceDetailType struct {
    ServiceCoreType
    Detail *Detail_4 `xml:"http://www.iata.org/IATA/EDIST Detail,omitempty"`
}

type Detail_4 struct {
    ServiceCombinations      *ServiceCombinationType  `xml:"http://www.iata.org/IATA/EDIST ServiceCombinations,omitempty"`
    ServiceCoupon            *ServiceCouponType       `xml:"http://www.iata.org/IATA/EDIST ServiceCoupon,omitempty"`
    ServiceFulfillment       *ServiceFulfillmentType  `xml:"http://www.iata.org/IATA/EDIST ServiceFulfillment,omitempty"`
    ServiceItemQuantityRules *ServiceItemQuantityType `xml:"http://www.iata.org/IATA/EDIST ServiceItemQuantityRules,omitempty"`
}

type ServiceQualifierPriceType struct {
    Encoding     *ServiceEncodingType `xml:"http://www.iata.org/IATA/EDIST Encoding,omitempty"`
    Fulfillment  *Fulfillment_1       `xml:"http://www.iata.org/IATA/EDIST Fulfillment,omitempty"`
    Associations *Associations_7      `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
    Include      bool                 `xml:"http://www.iata.org/IATA/EDIST Include,omitempty"`
    Refs         string               `xml:",attr,omitempty"`
}

type Fulfillment_1 struct {
    OfferValidDates *OfferValidDates_1      `xml:"http://www.iata.org/IATA/EDIST OfferValidDates,omitempty"`
    Provider        *FulfillmentPartnerType `xml:"http://www.iata.org/IATA/EDIST Provider,omitempty"`
    Location        *Location_4             `xml:"http://www.iata.org/IATA/EDIST Location,omitempty"`
    Refs            string                  `xml:",attr,omitempty"`
}

type OfferValidDates_1 struct {
    Start *CoreDateGrpType `xml:"http://www.iata.org/IATA/EDIST Start,omitempty"`
    End   *CoreDateGrpType `xml:"http://www.iata.org/IATA/EDIST End,omitempty"`
}

type Location_4 struct {
    AirportFulfillmentLocation *SrvcLocationAirportType `xml:"http://www.iata.org/IATA/EDIST AirportFulfillmentLocation,omitempty"`
    OtherFulfillmentLocation   *SrvcLocationAddressType `xml:"http://www.iata.org/IATA/EDIST OtherFulfillmentLocation,omitempty"`
}

type Associations_7 struct {
    SegmentReferences  *SegmentReferences `xml:"http://www.iata.org/IATA/EDIST SegmentReferences,omitempty"`
    TravelerReferences string             `xml:"http://www.iata.org/IATA/EDIST TravelerReferences,omitempty"`
}

type ServiceEncodingType struct {
    RFIC    *CodesetType `xml:"http://www.iata.org/IATA/EDIST RFIC,omitempty"`
    Type    *CodesetType `xml:"http://www.iata.org/IATA/EDIST Type,omitempty"`
    Code    string       `xml:"http://www.iata.org/IATA/EDIST Code,omitempty"`
    SubCode string       `xml:"http://www.iata.org/IATA/EDIST SubCode,omitempty"`
    Refs    string       `xml:",attr,omitempty"`
}

type ServiceDescriptionType struct {
    Description []*Description_1 `xml:"http://www.iata.org/IATA/EDIST Description,omitempty"`
    Refs        string           `xml:",attr,omitempty"`
}

type Description_1 struct {
    DescriptionType
    Application string `xml:"http://www.iata.org/IATA/EDIST Application,omitempty"`
}

type ServiceFilter string

type ServiceFilterType struct {
    GroupCode    string   `xml:"http://www.iata.org/IATA/EDIST GroupCode,omitempty"`
    SubGroupCode []string `xml:"http://www.iata.org/IATA/EDIST SubGroupCode,omitempty"`
    Refs         string   `xml:",attr,omitempty"`
}

type AirportFulfillmentLocation SrvcLocationAirportType

type OtherFulfillmentLocation SrvcLocationAddressType

type ServiceFulfillmentType struct {
    OfferValidDates *OfferValidDates_2      `xml:"http://www.iata.org/IATA/EDIST OfferValidDates,omitempty"`
    Provider        *FulfillmentPartnerType `xml:"http://www.iata.org/IATA/EDIST Provider,omitempty"`
    Location        *Location_5             `xml:"http://www.iata.org/IATA/EDIST Location,omitempty"`
    Refs            string                  `xml:",attr,omitempty"`
}

type OfferValidDates_2 struct {
    Start *CoreDateGrpType `xml:"http://www.iata.org/IATA/EDIST Start,omitempty"`
    End   *CoreDateGrpType `xml:"http://www.iata.org/IATA/EDIST End,omitempty"`
}

type Location_5 struct {
    AirportFulfillmentLocation *SrvcLocationAirportType `xml:"http://www.iata.org/IATA/EDIST AirportFulfillmentLocation,omitempty"`
    OtherFulfillmentLocation   *SrvcLocationAddressType `xml:"http://www.iata.org/IATA/EDIST OtherFulfillmentLocation,omitempty"`
}

type SrvcLocationAirportType struct {
    AirportCode *AirportCode `xml:"http://www.iata.org/IATA/EDIST AirportCode,omitempty"`
    Refs        string       `xml:",attr,omitempty"`
}

type SrvcLocationAddressType struct {
    Address *Address `xml:"http://www.iata.org/IATA/EDIST Address,omitempty"`
}

type ServicePriceType struct {
    Total               *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST Total,omitempty"`
    RefundableValue     *RefundableValue       `xml:"http://www.iata.org/IATA/EDIST RefundableValue,omitempty"`
    Details             *Details_6             `xml:"http://www.iata.org/IATA/EDIST Details,omitempty"`
    Taxes               *TaxDetailType         `xml:"http://www.iata.org/IATA/EDIST Taxes,omitempty"`
    Fees                *FeeSurchargeType      `xml:"http://www.iata.org/IATA/EDIST Fees,omitempty"`
    PassengerReferences string                 `xml:"http://www.iata.org/IATA/EDIST PassengerReferences,omitempty"`
    Refs                string                 `xml:",attr,omitempty"`
}

type RefundableValue struct {
    Amount     *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST Amount,omitempty"`
    Percentage float64                `xml:"http://www.iata.org/IATA/EDIST Percentage,omitempty"`
}

type Details_6 struct {
    SubTotal *SubTotal_2 `xml:"http://www.iata.org/IATA/EDIST SubTotal,omitempty"`
    Detail   []*Detail_5 `xml:"http://www.iata.org/IATA/EDIST Detail,omitempty"`
}

type SubTotal_2 struct {
    SimpleCurrencyPrice  *CurrencyAmountOptType     `xml:"http://www.iata.org/IATA/EDIST SimpleCurrencyPrice,omitempty"`
    EncodedCurrencyPrice *CurrencyAmountEncodedType `xml:"http://www.iata.org/IATA/EDIST EncodedCurrencyPrice,omitempty"`
    DetailCurrencyPrice  *DetailCurrencyPriceType   `xml:"http://www.iata.org/IATA/EDIST DetailCurrencyPrice,omitempty"`
    CombinationPricing   *CombinationPriceType      `xml:"http://www.iata.org/IATA/EDIST CombinationPricing,omitempty"`
    AwardPricing         *AwardPriceUnitType        `xml:"http://www.iata.org/IATA/EDIST AwardPricing,omitempty"`
}

type Detail_5 struct {
    Application string                 `xml:"http://www.iata.org/IATA/EDIST Application,omitempty"`
    Amount      *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST Amount,omitempty"`
}

type ServicePrice ServicePriceType

type ServiceCombinations ServiceCombinationType

type ServiceFulfillment ServiceFulfillmentType

type ServiceCoupon ServiceCouponType

type ServiceItemQuantityRules ServiceItemQuantityType

type ServiceCombinationType struct {
    Combination []*Combination `xml:"http://www.iata.org/IATA/EDIST Combination,omitempty"`
}

type Combination struct {
    Rule             string            `xml:"http://www.iata.org/IATA/EDIST Rule,omitempty"`
    ServiceID        []*ServiceID_Type `xml:"http://www.iata.org/IATA/EDIST ServiceID,omitempty"`
    ServiceReference []string          `xml:"http://www.iata.org/IATA/EDIST ServiceReference,omitempty"`
}

type ServiceCouponType struct {
    InstantPurchase *CodesetType `xml:"http://www.iata.org/IATA/EDIST InstantPurchase,omitempty"`
    FeeBasis        *CodesetType `xml:"http://www.iata.org/IATA/EDIST FeeBasis,omitempty"`
    CouponType      string       `xml:"http://www.iata.org/IATA/EDIST CouponType,omitempty"`
}

type ServiceItemQuantityType struct {
    MinimumQuantity int `xml:"http://www.iata.org/IATA/EDIST MinimumQuantity,omitempty"`
    MaximumQuantity int `xml:"http://www.iata.org/IATA/EDIST MaximumQuantity,omitempty"`
}

type EligibleServiceClassUpgrades InstrClassUpgradeType

type ServiceCommissionInfo InstrCommissionType

type ServiceFreeFormTextInstruction InstrRemarkType

type ServiceBookingInstructions InstrSpecialBookingType

type ServiceID ServiceID_Type

type ServiceID_Type struct {
    Refs      string `xml:",attr,omitempty"`
    ObjectKey string `xml:",attr,omitempty"`
    Owner     string `xml:",attr,omitempty"`
    Value     string `xml:",chardata"`
}

type FltDepartQualifiedQueryType struct {
    KeyWithMetaObjectBaseType
    LocationCode []*LocationCode `xml:"http://www.iata.org/IATA/EDIST LocationCode,omitempty"`
    Date         string          `xml:"http://www.iata.org/IATA/EDIST Date,omitempty"`
    Time         string          `xml:"http://www.iata.org/IATA/EDIST Time,omitempty"`
    LeadPricing  *LeadPricing    `xml:"http://www.iata.org/IATA/EDIST LeadPricing,omitempty"`
}

type LocationCode struct {
    PreferencesLevel   string `xml:",attr,omitempty"`
    PreferencesContext string `xml:",attr,omitempty"`
    Application        string `xml:",attr,omitempty"`
    Area               int    `xml:",attr,omitempty"`
    UOM                string `xml:",attr,omitempty"`
    Value              string `xml:",chardata"`
}

type LeadPricing struct {
    LeadDatePeriod *LeadDatePeriodType `xml:"http://www.iata.org/IATA/EDIST LeadDatePeriod,omitempty"`
    LeadTimePeriod *LeadTimePeriodType `xml:"http://www.iata.org/IATA/EDIST LeadTimePeriod,omitempty"`
}

type FltArriveQualifiedQueryType struct {
    LocationCode []*LocationCode_1 `xml:"http://www.iata.org/IATA/EDIST LocationCode,omitempty"`
    Date         string            `xml:"http://www.iata.org/IATA/EDIST Date,omitempty"`
    Time         string            `xml:"http://www.iata.org/IATA/EDIST Time,omitempty"`
    LeadPricing  *LeadPricing_1    `xml:"http://www.iata.org/IATA/EDIST LeadPricing,omitempty"`
    Refs         string            `xml:",attr,omitempty"`
}

type LocationCode_1 struct {
    PreferencesLevel   string `xml:",attr,omitempty"`
    PreferencesContext string `xml:",attr,omitempty"`
    Application        string `xml:",attr,omitempty"`
    Area               int    `xml:",attr,omitempty"`
    UOM                string `xml:",attr,omitempty"`
    Value              string `xml:",chardata"`
}

type LeadPricing_1 struct {
    LeadDatePeriod *LeadDatePeriodType `xml:"http://www.iata.org/IATA/EDIST LeadDatePeriod,omitempty"`
    LeadTimePeriod *LeadTimePeriodType `xml:"http://www.iata.org/IATA/EDIST LeadTimePeriod,omitempty"`
}

type AirportCityDeparture FltDepartQualifiedQueryType

type AffinityStateProvDepartType struct {
    Departure *StateProvQueryType `xml:"http://www.iata.org/IATA/EDIST Departure,omitempty"`
}

type AffinityStateProvArriveType struct {
    Arrival *StateProvQueryType `xml:"http://www.iata.org/IATA/EDIST Arrival,omitempty"`
}

type AffinityCountryDepartType struct {
    Departure *CountryQueryType `xml:"http://www.iata.org/IATA/EDIST Departure,omitempty"`
}

type AffinityCountryArriveType struct {
    Arrival *CountryQueryType `xml:"http://www.iata.org/IATA/EDIST Arrival,omitempty"`
}

type AffinityReferencePointDepartType struct {
    Departure *ReferencePointQueryType `xml:"http://www.iata.org/IATA/EDIST Departure,omitempty"`
}

type AffinityReferencePointArriveType struct {
    Arrival *ReferencePointQueryType `xml:"http://www.iata.org/IATA/EDIST Arrival,omitempty"`
}

type AirportCityArrival FltArriveQualifiedQueryType

type CountryDeparture AffinityCountryDepartType

type CountryArrival AffinityCountryArriveType

type ReferencePointDeparture AffinityReferencePointDepartType

type ReferencePointQueryType struct {
    ReferencePoints    *ReferencePoints `xml:"http://www.iata.org/IATA/EDIST ReferencePoints,omitempty"`
    Proximity          []*Proximity     `xml:"http://www.iata.org/IATA/EDIST Proximity,omitempty"`
    PreferencesLevel   string           `xml:",attr,omitempty"`
    PreferencesContext string           `xml:",attr,omitempty"`
}

type ReferencePoints struct {
    ReferencePoint []*ReferencePoint `xml:"http://www.iata.org/IATA/EDIST ReferencePoint,omitempty"`
}

type ReferencePoint struct {
    PreferencesLevel   string `xml:",attr,omitempty"`
    PreferencesContext string `xml:",attr,omitempty"`
    Value              string `xml:",chardata"`
}

type Proximity struct {
    ReferencePoint *ReferencePoint_1 `xml:"http://www.iata.org/IATA/EDIST ReferencePoint,omitempty"`
    Application    string            `xml:"http://www.iata.org/IATA/EDIST Application,omitempty"`
    AreaValue      *AreaValue        `xml:"http://www.iata.org/IATA/EDIST AreaValue,omitempty"`
}

type ReferencePoint_1 struct {
    PreferencesLevel   string `xml:",attr,omitempty"`
    PreferencesContext string `xml:",attr,omitempty"`
    Value              string `xml:",chardata"`
}

type AreaValue struct {
    UOM   string  `xml:",attr,omitempty"`
    Value float64 `xml:",chardata"`
}

type ReferencePointArrival AffinityReferencePointArriveType

type StateProvDeparture AffinityStateProvDepartType

type StateProvArrival AffinityStateProvArriveType

type LeadDatePeriod LeadDatePeriodType

type LeadTimePeriod LeadTimePeriodType

type LeadDatePeriodType struct {
    Before string `xml:",attr,omitempty"`
    After  string `xml:",attr,omitempty"`
    Refs   string `xml:",attr,omitempty"`
}

type LeadTimePeriodType struct {
    Before string `xml:",attr,omitempty"`
    After  string `xml:",attr,omitempty"`
    Refs   string `xml:",attr,omitempty"`
}

type Travelers struct {
    Traveler []*Traveler_2 `xml:"http://www.iata.org/IATA/EDIST Traveler,omitempty"`
}

type Traveler_2 struct {
    AnonymousTraveler  []*TravelerCoreType   `xml:"http://www.iata.org/IATA/EDIST AnonymousTraveler,omitempty"`
    RecognizedTraveler []*TravelerDetailType `xml:"http://www.iata.org/IATA/EDIST RecognizedTraveler,omitempty"`
}

type DiscountProgramQualifier DiscountProgramType

type DiscountProgramType struct {
    Account   *Account_3 `xml:"http://www.iata.org/IATA/EDIST Account,omitempty"`
    AssocCode string     `xml:"http://www.iata.org/IATA/EDIST AssocCode,omitempty"`
    Name      string     `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
    Refs      string     `xml:",attr,omitempty"`
}

type Account_3 struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type IncentiveProgramQualifier IncentiveProgramType

type IncentiveProgramType struct {
    Name         string          `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
    AccountID    *AccountID      `xml:"http://www.iata.org/IATA/EDIST AccountID,omitempty"`
    AirlineID    *AirlineID_Type `xml:"http://www.iata.org/IATA/EDIST AirlineID,omitempty"`
    MemberStatus *MemberStatus   `xml:"http://www.iata.org/IATA/EDIST MemberStatus,omitempty"`
    Refs         string          `xml:",attr,omitempty"`
}

type AccountID struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type MemberStatus struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type PrePaidProgramQualifier struct {
    PrepaidProgramDetail *PrepaidProgramDetail `xml:"http://www.iata.org/IATA/EDIST PrepaidProgramDetail,omitempty"`
    Refs                 string                `xml:",attr,omitempty"`
}

type PrepaidProgramDetail struct {
    AirlineID   *AirlineID_Type  `xml:"http://www.iata.org/IATA/EDIST AirlineID,omitempty"`
    Certificate []*Certificate_1 `xml:"http://www.iata.org/IATA/EDIST Certificate,omitempty"`
    ProgramName string           `xml:"http://www.iata.org/IATA/EDIST ProgramName,omitempty"`
    ProgramCode string           `xml:"http://www.iata.org/IATA/EDIST ProgramCode,omitempty"`
    Holder      *Holder_1        `xml:"http://www.iata.org/IATA/EDIST Holder,omitempty"`
}

type Certificate_1 struct {
    Number          *Number_4          `xml:"http://www.iata.org/IATA/EDIST Number,omitempty"`
    Application     string             `xml:"http://www.iata.org/IATA/EDIST Application,omitempty"`
    EffectivePeriod *EffectivePeriod_3 `xml:"http://www.iata.org/IATA/EDIST EffectivePeriod,omitempty"`
    Refs            string             `xml:",attr,omitempty"`
}

type Number_4 struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type EffectivePeriod_3 struct {
    YearPeriod      *YearPeriodRepType      `xml:"http://www.iata.org/IATA/EDIST YearPeriod,omitempty"`
    YearMonthPeriod *YearMonthPeriodRepType `xml:"http://www.iata.org/IATA/EDIST YearMonthPeriod,omitempty"`
    TimePeriod      *TimePeriodRepType      `xml:"http://www.iata.org/IATA/EDIST TimePeriod,omitempty"`
    QuarterPeriod   *QuarterPeriodRepType   `xml:"http://www.iata.org/IATA/EDIST QuarterPeriod,omitempty"`
    MonthPeriod     *MonthPeriodRepType     `xml:"http://www.iata.org/IATA/EDIST MonthPeriod,omitempty"`
    DayPeriod       *DayPeriodRepType       `xml:"http://www.iata.org/IATA/EDIST DayPeriod,omitempty"`
    DateTimePeriod  *DateTimePeriodRepType  `xml:"http://www.iata.org/IATA/EDIST DateTimePeriod,omitempty"`
    DatePeriod      *DatePeriodRepType      `xml:"http://www.iata.org/IATA/EDIST DatePeriod,omitempty"`
}

type Holder_1 struct {
    AgencyID  *UniqueIDContextType   `xml:"http://www.iata.org/IATA/EDIST AgencyID,omitempty"`
    PartnerID *PartnerCompanyID_Type `xml:"http://www.iata.org/IATA/EDIST PartnerID,omitempty"`
}

type ProgramStatusQualifier struct {
    ProgramStatus []string `xml:"http://www.iata.org/IATA/EDIST ProgramStatus,omitempty"`
    Refs          string   `xml:",attr,omitempty"`
}

type CardQualifierType struct {
    Type       string `xml:"http://www.iata.org/IATA/EDIST Type,omitempty"`
    IIN_Number string `xml:"http://www.iata.org/IATA/EDIST IIN_Number,omitempty"`
    Refs       string `xml:",attr,omitempty"`
}

type SocialMediaQualifierType struct {
    Service      string        `xml:"http://www.iata.org/IATA/EDIST Service,omitempty"`
    User         *User         `xml:"http://www.iata.org/IATA/EDIST User,omitempty"`
    EmailAddress *EmailID_Type `xml:"http://www.iata.org/IATA/EDIST EmailAddress,omitempty"`
    Refs         string        `xml:",attr,omitempty"`
}

type User struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type StayPeriodDateRange StayPeriodDateRangeType

type StayPeriodDateRangeType struct {
    StartDate   string `xml:"http://www.iata.org/IATA/EDIST StartDate,omitempty"`
    EndDate     string `xml:"http://www.iata.org/IATA/EDIST EndDate,omitempty"`
    MinimumStay string `xml:"http://www.iata.org/IATA/EDIST MinimumStay,omitempty"`
    MaximumStay string `xml:"http://www.iata.org/IATA/EDIST MaximumStay,omitempty"`
    Refs        string `xml:",attr,omitempty"`
}

type StayPeriodSeason StayPeriodSeasonType

type StayPeriodSeasonType struct {
    Month         []*MonthRepType         `xml:"http://www.iata.org/IATA/EDIST Month,omitempty"`
    QuarterPeriod []*QuarterPeriodRepType `xml:"http://www.iata.org/IATA/EDIST QuarterPeriod,omitempty"`
    MinimumStay   string                  `xml:"http://www.iata.org/IATA/EDIST MinimumStay,omitempty"`
    MaximumStay   string                  `xml:"http://www.iata.org/IATA/EDIST MaximumStay,omitempty"`
    Refs          string                  `xml:",attr,omitempty"`
}

type GroupBudget GroupBudgetType

type GroupBudgetType struct {
    Amount *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST Amount,omitempty"`
}

type PerPersonBudget PersonBudgetType

type PersonBudgetType struct {
    Amount *CurrencyAmountOptType `xml:"http://www.iata.org/IATA/EDIST Amount,omitempty"`
}

type BaggagePricingQualifier BaggagePricingQualifierType

type BaggagePricingQualifierType struct {
    BaggageOption         []*CodesetType `xml:"http://www.iata.org/IATA/EDIST BaggageOption,omitempty"`
    RequestAction         *CodesetType   `xml:"http://www.iata.org/IATA/EDIST RequestAction,omitempty"`
    OptionalCharges       string         `xml:"http://www.iata.org/IATA/EDIST OptionalCharges,omitempty"`
    FixedPrePaidInd       bool           `xml:",attr,omitempty"`
    CommercialAgreementID string         `xml:",attr,omitempty"`
    DeferralInd           bool           `xml:",attr,omitempty"`
    Refs                  string         `xml:",attr,omitempty"`
}

type ExistingOrderQualifier struct {
    OrderQualiferType
    TravelerReferences string                `xml:"http://www.iata.org/IATA/EDIST TravelerReferences,omitempty"`
    BookingReference   *BookingReferenceType `xml:"http://www.iata.org/IATA/EDIST BookingReference,omitempty"`
}

type FareQualifierType struct {
    AirlineID    *AirlineID_Type `xml:"http://www.iata.org/IATA/EDIST AirlineID,omitempty"`
    CompanyIndex *CompanyIndex_1 `xml:"http://www.iata.org/IATA/EDIST CompanyIndex,omitempty"`
    Contract     *Contract_1     `xml:"http://www.iata.org/IATA/EDIST Contract,omitempty"`
    Account      *Account_4      `xml:"http://www.iata.org/IATA/EDIST Account,omitempty"`
    Refs         string          `xml:",attr,omitempty"`
}

type CompanyIndex_1 struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type Contract_1 struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type Account_4 struct {
    Refs  string `xml:",attr,omitempty"`
    Value string `xml:",chardata"`
}

type PaymentCardQualifiers CardQualifierType

type ProgramQualifiers struct {
    ProgramQualifier []*ProgramQualifierType `xml:"http://www.iata.org/IATA/EDIST ProgramQualifier,omitempty"`
    Refs             string                  `xml:",attr,omitempty"`
}

type PromotionQualifiers struct {
    PromoQualifierType
    CC_IssuingCountryInd string `xml:",attr,omitempty"`
}

type ProgramQualifierType struct {
    DiscountProgramQualifier  *DiscountProgramType     `xml:"http://www.iata.org/IATA/EDIST DiscountProgramQualifier,omitempty"`
    IncentiveProgramQualifier *IncentiveProgramType    `xml:"http://www.iata.org/IATA/EDIST IncentiveProgramQualifier,omitempty"`
    PrePaidProgramQualifier   *PrePaidProgramQualifier `xml:"http://www.iata.org/IATA/EDIST PrePaidProgramQualifier,omitempty"`
    ProgramStatusQualifier    *ProgramStatusQualifier  `xml:"http://www.iata.org/IATA/EDIST ProgramStatusQualifier,omitempty"`
}

type PromoQualifierType struct {
    PromotionType
}

type QualifiersType struct {
    Qualifier []*Qualifier_1 `xml:"http://www.iata.org/IATA/EDIST Qualifier,omitempty"`
}

type Qualifier_1 struct {
    SeatQualifier    *SeatQualifier             `xml:"http://www.iata.org/IATA/EDIST SeatQualifier,omitempty"`
    ServiceQualifier *ServiceQualifierPriceType `xml:"http://www.iata.org/IATA/EDIST ServiceQualifier,omitempty"`
    Associations     *OrderItemAssociationType  `xml:"http://www.iata.org/IATA/EDIST Associations,omitempty"`
    Refs             string                     `xml:",attr,omitempty"`
}

type SeatQualifier struct {
    Assignment []*Assignment `xml:"http://www.iata.org/IATA/EDIST Assignment,omitempty"`
}

type Assignment struct {
    SeatCoreType
    SeatAssociation *SeatAssociation `xml:"http://www.iata.org/IATA/EDIST SeatAssociation,omitempty"`
}

type SeatAssociation struct {
    SegmentReferences *SegmentReferences `xml:"http://www.iata.org/IATA/EDIST SegmentReferences,omitempty"`
    TravelerReference string             `xml:"http://www.iata.org/IATA/EDIST TravelerReference,omitempty"`
}

type ServiceQualifier ServiceQualifierPriceType

type SocialMediaQualifiers SocialQualiferType

type SpecialFareQualifiers FareQualifierType

type SpecialNeedQualifiers SpecialQualiferType

type SocialQualiferType struct {
    SocialMediaQualifierType
}

type SpecialQualiferType struct {
    SpecialType
}

type TripPurposeQualifier TripPurposeListType

type AirlinePreferences AirlinePreferencesType

type AlliancePreferences AlliancePreferencesType

type AirlinePreferencesType struct {
    Airline []*Airline_1 `xml:"http://www.iata.org/IATA/EDIST Airline,omitempty"`
    Refs    string       `xml:",attr,omitempty"`
}

type Airline_1 struct {
    AirlineID          *AirlineID_Type `xml:"http://www.iata.org/IATA/EDIST AirlineID,omitempty"`
    GeoIndicatorType   *CodesetType    `xml:"http://www.iata.org/IATA/EDIST GeoIndicatorType,omitempty"`
    FlightHaulLength   *CodesetType    `xml:"http://www.iata.org/IATA/EDIST FlightHaulLength,omitempty"`
    Refs               string          `xml:",attr,omitempty"`
    PreferencesLevel   string          `xml:",attr,omitempty"`
    PreferencesContext string          `xml:",attr,omitempty"`
}

type AlliancePreferencesType struct {
    Alliance []*Alliance `xml:"http://www.iata.org/IATA/EDIST Alliance,omitempty"`
    Refs     string      `xml:",attr,omitempty"`
}

type Alliance struct {
    CodesetType
    GeoIndicatorType   *CodesetType `xml:"http://www.iata.org/IATA/EDIST GeoIndicatorType,omitempty"`
    FlightHaulLength   *CodesetType `xml:"http://www.iata.org/IATA/EDIST FlightHaulLength,omitempty"`
    PreferencesLevel   string       `xml:",attr,omitempty"`
    PreferencesContext string       `xml:",attr,omitempty"`
}

type BestPricingPreferencesType struct {
    BestPricingOption string `xml:"http://www.iata.org/IATA/EDIST BestPricingOption,omitempty"`
}

type CabinPreferences CabinPreferencesType

type CabinPreferencesType struct {
    CabinType []*CabinType `xml:"http://www.iata.org/IATA/EDIST CabinType,omitempty"`
    Refs      string       `xml:",attr,omitempty"`
}

type FarePreferencesType struct {
    Types              *Types                    `xml:"http://www.iata.org/IATA/EDIST Types,omitempty"`
    FareCodes          *FareCodes                `xml:"http://www.iata.org/IATA/EDIST FareCodes,omitempty"`
    TicketDesigs       *TicketDesigs             `xml:"http://www.iata.org/IATA/EDIST TicketDesigs,omitempty"`
    Exclusion          *Exclusion                `xml:"http://www.iata.org/IATA/EDIST Exclusion,omitempty"`
    GroupFare          *GroupFarePreferencesType `xml:"http://www.iata.org/IATA/EDIST GroupFare,omitempty"`
    PreferencesLevel   string                    `xml:",attr,omitempty"`
    PreferencesContext string                    `xml:",attr,omitempty"`
    Refs               string                    `xml:",attr,omitempty"`
}

type Types struct {
    Type []*Type_2 `xml:"http://www.iata.org/IATA/EDIST Type,omitempty"`
}

type Type_2 struct {
    CodesetType
    PreferencesLevel   string `xml:",attr,omitempty"`
    PreferencesContext string `xml:",attr,omitempty"`
}

type FareCodes struct {
    Code               []*Code_2 `xml:"http://www.iata.org/IATA/EDIST Code,omitempty"`
    PreferencesLevel   string    `xml:",attr,omitempty"`
    PreferencesContext string    `xml:",attr,omitempty"`
}

type Code_2 struct {
    FareBasisCodeType
    PreferencesLevel   string `xml:",attr,omitempty"`
    PreferencesContext string `xml:",attr,omitempty"`
}

type TicketDesigs struct {
    TicketDesig        []*TicketDesig_1 `xml:"http://www.iata.org/IATA/EDIST TicketDesig,omitempty"`
    PreferencesLevel   string           `xml:",attr,omitempty"`
    PreferencesContext string           `xml:",attr,omitempty"`
}

type TicketDesig_1 struct {
    TicketDesignatorType
    PreferencesLevel   string `xml:",attr,omitempty"`
    PreferencesContext string `xml:",attr,omitempty"`
}

type Exclusion struct {
    NoMinStayInd     bool `xml:"http://www.iata.org/IATA/EDIST NoMinStayInd,omitempty"`
    NoMaxStayInd     bool `xml:"http://www.iata.org/IATA/EDIST NoMaxStayInd,omitempty"`
    NoAdvPurchaseInd bool `xml:"http://www.iata.org/IATA/EDIST NoAdvPurchaseInd,omitempty"`
    NoPenaltyInd     bool `xml:"http://www.iata.org/IATA/EDIST NoPenaltyInd,omitempty"`
}

type FlightPreferencesType struct {
    Aircraft       *Aircraft         `xml:"http://www.iata.org/IATA/EDIST Aircraft,omitempty"`
    Characteristic *Characteristic_2 `xml:"http://www.iata.org/IATA/EDIST Characteristic,omitempty"`
    WaitListing    *WaitListing      `xml:"http://www.iata.org/IATA/EDIST WaitListing,omitempty"`
    Refs           string            `xml:",attr,omitempty"`
}

type Aircraft struct {
    Equipment   *AircraftCodeQueryType     `xml:"http://www.iata.org/IATA/EDIST Equipment,omitempty"`
    Cabins      *Cabins                    `xml:"http://www.iata.org/IATA/EDIST Cabins,omitempty"`
    Classes     *Classes_1                 `xml:"http://www.iata.org/IATA/EDIST Classes,omitempty"`
    Seats       *Seats                     `xml:"http://www.iata.org/IATA/EDIST Seats,omitempty"`
    Rows        *Rows_3                    `xml:"http://www.iata.org/IATA/EDIST Rows,omitempty"`
    Features    *AircraftFeaturesQueryType `xml:"http://www.iata.org/IATA/EDIST Features,omitempty"`
    TailNumbers *AircraftTailNmbrQueryType `xml:"http://www.iata.org/IATA/EDIST TailNumbers,omitempty"`
    Refs        string                     `xml:",attr,omitempty"`
}

type Cabins struct {
    Cabin []*Cabin `xml:"http://www.iata.org/IATA/EDIST Cabin,omitempty"`
    Refs  string   `xml:",attr,omitempty"`
}

type Cabin struct {
    CodesetType
    PreferencesLevel   string `xml:",attr,omitempty"`
    PreferencesContext string `xml:",attr,omitempty"`
}

type Classes_1 struct {
    Class []*Class `xml:"http://www.iata.org/IATA/EDIST Class,omitempty"`
    Refs  string   `xml:",attr,omitempty"`
}

type Class struct {
    PreferencesLevel   string `xml:",attr,omitempty"`
    PreferencesContext string `xml:",attr,omitempty"`
    Value              string `xml:",chardata"`
}

type Seats struct {
    Characteristic []*Characteristic_1 `xml:"http://www.iata.org/IATA/EDIST Characteristic,omitempty"`
    Position       *Position_1         `xml:"http://www.iata.org/IATA/EDIST Position,omitempty"`
    Refs           string              `xml:",attr,omitempty"`
}

type Characteristic_1 struct {
    CodesetType
    PreferencesLevel   string `xml:",attr,omitempty"`
    PreferencesContext string `xml:",attr,omitempty"`
}

type Position_1 struct {
    CodesetType
    SeatRowNmber string `xml:"http://www.iata.org/IATA/EDIST SeatRowNmber,omitempty"`
}

type Rows_3 struct {
    Feature []*Feature_1 `xml:"http://www.iata.org/IATA/EDIST Feature,omitempty"`
    Number  []*Number_5  `xml:"http://www.iata.org/IATA/EDIST Number,omitempty"`
}

type Feature_1 struct {
    CodesetType
    PreferencesLevel   string `xml:",attr,omitempty"`
    PreferencesContext string `xml:",attr,omitempty"`
}

type Number_5 struct {
    PreferencesLevel   string `xml:",attr,omitempty"`
    PreferencesContext string `xml:",attr,omitempty"`
    Value              int    `xml:",chardata"`
}

type Characteristic_2 struct {
    NonStopPreferences       *NonStopPreferences       `xml:"http://www.iata.org/IATA/EDIST NonStopPreferences,omitempty"`
    DirectPreferences        *DirectPreferences        `xml:"http://www.iata.org/IATA/EDIST DirectPreferences,omitempty"`
    OvernightStopPreferences *OvernightStopPreferences `xml:"http://www.iata.org/IATA/EDIST OvernightStopPreferences,omitempty"`
    AirportChangePreferences *AirportChangePreferences `xml:"http://www.iata.org/IATA/EDIST AirportChangePreferences,omitempty"`
    RedEyePreferences        *RedEyePreferences        `xml:"http://www.iata.org/IATA/EDIST RedEyePreferences,omitempty"`
    Refs                     string                    `xml:",attr,omitempty"`
}

type NonStopPreferences struct {
    PreferencesContext string `xml:",attr,omitempty"`
    Value              string `xml:",chardata"`
}

type DirectPreferences struct {
    PreferencesContext string `xml:",attr,omitempty"`
    Value              string `xml:",chardata"`
}

type OvernightStopPreferences struct {
    PreferencesContext string `xml:",attr,omitempty"`
    Value              string `xml:",chardata"`
}

type AirportChangePreferences struct {
    PreferencesContext string `xml:",attr,omitempty"`
    Value              string `xml:",chardata"`
}

type RedEyePreferences struct {
    PreferencesContext string `xml:",attr,omitempty"`
    Value              string `xml:",chardata"`
}

type WaitListing struct {
    WaitListPreferences *WaitListPreferences `xml:"http://www.iata.org/IATA/EDIST WaitListPreferences,omitempty"`
    Refs                string               `xml:",attr,omitempty"`
}

type WaitListPreferences struct {
    PreferencesContext string `xml:",attr,omitempty"`
    Value              string `xml:",chardata"`
}

type FlightPreferences FlightPreferencesType

type FarePreferences FarePreferencesType

type FltSegmentMaxTimePreferencesType struct {
    MaxFlightTime      *MaxFlightTime `xml:"http://www.iata.org/IATA/EDIST MaxFlightTime,omitempty"`
    PreferencesLevel   string         `xml:",attr,omitempty"`
    PreferencesContext string         `xml:",attr,omitempty"`
    Refs               string         `xml:",attr,omitempty"`
}

type MaxFlightTime struct {
    TimeAmount float64 `xml:"http://www.iata.org/IATA/EDIST TimeAmount,omitempty"`
    UOM        string  `xml:"http://www.iata.org/IATA/EDIST UOM,omitempty"`
}

type GroupFarePreferencesType struct {
    GroupFare []*GroupFare `xml:"http://www.iata.org/IATA/EDIST GroupFare,omitempty"`
    Refs      string       `xml:",attr,omitempty"`
}

type GroupFare struct {
    FareBasis []*FareBasis_1 `xml:"http://www.iata.org/IATA/EDIST FareBasis,omitempty"`
    PTC       string         `xml:"http://www.iata.org/IATA/EDIST PTC,omitempty"`
}

type FareBasis_1 struct {
    FareBasisCodeType
    SegmentReferences *SegmentReferences `xml:"http://www.iata.org/IATA/EDIST SegmentReferences,omitempty"`
}

type Preference struct {
    AirlinePreferences           *AirlinePreferencesType           `xml:"http://www.iata.org/IATA/EDIST AirlinePreferences,omitempty"`
    AlliancePreferences          *AlliancePreferencesType          `xml:"http://www.iata.org/IATA/EDIST AlliancePreferences,omitempty"`
    FarePreferences              *FarePreferencesType              `xml:"http://www.iata.org/IATA/EDIST FarePreferences,omitempty"`
    FlightPreferences            *FlightPreferencesType            `xml:"http://www.iata.org/IATA/EDIST FlightPreferences,omitempty"`
    PricingMethodPreference      *BestPricingPreferencesType       `xml:"http://www.iata.org/IATA/EDIST PricingMethodPreference,omitempty"`
    SegMaxTimePreferences        *FltSegmentMaxTimePreferencesType `xml:"http://www.iata.org/IATA/EDIST SegMaxTimePreferences,omitempty"`
    ServicePricingOnlyPreference *ServicePricingOnlyPreference     `xml:"http://www.iata.org/IATA/EDIST ServicePricingOnlyPreference,omitempty"`
    TransferPreferences          *TransferPreferencesType          `xml:"http://www.iata.org/IATA/EDIST TransferPreferences,omitempty"`
}

type PricingMethodPreference BestPricingPreferencesType

type TripTime TravelTimePreferencesType

type TripDistance TravelDistancePreferencesType

type TravelDistancePreferencesType struct {
    DistanceAmount     float64 `xml:"http://www.iata.org/IATA/EDIST DistanceAmount,omitempty"`
    Application        string  `xml:"http://www.iata.org/IATA/EDIST Application,omitempty"`
    UOM                string  `xml:"http://www.iata.org/IATA/EDIST UOM,omitempty"`
    PreferencesLevel   string  `xml:",attr,omitempty"`
    PreferencesContext string  `xml:",attr,omitempty"`
    Refs               string  `xml:",attr,omitempty"`
}

type TravelTimePreferencesType struct {
    TimeAmount         float64 `xml:"http://www.iata.org/IATA/EDIST TimeAmount,omitempty"`
    Application        string  `xml:"http://www.iata.org/IATA/EDIST Application,omitempty"`
    PreferencesLevel   string  `xml:",attr,omitempty"`
    PreferencesContext string  `xml:",attr,omitempty"`
    UOM                string  `xml:",attr,omitempty"`
    Refs               string  `xml:",attr,omitempty"`
}

type TransferPreferences TransferPreferencesType

type SegMaxTimePreferences FltSegmentMaxTimePreferencesType

type ServicePricingOnlyPreference struct {
    ServicePricingOnlyInd bool `xml:"http://www.iata.org/IATA/EDIST ServicePricingOnlyInd,omitempty"`
}

type TransferPreferencesType struct {
    Connection *Connection `xml:"http://www.iata.org/IATA/EDIST Connection,omitempty"`
    Pricing    *Pricing_1  `xml:"http://www.iata.org/IATA/EDIST Pricing,omitempty"`
    Refs       string      `xml:",attr,omitempty"`
}

type Connection struct {
    Codes     *Codes     `xml:"http://www.iata.org/IATA/EDIST Codes,omitempty"`
    MaxNumber int        `xml:"http://www.iata.org/IATA/EDIST MaxNumber,omitempty"`
    MaxTime   string     `xml:"http://www.iata.org/IATA/EDIST MaxTime,omitempty"`
    MinTime   string     `xml:"http://www.iata.org/IATA/EDIST MinTime,omitempty"`
    Interline *Interline `xml:"http://www.iata.org/IATA/EDIST Interline,omitempty"`
    Refs      string     `xml:",attr,omitempty"`
}

type Codes struct {
    Code []*Code_3 `xml:"http://www.iata.org/IATA/EDIST Code,omitempty"`
}

type Code_3 struct {
    PreferencesLevel   string `xml:",attr,omitempty"`
    PreferencesContext string `xml:",attr,omitempty"`
    Value              string `xml:",chardata"`
}

type Interline struct {
    CodesetType
    PreferencesLevel   string `xml:",attr,omitempty"`
    PreferencesContext string `xml:",attr,omitempty"`
}

type Pricing_1 struct {
    ConnectionPricingInd bool `xml:",attr,omitempty"`
}

type SuccessType struct {
}

type WarningsType struct {
    Warning []*WarningType `xml:"http://www.iata.org/IATA/EDIST Warning,omitempty"`
}

type WarningType struct {
    FreeTextType
    Type      string        `xml:",attr,omitempty"`
    ShortText string        `xml:",attr,omitempty"`
    Code      string        `xml:",attr,omitempty"`
    DocURL    string        `xml:",attr,omitempty"`
    Status    string        `xml:",attr,omitempty"`
    Tag       string        `xml:",attr,omitempty"`
    RecordID  string        `xml:",attr,omitempty"`
    Owner     string        `xml:",attr,omitempty"`
}

type TaxCouponInfoType struct {
    TicketDocument            []*TicketDocument_1 `xml:"http://www.iata.org/IATA/EDIST TicketDocument,omitempty"`
    BirthDate                 string              `xml:",attr,omitempty"`
    JourneyTurnaroundCityCode string              `xml:",attr,omitempty"`
}

type TicketDocument_1 struct {
    CouponNumber      []*CouponNumber `xml:"http://www.iata.org/IATA/EDIST CouponNumber,omitempty"`
    TicketDocumentNbr string          `xml:",attr,omitempty"`
    DateOfIssue       string          `xml:",attr,omitempty"`
}

type CouponNumber struct {
    TaxCouponInfo       *TaxCouponInfo         `xml:"http://www.iata.org/IATA/EDIST TaxCouponInfo,omitempty"`
    Tax                 []*Tax_2               `xml:"http://www.iata.org/IATA/EDIST Tax,omitempty"`
    UnticketedPointInfo []*UnticketedPointInfo `xml:"http://www.iata.org/IATA/EDIST UnticketedPointInfo,omitempty"`
    Number              int                    `xml:",attr,omitempty"`
}

type TaxCouponInfo struct {
    Cabin        string `xml:",attr,omitempty"`
    AirEquipType string `xml:",attr,omitempty"`
}

type Tax_2 struct {
    TaxDetailType
    AirportCode              string  `xml:",attr,omitempty"`
    ApplicableAmount         float64 `xml:",attr,omitempty"`
    CurrencyType             string  `xml:",attr,omitempty"`
    ReportedAmount           float64 `xml:",attr,omitempty"`
    SegmentOriginAirportCode string  `xml:",attr,omitempty"`
    SegmentDestAirportCode   string  `xml:",attr,omitempty"`
}

type UnticketedPointInfo struct {
    CityAirportCode string `xml:",attr,omitempty"`
    ArrivalDate     string `xml:",attr,omitempty"`
    DepartureDate   string `xml:",attr,omitempty"`
    AirEquipType    string `xml:",attr,omitempty"`
}

type StringLength1to8 string

type StringLength1to16 string

type StringLength1to5 string

type Numeric1to4 int

type ReissuedFlownType struct {
    FlightCouponData      *FlightCouponData `xml:"http://www.iata.org/IATA/EDIST FlightCouponData,omitempty"`
    Number                int               `xml:",attr,omitempty"`
    CouponItinerarySeqNbr int               `xml:",attr,omitempty"`
    FareBasisCode         string            `xml:",attr,omitempty"`
    TicketDocumentNbr     string            `xml:",attr,omitempty"`
    DateOfIssue           string            `xml:",attr,omitempty"`
    WaiverCode            string            `xml:",attr,omitempty"`
    TicketDesignatorCode  string            `xml:",attr,omitempty"`
}

type FlightCouponData struct {
    CouponFlightSegmentType
    IntermediateStop   []*IntermediateStop `xml:"http://www.iata.org/IATA/EDIST IntermediateStop,omitempty"`
    StopoverInd        bool                `xml:",attr,omitempty"`
    InvoluntaryIndCode string              `xml:",attr,omitempty"`
}

type IntermediateStop struct {
    AiportCode string `xml:",attr,omitempty"`
}

type AlphaNumericStringLength1to19 string

type IATA_PayloadStdAttributes struct {
    EchoToken               string `xml:",attr,omitempty"`
    TimeStamp               string `xml:",attr,omitempty"`
    Target                  string `xml:",attr,omitempty"`
    Version                 string `xml:",attr,omitempty"`
    TransactionIdentifier   string `xml:",attr,omitempty"`
    SequenceNmbr            int    `xml:",attr,omitempty"`
    TransactionStatusCode   string `xml:",attr,omitempty"`
    PrimaryLangID           string `xml:",attr,omitempty"`
    AltLangID               string `xml:",attr,omitempty"`
    RetransmissionIndicator bool   `xml:",attr,omitempty"`
    CorrelationID           string `xml:",attr,omitempty"`
    AsynchronousAllowedInd  bool   `xml:",attr,omitempty"`
}

type StringLength1to128 string

type PrimaryLangID_Group struct {
    PrimaryLangID string `xml:",attr,omitempty"`
}

type AltLangID_Group struct {
    AltLangID string `xml:",attr,omitempty"`
}

type GeographicSpecificationType struct {
    GeoSpecCode []*GeoSpecCode `xml:"http://www.iata.org/IATA/EDIST GeoSpecCode,omitempty"`
    Coordinates *Coordinates   `xml:"http://www.iata.org/IATA/EDIST Coordinates,omitempty"`
}

type Coordinates struct {
    PositionType
    CoordinateRadius *CoordinateRadius `xml:"http://www.iata.org/IATA/EDIST CoordinateRadius,omitempty"`
}

type CoordinateRadius struct {
    UOM   string  `xml:",attr,omitempty"`
    Value float64 `xml:",chardata"`
}

type NonGeographicSpecificationType struct {
    TravelAgencyInd bool                 `xml:"http://www.iata.org/IATA/EDIST TravelAgencyInd,omitempty"`
    AggregatorID    *UniqueIDContextType `xml:"http://www.iata.org/IATA/EDIST AggregatorID,omitempty"`
    PointOfSaleCode []*PointOfSaleCode   `xml:"http://www.iata.org/IATA/EDIST PointOfSaleCode,omitempty"`
}

type PointOfSaleCode struct {
    PointOfSaleType string `xml:",attr,omitempty"`
    Value           string `xml:",chardata"`
}

type OfferGeographicSpecificationType struct {
    DirectionalIndicator  string                       `xml:"http://www.iata.org/IATA/EDIST DirectionalIndicator,omitempty"`
    OfferOriginPoint      *GeographicSpecificationType `xml:"http://www.iata.org/IATA/EDIST OfferOriginPoint,omitempty"`
    OfferDestinationPoint *GeographicSpecificationType `xml:"http://www.iata.org/IATA/EDIST OfferDestinationPoint,omitempty"`
    TravelWithin          *GeographicSpecificationType `xml:"http://www.iata.org/IATA/EDIST TravelWithin,omitempty"`
}

type ProfileOwnerType AirlineDesigSimpleType

type PointOfSaleCodeType string

type GeoSpecCode struct {
    GeoSpecCodeType string `xml:",attr,omitempty"`
    Value           string `xml:",chardata"`
}

type GeoSpecCodeType string

type OfferedServiceType string

type StringLength1to255 string

type BaggageDeterminingCarrierType struct {
    AirlineID  *AirlineID_Type `xml:"http://www.iata.org/IATA/EDIST AirlineID,omitempty"`
    Name       string          `xml:"http://www.iata.org/IATA/EDIST Name,omitempty"`
    BagRule    string          `xml:"http://www.iata.org/IATA/EDIST BagRule,omitempty"`
    BDC_Reason string          `xml:",attr,omitempty"`
}

type SettlementType struct {
    Surcharges *Surcharges_4     `xml:"http://www.iata.org/IATA/EDIST Surcharges,omitempty"`
    Fees       *FeeSurchargeType `xml:"http://www.iata.org/IATA/EDIST Fees,omitempty"`
}

type Surcharges_4 struct {
    Surcharge []*FeeSurchargeType `xml:"http://www.iata.org/IATA/EDIST Surcharge,omitempty"`
}

type FeeDisclosureType struct {
    Surcharges  *Surcharges_5             `xml:"http://www.iata.org/IATA/EDIST Surcharges,omitempty"`
    Taxes       *TaxDetailType            `xml:"http://www.iata.org/IATA/EDIST Taxes,omitempty"`
    Fees        *FeeSurchargeType         `xml:"http://www.iata.org/IATA/EDIST Fees,omitempty"`
    Refundvalue *TotalFareTransactionType `xml:"http://www.iata.org/IATA/EDIST Refundvalue,omitempty"`
    RefundInd   bool                      `xml:",attr,omitempty"`
}

type Surcharges_5 struct {
    Surcharge []*FeeSurchargeType `xml:"http://www.iata.org/IATA/EDIST Surcharge,omitempty"`
}

type ImageID struct {
    Owner                string `xml:",attr,omitempty"`
    MediaDimensionFormat string `xml:",attr,omitempty"`
    Value                string `xml:",chardata"`
}

type TicketIdentificationType struct {
    Owner              string        `xml:"http://www.iata.org/IATA/EDIST Owner,omitempty"`
    Security           *SecurityType `xml:"http://www.iata.org/IATA/EDIST Security,omitempty"`
    TicketIssueDate    string        `xml:"http://www.iata.org/IATA/EDIST TicketIssueDate,omitempty"`
    TicketIssueTime    string        `xml:"http://www.iata.org/IATA/EDIST TicketIssueTime,omitempty"`
    TicketIssuePlace   string        `xml:"http://www.iata.org/IATA/EDIST TicketIssuePlace,omitempty"`
    TicketIssueCountry string        `xml:"http://www.iata.org/IATA/EDIST TicketIssueCountry,omitempty"`
    DocumentNumber     string        `xml:"http://www.iata.org/IATA/EDIST DocumentNumber,omitempty"`
    Refs               string        `xml:",attr,omitempty"`
    ObjectKey          string        `xml:",attr,omitempty"`
}

type AirShoppingRQ struct {
    XMLName                 xml.Name                `xml:"http://www.iata.org/IATA/EDIST AirShoppingRQ,omitempty"`
    PointOfSale             *PointOfSaleType        `xml:"http://www.iata.org/IATA/EDIST PointOfSale,omitempty"`
    Document                MsgDocumentType        `xml:"http://www.iata.org/IATA/EDIST Document"`
    Party                   *MsgPartiesType         `xml:"http://www.iata.org/IATA/EDIST Party,omitempty"`
    Parameters              *AirShopReqParamsType   `xml:"http://www.iata.org/IATA/EDIST Parameters,omitempty"`
    Travelers               *Travelers              `xml:"http://www.iata.org/IATA/EDIST Travelers,omitempty"`
    CoreQuery               *CoreQuery              `xml:"http://www.iata.org/IATA/EDIST CoreQuery,omitempty"`
    Qualifier               *Qualifier_2            `xml:"http://www.iata.org/IATA/EDIST Qualifier,omitempty"`
    Preference              *Preference_1           `xml:"http://www.iata.org/IATA/EDIST Preference,omitempty"`
    Metadata                *AirShopReqMetadataType `xml:"http://www.iata.org/IATA/EDIST Metadata,omitempty"`
    Policies                *Policies               `xml:"http://www.iata.org/IATA/EDIST Policies,omitempty"`
    EchoToken               string                  `xml:",attr,omitempty"`
    TimeStamp               string                  `xml:",attr,omitempty"`
    Target                  string                  `xml:",attr,omitempty"`
    Version                 string                  `xml:",attr,omitempty"`
    TransactionIdentifier   string                  `xml:",attr,omitempty"`
    SequenceNmbr            int                     `xml:",attr,omitempty"`
    TransactionStatusCode   string                  `xml:",attr,omitempty"`
    PrimaryLangID           string                  `xml:",attr,omitempty"`
    AltLangID               string                  `xml:",attr,omitempty"`
    RetransmissionIndicator bool                    `xml:",attr,omitempty"`
    CorrelationID           string                  `xml:",attr,omitempty"`
    AsynchronousAllowedInd  bool                    `xml:",attr,omitempty"`
}

type CoreQuery struct {
    Affinity           *AirShopReqAffinityQueryType  `xml:"http://www.iata.org/IATA/EDIST Affinity,omitempty"`
    OriginDestinations *AirShopReqAttributeQueryType `xml:"http://www.iata.org/IATA/EDIST OriginDestinations,omitempty"`
    FlightSpecific     *AirShopFlightSpecificType    `xml:"http://www.iata.org/IATA/EDIST FlightSpecific,omitempty"`
    ShoppingResponseID *ShoppingResponseID_Type      `xml:"http://www.iata.org/IATA/EDIST ShoppingResponseID,omitempty"`
}

type Qualifier_2 struct {
    BaggagePricingQualifier *BaggagePricingQualifierType `xml:"http://www.iata.org/IATA/EDIST BaggagePricingQualifier,omitempty"`
    ExistingOrderQualifier  *ExistingOrderQualifier      `xml:"http://www.iata.org/IATA/EDIST ExistingOrderQualifier,omitempty"`
    PaymentCardQualifiers   *CardQualifierType           `xml:"http://www.iata.org/IATA/EDIST PaymentCardQualifiers,omitempty"`
    ProgramQualifiers       *ProgramQualifiers           `xml:"http://www.iata.org/IATA/EDIST ProgramQualifiers,omitempty"`
    PromotionQualifiers     *PromotionQualifiers         `xml:"http://www.iata.org/IATA/EDIST PromotionQualifiers,omitempty"`
    SeatQualifier           *SeatQualifier               `xml:"http://www.iata.org/IATA/EDIST SeatQualifier,omitempty"`
    ServiceQualifier        *ServiceQualifierPriceType   `xml:"http://www.iata.org/IATA/EDIST ServiceQualifier,omitempty"`
    SocialMediaQualifiers   *SocialMediaQualifierType    `xml:"http://www.iata.org/IATA/EDIST SocialMediaQualifiers,omitempty"`
    SpecialFareQualifiers   *FareQualifierType           `xml:"http://www.iata.org/IATA/EDIST SpecialFareQualifiers,omitempty"`
    SpecialNeedQualifiers   *SpecialType                 `xml:"http://www.iata.org/IATA/EDIST SpecialNeedQualifiers,omitempty"`
    TripPurposeQualifier    string                       `xml:"http://www.iata.org/IATA/EDIST TripPurposeQualifier,omitempty"`
}

type Preference_1 struct {
    AirlinePreferences           *AirlinePreferencesType           `xml:"http://www.iata.org/IATA/EDIST AirlinePreferences,omitempty"`
    AlliancePreferences          *AlliancePreferencesType          `xml:"http://www.iata.org/IATA/EDIST AlliancePreferences,omitempty"`
    FarePreferences              *FarePreferencesType              `xml:"http://www.iata.org/IATA/EDIST FarePreferences,omitempty"`
    FlightPreferences            *FlightPreferencesType            `xml:"http://www.iata.org/IATA/EDIST FlightPreferences,omitempty"`
    PricingMethodPreference      *BestPricingPreferencesType       `xml:"http://www.iata.org/IATA/EDIST PricingMethodPreference,omitempty"`
    SegMaxTimePreferences        *FltSegmentMaxTimePreferencesType `xml:"http://www.iata.org/IATA/EDIST SegMaxTimePreferences,omitempty"`
    ServicePricingOnlyPreference *ServicePricingOnlyPreference     `xml:"http://www.iata.org/IATA/EDIST ServicePricingOnlyPreference,omitempty"`
    TransferPreferences          *TransferPreferencesType          `xml:"http://www.iata.org/IATA/EDIST TransferPreferences,omitempty"`
    CabinPreferences             *CabinPreferencesType             `xml:"http://www.iata.org/IATA/EDIST CabinPreferences,omitempty"`
}

type AirShopReqMetadataType struct {
    Shopping *Shopping   `xml:"http://www.iata.org/IATA/EDIST Shopping,omitempty"`
    Traveler *Traveler_3 `xml:"http://www.iata.org/IATA/EDIST Traveler,omitempty"`
    Other    *Other_1    `xml:"http://www.iata.org/IATA/EDIST Other,omitempty"`
}

type Shopping struct {
    ShopMetadataGroup *ShopMetadataGroup `xml:"http://www.iata.org/IATA/EDIST ShopMetadataGroup,omitempty"`
}

type Traveler_3 struct {
    TravelerMetadata *TravelerMetadataType `xml:"http://www.iata.org/IATA/EDIST TravelerMetadata,omitempty"`
}

type Other_1 struct {
    OtherMetadata []*OtherMetadata `xml:"http://www.iata.org/IATA/EDIST OtherMetadata,omitempty"`
}

type OtherMetadata struct {
    AddressMetadatas       *AddressMetadatas       `xml:"http://www.iata.org/IATA/EDIST AddressMetadatas,omitempty"`
    AircraftMetadatas      *AircraftMetadatas      `xml:"http://www.iata.org/IATA/EDIST AircraftMetadatas,omitempty"`
    AirportMetadatas       *AirportMetadatas       `xml:"http://www.iata.org/IATA/EDIST AirportMetadatas,omitempty"`
    CityMetadatas          *CityMetadatas          `xml:"http://www.iata.org/IATA/EDIST CityMetadatas,omitempty"`
    CodesetMetadatas       *CodesetMetadatas       `xml:"http://www.iata.org/IATA/EDIST CodesetMetadatas,omitempty"`
    ContactMetadatas       *ContactMetadatas       `xml:"http://www.iata.org/IATA/EDIST ContactMetadatas,omitempty"`
    CurrencyMetadatas      *CurrencyMetadatas      `xml:"http://www.iata.org/IATA/EDIST CurrencyMetadatas,omitempty"`
    DescriptionMetadatas   *DescriptionMetadatas   `xml:"http://www.iata.org/IATA/EDIST DescriptionMetadatas,omitempty"`
    EquivalentID_Metadatas *EquivalentID_Metadatas `xml:"http://www.iata.org/IATA/EDIST EquivalentID_Metadatas,omitempty"`
    LanguageMetadatas      *LanguageMetadatas      `xml:"http://www.iata.org/IATA/EDIST LanguageMetadatas,omitempty"`
    PaymentCardMetadatas   *PaymentCardMetadatas   `xml:"http://www.iata.org/IATA/EDIST PaymentCardMetadatas,omitempty"`
    PaymentFormMetadatas   *PaymentFormMetadatas   `xml:"http://www.iata.org/IATA/EDIST PaymentFormMetadatas,omitempty"`
    PriceMetadatas         *PriceMetadatas         `xml:"http://www.iata.org/IATA/EDIST PriceMetadatas,omitempty"`
    RuleMetadatas          *RuleMetadatas          `xml:"http://www.iata.org/IATA/EDIST RuleMetadatas,omitempty"`
    StateProvMetadatas     *StateProvMetadatas     `xml:"http://www.iata.org/IATA/EDIST StateProvMetadatas,omitempty"`
    ZoneMetadatas          *ZoneMetadatas          `xml:"http://www.iata.org/IATA/EDIST ZoneMetadatas,omitempty"`
}

type AirShopReqParamsType struct {
    MessageParamsBaseType
    Inventory                 *Inventory                     `xml:"http://www.iata.org/IATA/EDIST Inventory,omitempty"`
    ServiceFilters            *ServiceFilters                `xml:"http://www.iata.org/IATA/EDIST ServiceFilters,omitempty"`
    Pricing                   *Pricing_2                     `xml:"http://www.iata.org/IATA/EDIST Pricing,omitempty"`
    Group                     *Group_2                       `xml:"http://www.iata.org/IATA/EDIST Group,omitempty"`
    BaggageDeterminingCarrier *BaggageDeterminingCarrierType `xml:"http://www.iata.org/IATA/EDIST BaggageDeterminingCarrier,omitempty"`
}

type Inventory struct {
    GuaranteeInd bool `xml:"http://www.iata.org/IATA/EDIST GuaranteeInd,omitempty"`
}

type ServiceFilters struct {
    AssociatedObjectBaseType
    ServiceFilter []*ServiceFilterType `xml:"http://www.iata.org/IATA/EDIST ServiceFilter,omitempty"`
}

type Pricing_2 struct {
    OverrideCurrency string        `xml:"http://www.iata.org/IATA/EDIST OverrideCurrency,omitempty"`
    FeeExemption     *FeeExemption `xml:"http://www.iata.org/IATA/EDIST FeeExemption,omitempty"`
    AutoExchangeInd  bool          `xml:",attr,omitempty"`
    AwardIncludedInd bool          `xml:",attr,omitempty"`
    AwardOnlyInd     bool          `xml:",attr,omitempty"`
    SimpleInd        bool          `xml:",attr,omitempty"`
}

type FeeExemption struct {
    AssociatedObjectBaseType
    Fee            []*Fee_2           `xml:"http://www.iata.org/IATA/EDIST Fee,omitempty"`
    CountryCode    []*CountryCodeType `xml:"http://www.iata.org/IATA/EDIST CountryCode,omitempty"`
    ProvinceCode   []string           `xml:"http://www.iata.org/IATA/EDIST ProvinceCode,omitempty"`
    GovernmentBody string             `xml:"http://www.iata.org/IATA/EDIST GovernmentBody,omitempty"`
}

type Fee_2 struct {
    Code    *Code_4  `xml:"http://www.iata.org/IATA/EDIST Code,omitempty"`
    TaxType []string `xml:"http://www.iata.org/IATA/EDIST TaxType,omitempty"`
    Refs    string   `xml:",attr,omitempty"`
}

type Code_4 struct {
    CollectionPoint string `xml:",attr,omitempty"`
    Value           string `xml:",chardata"`
}

type Group_2 struct {
    PrimaryContact *PrimaryContact `xml:"http://www.iata.org/IATA/EDIST PrimaryContact,omitempty"`
}

type Affinity AirShopReqAffinityQueryType

type AirShopFlightSpecificType struct {
    FlightSegment     []*FlightSegment_1   `xml:"http://www.iata.org/IATA/EDIST FlightSegment,omitempty"`
    Flight            []*Flight_6          `xml:"http://www.iata.org/IATA/EDIST Flight,omitempty"`
    OriginDestination []*OriginDestination `xml:"http://www.iata.org/IATA/EDIST OriginDestination,omitempty"`
}

type FlightSegment_1 struct {
    Departure        *FlightDepartureType        `xml:"http://www.iata.org/IATA/EDIST Departure,omitempty"`
    Arrival          *FlightArrivalType          `xml:"http://www.iata.org/IATA/EDIST Arrival,omitempty"`
    MarketingAirline *MarketingCarrierFlightType `xml:"http://www.iata.org/IATA/EDIST MarketingAirline,omitempty"`
    OperatingAirline *OperatingCarrierFlightType `xml:"http://www.iata.org/IATA/EDIST OperatingAirline,omitempty"`
    SegmentKey       int                         `xml:",attr,omitempty"`
}

type Flight_6 struct {
    Journey           *TotalJourneyType  `xml:"http://www.iata.org/IATA/EDIST Journey,omitempty"`
    SegmentReferences *SegmentReferences `xml:"http://www.iata.org/IATA/EDIST SegmentReferences,omitempty"`
    Refs              string             `xml:",attr,omitempty"`
    FlightKey         string             `xml:",attr,omitempty"`
}

type AirShopReqAttributeQueryType struct {
    OriginDestination []*OriginDestination_1 `xml:"http://www.iata.org/IATA/EDIST OriginDestination,omitempty"`
}

type OriginDestination_1 struct {
    AssociatedObjectBaseType
    Departure               *FlightDepartureType              `xml:"http://www.iata.org/IATA/EDIST Departure,omitempty"`
    Arrival                 *FlightArrivalType                `xml:"http://www.iata.org/IATA/EDIST Arrival,omitempty"`
    SegMaxTimePreferences   *FltSegmentMaxTimePreferencesType `xml:"http://www.iata.org/IATA/EDIST SegMaxTimePreferences,omitempty"`
    ShoppingResponseID      *ShoppingResponseID_Type          `xml:"http://www.iata.org/IATA/EDIST ShoppingResponseID,omitempty"`
    MarketingCarrierAirline *MarketingCarrierType             `xml:"http://www.iata.org/IATA/EDIST MarketingCarrierAirline,omitempty"`
    CalendarDates           *CalendarDates                    `xml:"http://www.iata.org/IATA/EDIST CalendarDates,omitempty"`
    OriginDestinationKey    string                            `xml:",attr,omitempty"`
}

type CalendarDates struct {
    DaysBefore int `xml:",attr,omitempty"`
    DaysAfter  int `xml:",attr,omitempty"`
}

type AirShopReqAffinityQueryType struct {
    OriginDestination   []*OriginDestination_2         `xml:"http://www.iata.org/IATA/EDIST OriginDestination,omitempty"`
    StayPeriodDateRange *StayPeriodDateRangeType       `xml:"http://www.iata.org/IATA/EDIST StayPeriodDateRange,omitempty"`
    StayPeriodSeason    *StayPeriodSeasonType          `xml:"http://www.iata.org/IATA/EDIST StayPeriodSeason,omitempty"`
    TripDistance        *TravelDistancePreferencesType `xml:"http://www.iata.org/IATA/EDIST TripDistance,omitempty"`
    TripTime            *TravelTimePreferencesType     `xml:"http://www.iata.org/IATA/EDIST TripTime,omitempty"`
    Budget              *Budget                        `xml:"http://www.iata.org/IATA/EDIST Budget,omitempty"`
    KeywordPreferences  *KeywordPreferences            `xml:"http://www.iata.org/IATA/EDIST KeywordPreferences,omitempty"`
    ShoppingResponseID  *ShoppingResponseID_Type       `xml:"http://www.iata.org/IATA/EDIST ShoppingResponseID,omitempty"`
}

type OriginDestination_2 struct {
    AssociatedObjectBaseType
    AirportCityDeparture    *FltDepartQualifiedQueryType      `xml:"http://www.iata.org/IATA/EDIST AirportCityDeparture,omitempty"`
    CountryDeparture        *AffinityCountryDepartType        `xml:"http://www.iata.org/IATA/EDIST CountryDeparture,omitempty"`
    ReferencePointDeparture *AffinityReferencePointDepartType `xml:"http://www.iata.org/IATA/EDIST ReferencePointDeparture,omitempty"`
    StateProvDeparture      *AffinityStateProvDepartType      `xml:"http://www.iata.org/IATA/EDIST StateProvDeparture,omitempty"`
    AirportCityArrival      *FltArriveQualifiedQueryType      `xml:"http://www.iata.org/IATA/EDIST AirportCityArrival,omitempty"`
    CountryArrival          *AffinityCountryArriveType        `xml:"http://www.iata.org/IATA/EDIST CountryArrival,omitempty"`
    ReferencePointArrival   *AffinityReferencePointArriveType `xml:"http://www.iata.org/IATA/EDIST ReferencePointArrival,omitempty"`
    StateProvArrival        *AffinityStateProvArriveType      `xml:"http://www.iata.org/IATA/EDIST StateProvArrival,omitempty"`
    SegMaxTimePreferences   *FltSegmentMaxTimePreferencesType `xml:"http://www.iata.org/IATA/EDIST SegMaxTimePreferences,omitempty"`
}

type Budget struct {
    GroupBudget     *GroupBudgetType  `xml:"http://www.iata.org/IATA/EDIST GroupBudget,omitempty"`
    PerPersonBudget *PersonBudgetType `xml:"http://www.iata.org/IATA/EDIST PerPersonBudget,omitempty"`
}

type KeywordPreferences struct {
    KeywordPreference []*KeywordPreference `xml:"http://www.iata.org/IATA/EDIST KeywordPreference,omitempty"`
}

type KeywordPreference struct {
    KeyWordType
    PreferencesLevel   string `xml:",attr,omitempty"`
    PreferencesContext string `xml:",attr,omitempty"`
}

type Attribute AirShopReqAttributeQueryType

type FlightSpecific AirShopFlightSpecificType

type AirShoppingRS struct {
    XMLName                 xml.Name                 `xml:"http://www.iata.org/IATA/EDIST AirShoppingRS,omitempty"`
    Document                *MsgDocumentType         `xml:"http://www.iata.org/IATA/EDIST Document,omitempty"`
    Success                 *SuccessType             `xml:"http://www.iata.org/IATA/EDIST Success,omitempty"`
    Warnings                *WarningsType            `xml:"http://www.iata.org/IATA/EDIST Warnings,omitempty"`
    AirShoppingProcessing   *OrdViewProcessType      `xml:"http://www.iata.org/IATA/EDIST AirShoppingProcessing,omitempty"`
    ShoppingResponseID      *ShoppingResponseID_Type `xml:"http://www.iata.org/IATA/EDIST ShoppingResponseID,omitempty"`
    OffersGroup             *OffersGroup             `xml:"http://www.iata.org/IATA/EDIST OffersGroup,omitempty"`
    Payments                *Payments_1              `xml:"http://www.iata.org/IATA/EDIST Payments,omitempty"`
    Promotions              *Promotions_3            `xml:"http://www.iata.org/IATA/EDIST Promotions,omitempty"`
    Commission              *CommissionType          `xml:"http://www.iata.org/IATA/EDIST Commission,omitempty"`
    DataLists               *DataListType            `xml:"http://www.iata.org/IATA/EDIST DataLists,omitempty"`
    Metadata                *AirShopResMetadataType  `xml:"http://www.iata.org/IATA/EDIST Metadata,omitempty"`
    Policies                *Policies                `xml:"http://www.iata.org/IATA/EDIST Policies,omitempty"`
    Errors                  *ErrorsType              `xml:"http://www.iata.org/IATA/EDIST Errors,omitempty"`
    EchoToken               string                   `xml:",attr,omitempty"`
    TimeStamp               string                   `xml:",attr,omitempty"`
    Target                  string                   `xml:",attr,omitempty"`
    Version                 string                   `xml:",attr,omitempty"`
    TransactionIdentifier   string                   `xml:",attr,omitempty"`
    SequenceNmbr            int                      `xml:",attr,omitempty"`
    TransactionStatusCode   string                   `xml:",attr,omitempty"`
    PrimaryLangID           string                   `xml:",attr,omitempty"`
    AltLangID               string                   `xml:",attr,omitempty"`
    RetransmissionIndicator bool                     `xml:",attr,omitempty"`
    CorrelationID           string                   `xml:",attr,omitempty"`
    AsynchronousAllowedInd  bool                     `xml:",attr,omitempty"`
}

type OffersGroup struct {
    AllOffersSnapshot *AirlineOffersSnapshotType `xml:"http://www.iata.org/IATA/EDIST AllOffersSnapshot,omitempty"`
    AirlineOffers     []*AirlineOffers           `xml:"http://www.iata.org/IATA/EDIST AirlineOffers,omitempty"`
}

type AirlineOffers struct {
    TotalOfferQuantity   int                        `xml:"http://www.iata.org/IATA/EDIST TotalOfferQuantity,omitempty"`
    AirlineOfferSnapshot *AirlineOffersSnapshotType `xml:"http://www.iata.org/IATA/EDIST AirlineOfferSnapshot,omitempty"`
    Owner                *AirlineID_Type            `xml:"http://www.iata.org/IATA/EDIST Owner,omitempty"`
    AirlineOffer         []*AirlineOffer            `xml:"http://www.iata.org/IATA/EDIST AirlineOffer,omitempty"`
    PriceCalendar        []*PriceCalendar           `xml:"http://www.iata.org/IATA/EDIST PriceCalendar,omitempty"`
}

type AirlineOffer struct {
    OfferType
    PricedOffer *PricedFlightOfferType `xml:"http://www.iata.org/IATA/EDIST PricedOffer,omitempty"`
}

type PriceCalendar struct {
    PriceCalendarDate []*PriceCalendarDate `xml:"http://www.iata.org/IATA/EDIST PriceCalendarDate,omitempty"`
    TotalPrice        *EncodedPriceType    `xml:"http://www.iata.org/IATA/EDIST TotalPrice,omitempty"`
    LeadPriceInd      string               `xml:",attr,omitempty"`
}

type PriceCalendarDate struct {
    OriginDestinationReference string `xml:",attr,omitempty"`
    Value                      string `xml:",chardata"`
}

type Payments_1 struct {
    Payment []*OrderPaymentFormType `xml:"http://www.iata.org/IATA/EDIST Payment,omitempty"`
}

type Promotions_3 struct {
    Promotion []*PromotionType `xml:"http://www.iata.org/IATA/EDIST Promotion,omitempty"`
}

type AirShopResParamsType struct {
    MessageParamsBaseType
    Pricing            *Pricing_3        `xml:"http://www.iata.org/IATA/EDIST Pricing,omitempty"`
    InventoryGuarantee *InvGuaranteeType `xml:"http://www.iata.org/IATA/EDIST InventoryGuarantee,omitempty"`
    Group              *Group_3          `xml:"http://www.iata.org/IATA/EDIST Group,omitempty"`
}

type Pricing_3 struct {
    OverrideCurrency string          `xml:"http://www.iata.org/IATA/EDIST OverrideCurrency,omitempty"`
    FeeExemption     *FeeExemption_1 `xml:"http://www.iata.org/IATA/EDIST FeeExemption,omitempty"`
    AutoExchangeInd  bool            `xml:",attr,omitempty"`
    AwardIncludedInd bool            `xml:",attr,omitempty"`
    AwardOnlyInd     bool            `xml:",attr,omitempty"`
    SimpleInd        bool            `xml:",attr,omitempty"`
}

type FeeExemption_1 struct {
    AssociatedObjectBaseType
    Fee            []*Fee_3           `xml:"http://www.iata.org/IATA/EDIST Fee,omitempty"`
    CountryCode    []*CountryCodeType `xml:"http://www.iata.org/IATA/EDIST CountryCode,omitempty"`
    ProvinceCode   []string           `xml:"http://www.iata.org/IATA/EDIST ProvinceCode,omitempty"`
    GovernmentBody string             `xml:"http://www.iata.org/IATA/EDIST GovernmentBody,omitempty"`
}

type Fee_3 struct {
    Code    *Code_5  `xml:"http://www.iata.org/IATA/EDIST Code,omitempty"`
    TaxType []string `xml:"http://www.iata.org/IATA/EDIST TaxType,omitempty"`
    Refs    string   `xml:",attr,omitempty"`
}

type Code_5 struct {
    CollectionPoint string `xml:",attr,omitempty"`
    Value           string `xml:",chardata"`
}

type Group_3 struct {
    PrimaryContact *PrimaryContact `xml:"http://www.iata.org/IATA/EDIST PrimaryContact,omitempty"`
}

type AirShopResMetadataType struct {
    Shopping *Shopping_1 `xml:"http://www.iata.org/IATA/EDIST Shopping,omitempty"`
    Traveler *Traveler_4 `xml:"http://www.iata.org/IATA/EDIST Traveler,omitempty"`
    Other    *Other_2    `xml:"http://www.iata.org/IATA/EDIST Other,omitempty"`
}

type Shopping_1 struct {
    ShopMetadataGroup *ShopMetadataGroup `xml:"http://www.iata.org/IATA/EDIST ShopMetadataGroup,omitempty"`
}

type Traveler_4 struct {
    TravelerMetadata *TravelerMetadataType `xml:"http://www.iata.org/IATA/EDIST TravelerMetadata,omitempty"`
}

type Other_2 struct {
    OtherMetadata []*OtherMetadata_1 `xml:"http://www.iata.org/IATA/EDIST OtherMetadata,omitempty"`
}

type OtherMetadata_1 struct {
    AircraftMetadatas      *AircraftMetadatas      `xml:"http://www.iata.org/IATA/EDIST AircraftMetadatas,omitempty"`
    AirportMetadatas       *AirportMetadatas       `xml:"http://www.iata.org/IATA/EDIST AirportMetadatas,omitempty"`
    CityMetadatas          *CityMetadatas          `xml:"http://www.iata.org/IATA/EDIST CityMetadatas,omitempty"`
    CodesetMetadatas       *CodesetMetadatas       `xml:"http://www.iata.org/IATA/EDIST CodesetMetadatas,omitempty"`
    ContactMetadatas       *ContactMetadatas       `xml:"http://www.iata.org/IATA/EDIST ContactMetadatas,omitempty"`
    CountryMetadatas       *CountryMetadatas       `xml:"http://www.iata.org/IATA/EDIST CountryMetadatas,omitempty"`
    CurrencyMetadatas      *CurrencyMetadatas      `xml:"http://www.iata.org/IATA/EDIST CurrencyMetadatas,omitempty"`
    DescriptionMetadatas   *DescriptionMetadatas   `xml:"http://www.iata.org/IATA/EDIST DescriptionMetadatas,omitempty"`
    EquivalentID_Metadatas *EquivalentID_Metadatas `xml:"http://www.iata.org/IATA/EDIST EquivalentID_Metadatas,omitempty"`
    LanguageMetadatas      *LanguageMetadatas      `xml:"http://www.iata.org/IATA/EDIST LanguageMetadatas,omitempty"`
    PaymentCardMetadatas   *PaymentCardMetadatas   `xml:"http://www.iata.org/IATA/EDIST PaymentCardMetadatas,omitempty"`
    PaymentFormMetadatas   *PaymentFormMetadatas   `xml:"http://www.iata.org/IATA/EDIST PaymentFormMetadatas,omitempty"`
    PriceMetadatas         *PriceMetadatas         `xml:"http://www.iata.org/IATA/EDIST PriceMetadatas,omitempty"`
    RuleMetadatas          *RuleMetadatas          `xml:"http://www.iata.org/IATA/EDIST RuleMetadatas,omitempty"`
    StateProvMetadatas     *StateProvMetadatas     `xml:"http://www.iata.org/IATA/EDIST StateProvMetadatas,omitempty"`
    ZoneMetadatas          *ZoneMetadatas          `xml:"http://www.iata.org/IATA/EDIST ZoneMetadatas,omitempty"`
    AddressMetadatas       *AddressMetadatas       `xml:"http://www.iata.org/IATA/EDIST AddressMetadatas,omitempty"`
}

type AirShoppingProcessing OrdViewProcessType

type OrdViewProcessType struct {
    OrderProcessResultType
    Alerts  *AlertsType `xml:"http://www.iata.org/IATA/EDIST Alerts,omitempty"`
    Notices *Notices    `xml:"http://www.iata.org/IATA/EDIST Notices,omitempty"`
    Remarks *RemarkType `xml:"http://www.iata.org/IATA/EDIST Remarks,omitempty"`
}
