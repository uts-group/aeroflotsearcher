package aeroflot

import (
    "bytes"
    "net/http"
    "crypto/tls"
)

const (
    contentType = "application/x-iata.ndc.v1+xml"
)

type (
    Config struct {
        Url      string `json:"url"`
        ClientId string `json:"clientId"`
    }

    client struct {
        cfg  *Config
        http *http.Client
    }
)

func (c *client) Send(requestBody []byte) []byte {
    request, _ := http.NewRequest("POST", c.cfg.Url, bytes.NewReader(requestBody))
    request.Header.Add("Content-Type", contentType)
    request.Header.Add("X-IBM-client-Id", c.cfg.ClientId)

    response, _ := c.http.Do(request)
    defer response.Body.Close()

    buf := new(bytes.Buffer)
    buf.ReadFrom(response.Body)
    return buf.Bytes()
}

func NewClient(c *Config) *client {
    tr := &http.Transport{
        TLSClientConfig: &tls.Config{
            InsecureSkipVerify: true}}

    return &client{
        cfg:  c,
        http: &http.Client{Transport: tr}}
}
