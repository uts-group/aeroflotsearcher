package search

import (
    "testing"
    "io/ioutil"
    "bitbucket.org/uts-group/go-messages/air"
)

func TestMakeProviderData(t *testing.T) {
    rawData, _ := ioutil.ReadFile("./testdata/TestMakeProviderData.xml")
    resp := ParseResponse(rawData)

    if len(resp) != 243 {
        t.Fatalf("Whong offers number. Expected 243, got %d", len(resp))
    }

    offer := resp[0]

    if offer.Price != 2060.94 {
        t.Errorf("Wrong offer price. Expected 2060.94 got %f", offer.Price)
    }

    if offer.Tax != 1000.94 {
        t.Errorf("Wrong offer tax. Expected 1000.94 got %f", offer.Tax)
    }

    if offer.CurrencyCode != "USD" {
        t.Errorf("Wrong offer currency. Expected USD got %s", offer.CurrencyCode)
    }

    if len(offer.Segments) != 2 {
        t.Fatalf("Whong segments number. Expected 2, got %d", len(offer.Segments))
    }

    if len(offer.Segments[0].Flights) != 2 {
        t.Fatalf("Whong flights number on first segment. Expected 2, got %d", len(offer.Segments[0].Flights))
    }

    if len(offer.Segments[1].Flights) != 2 {
        t.Fatalf("Whong flights number on second segment. Expected 2, got %d", len(offer.Segments[1].Flights))
    }

    pd := new(air.ProviderData)
    pd.Unpack(offer.ProviderData)

    respId := "LED.20180520.NYC-NYC.20180603.LED_2018-02-19T11:08:43.526268"
    if pd.ResponseId != respId {
        t.Fatalf("Wrong respnse id. Expected %q, got %q", respId, pd.ResponseId)
    }

    offId := "2ADT.1CHD.1INF-LED.201805200350.75.180.180.SVO.SU.0037.N.NVUA-SVO.201805200920.585.180.-240.JFK.SU.0100.N.NVUA-JFK.201806030105.555.-240.180.SVO.SU.0123.L.LVUKA-SVO.201806032100.80.180.180.LED.SU.0032.L.LVUKA"
    if pd.OfferId != offId {
        t.Fatalf("Wrong respnse id. Expected %q, got %q", offId, pd.OfferId)
    }

    f, ok := pd.FaresByPaxCat[air.PaxAdult]
    if !ok {
        t.Fatalf("Fares list should has adult data")
    }

    if len(f.Fares) != 4 {
        t.Fatalf("Fares list should has adult data")
    }

    if f.Price != 689.71 {
        t.Errorf("Wrong fare price. Expected 689.71, got %f", f.Price)
    }

    if f.Tax != 317.71 {
        t.Errorf("Wrong fare tax. Expected 317.71, got %f", f.Tax)
    }

    if len(f.Fares) != 4 {
        t.Fatalf("Wrong fares list.")
    }

    if f.Fares[2] != "LVUKA" {
        t.Fatalf("Wrong fare name. Expected LVUKA, got %s", f.Fares[2])
    }
}
