package search

import (
    "bitbucket.org/uts-group/aeroflotsearcher/aeroflot"
    "encoding/xml"
    "bitbucket.org/uts-group/micro-tumbler/protocol/air"
)

func ParseResponse(response []byte) []*air.SearchResult {
    res := new(aeroflot.AirShoppingRS)
    err := xml.Unmarshal(response, res)
    if err != nil {
        panic(err)
    }

    return makeResponse(res)
}

func makeResponse(resp *aeroflot.AirShoppingRS) []*air.SearchResult {
    var res []*air.SearchResult

    // responseId := resp.ShoppingResponseID.ResponseID.Value
    segs := buildSegmentsMap(resp.DataLists.FlightSegmentList)
    paxes := buildTravellersList(resp.DataLists.AnonymousTravelerList)
    for _, curOffer := range resp.OffersGroup.AirlineOffers {
        for _, alOffer := range curOffer.AirlineOffer {
            curRes := new(air.SearchResult)
            curRes.Segments = segments(alOffer.PricedOffer.Associations[0].ApplicableFlight.FlightSegmentReference, &segs)
            curRes.ProviderData = providerData(resp.ShoppingResponseID.ResponseID.Value, alOffer, paxes)
            curRes.Price = float32(alOffer.TotalPrice.SimpleCurrencyPrice.Value)
            curRes.Tax = float32(taxes(alOffer.PricedOffer.OfferPrice, paxes))
            curRes.CurrencyCode = alOffer.TotalPrice.SimpleCurrencyPrice.Code

            res = append(res, curRes)
        }

    }

    return res
}

func buildSegmentsMap(list *aeroflot.FlightSegmentList) map[string]air.ResultFlight {
    res := make(map[string]air.ResultFlight)
    for _, s := range list.FlightSegment {
        seg := air.ResultFlight{
            FlightNumber:     s.MarketingCarrier.FlightNumber.Value,
            DepartureDate:    s.Departure.Date + " " + s.Departure.Time,
            ArrivalDate:      s.Arrival.Date + " " + s.Arrival.Time,
            FromTerminal:     "",
            ToTerminal:       "",
            AirPlane:         s.Equipment.AircraftCode.Value,
            FromAirport:      s.Departure.AirportCode.Value,
            ToAirport:        s.Arrival.AirportCode.Value,
            MarketingCompany: s.MarketingCarrier.AirlineID.Value,
            OperatingCompany: s.OperatingCarrier.AirlineID.Value,
            StopsNumber:      int32(0)}

        res[s.SegmentKey] = seg
    }

    return res
}

// Хеширование данных по пассажирам для удобства использования
func buildTravellersList(tl *aeroflot.AnonymousTravelerList) map[string]*aeroflot.TravelerCoreType {
    res := make(map[string]*aeroflot.TravelerCoreType)
    for _, v := range tl.AnonymousTraveler {
        res[v.ObjectKey] = v
    }

    return res
}

// Парсинг сегменты
func segments(offerSegs []*aeroflot.FlightSegmentReference, segsMap *map[string]air.ResultFlight) (res []*air.ResultSegment) {
    var curSeg *air.ResultSegment

    for _, seg := range offerSegs {
        segCode := seg.Ref
        // это копия перелёта, в общем списке он не меняется
        fl := (*segsMap)[segCode]
        fl.Rbd = seg.ClassOfService.Code.Value
        if seg.MarriedSegmentGroup == 0 {
            curSeg = new(air.ResultSegment)
            res = append(res, curSeg)
        }
        curSeg.Flights = append(curSeg.Flights, &fl)
    }

    return
}

// расчёт общей суммы такс
func taxes(offers []*aeroflot.OfferPriceLeadType, paxes map[string]*aeroflot.TravelerCoreType) float64 {
    res := 0.0
    for _, o := range offers {
        curTax := o.RequestedDate.PriceDetail.Taxes.Total.Value
        for _, a := range o.RequestedDate.Associations {
            t := paxes[a.AssociatedTraveler.TravelerReferences]
            res += curTax * float64(t.PTC.Quantity)
        }
    }
    return res
}

func providerData(respId string, offer *aeroflot.AirlineOffer, paxes map[string]*aeroflot.TravelerCoreType) string {
    resOb := &air.ProviderData{
        ResponseId: respId,
        OfferId:    offer.OfferID.Value,
        TotalPrice: float32(offer.TotalPrice.SimpleCurrencyPrice.Value),
        TotalPriceCurrency: offer.TotalPrice.SimpleCurrencyPrice.Code,
        // BasePrice: float32(offer.RequestedDate.PriceDetail.BaseAmount.Value),
        // Taxes: float32(offer.RequestedDate.PriceDetail.Taxes.Total.Value),
        // BasePriceCurrency: offer.RequestedDate.PriceDetail.Taxes.Total.Code,
        FaresByPaxCat: make(map[string]*air.FareInfo)}

    for _, o := range offer.PricedOffer.OfferPrice {
        for _, a := range o.RequestedDate.Associations {
            p := paxes[a.AssociatedTraveler.TravelerReferences]
            price := float32(o.RequestedDate.PriceDetail.TotalAmount.SimpleCurrencyPrice.Value)
            tax := float32(o.RequestedDate.PriceDetail.Taxes.Total.Value)
            info := &air.FareInfo{
                Price: price,
                Tax:   tax,
                Currency: o.RequestedDate.PriceDetail.Taxes.Total.Code}
            for _, f := range o.FareDetail.FareComponent {
                info.Fares = append(info.Fares, f.FareBasis.FareBasisCode.Code)
            }
            resOb.FaresByPaxCat[p.PTC.Value] = info
        }
    }

    res, _ := resOb.Pack()
    return res
}
