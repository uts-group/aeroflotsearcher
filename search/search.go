package search

import (
    "bitbucket.org/uts-group/aeroflotsearcher/aeroflot"
    "encoding/xml"
    "bitbucket.org/uts-group/micro-tumbler/protocol/air"
)

var cabinTypesMap = map[string]string{
    "Y": "3",
    "C": "2",
    "F": "4"}

func Request(req *air.SearchRequestBody) []byte {
    request := &aeroflot.AirShoppingRQ{
        PointOfSale: &aeroflot.PointOfSaleType{
            Location: &aeroflot.Location_1{
                CountryCode: &aeroflot.CountryCodeType{Value: "RU"}}},
        Party: &aeroflot.MsgPartiesType{
            Sender: &aeroflot.Sender{
                TravelAgencySender: &aeroflot.TrvlAgencyMsgPartyCoreType{
                    TravelAgencyType: aeroflot.TravelAgencyType{
                        AgencyID: &aeroflot.UniqueIDContextType{
                            Value: "Uts"}}}}},
        Parameters: &aeroflot.AirShopReqParamsType{
            MessageParamsBaseType: aeroflot.MessageParamsBaseType{
                Languages: &aeroflot.Languages_1{
                    LanguageCode: []*aeroflot.LanguageCodeType{
                        {Value: "en"}}}}},
        Travelers: &aeroflot.Travelers{
            Traveler: []*aeroflot.Traveler_2{
                {AnonymousTraveler: makeTravellers(req.Adults, req.Children, req.Infants)}}},
        CoreQuery: &aeroflot.CoreQuery{
            OriginDestinations: &aeroflot.AirShopReqAttributeQueryType{
                OriginDestination: makeSegments(req.Segments)}},
        Preference: &aeroflot.Preference_1{
            CabinPreferences: &aeroflot.CabinPreferencesType{
                CabinType: []*aeroflot.CabinType{
                    {CodesetType: aeroflot.CodesetType{Code: cabinTypesMap[req.Class]}}}}},
        Version: "16.2"}

    xmlReq, err := xml.Marshal(&request)
    if err != nil {
        panic(err)
    }

    return xmlReq
}

func makeTravellers(adults, children, infants uint32) []*aeroflot.TravelerCoreType {
    var res []*aeroflot.TravelerCoreType

    if adults != 0 {
        res = append(res, &aeroflot.TravelerCoreType{PTC: &aeroflot.PTC{Quantity: int(adults), Value: "ADT"}})
    }

    if children != 0 {
        res = append(res, &aeroflot.TravelerCoreType{PTC: &aeroflot.PTC{Quantity: int(children), Value: "CHD"}})
    }

    if infants != 0 {
        res = append(res, &aeroflot.TravelerCoreType{PTC: &aeroflot.PTC{Quantity: int(infants), Value: "INF"}})
    }

    return res
}

func makeSegments(segList []*air.RequestSegment) []*aeroflot.OriginDestination_1 {
    var res []*aeroflot.OriginDestination_1

    for _, s := range segList {
        newSeg := &aeroflot.OriginDestination_1{
            Departure: &aeroflot.FlightDepartureType{
                AirportCode: &aeroflot.AirportCode_1{Value: s.CityFrom},
                Date:        s.Date},
            Arrival: &aeroflot.FlightArrivalType{
                AirportCode: &aeroflot.AirportCode_2{Value: s.CityTo}}}

        res = append(res, newSeg)
    }
    return res
}
