package main

import (
    "flag"
    "bitbucket.org/uts-group/aeroflotsearcher/search"
    "github.com/nats-io/go-nats"
    "encoding/json"
    "bitbucket.org/uts-group/aeroflotsearcher/aeroflot"
    "fmt"
    "bitbucket.org/uts-group/micro-tumbler/app"
    "bitbucket.org/uts-group/micro-tumbler/protocol/air"
)

const REQUEST_QUERY = "air.search.request"

type Config struct {
    ConfigProviderUrl string
    Schema            string
}

var (
    validator *app.Validator
    config *Config
)

func init() {
    config = new(Config)
    app.LoadConfigs(config)
    validator = app.NewSchemaValidator(config.Schema)
}

func main() {
    threadsAmount := flag.Int("threads", 1, "maximum threads amount")
    flag.Parse()

    cfg := app.BaseConfig{
        NumThreads: *threadsAmount,
        QueryListener: app.NewNatsListener(REQUEST_QUERY, nats.DefaultURL),
        Handler: processSearch,
        ConfigRetrieverUrl: config.ConfigProviderUrl}
    app.Main(&cfg)
}

func processSearch(cfg app.ServiceConfig, data []byte) []byte {
    req := new(air.SearchRequestBody)
    err := json.Unmarshal(data, req)
    if err != nil {
        return app.MakeErrorBody("Request unmarshalling error")
    }

    // валидируем запрос
    // if ok, errs := validator.Validate(req); !ok {
    //     return msgBox, makeError(errs...)
    // }

    c := aeroflot.NewClient(&aeroflot.Config{
        Url: cfg["url"],
        ClientId: cfg["clientId"]})

    rawReq := search.Request(req)
    rawResp := c.Send(rawReq)

    tRes := search.ParseResponse(rawResp)

    resp, err := json.Marshal(tRes)
    if err != nil {
        return app.MakeErrorBody(fmt.Sprintf("Error response marshalling. %q", err))
    }

    return resp
}